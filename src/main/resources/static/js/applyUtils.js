
var vm = new Vue({
    el: '#subject',
    data: {
        msg: {
            'page': {
                'total': 1,
                'pages': 1
            }
        }
    },
    filters: {
        formatTime: function (value) {
            var time = new Date(value);
            return time.getFullYear() + "年" + (time.getMonth()+1) + "月" + time.getDay() + "日" + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
        }
    },
    mounted: function () {
    },
    methods: {}
});


function getReqApiResult(url, param) {
    //发送get请求
    axios.get(url, {
        params: param
    })
        .then(function (res) {
            vm.msg = res.data.result;
        })
        .catch(function (error) {
            console.log(error);
        });
}

function getReqApi(url, param) {
    //发送get请求
    axios.get(url, {
        params: param
    })
        .then(function (res) {
            vm.msg = res.data;
        })
        .catch(function (error) {
            console.log(error);
        });
}

function getReqApiPutCallBack(url, param, callback) {
    //发送get请求
    axios.get(url, {
        params: param
    })
        .then(function (res) {
            vm.msg = res.data;
            vm.$nextTick(function () {
                callback && callback();
            });
        })
        .catch(function (error) {
            console.log(error);
        });
}

function GetRequest() {
    var url = location.search;
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for (var i = 0; i < strs.length; i++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
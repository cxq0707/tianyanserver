
layui.use('element', function () {
    var element = layui.element;
});
var Bucket = 'auto-chess-box-1251720259';
var Region = 'ap-chengdu';
// 初始化实例
var cos = new COS({
    getAuthorization: function (options, callback) {
        // 异步获取临时密钥
        $.getJSON('/console/message/getKey', {
            bucket: options.Bucket,
            region: options.Region,
        }, function (data) {
            callback({
                TmpSecretId: data.credentials.tmpSecretId,
                TmpSecretKey: data.credentials.tmpSecretKey,
                XCosSecurityToken: data.credentials.sessionToken,
                ExpiredTime: data.expiredTime,
            });
        });

    },
    ProgressInterval:500
});

// 监听选文件
function uploadFile(file, insert) {
    console.info(file);
    if (!file) return;
    var layWindows =  layer.open({
        title: '文件上传中',
        closeBtn: 0,
        btn:[],
        content: '\n' +
            '      <div></div>  <div class="layui-progress layui-progress-big" lay-showPercent="true">\n' +
            '            <div class="layui-progress-bar layui-bg-blue" id="plan" style="width: 0%"><span id ="planText" class="layui-progress-text">0%</span></div>\n' +
            '        </div></div>'
    });
    // 分片上传文件，详细说明请参阅 JavaScript SDK 接口文档
    var index = file.name.lastIndexOf(".");
    var suffix = file.name.substr(index + 1);
    var date = Date.now();
    cos.sliceUploadFile({
            Bucket: Bucket,
            Region: Region,
            Key: "file/" + date +"."+  suffix,
            Body: file,
            onProgress: function (info) {
                var percent = parseInt(info.percent * 10000) / 100;
                $("#plan").css("width",percent+"%");
                $("#planText").html(percent+"%");

            }
        }, function (err, data) {
            console.info(err);
            console.info(data);
            var url = data.Location;
             insert&&insert("https://" + url);
            setTimeout(function () {
                layer.close(layWindows);
            },200)
        }
    );

};



// 监听选文件
function uploadApk(file, insert,fileName,obj) {
    console.info(file);
    if (!file) return;
    var layWindows =  layer.open({
        title: '文件上传中',
        closeBtn: 0,
        btn:[],
        content: '\n' +
            '      <div></div>  <div class="layui-progress layui-progress-big" lay-showPercent="true">\n' +
            '            <div class="layui-progress-bar layui-bg-blue" id="plan" style="width: 0%"><span id ="planText" class="layui-progress-text">0%</span></div>\n' +
            '        </div></div>'
    });
    // 分片上传文件，详细说明请参阅 JavaScript SDK 接口文档
    cos.sliceUploadFile({
            Bucket: Bucket,
            Region: Region,
            Key: "file/" + fileName,
            Body: file,
            onProgress: function (info) {
                var percent = parseInt(info.percent * 10000) / 100;
                $("#plan").css("width",percent+"%");
                $("#planText").html(percent+"%");

            }
        }, function (err, data) {
            var url = data.Location;
            insert&&insert("https://" + url,obj);
            setTimeout(function () {
                layer.close(layWindows);
            },500)
        }
    );

};
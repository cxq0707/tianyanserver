
### 修改表t_message的结构
### 新增字段 author_id 讯息作者id
### 新增字段 approve_state 审核状态
alter table t_message add column author_id bigint not null default 1 comment '作者的id';

alter table t_message add column approve_state int not null default 1 comment '审核状态，0表示审核中，1表示审核通过，2表示审核未通过';


## 修改表t_calory_trade的结构
alter table t_calory_trade add column entity_id bigint default null comment '实体id';

## 修改表user_dynamic的结构
alter table user_dynamic add column remove int not null default 1 comment '是否展示0不展示，1展示'

## 修改表squad的结构
alter table squad add column remove int not null default 1 comment '是否展示0不展示，1展示'

##新增表t_chest
DROP TABLE IF EXISTS `t_chest`;
CREATE TABLE `t_chest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_type` int(11) DEFAULT NULL COMMENT '游戏类型',
  `name` varchar(255) DEFAULT NULL COMMENT '宝箱名称',
  `describe` varchar(255) DEFAULT NULL COMMENT '宝箱描述',
  `pic` varchar(255) DEFAULT NULL COMMENT '宝箱图片',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

##新增表t_stuff
DROP TABLE IF EXISTS `t_stuff`;
CREATE TABLE `t_stuff` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '物品名称',
  `pic` varchar(255) DEFAULT NULL COMMENT '物品图片',
  `rate` varchar(50) DEFAULT '' COMMENT '物品评级，如传说史诗',
  `proportion` int(11) DEFAULT NULL COMMENT '物品比重',
  `describe` varchar(255) DEFAULT NULL COMMENT '物品描述',
  `level` int(11) NOT NULL DEFAULT '1' COMMENT '英雄等级',
    `game_type` int(11) DEFAULT NULL COMMENT '游戏类型',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


##新增表t_chest_stuff
DROP TABLE IF EXISTS `t_chest_stuff`;
CREATE TABLE `t_chest_stuff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chest_id` int(11) NOT NULL,
  `stuff_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#########################################################
#########################################################
# 添加表t_gift 礼物表(暂不使用,暂不使用)
# create table t_gift(
#   `id` bigint primary key auto_increment,
#   `name` varchar(100) not null default '' comment '礼物名称',
#   `description` varchar(255) not null default '' comment '礼物描述',
#   `pic_url` varchar(255) not null default '' comment '礼物图片的url',
#   `create_time` timestamp not null default current_timestamp,
#   `update_time` timestamp not null default current_timestamp on update current_timestamp
# )engine =InnoDB default charset =utf8 ;

# 添加表t_activity_wish 许愿活动表
create table t_activity_wish(
  `id` bigint primary key auto_increment,
  `name` varchar(100) not null default '' comment '许愿活动名称',
  `description` varchar(255) not null default '' comment '许愿活动描述',
  `start_time` timestamp not null comment '许愿活动开始时间',
  `end_time` timestamp not null comment '许愿活动结束时间',
  `open_time` timestamp not null comment '许愿活动开奖时间',
  `participate_number` int not null default 0 comment '许愿活动的参加人数',
  `gift_id` bigint default null comment '许愿活动中奖用户奖励的礼物id',
  `gift_number` int not null comment '礼物的数量',
  `create_time` timestamp not null default current_timestamp,
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;
## 修改表t_activity_wish ，添加字段cost_calory (耗费卡路里数量)
alter table t_activity_wish add column cost_calory int not null comment '许愿活动消耗卡路里的数量';
alter table t_activity_wish add column gift_name varchar(100) not null default '' comment '礼物名称';
alter table t_activity_wish add column gift_description varchar(255) not null default '' comment '礼物描述';
alter table t_activity_wish add column gift_pic_url varchar(255) not null default '' comment '礼物图片的url';

  #   `name` varchar(100) not null default '' comment '礼物名称',
  #   `description` varchar(255) not null default '' comment '礼物描述',
  #   `pic_url` varchar(255) not null default '' comment '礼物图片的url',

  # 添加表t_activity_wish_participate 许愿活动参与表
create table t_activity_wish_participate(
  `id` bigint primary key auto_increment,
  `activity_wish_id` bigint not null comment '许愿活动id',
  `user_id` bigint not null comment '许愿活动参与人的id',
  `create_time` timestamp not null default current_timestamp comment '许愿活动参与时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

# 修改表t_activity_wish_participate的结构
alter table t_activity_wish_participate add column is_upload_share bit not null default b'0' comment '是否上传分享';

# 添加表t_wish_award 许愿活动最终获奖表
create table t_wish_award(
  `id` bigint primary key auto_increment,
  `activity_wish_id` bigint not null ,
  `user_id` bigint not null comment '获奖用户的id',
  `exchange_code`  varchar(100) comment '兑换码',
  `create_time` timestamp not null default current_timestamp comment '许愿活动参与时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 修改表t_wish_award 添加字段 description(获奖描述)
alter table t_wish_award add column description varchar(255) not null default '' comment '获奖描述信息';

## 新增表t_exchange_information 兑换信息表 (暂不使用，暂不使用)
create table t_exchange_information(
  `id` bigint primary key auto_increment,
  `exchange_code` varchar(100) comment '兑换码',
  `exchange_information` varchar(255) comment '兑换信息，即获奖描述信息',
  `is_use` bit not null default b'0' comment '是否使用过',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 新增表t_upload_share 上传分享表
create table t_upload_share(
  `id` bigint primary key auto_increment,
  `user_id` bigint not null ,
  `activity_wish_id` bigint not null ,
  `upload_pic_url` varchar(255) not null default '' comment '上传的分享图片的地址',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 新增表t_activity_wish_exchange_information 许愿活动兑换信息
create table t_activity_wish_exchange_information(
  `id` bigint primary key auto_increment,
  `activity_wish_id` bigint not null comment '许愿活动的id',
  `exchange_code` varchar(100) comment '兑换码',
  `exchange_information` varchar(255) comment '兑换信息，即获奖描述信息',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 向表中添加数据
## 向表t_activity_wish表中插入数据
insert into t_activity_wish values (1,'huodong1','miaoshu','2019-07-24 10:00:00','2019-07-24 12:00:00',
                                      '2019-07-24 12:00:00',0,1,3,now(),now(),20,'礼物名称1','礼物描述1','www.tupian.png');

insert into t_activity_wish values (20,'hd marksmort','asdlasdl','2019-07-26 10:00:00','2019-07-26 12:00:00',
                                      '2019-07-26 12:00:00',0,null ,3,now(),now(),5,'礼物名称asdl','礼物描述asdl','www.tupianasdd.png');

## 向表t_activity_wish_exchange_information
insert into t_activity_wish_exchange_information values (1,1,'xy1024','ababab,xy1024,cccccccvbcccccc',now(),now());
insert into t_activity_wish_exchange_information values (2,1,'xy1029','ababab,xy1029,ccccccccvbcccc',now(),now());
insert into t_activity_wish_exchange_information values (3,1,'xy1090','ababab,xy1090,cccccccccvbccc',now(),now());

insert into t_activity_wish_exchange_information values (50,20,'ty1024','ababab,ty1024,cccccctttcvbcccccc',now(),now());
insert into t_activity_wish_exchange_information values (51,20,'ty1029','ababab,ty1029,ccccccctttcvbcccc',now(),now());
insert into t_activity_wish_exchange_information values (52,20,'ty1090','ababab,ty1090,cccccccttccvbccc',now(),now());









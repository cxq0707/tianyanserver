### 使用数据库auto_chess-box
###########################

## 创建表t_message 讯息表
## 讯息有三种类型，分别为赛事；资讯；与视频类型
create table t_message(
    `id` bigint not null primary key auto_increment,
    `title` varchar(50) not null ,
    `pic` varchar(200) not null ,
    `category_id` bigint not null ,
    `create_time` timestamp not null comment '发布时间',
    `update_time` timestamp not null,
    `media_type` tinyint not null default 0 comment '媒体类型，视频或者图文',
    `priority` double not null default 10000.00 comment '优先级系数，排序使用'
) engine =InnoDB default charset =utf8 ;

# alter table t_message add media_type tinyint not null default 0;
## 创建表t_category 讯息类别表
create table t_category(
    `id` bigint not null primary key auto_increment,
    `name` varchar(50) not null
)engine =InnoDB default charset =utf8 ;

## 向表t_category表中插入数据
insert into t_category(`name` ) value ('视频');
insert into t_category(`name` ) value ('赛事');
insert into t_category(`name` ) value ('资讯');

## 创建表t_message_detail
create table t_message_detail(
    `id` bigint not null primary key auto_increment,
    `message_id` bigint not null ,
    `video_link_url` varchar(255) not null default '',
    `content` text not null
)engine =InnoDB default charset =utf8 ;



## 创建讯息点赞表t_message_support
create table  t_message_support(
    `id` bigint not null primary key auto_increment,
    `message_id` bigint not null ,
    `user_id` bigint not null comment '点赞人的id',
    `user_name` varchar(50) not null comment '点赞人的用户名',
    `support_time` timestamp not null comment '点赞时间'
)engine =InnoDB default charset =utf8 ;


## 创建表讯息评论表 t_message_comment
create table t_message_comment(
    `id` bigint not null primary key auto_increment ,
    `message_id` bigint not null ,
    `user_id` bigint not null comment '评论人的id',
    `user_name` varchar(50) not null comment '评论人的用户名',
    `user_head_portrait` varchar(100) not null comment '用户头像地址',
    `comment_time` timestamp not null comment '评论时间',
    `comment_content` varchar(200) not null comment '评论内容'
)engine =InnoDB default charset =utf8 ;

## 修改表t_message_comment的结构
alter table t_message_comment  add column to_message_comment_id bigint not null default -1 ;

## 创建表讯息点赞评论点击量统计信息表t_message_support_comment_hit_stat
create table t_message_support_comment_hit_stat(
    `id` bigint not null primary key auto_increment,
    `message_id` bigint not null ,
    `supports` int default 0,
    `comments` int default 0,
    `hits` bigint default 0
)engine =InnoDB default charset =utf8 ;


## 创建表讯息评论点赞表t_messagecomment_support
create table t_messagecomment_support(
    `id` bigint not null primary key auto_increment,
    `message_comment_id` bigint not null ,
    `user_id` bigint not null comment '点赞人的id',
    `user_name` varchar(50) not null comment '点赞人的用户名',
    `support_time` timestamp not null comment '点赞时间'
)engine =InnoDB default charset =utf8 ;

## 修改表t_message_comment的结构
alter table t_message_comment change user_name user_nick_name varchar(50) comment '用户的昵称';
alter table t_message_comment add comment_image_list varchar(200) comment '评论内容中的图片列表';
alter table t_message_comment add supports int not null default 0 comment '评论点赞数量';
alter table t_message_comment add replys int not null default 0 comment '评论的回复数';


## 修改表t_message的结构，新增一个字段is_recommend 是否推荐
alter table t_message add is_recommend bit(1) not null default b'0' comment '信息是否推荐，默认的值为否';

## 修改表t_message_detail的机构，删除字段create_time与update
alter table t_message_detail drop create_time;
alter table t_message_detail drop update_time;


## 向表t_message中插入测试数据
insert into t_message values (1,'斯帕莱蒂：关于换帅，媒体已经说了三个月了--shipin',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',1,now(),now(),b'0');

insert into t_message values (2,'哈里斯：抢七失利给我动力让我走更远，没想自由球员市场--shipin',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/eb0a25399e072920fd85c7b108cbb64df17825e8.png',1,now(),now(),b'1');

insert into t_message values (3,'你知道吗？美媒整理汤普森与波特兰之间千丝万缕的联系--shipin',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',1,now(),now(),b'1');

insert into t_message values (4,'齐祖下逐客令！皇马功勋门将今夏走人 齐祖二儿子上位--shipin',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',1,now(),now(),b'1');

insert into t_message values (5,'斯蒂芬-杰克逊：火箭无法在德安东尼执教下夺冠--shipin',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',1,now(),now(),b'0');

insert into t_message values (6,'库兹马：我大概会是明日乐透抽签现场最潮的人--zixun',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',3,now(),now(),b'0');

insert into t_message values (7,'哈里斯：抢七失利给我动力让我走更远，没想自由球员市场--zixun',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/eb0a25399e072920fd85c7b108cbb64df17825e8.png',3,now(),now(),b'1');

insert into t_message values (8,'波尔图旗下足校球员遇难，学校暂停一天训练表示哀悼--zixun',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',3,now(),now(),b'0');

insert into t_message values (9,'火爆！本赛季场均76976名球迷前往诺坎普现场观战--zixun',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',3,now(),now(),b'1');

insert into t_message values (10,'帝国大厦为老鹰和爱国者亮灯--zixun',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',3,now(),now(),b'0');

insert into t_message values (11,'莱比锡CEO：维尔纳也可能去巴黎、利物浦或多特--zixun',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',3,now(),now(),b'0');

insert into t_message values (12,'主场告别战进球，普利西奇当选德甲本轮最佳球员--saishi',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/eb0a25399e072920fd85c7b108cbb64df17825e8.png',2,now(),now(),b'0');

insert into t_message values (13,'克洛普：恭喜沙尔克得到瓦格纳，虽然多特人不爱听这话--saishi',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',2,now(),now(),b'1');

insert into t_message values (14,'英超前6主帅年薪：瓜帅1500万镑居首，萨里最低--saishi',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/eb0a25399e072920fd85c7b108cbb64df17825e8.png',2,now(),now(),b'0');

insert into t_message values (15,'美记：伊巴卡与小加用西班牙语沟通包夹恩比德--saishi',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/db8bada08b59a724ab74e97c421f235e9fa1893c.png',2,now(),now(),b'0');

insert into t_message values (16,'亲眼看到的绝杀中 你觉得最震撼的是哪个球？--saishi',
'https://c2.hoopchina.com.cn/uploads/star/event/images/190514/eb0a25399e072920fd85c7b108cbb64df17825e8.png',2,now(),now(),b'1');

## 向表t_message_detail插入测试数据
insert into t_message_detail values (1,1,'详情内容xy1详情内容xy1-----aa001');
insert into t_message_detail values (2,2,'详情内容xy2详情内容xy2-----aa002');
insert into t_message_detail values (3,3,'详情内容xy3详情内容xy3-----aa003');
insert into t_message_detail values (4,4,'详情内容xy4详情内容xy4-----aa004');
insert into t_message_detail values (5,5,'详情内容xy5详情内容xy5-----aa005');
insert into t_message_detail values (6,6,'详情内容xy6详情内容xy6-----aa006');
insert into t_message_detail values (7,7,'详情内容xy7详情内容xy7-----aa007');
insert into t_message_detail values (8,8,'详情内容xy8详情内容xy8-----aa008');
insert into t_message_detail values (9,9,'详情内容xy9详情内容xy9-----aa009');
insert into t_message_detail values (10,10,'详情内容xy10详情内容xy10-----aa0010');
insert into t_message_detail values (11,11,'详情内容xy11详情内容xy11-----aa0011');
insert into t_message_detail values (12,12,'详情内容xy12详情内容xy12-----aa0012');
insert into t_message_detail values (13,13,'详情内容xy13详情内容xy13-----aa0013');
insert into t_message_detail values (14,14,'详情内容xy14详情内容xy14-----aa0014');
insert into t_message_detail values (15,15,'详情内容xy15详情内容xy15-----aa0015');
insert into t_message_detail values (16,16,'详情内容xy16详情内容xy16-----aa0016');

## 向表t_message_support_comment_hit_stat表插入数据
insert into t_message_support_comment_hit_stat values (1,1,0,0,0);
insert into t_message_support_comment_hit_stat values (2,2,0,0,0);
insert into t_message_support_comment_hit_stat values (3,3,0,0,0);
insert into t_message_support_comment_hit_stat values (4,4,0,0,0);
insert into t_message_support_comment_hit_stat values (5,5,0,0,0);
insert into t_message_support_comment_hit_stat values (6,6,0,0,0);
insert into t_message_support_comment_hit_stat values (7,7,0,0,0);
insert into t_message_support_comment_hit_stat values (8,8,0,0,0);
insert into t_message_support_comment_hit_stat values (9,9,0,0,0);
insert into t_message_support_comment_hit_stat values (10,10,0,0,0);
insert into t_message_support_comment_hit_stat values (11,11,0,0,0);
insert into t_message_support_comment_hit_stat values (12,12,0,0,0);
insert into t_message_support_comment_hit_stat values (13,13,0,0,0);
insert into t_message_support_comment_hit_stat values (14,14,0,0,0);
insert into t_message_support_comment_hit_stat values (15,15,0,0,0);
insert into t_message_support_comment_hit_stat values (16,16,0,0,0);

## 修改表t_message_comment表的结构
## 删除字段user_nick_name 与 user_head_portrait
alter table t_message_comment drop user_nick_name;
alter table t_message_comment drop user_head_portrait;

## 向表t_message_comment表添加数据
insert into t_message_comment values (1000,5,2,'2019-05-15 09:10:10','相信有很多小伙伴都卡在砖石或者星耀上不去',-1,
               'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

insert into t_message_comment values (1001,5,3,'2019-05-15 09:15:10','然后呢',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

insert into t_message_comment values (1002,5,4,'2019-05-15 09:20:10','其实砖石星耀有些人专玩一个英雄，就是差了点意识，讲了那么多，什么意识呢？',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

insert into t_message_comment values (1003,5,5,'2019-05-15 09:25:10','其实很简单，多看小地图关注对面打野辅助跟法师的位置，如果看不到打野跟辅助想必就是蹲在那个地方抓你',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,5);

insert into t_message_comment values (1004,5,2,'2019-05-15 09:30:10','说到底，钻石星耀局还是BP针对性不够，不看阵容，乱拿英雄。',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

insert into t_message_comment values (1005,5,3,'2019-05-15 09:31:10','操作再猛也不如英雄本身的克制来得重要。',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

insert into t_message_comment values (1006,5,4,'2019-05-15 09:33:10','问题在于我一个英雄玩好多次就不想玩这个英雄了 想换个英雄玩。',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

insert into t_message_comment values (1007,5,4,'2019-05-15 10:10:10','其实还是看版本走势，看当前版本最强英雄来上分',-1,
                                      'https://imgsa.baidu.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

### 对讯息评论id为1003的评论进行回复
### 添加回复相关数据
insert into t_message_comment values (10001,5,13,'2019-05-15 09:30:10','xxyy囧',1003,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);
insert into t_message_comment values (10002,5,12,'2019-05-15 09:33:10','带不动真没办法',1003,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);
insert into t_message_comment values (10003,5,19,'2019-05-15 09:38:17','上了王者你只会一个位置也不行啊',1003,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);
insert into t_message_comment values (10004,5,11,'2019-05-15 09:50:16','女生的话推荐玩一下张飞蔡文姬，大桥不会玩不要拿，因为会坑队友的',1003,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,3);
insert into t_message_comment values (10005,5,3,'2019-05-15 11:30:22','我好友一个小可爱喜欢玩女娲',1003,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);
#######
insert into t_message_comment values (100003,5,9,'2019-05-15 14:38:17','上了王者你只会一个位置也不行啊',10004,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);
insert into t_message_comment values (100004,5,11,'2019-05-15 14:50:16','女生的话推荐玩一下张飞蔡文姬，大桥不会玩不要拿，因为会坑队友的',10004,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);
insert into t_message_comment values (100005,5,12,'2019-05-15 14:20:22','我好友一个小可爱喜欢玩女娲',10004,
                                      'https://imgsa.com/forum/8094b36adaf99a4.jpg,http://imgsa.com/pntp094b36adaf99a4.jpg',0,0);

## 修改表t_message_support的结构，删除字段user_name
alter table t_message_support drop user_name;

## 修改表t_messagecomment_support的结构，删除字段user_name
alter table t_messagecomment_support drop user_name;

## 创建表t_slide_picture 轮播图表
create table t_slide_picture(
  `id` bigint not null primary key auto_increment,
  `name` varchar(100) not null default '',
  `path` varchar(100) not null comment '图片路径',
  `link_message_id` bigint not null comment '关联的讯息id',
  `link_message_url` varchar(100) not null comment '图片链接的讯息地址',
  `create_time` timestamp not null,
  `is_current_use` bit(1) not null default b'0' comment '当前是否启用',
  `sort` int not null default 0 comment '轮播图播放的顺序'
)engine =InnoDB default charset =utf8 ;

## 向表t_slide_picture中插入测试数据
insert into t_slide_picture values (201,'轮播图xy','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
'http://api.xiaoyuan.com/messageDetail/1080','2019-05-16 09:12:10',b'0',130);

insert into t_slide_picture values (202,'轮播图202','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/1183','2019-05-16 09:12:10',b'1',10);

insert into t_slide_picture values (203,'轮播图203','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/1582','2019-05-16 09:12:10',b'1',20);

insert into t_slide_picture values (204,'轮播图204','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/12080','2019-05-16 09:12:10',b'1',80);

insert into t_slide_picture values (205,'轮播图205','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/10380','2019-05-16 09:12:10',b'0',180);

insert into t_slide_picture values (206,'轮播图206','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/10820','2019-05-16 09:12:10',b'0',210);

insert into t_slide_picture values (207,'轮播图207','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/10877','2019-05-16 09:12:10',b'0',140);

insert into t_slide_picture values (208,'轮播图208','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/10820','2019-05-16 09:12:10',b'1',310);

insert into t_slide_picture values (209,'轮播图209','http://img3.imgtn.bdimg.com/it/u=3240156289,1305844938&fm=26&gp=0.jpg',
                                    'http://api.xiaoyuan.com/messageDetail/541080','2019-05-16 09:12:10',b'1',100);


##################################################################################################################################
## 收藏表t_store
create table t_store(
  `id` bigint not null primary key auto_increment,
  `store_category_id` bigint not null comment '收藏类别id，收藏类别包括阵容，讯息等',
  `user_id` bigint not null comment '收藏人的id',
  `entity_id` bigint not null comment '收藏的实体id，如阵容id，讯息id',
  `store_time` timestamp not null comment '收藏时间'
)engine =InnoDB default charset =utf8 ;

## 收藏类别表t_store_category
create table t_store_category(
  `id` bigint not null primary key auto_increment,
  `name` varchar(100) not null default '' comment '收藏类别名称'
)engine =InnoDB default charset =utf8 ;

## 向表t_store_category插入记录
insert into t_store_category values (1,'阵容');
insert into t_store_category values (2,'讯息');

## 向表t_store表插入记录
## id,store_category_id,user_id,entity_id,store_time
insert into t_store values (1,1,10,13,'2019-05-10 17:18:10');
insert into t_store values (2,1,10,30,'2019-05-11 17:18:10');
insert into t_store values (3,1,10,31,'2019-05-12 17:18:10');
insert into t_store values (4,1,10,32,'2019-05-13 17:10:10');
insert into t_store values (5,1,10,33,'2019-05-13 17:18:10');
insert into t_store values (6,1,10,34,'2019-05-14 17:22:12');
insert into t_store values (7,1,10,35,'2019-05-14 17:48:20');
insert into t_store values (8,1,10,53,'2019-05-15 07:33:10');
insert into t_store values (9,1,10,54,'2019-05-15 17:18:10');
insert into t_store values (10,1,10,55,'2019-05-15 17:38:10');
insert into t_store values (11,1,10,56,'2019-05-15 17:48:12');
insert into t_store values (12,1,10,59,'2019-05-15 17:55:10');
insert into t_store values (13,1,10,60,'2019-05-15 17:56:13');
insert into t_store values (14,1,10,61,'2019-05-15 17:58:10');
insert into t_store values (15,1,10,69,'2019-05-17 09:22:10');

insert into t_store values (16,2,10,4,'2019-05-21 10:18:10');
insert into t_store values (17,2,10,5,'2019-05-21 11:18:10');
insert into t_store values (18,2,10,6,'2019-05-21 12:18:10');



















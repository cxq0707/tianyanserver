/*创建英雄评论表*/
DROP TABLE IF EXISTS `t_hero_comment`;
CREATE TABLE `t_hero_comment`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hero_id` int(20) NOT NULL COMMENT '英雄id',
  `user_id` bigint(20) NOT NULL COMMENT '评论人的id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '评论时间',
  `comment_content` varchar(350) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论内容',
  `to_comment_id` bigint(20) NOT NULL DEFAULT -1,
  `comment_image_list` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评论内容中的图片列表',
  `supports` int(11) NOT NULL DEFAULT 0 COMMENT '评论点赞数量',
  `replys` int(11) NOT NULL DEFAULT 0 COMMENT '评论的回复数',
  `update_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_excellent` int(2) NOT NULL DEFAULT 0 COMMENT '是否是精选的',
  `game_type` int(2) NULL DEFAULT NULL COMMENT '游戏类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 487 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
/*创建英雄评论点赞表*/
DROP TABLE IF EXISTS `t_hero_comment_support`;
CREATE TABLE `t_hero_comment_support`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT '点赞人的id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '点赞时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
/*创建英雄数据统计表*/
DROP TABLE IF EXISTS `t_hero_data_statistics`;
CREATE TABLE `t_hero_data_statistics`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `hero_id` int(11) NOT NULL COMMENT '棋子id',
  `game_type` int(2) NOT NULL DEFAULT 1 COMMENT '游戏类型',
  `supports` int(255) NOT NULL DEFAULT 0 COMMENT '点赞数',
  `comments` int(255) NOT NULL DEFAULT 0 COMMENT '评论数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
/*创建英雄推荐阵容表*/
DROP TABLE IF EXISTS `t_hero_recommend_squad`;
CREATE TABLE `t_hero_recommend_squad`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hero_id` int(11) NOT NULL COMMENT '英雄id',
  `squad_id` int(11) NOT NULL COMMENT '关联阵容id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '评论时间',
  `game_type` int(2) NULL DEFAULT NULL COMMENT '游戏类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
/*创建英雄点赞表*/
DROP TABLE IF EXISTS `t_hero_support`;
CREATE TABLE `t_hero_support`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hero_id` bigint(20) NOT NULL COMMENT '英雄id',
  `user_id` bigint(20) NOT NULL COMMENT '点赞人的id',
  `create_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '点赞时间',
  `game_type` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 677 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

/*创建饰品表*/
DROP TABLE IF EXISTS `t_ornament`;
CREATE TABLE `t_ornament`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '饰品名称',
  `ico` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '饰品在线图片',
  `details` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '饰品详情',
  `ornament_type` int(255) NOT NULL DEFAULT 1 COMMENT '饰品类型，1是称号',
  `image_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的本地图片地址',
  `calorie` int(11) NULL DEFAULT NULL COMMENT '价值多少卡',
  `stock` int(11) NULL DEFAULT 1 COMMENT '库存',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `update_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

/*创建用户拥有饰品表*/
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_user_ormament
-- ----------------------------
DROP TABLE IF EXISTS `t_user_ormament`;
CREATE TABLE `t_user_ormament`  (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户id',
  `ormament_id` int(11) NULL DEFAULT NULL COMMENT '拥有的饰品id',
  `state` int(2) NULL DEFAULT NULL COMMENT '状态：0未佩戴，1佩戴',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

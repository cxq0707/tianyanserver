###
### database auto_chess_box
### mysql server version 5.6.44

drop table if exists t_attention ;
create table t_attention(
  `id` bigint not null primary key auto_increment,
  `user_id` bigint not null comment '关注人的id',
  `to_user_id` bigint not null comment '被关注人的id',
  `attention_time` timestamp not null default '2000-01-01 00:00:00',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 修改表t_message
alter table t_message add column video_link_url varchar(255) not null default '' comment '视频链接地址';
alter table t_message add column is_hot bit(1) not null default b'0' comment '是否热门';

## 修改表t_message_comment
alter table t_message_comment add column update_time timestamp not null default current_timestamp on update current_timestamp;
alter table t_message_comment add column is_excellent bit(1) not null default b'0' comment '是否是精选的';
alter table t_message_comment add column flag_time timestamp not null default '2000-01-01 00:00:00'
  comment '标志时间，当点赞数更新时，该时间会更新；当回复数更新时，该时间不会更新';


### 新增管理消息类别表t_manage_message_category
create table t_manage_message_category(
  `id` bigint not null primary key auto_increment,
  `name` varchar(100) not null default '',
  `create_time` timestamp not null default current_timestamp,
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset = utf8 ;

## 向表t_manage_message_category中插入数据
insert into t_manage_message_category values (1,'阵容',now(),now());
insert into t_manage_message_category values (2,'阵容评论',now(),now());
insert into t_manage_message_category values (3,'帖子',now(),now());
insert into t_manage_message_category values (4,'帖子评论',now(),now());
insert into t_manage_message_category values (5,'讯息',now(),now());
insert into t_manage_message_category values (6,'讯息评论',now(),now());

### 新增管理消息表t_manage_message
create table t_manage_message(
  `id` bigint not null primary key auto_increment,
  `user_id` bigint not null comment '主体的id',
  `to_user_id` bigint not null comment '客体的id',
  `manage_message_category_id` bigint not null comment '管理消息类别id，1为阵容，2阵容评论，3为帖子，4为帖子评论，5为讯息，6为讯息评论',
  `entity_id` bigint not null comment '客体的实体id，即阵容id或帖子id或讯息id或阵容评论id等',
  `action_id` bigint default null comment '主体的行为id，如评论id，点赞id',
  `action_type` int not null default 0 comment '行为类型，0表示点赞行为，1为评论行为',
  `create_time` timestamp not null default current_timestamp,
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;



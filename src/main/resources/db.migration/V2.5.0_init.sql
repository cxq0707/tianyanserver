## 修改表squad的结构
alter table squad add column visible int not null default 1  comment '0代表仅自己可见，1代表所有人可见';
ALTER TABLE `squad`
ADD COLUMN `gpa`  double NOT NULL DEFAULT 0 COMMENT '平均分' ;
## 修改表t_squad_details的结构
ALTER TABLE `t_squad_details` ADD COLUMN `prop_list`  varchar(255) NULL COMMENT '道具ID集合'


## 新增表t_prop道具表
DROP TABLE IF EXISTS `t_prop`;
CREATE TABLE `t_prop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '道具名称',
  `details` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '道具详情',
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '道具图片',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `game_type` int(11) NOT NULL DEFAULT '1' COMMENT '游戏类型',
  `state` int(11) NOT NULL DEFAULT '1' COMMENT '0装备列表不显示1显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='道具表';

-- ----------------------------
-- Records of t_prop
-- ----------------------------
INSERT INTO `t_prop` VALUES ('1', '无尽之刃', '暴击伤害+50%', null, '2019-07-31 16:11:27', '2019-08-01 14:16:47', '4', '1');
INSERT INTO `t_prop` VALUES ('2', '幻影之舞', '攻击速度+50%', null, '2019-07-31 16:11:54', '2019-08-01 14:16:48', '4', '1');

## 新增表t_prop_items道具合成表
DROP TABLE IF EXISTS `t_prop_items`;
CREATE TABLE `t_prop_items` (
  `id` int(11) NOT NULL,
  `prop_id` int(11) NOT NULL COMMENT '道具ID',
  `material_id` int(11) NOT NULL COMMENT '材料ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='道具合成表';


##
## 新增表t_android_version 安卓版本信息表
create table t_android_version(
  `id` bigint primary key auto_increment,
  `version` varchar(32) not null comment '版本号',
  `serial` int not null comment '版本的批次，逐级递增的数',
  `publish_time` timestamp not null default current_timestamp comment '版本发布时间',
  `description` varchar(255) not null default '' comment '版本描述内容',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 向表t_android_version表中插入数据
insert into t_android_version values (1, 'v2.2.5', 1, '2019-07-15 18:00:00', 'abcaabca',now(), now());
insert into t_android_version values (2, 'v2.3.0', 2, '2019-07-20 18:00:00', 'xxpcaabca',now(), now());
insert into t_android_version values (3, 'v2.4.0', 3, '2019-07-25 18:00:00', 'tcppcaabcd',now(), now());


## 新增表t_version_type 安卓版本渠道下载表
create table t_version_type(
  `id` bigint primary key auto_increment,
  `android_version_id` bigint not null ,
  `type` int not null comment '下载渠道的类型',
  `download_url` varchar(255) not null comment '下载的地址',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

# 修改表t_version_type
alter table t_version_type add column source_name varchar(255) not null default '' comment '下载渠道名称';

## 向表t_version_type插入数据
insert into t_version_type values (1, 1, 1, 'http://www.xiaoyuan.down1',now(), now(),'安智');
insert into t_version_type values (2, 1, 2, 'http://www.xiaoyuan.down1',now(), now(),'百度');
insert into t_version_type values (3, 1, 3, 'http://www.xiaoyuan.down1',now(), now(),'360');
insert into t_version_type values (4, 1, 4, 'http://www.xiaoyuan.down1',now(), now(),'应用宝');

insert into t_version_type values (5, 2, 1, 'http://www.xiaoyuan.down2',now(), now(),'安智');
insert into t_version_type values (6, 2, 2, 'http://www.xiaoyuan.down2',now(), now(),'百度');
insert into t_version_type values (7, 2, 3, 'http://www.xiaoyuan.down2',now(), now(),'360');
insert into t_version_type values (8, 2, 4, 'http://www.xiaoyuan.down2',now(), now(),'应用宝');

insert into t_version_type values (9,  3, 1, 'http://www.xiaoyuan.down3',now(), now(),'安智');
insert into t_version_type values (10, 3, 2, 'http://www.xiaoyuan.down3',now(), now(),'百度');
insert into t_version_type values (11, 3, 3, 'http://www.xiaoyuan.down3',now(), now(),'360');
insert into t_version_type values (12, 3, 4, 'http://www.xiaoyuan.down3',now(), now(),'应用宝');


# 创建表阵容插眼表t_squad_chayan
create table t_squad_chayan(
  `id` bigint primary key auto_increment,
  `squad_id` bigint not null comment '阵容的id',
  `user_id` bigint not null comment '插眼的用户id',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;


# 创建表阵容评分表 t_squad_star
create table t_squad_star(
  `id` bigint primary key auto_increment,
  `squad_id` bigint not null comment '阵容的id',
  `user_id` bigint not null comment '插眼的用户id',
  `star` int not null default 0 comment '阵容评分，只能为0,2,4,6,8,10',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

# 修改帖子表
alter table user_dynamic add column is_on_top int not null default 0 comment '帖子是否置顶';
alter table user_dynamic add column is_excellent int not null default 0 comment '帖子是否精选';








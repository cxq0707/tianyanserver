## 新增t_lucky_ticket 用户幸运券表
DROP TABLE IF EXISTS `t_lucky_ticket`;
CREATE TABLE `t_lucky_ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '关联的用户ID',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '幸运卷数量',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

## 新增t_lucky_ticket_info 用户幸运券表明细表
DROP TABLE IF EXISTS `t_lucky_ticket_info`;
CREATE TABLE `t_lucky_ticket_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `explain` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '获取明细',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1代表邀请用户获取,2代表开蛋获取,3新人礼包获取',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

## 新增t_invite_info 用户邀请详情表
DROP TABLE IF EXISTS `t_invite_info`;
CREATE TABLE `t_invite_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '邀请人ID',
  `to_user_id` bigint(20) NOT NULL COMMENT '被邀请人ID',
  `calorie_num` int(11) NOT NULL DEFAULT '0' COMMENT '邀请人获得的卡路里数量',
  `lucky_ticket_num` int(11) NOT NULL DEFAULT '0' COMMENT '邀请人获得的幸运券数量',
  `to_calorie_num` int(11) NOT NULL DEFAULT '0' COMMENT '被邀请人获得的卡路里数量',
  `to_lucky_ticket_num` int(11) NOT NULL DEFAULT '0' COMMENT '被邀请人获得的幸运券数量',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


## 新增表t_activity_wish_comment
create table t_activity_wish_comment(
  `id` bigint primary key auto_increment,
  `activity_wish_id` bigint not null comment '许愿活动id',
  `user_id` bigint not null comment '评论人的id',
  `to_comment_id` bigint default null comment '关联的父评论的id',
  `comment_content` varchar(255) default null comment '评论的内容',
  `comment_image_list` varchar(255) default null ,
  `supports` int not null default 0 comment '点赞数',
  `create_time` timestamp not null default current_timestamp comment '创建时间',
  `update_time` timestamp not null default current_timestamp on update current_timestamp
)engine =InnoDB default charset =utf8 ;

## 新增open_egg_prize_config 开蛋配置表
DROP TABLE IF EXISTS `open_egg_prize_config`;
CREATE TABLE `open_egg_prize_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(255) NOT NULL DEFAULT '1',
  `calorie_num` int(11) NOT NULL DEFAULT '0',
  `lucky_ticket_num` int(255) NOT NULL DEFAULT '0',
  `section` int(11) NOT NULL DEFAULT '100',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

## 新增t_open_egg_add_num 增加开蛋次数记录表
DROP TABLE IF EXISTS `t_open_egg_add_num`;
CREATE TABLE `t_open_egg_add_num` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(255) NOT NULL DEFAULT '1' COMMENT '观看视频增加次数为1 消耗卡路里增加次数为2',
  `num` int(11) NOT NULL DEFAULT '1' COMMENT '具体增加多少次',
  `create_time` timestamp NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

## 新增t_open_egg_info 开蛋记录表
DROP TABLE IF EXISTS `t_open_egg_info`;
CREATE TABLE `t_open_egg_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '开蛋的用户ID',
  `calorie_num` int(11) NOT NULL DEFAULT '0' COMMENT '获得的卡路里数量',
  `lucky_ticket_num` int(11) NOT NULL DEFAULT '0' COMMENT '获得的幸运券数量',
  `cdkey` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '兑换码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

##t_activity_wish表新增字段is_show是否在阵容展示
 alter table t_activity_wish add column `is_show` int(11) not null default '0' COMMENT '是否在阵容展示';

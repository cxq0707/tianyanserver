##user_dynamic表新增字段isverify审核状态,media_type媒体类型,video_url视频url,video_cover_url视频封面url
alter table user_dynamic add column `isverify` tinyint(3) not null default '2' COMMENT '0审核未通过 1审核中 2审核通过';
alter table user_dynamic add column `media_type` tinyint(3) not null default '1' COMMENT '媒体类型1图文2视频';
alter table user_dynamic add column `video_url` varchar(255)  COMMENT '视频地址';
alter table user_dynamic add column `video_cover_url` varchar(255)  COMMENT '视频封面地址';
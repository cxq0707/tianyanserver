##############
### 修改表t_message的结构
## 添加字段board_type 版块类型，0综合,1刀塔自走棋，2多多自走棋,3云顶之弈, 4刀塔霸业, 5赤潮自走棋
alter table t_message add column board_type int not null default 0
comment '版块类型，0综合,1刀塔自走棋，2多多自走棋,3云顶之弈, 4刀塔霸业, 5赤潮自走棋';

### 修改表t_manage_message的结构
## 新增字段msg_body 消息体，字符串类型
alter table t_manage_message add column msg_body varchar(255) default null ;












package com.xy.zizoumohe.interceptor;


import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.ExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;


/**
 * @Author 18011618
 * @Description 全局异常拦截器
 */

@RestControllerAdvice
public class GlobalExceptionInterceptor {

    private static final String logExceptionFormat = "Capture Exception By GlobalExceptionHandler: Code: %s Detail: %s";
    private static Logger log = LoggerFactory.getLogger(GlobalExceptionInterceptor.class);

    //400错误
    @ExceptionHandler({HttpMessageNotReadableException.class})
    public PageMsg requestNotReadable(HttpMessageNotReadableException ex) {
        System.out.println("400..requestNotReadable");
        return resultFormat(400, ex);
    }

    //400错误
    @ExceptionHandler({TypeMismatchException.class})
    public PageMsg requestTypeMismatch(TypeMismatchException ex) {
        System.out.println("400..TypeMismatchException");
        return resultFormat(400, ex);
    }

    //400错误
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public PageMsg requestMissingServletRequest(MissingServletRequestParameterException ex) {
        System.out.println("400..MissingServletRequest");
        return resultFormat(400, ex);
    }


    //500错误
    @ExceptionHandler({ConversionNotSupportedException.class, HttpMessageNotWritableException.class})
    public PageMsg server500(RuntimeException ex) {
        System.out.println("500...");
        return resultFormat(500, ex);
    }

    //业务异常
    @ExceptionHandler({ApplicationException.class})
    public PageMsg ApplicationException(ApplicationException ex) {
        return resultFormat(ex.code, ex);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public PageMsg MethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return resultFormat(500, ex);
    }

    //其他错误
    @ExceptionHandler({Exception.class})
    public PageMsg exception(Exception ex) {
        return resultFormat(500, ex);
    }

    private <T extends Throwable> PageMsg resultFormat(Integer code, T ex) {
        if (ex instanceof MethodArgumentNotValidException) {
            log.error(String.format(logExceptionFormat, code, ex.getMessage()));
            return PageMsg.failure().setMessage("入参错误").setCode(400);
        }
        log.error(String.format(logExceptionFormat, code, ex.getMessage()));
        if(ex instanceof ApplicationException){
            return PageMsg.failure().setMessage(ex.getMessage()).setCode(code);
        }
        log.error(ExceptionUtil.getMessage(ex));
        ex.printStackTrace();
        return PageMsg.failure().setMessage("错误").setCode(code);
    }

}
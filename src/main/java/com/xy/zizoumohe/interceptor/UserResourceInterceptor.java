package com.xy.zizoumohe.interceptor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.entity.Resource;
import com.xy.zizoumohe.entity.UserResource;
import com.xy.zizoumohe.mapper.ResourceMapper;
import com.xy.zizoumohe.mapper.UserMapper;
import com.xy.zizoumohe.mapper.UserResourceMapper;
import com.xy.zizoumohe.utils.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@Component
public class UserResourceInterceptor implements HandlerInterceptor {
    @Autowired
    ResourceMapper resourceMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserResourceMapper userResourceMapper;
    private static Logger logger = LoggerFactory.getLogger(UserResourceInterceptor.class);

    public final String[] URL = {"dynamic/issue-dynamic", "dynamic/publish-comment",
            "dynamic/dynamic-support", "dynamic/dynamic-comment-support",
            "squad/issue-squad", "squad/publish-comment",
            "squad/squad-support", "squad-comment-support",
            "/api/ornament", "/refreshToken",
            "create", "remove", "user-alter",
            "msgCentre/list", "add", "cancel", "getGiftList",
            "makeWish", "inspire", "getTask", "inbox", "myHomePage"
            , "applyFor", "chayan", "egg/open", "addOpenNumByCalorie", "makeWish"};

    @Override
    public boolean preHandle(HttpServletRequest req,
                             HttpServletResponse res, Object o) throws Exception {
        String str = req.getParameter("currentLoginUserId");
        if (str == null || str.equals("") || str.equals(0)) {
            str = req.getParameter("userId");
        }
        RequestWrapper myRequestWrapper = new RequestWrapper((HttpServletRequest) req);
        String body = myRequestWrapper.getBody();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(body); // 读取Json
        Long userId = null;
        if (null != rootNode && str == null) {
            userId = rootNode.path("currentLoginUserId").asLong();
            if (userId.equals("") || userId.intValue() == 0) {
                userId = rootNode.path("userId").asLong();
            }
        }
        if (null != str) {
            userId = Long.valueOf(str);
        }
        if (null == userId) {
            return true;
        }
        String url = req.getRequestURI();
        String isUrl = isUrl(url);
        if (null == isUrl) {
            return true;
        }
        UserResource userResource = userResourceMapper.isPower(userId);
        if (userMapper.selectByPrimaryKey(userId) == null) {
            returnJson(res, PageMsg.failure().setCode(800).setMessage("入参错误"));
        }
        if (null == userResource) {
            return true;
        }
        returnJson(res, PageMsg.failure().setCode(1006).setMessage("无权限的行为"));
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView)
            throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    }

    private String isUrl(String url) {
        for (String str : URL) {
            if (url.contains(str)) {
                return str;
            }
        }
        return null;
    }

    private void returnJson(HttpServletResponse response, PageMsg pageMsg) throws Exception {
        PrintWriter writer = null;
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(pageMsg);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(json);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }
}

package com.xy.zizoumohe.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.entity.Token;
import com.xy.zizoumohe.service.TokenService;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;


@Component
public class TokenInterceptor implements HandlerInterceptor {
    private static Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);
    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    public final String[] URL = {"dynamic/issue-dynamic", "dynamic/publish-comment",
            "dynamic/dynamic-support", "dynamic/dynamic-comment-support",
            "squad/issue-squad", "squad/publish-comment",
            "squad/squad-support", "squad-comment-support",
            "/api/ornament", "/refreshToken",
            "create", "remove", "user-alter",
            "msgCentre/list", "add", "cancel", "getGiftList",
            "makeWish", "inspire", "getTask", "inbox", "myHomePage"
            , "applyFor", "chayan", "egg/open", "addOpenNumByCalorie", "makeWish"};

    public final String[] NOT_NEED_LOGIN_API = {"/api/user/userLogin", "/api/user/weChat/Login", "/api/user/homePage", "/api/user/refreshToken", "/api/message/detail", "/api/message/list", "/api/messageComment/list"};

    //提供查询
    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
            throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3)
            throws Exception {
    }

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object obj) {
        try {
            Long userId = userService.getCurrentUserId();
            String url = req.getRequestURI();
            if (!isUrlNeedLogin(url)) {
                return true;
            }
            res.setCharacterEncoding("UTF-8");
            String headerToken = req.getHeader("SmallTownToken");
            //判断请求信息
            if (null == headerToken || headerToken.trim().equals("")) {
                returnJson(res, PageMsg.failure().setCode(1003).setMessage("用户信息已经过期请重新登录"));
                logger.error("在TokenInterceptor.java 85行由于传入token为空导致用户信息过期");
                return false;
            }
            //解析Token信息

            Long itokenUserId = userId;
            //根据客户Token查找数据库Token
            Token myToken = tokenService.selectByUserId(itokenUserId);
            //数据库没有Token记录
            if (null == myToken) {
                logger.error("在TokenInterceptor.java 95行由于数据库不存在该用户的token导致用户信息过期");
                returnJson(res, PageMsg.failure().setCode(1003).setMessage("用户信息已经过期请重新登录，数据库不存在该用户的token"));
                return false;
            }
            //数据库Token与用户Token比较
            System.out.println(headerToken);
            System.out.println(myToken.getToken());
            if (!headerToken.equals(myToken.getToken())) {
                logger.error("在TokenInterceptor.java 103行由于入参token与数据库token不一致导致用户信息过期");
                logger.error("入参的token为:" + headerToken + "数据库token为" + "myToken.getToken()");
                returnJson(res, PageMsg.failure().setCode(1003).setMessage("用户token与数据库不一致，已经过期"));
                return false;
            }
            //判断Token过期
            long tokenDate = myToken.getBuildtime();
            long chaoshi = new Date().getTime() - tokenDate;
            if (chaoshi > 1000 * 60 * 60 * 24 * 60L) {
                logger.error("在TokenInterceptor.java 112行由于太长时间未刷新token导致用户信息过期");
                returnJson(res, PageMsg.failure().setCode(1004).setMessage("未刷新token导致用户信息过期"));
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("在TokenInterceptor.java 115行由于出现异常导致用户信息过期");
            returnJson(res, PageMsg.failure().setCode(1005).setMessage("用户信息已经过期请重新登录"));
            return false;
        }
        //最后才放行
        return true;
    }

    private void returnJson(HttpServletResponse response, PageMsg pageMsg) {
        PrintWriter writer = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String json = objectMapper.writeValueAsString(pageMsg);
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html; charset=utf-8");

            writer = response.getWriter();
            writer.print(json);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    private boolean isUrlNeedLogin(String url) {
        for (String str : NOT_NEED_LOGIN_API) {
            if (url.contains(str)) {
                return false;
            }
        }
        return true;
    }

    private String isUrl(String url) {
        for (String str : URL) {
            if (url.contains(str)) {
                return str;
            }
        }
        return null;
    }

}

package com.xy.zizoumohe.interceptor;

import com.xy.zizoumohe.entity.Admin;
import com.xy.zizoumohe.utils.IpHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.logging.Logger;


@Component
public class AdminInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest,
                             HttpServletResponse httpServletResponse, Object o) throws Exception {
        Admin admin  = (Admin) httpServletRequest.getSession().getAttribute("admin");
        String url =  httpServletRequest.getRequestURI();
        if(null==admin){
            httpServletRequest.getRequestDispatcher("/admin/login-page").forward(httpServletRequest, httpServletResponse);
          return   false;
        }
        Long outTime = (Long) httpServletRequest.getSession().getAttribute("outTime");
        long time = new Date().getTime();
        if(null==outTime||time>outTime.longValue()){
            httpServletRequest.getSession().setAttribute("admin",null);
            httpServletResponse.sendRedirect("/admin/login-page");
            return   false;
        }
        httpServletRequest.getSession().setAttribute("outTime",new Date().getTime()+1000*60*30);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView)
            throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
    }
}

package com.xy.zizoumohe.interceptor;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfigurer implements WebMvcConfigurer {
    @Autowired
    private TokenInterceptor tokenInterceptor;
    @Autowired
    private AdminInterceptor adminInterceptor;
    @Autowired
    private UserResourceInterceptor userResourceInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
/*       registry.addInterceptor(adminInterceptor).addPathPatterns("/console/**");
       registry.addInterceptor(userResourceInterceptor).addPathPatterns("/api/**");
      registry.addInterceptor(tokenInterceptor).addPathPatterns("/api/**");*/
    }
}

package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AddressRequestVO {
    private String userName;
    private String nationalCode;
    private String telNumber;
    private String postalCode;
    private String provinceName;
    private String cityName;
    private String countyName;
    private String detailInfo;
}

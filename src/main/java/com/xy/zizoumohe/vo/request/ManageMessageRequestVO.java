package com.xy.zizoumohe.vo.request;

import java.io.Serializable;

public class ManageMessageRequestVO implements Serializable {


    private Long adminId;

    private Long userId;

    private String msgBody;

    public ManageMessageRequestVO(){}

    public ManageMessageRequestVO(Long adminId, Long userId, String msgBody) {
        this.adminId = adminId;
        this.userId = userId;
        this.msgBody = msgBody;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }
}

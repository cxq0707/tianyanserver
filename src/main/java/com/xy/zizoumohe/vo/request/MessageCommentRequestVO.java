package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class MessageCommentRequestVO implements Serializable {
    // 讯息的id
    @NotNull
    private Long messageId;
    // 评论人的id
    @NotNull
    private Long userId;
    // 评论内容
    private String commentContent;

    private Integer mediaType;

    private String audioUrl;

    private Integer audioDuration;

    // 关联的父评论的id
    private Long toMessageCommentId;
    // 评论中图片列表
    private List<String> commentImageList;

}

package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class LoginByWechatVO {
    @NotNull
    private String code;

    @NotNull
    private String avatarUrl;

    @NotNull
    private String nickName;
    private Integer gender = 0;
    private String language = "";

    private String province = "";
    private String country = "";
    private String signature = "";
    private String encryptedData = "";
    private String iv = "";
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private String locationString = "China";

    private String inviteCode;
}

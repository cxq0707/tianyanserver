package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class ActivityWishCommentRequestVO implements Serializable {

    private Long activityWishId;

    private Long userId;

    private String commentContent;

    private List<String> commentImageList;

    private Long toCommentId;



}

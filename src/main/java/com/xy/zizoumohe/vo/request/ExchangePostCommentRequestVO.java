package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Setter
@Getter
public class ExchangePostCommentRequestVO {

    private String title;

    private String content;

    private Date createdTime;

    private Date updatedTime;

    @NotNull
    private List<String> imgList;
}

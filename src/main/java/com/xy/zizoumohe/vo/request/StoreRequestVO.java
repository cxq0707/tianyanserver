package com.xy.zizoumohe.vo.request;

import java.io.Serializable;

public class StoreRequestVO implements Serializable {

    private Long storeCategoryId;

    private Long userId;

    private Long entityId;

    public StoreRequestVO() {
    }

    public StoreRequestVO(Long storeCategoryId, Long userId, Long entityId) {
        this.storeCategoryId = storeCategoryId;
        this.userId = userId;
        this.entityId = entityId;
    }

    public Long getStoreCategoryId() {
        return storeCategoryId;
    }

    public void setStoreCategoryId(Long storeCategoryId) {
        this.storeCategoryId = storeCategoryId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

}

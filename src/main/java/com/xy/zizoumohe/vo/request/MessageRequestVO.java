package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class MessageRequestVO extends BaseRequestVO {

    private Long messageId;

    private String title = "";

    private Double priority;

    private String pic;

    private Boolean isRecommend = true;

    private Boolean isHot;

    private Long categoryId = 1L;

    private String videoLinkUrl;

    private String content;

    private Integer mediaType;

    private Integer boardType = 0;

    // 讯息作者id
    @NotNull
    private Long userId;
    // 审核状态
    private Integer approveState;
    // 跳转行为类型
    private Integer actionType;
    // 跳转的url
    private String forwardUrl;
    // 简介
    private String briefIntroduction;
    private int uiPattern;
    private String imgList;
}

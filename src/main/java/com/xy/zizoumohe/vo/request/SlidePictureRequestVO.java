package com.xy.zizoumohe.vo.request;

public class SlidePictureRequestVO {

    private Long slidePictureId;

    private String name;

    private String path;

    private Long linkMessageId;

    private String linkMessageUrl;

    private Boolean isCurrentUse;

    private Integer sort;
    private Integer actionType;

    private String forwardUrl;

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    public Long getSlidePictureId() {
        return slidePictureId;
    }

    public void setSlidePictureId(Long slidePictureId) {
        this.slidePictureId = slidePictureId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getLinkMessageUrl() {
        return linkMessageUrl;
    }

    public void setLinkMessageUrl(String linkMessageUrl) {
        this.linkMessageUrl = linkMessageUrl;
    }

    public Boolean getIsCurrentUse() {
        return isCurrentUse;
    }

    public void setIsCurrentUse(Boolean isCurrentUse) {
        this.isCurrentUse = isCurrentUse;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Long getLinkMessageId() {
        return linkMessageId;
    }

    public void setLinkMessageId(Long linkMessageId) {
        this.linkMessageId = linkMessageId;
    }
}

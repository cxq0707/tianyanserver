package com.xy.zizoumohe.vo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuizRequestVO {
    private int quizType;

    private String quizTitle;
    private String quizMediaUrl;

    private int quizSubject;
    private int quizGender;
    private int quizMediaType;
    private int quizLevel;
    private int quizScore;
    private int quizIconType;
    private Integer normalRewardValue;
    private Integer specialRewardValue;
    private int countDownTime;
    private int referType;
    private String referHtml;
    private Integer referMessageId;
    private String quizContent;
    private String quizRightOption;
    private String referUrl;

}
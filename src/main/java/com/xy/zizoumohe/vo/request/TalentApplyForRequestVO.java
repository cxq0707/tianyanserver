package com.xy.zizoumohe.vo.request;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class TalentApplyForRequestVO {

    @NotNull
    private Long userId;

    @NotNull
    private String content;

    @NotNull
    private Integer gameType;

    private Integer state;


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }


    public Integer getGameType() {
        return gameType;
    }


    public void setGameType(Integer gameType) {
        this.gameType = gameType;
    }


    public Integer getState() {
        return state;
    }


    public void setState(Integer state) {
        this.state = state;
    }
}
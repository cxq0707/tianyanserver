package com.xy.zizoumohe.vo.request;

import java.io.Serializable;
import java.util.Date;

/**
 * @author zachary zhu
 * @date 2019/7/22  11:46
 * @corporation UESTC
 */
public class ActivityConsoleWishRequestVO implements Serializable {

    // 许愿活动的名称
    private String name;

    // 许愿活动的描述
    private String description;

    // 许愿活动的开始时间
    private String  startTime;

    // 许愿活动的结束时间
    private String endTime;

    //许愿活动的开奖时间
    private String openTime;

    // 许愿活动的礼物id
    private Long giftId;

    // 许愿活动的礼物数量
    private Integer giftNumber;

    // 耗费卡路里数量
    private Integer costCalory;
    //礼物名称
    private String giftName;
    //礼物详情
    private String giftDescription;
    //礼物图片
    private String giftPicUrl;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftDescription() {
        return giftDescription;
    }

    public void setGiftDescription(String giftDescription) {
        this.giftDescription = giftDescription;
    }

    public String getGiftPicUrl() {
        return giftPicUrl;
    }

    public void setGiftPicUrl(String giftPicUrl) {
        this.giftPicUrl = giftPicUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Long getGiftId() {
        return giftId;
    }

    public void setGiftId(Long giftId) {
        this.giftId = giftId;
    }

    public Integer getGiftNumber() {
        return giftNumber;
    }

    public void setGiftNumber(Integer giftNumber) {
        this.giftNumber = giftNumber;
    }

    public Integer getCostCalory() {
        return costCalory;
    }

    public void setCostCalory(Integer costCalory) {
        this.costCalory = costCalory;
    }
}

package com.xy.zizoumohe.vo.request;

public class PostMessageVO {

    private String title = "";

    private Double priority;

    private String pic;

    private Boolean isRecommend = true;

    private Boolean isHot;

    private Long categoryId = 1L;

    private String videoLinkUrl;

    private String content;

    private Integer mediaType = 0;

    private Integer boardType = 0;

    // 审核状态
    private Integer approveState;
    // 跳转行为类型
    private Integer actionType;
    // 跳转的url
    private String forwardUrl;
    // 简介
    private String briefIntroduction;
    private int uiPattern;
    private String imgList;


    public int getUiPattern() {
        return uiPattern;
    }

    public void setUiPattern(int uiPattern) {
        this.uiPattern = uiPattern;
    }

    public String getImgList() {
        return imgList;
    }

    public void setImgList(String imgList) {
        this.imgList = imgList;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public String getForwardUrl() {
        return forwardUrl;
    }

    public void setForwardUrl(String forwardUrl) {
        this.forwardUrl = forwardUrl;
    }

    public String getBriefIntroduction() {
        return briefIntroduction;
    }

    public void setBriefIntroduction(String briefIntroduction) {
        this.briefIntroduction = briefIntroduction;
    }

    public Integer getBoardType() {
        return boardType;
    }

    public void setBoardType(Integer boardType) {
        this.boardType = boardType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPriority() {
        return priority;
    }

    public void setPriority(Double priority) {
        this.priority = priority;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Boolean getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Boolean isRecommend) {
        this.isRecommend = isRecommend;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getVideoLinkUrl() {
        return videoLinkUrl;
    }

    public void setVideoLinkUrl(String videoLinkUrl) {
        this.videoLinkUrl = videoLinkUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getMediaType() {
        return mediaType;
    }

    public void setMediaType(Integer mediaType) {
        this.mediaType = mediaType;
    }
    public Boolean getIsHot() {
        return this.isHot;
    }

    public void setIsHot(Boolean isHot) {
        this.isHot = isHot;
    }

    public Integer getApproveState() {
        return approveState;
    }

    public void setApproveState(Integer approveState) {
        this.approveState = approveState;
    }
}

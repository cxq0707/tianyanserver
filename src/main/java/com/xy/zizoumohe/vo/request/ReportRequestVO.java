package com.xy.zizoumohe.vo.request;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class ReportRequestVO {
    @NotNull
    private Long userId;
    @NotNull
    private Long beReportId;
    @NotNull
    private Integer type;
    private String content;


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Long getBeReportId() {
        return beReportId;
    }


    public void setBeReportId(Long beReportId) {
        this.beReportId = beReportId;
    }


    public Integer getType() {
        return type;
    }


    public void setType(Integer type) {
        this.type = type;
    }


    public String getContent() {
        return content;
    }


    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

}
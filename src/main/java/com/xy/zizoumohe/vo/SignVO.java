package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class SignVO implements Serializable {

    private Long signId;
    // 签到人的id
    private Long userId;
    // 连续签到的天数
    private Integer lastForDays;
    // 奖励的卡路里值
    private Integer award;
    // 创建时间，也即签到时间
    private Long signTime;
    // 之前是否领取过新人大礼包
    private int hasReceivePacket;
    // 新人大礼包中幸运券数量
    private Integer fortuneTickets;
    // 新人大礼包中卡路里的数量
    private Integer caloryNum;

    public SignVO(){}

    public SignVO(Long signId){
        this.signId = signId;
    }

    public SignVO(Long signId, Long signTime){
        this.signId = signId;
        this.signTime = signTime;
    }

}

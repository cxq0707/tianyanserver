package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WishAwardSimpleVO implements Serializable {

    private Long giftId;

    private String giftName;

    private String giftDescription;

    private String giftPicUrl;

    // 用户的获奖状态，0未知(未登录),1未参与，2未获奖，3已获奖
    private Integer state;

    private String acquireAwardDescription;

}

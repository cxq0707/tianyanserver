package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class MessageCommentSimpleVO implements Serializable {

    @JsonProperty("commentId")
    private Long messageCommentId;

    private Long messageId;

    private Long userId;

    private String nickname;

    private String userHeadPortrait;

    private String commentContent;

    private List<String> commentImageList;

    public MessageCommentSimpleVO(){}

    public MessageCommentSimpleVO(Long messageCommentId) {
        this.messageCommentId = messageCommentId;
    }
}


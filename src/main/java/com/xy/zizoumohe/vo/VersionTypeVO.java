package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class VersionTypeVO implements Serializable {

    private Long androidVersionId;

    private Integer type;

    private String downloadUrl;

    private String sourceName;


}

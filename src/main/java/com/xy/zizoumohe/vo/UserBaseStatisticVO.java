package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class UserBaseStatisticVO  extends UserVO implements Serializable {

    private Long userId;

//    private UserVO user;

    private Long toSupports;

    private Integer fans;

    private Integer attentions;
    private Long calorie;
    // 0表示未关注 1表示已关注 2表示相互关注
    private Integer attentionState;
    private List<TitleVO> titleList;
    private String inviteCode;

}

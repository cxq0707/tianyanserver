package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class AttentionVO implements Serializable {
    private Long attentionId;
    // 关注人的用户id
    private Long userId;
    // 关注时间
    private Long attentionTime;
    // 被关注人的用户信息
    private UserVO toUser;
    // 关注状态,1表示未关注，2表示已关注，3表示相互关注
    private Integer attentionState;

    public AttentionVO(){}

    public AttentionVO(Long attentionId){
        this.attentionId = attentionId;
    }

    public AttentionVO(Long attentionId, Long attentionTime) {
        this.attentionId = attentionId;
        this.attentionTime = attentionTime;
    }

}

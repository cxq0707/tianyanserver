package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TitleVO {
    @JsonProperty("ornamentId")
    private Integer id;
    private String name;
    private String details;
    private String ico;

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

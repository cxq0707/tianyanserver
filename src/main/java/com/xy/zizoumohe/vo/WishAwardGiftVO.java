package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WishAwardGiftVO implements Serializable {

    private Long activityWishId;

    private Long userId;
    // 兑奖码
    private String exchangeCode;
    // 许愿活动开奖时间
    private Long openTime;
    // 获奖描述
    @JsonProperty("acquireAwardDescription")
    private String description;
    //
    private Long giftId;

    private String giftName;

    private String giftDescription;

    private String giftPicUrl;

}

package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserOrnamentVO {
    @JsonProperty("ornamentId")
    private Integer id;
    private String name;
    private String ico;
    private String details;
    private Integer ornamentType;
    @JsonProperty("ornamentCalorie")
    private Integer calorie;
    private Long createTime;

    public Integer getCalorie() {
        return calorie;
    }

    public void setCalorie(Integer calorie) {
        this.calorie = calorie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Integer getOrnamentType() {
        return ornamentType;
    }

    public void setOrnamentType(Integer ornamentType) {
        this.ornamentType = ornamentType;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

}

package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class MessageSupportVO implements Serializable {

    private Long id;

    private Long messageId;

    private Long userId;

    private String userName;

    private Date supportTime;

}

package com.xy.zizoumohe.vo;

public class RemainOpenEggNumVO {
    private int remainOpenEggNum;
    private int remainCalorieNum;
    private int remainInspireNum;

    public int getRemainOpenEggNum() {
        return remainOpenEggNum;
    }

    public void setRemainOpenEggNum(int remainOpenEggNum) {
        this.remainOpenEggNum = remainOpenEggNum;
    }

    public int getRemainCalorieNum() {
        return remainCalorieNum;
    }

    public void setRemainCalorieNum(int remainCalorieNum) {
        this.remainCalorieNum = remainCalorieNum;
    }

    public int getRemainInspireNum() {
        return remainInspireNum;
    }

    public void setRemainInspireNum(int remainInspireNum) {
        this.remainInspireNum = remainInspireNum;
    }
}

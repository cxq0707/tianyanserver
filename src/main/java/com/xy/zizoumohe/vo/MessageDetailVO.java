package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Setter
@Getter
public class MessageDetailVO implements Serializable {

    private Long messageId;

    private Long categoryId;

    private String categoryName;

    private String title;

    private Long createTime;

    private String videoLinkUrl;

    private Long hits;

    private Integer supports;

    private Integer comments;

    private String content;

    private Integer mediaType;

    // 当前用户是否点赞过
    private Boolean isSupport = false;
    // 当前用户是否收藏过
    private Boolean isStore = false;

    private Double priority;

    // 封面图
    private String pic;

    private Long authorId;
    // 讯息作者信息
    private UserVO user;

    private Integer attentionState = 0;

    public MessageDetailVO(){}

    public MessageDetailVO(Long messageId) {
        this.messageId = messageId;
    }
    private Double latitude;
    private Double longitude;
    private String locationString;

}

package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.xy.zizoumohe.entity.User;

import java.util.Date;

@JsonIgnoreProperties({"dynamicImage"})
public class UserDynamicVO {
    private Long dynamicId;
    private Long userId;
    private Long dynamicSupport;
    private Long dynamicComment;
    private Long dynamicTime;
    private Long dynamicSupportId;
    private String dynamicContent;
    private Integer topicType;
    private String[] dynamicImagePlus;
    @JsonProperty("user")
    private UserVO userVO;
    private String title;
    // 是否置顶
    private Integer isOnTop;
    // 是否精选
    private Integer isExcellent;
    private Integer isverify;
    private Integer mediaType;
    private String videoUrl;
    private String videoCoverUrl;
    private String articleContent;
    private Integer attentionState;

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public Integer getAttentionState() {
        return attentionState;
    }

    public void setAttentionState(Integer attentionState) {
        this.attentionState = attentionState;
    }

    public Integer getIsverify() {
        return isverify;
    }

    public void setIsverify(Integer isverify) {
        this.isverify = isverify;
    }

    public Integer getMediaType() {
        return mediaType;
    }

    public void setMediaType(Integer mediaType) {
        this.mediaType = mediaType;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoCoverUrl() {
        return videoCoverUrl;
    }

    public void setVideoCoverUrl(String videoCoverUrl) {
        this.videoCoverUrl = videoCoverUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTopicType() {
        return topicType;
    }

    public void setTopicType(Integer topicType) {
        this.topicType = topicType;
    }

    public Long getDynamicTime() {
        return dynamicTime;
    }

    public void setDynamicTime(Long dynamicTime) {
        this.dynamicTime = dynamicTime;
    }

    public String[] getDynamicImagePlus() {
        return dynamicImagePlus;
    }

    public void setDynamicImagePlus(String[] dynamicImagePlus) {
        this.dynamicImagePlus = dynamicImagePlus;
    }

    public Long getDynamicId() {
        return dynamicId;
    }

    public void setDynamicId(Long dynamicId) {
        this.dynamicId = dynamicId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDynamicSupport() {
        return dynamicSupport;
    }

    public void setDynamicSupport(Long dynamicSupport) {
        this.dynamicSupport = dynamicSupport;
    }

    public Long getDynamicComment() {
        return dynamicComment;
    }

    public void setDynamicComment(Long dynamicComment) {
        this.dynamicComment = dynamicComment;
    }

    public String getDynamicContent() {
        return dynamicContent;
    }

    public void setDynamicContent(String dynamicContent) {
        this.dynamicContent = dynamicContent == null ? null : dynamicContent.trim();
    }

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    public Long getDynamicSupportId() {
        return dynamicSupportId;
    }

    public void setDynamicSupportId(Long dynamicSupportId) {
        this.dynamicSupportId = dynamicSupportId;
    }

    public Integer getIsOnTop() {
        return isOnTop;
    }

    public void setIsOnTop(Integer isOnTop) {
        this.isOnTop = isOnTop;
    }

    public Integer getIsExcellent() {
        return isExcellent;
    }

    public void setIsExcellent(Integer isExcellent) {
        this.isExcellent = isExcellent;
    }
}
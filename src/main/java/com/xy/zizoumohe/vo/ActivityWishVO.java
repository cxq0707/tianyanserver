package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Setter
@Getter
public class ActivityWishVO implements Serializable {

    private Long activityWishId;
    // 许愿活动的开奖时间
    private Long openTime;
    // 许愿活动的参与人数
    private Integer participateNumber;
    // 礼物的id
    private Long giftId;
    // 礼物的名称
    private String giftName;
    // 礼物的描述
    private String giftDescription;
    // 礼物的图片地址
    private String giftPicUrl;
    // 礼物的数量
    private Integer giftNumber;
    // 许愿活动的发布时间
    private Long createTime;
    // 耗费卡路里数量
    private Integer costCalory;
    // 上传分享状态 0未知，1未上传，2表示已上传
    private Integer uploadShareState;
    // 开奖状态，1未开奖，2已开奖
    private Integer openState;
    // 当前用户的参与状态 0未知，1未参与，2已参与
    private Integer participateState;
    // 系统当前时间
    private Long currentTime;
    // 当前时间距离开奖时间的时间差
    private Long differenceTime;

}

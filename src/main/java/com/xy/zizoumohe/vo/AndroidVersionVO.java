package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AndroidVersionVO implements Serializable {

    private Long androidVersionId;

    @JsonProperty("latestVersion")
    private String version;

    private String description;
    private Long publishTime;

    private Integer serial;

    // 默认的下载地址
    private String downloadUrl;

    @JsonIgnore
    private List<VersionTypeVO>  versionTypeVOList = new ArrayList<>();

    public Long getAndroidVersionId() {
        return androidVersionId;
    }

    public void setAndroidVersionId(Long androidVersionId) {
        this.androidVersionId = androidVersionId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Long publishTime) {
        this.publishTime = publishTime;
    }

    public List<VersionTypeVO> getVersionTypeVOList() {
        return versionTypeVOList;
    }

    public Integer getSerial() {
        return serial;
    }

    public void setSerial(Integer serial) {
        this.serial = serial;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }
}

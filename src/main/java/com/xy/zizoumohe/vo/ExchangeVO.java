package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExchangeVO {
    private int result;
    private Integer value;

    public ExchangeVO(int result, Integer value) {
        this.result = result;
        this.value = value;
    }
}
package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ActivityWishCommentVO implements Serializable {

    private Long commentId;

    private Long activityWishId;

    private Long userId;

    private String commentContent;

    private List<String> commentImageList;

    private UserVO user;

    private Long commentTime;

    private int isSupport;

    private Integer supports;

    private ActivityWishCommentSimpleVO toComment;

    public List<String> setImageList() {
        this.commentImageList = new ArrayList<>();
        return commentImageList;
    }

}

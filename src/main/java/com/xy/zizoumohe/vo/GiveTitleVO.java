package com.xy.zizoumohe.vo;

import java.util.List;

public class GiveTitleVO {
    private Long userId;
    private String userHeadPortrait;
    private String userName;
    private String nickname;
    private List<TitleVO>  titleList;
    private int isTitle;
    private int userGainSupports;

    public int getUserGainSupports() {
        return userGainSupports;
    }

    public void setUserGainSupports(int userGainSupports) {
        this.userGainSupports = userGainSupports;
    }

    public int getIsTitle() {
        return isTitle;
    }

    public void setIsTitle(int isTitle) {
        this.isTitle = isTitle;
    }

    public List<TitleVO> getTitleList() {
        return titleList;
    }

    public void setTitleList(List<TitleVO> titleList) {
        this.titleList = titleList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getUserHeadPortrait() {
        return userHeadPortrait;
    }

    public void setUserHeadPortrait(String userHeadPortrait) {
        this.userHeadPortrait = userHeadPortrait;
    }

}
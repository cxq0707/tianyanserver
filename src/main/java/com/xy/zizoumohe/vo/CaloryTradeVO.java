package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class CaloryTradeVO implements Serializable {

    private Long caloryTradeId;
    @JsonProperty("userId")
    private Long accountId;

    private Integer tradeNum;

    private Boolean isIncome;

    private String tradeDescription;

    private Integer tradeType;

    private Long createTime;

    public CaloryTradeVO(){}

    public CaloryTradeVO(Long caloryTradeId){
        this.caloryTradeId = caloryTradeId;
    }

    public CaloryTradeVO(Long caloryTradeId, Long createTime){
        this.caloryTradeId = caloryTradeId;
        this.createTime = createTime;
    }

}

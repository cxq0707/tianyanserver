package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ActionDescriptionVO implements Serializable {

    private Long actionId;

    private String commentContent;

    private List<String> commentImageList;

}

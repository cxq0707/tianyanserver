package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class LongTaskVO implements Serializable {

    private String name;

    private Integer type;

    private String taskDescription;

    private Integer available;

    public LongTaskVO(){}

    public LongTaskVO(String name, Integer type, String taskDescription){
        this.name = name;
        this.type = type;
        this.taskDescription = taskDescription;
    }

    public LongTaskVO(String name,Integer type, String taskDescription, Integer available){
        this.name = name;
        this.type = type;
        this.taskDescription = taskDescription;
        this.available = available;
    }

}

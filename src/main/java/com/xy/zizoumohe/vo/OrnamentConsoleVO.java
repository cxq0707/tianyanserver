package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OrnamentConsoleVO {
    private Long userId;
    private Integer id;
    private String name;
    private boolean flag;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean getFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

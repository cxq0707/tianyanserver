package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class MessageModelVO implements Serializable {
    private Long messageId;
    private String title;
    private Double priority;
    private String pic;
    private Boolean isRecommend;
    private Long categoryId;
    private String videoLinkUrl;
    private String content;
    private Integer mediaType;
    private Boolean isHot;
    private Integer boardType;
    // 讯息作者id
    private Long authorId;
    // 审核状态
    private Integer approveState;
    // 跳转行为类型
    private Integer actionType;
    // 跳转的url
    private String forwardUrl;
    // 简介
    private String briefIntroduction;
    private int uiPattern;
    private Double latitude;
    private Double longitude;
    private String locationString;

}

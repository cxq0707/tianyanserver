package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class StoreSquadVO implements Serializable {

    private Long storeId;

    @JsonProperty("user")
    private UserVO userVO;
    private Long squadId;
    private String squadName;
    private Long squadSupportId;
    private Long squadSupport;
    private String commentName;
    private String commentContent;
    private Long squadComment;
    private String commentPortrait;
    private String squadKey;
    private Integer isHot;
    private Integer isClassics;
    private Boolean isStore;
    private Long squadTime;
    private List<Map<String,Object>> heroListPlus;
    private Long userId;
    private Integer attentionState;
    private String squadDescribe;
    private List<Integer> tags;
    private Integer boardType;
    @JsonProperty("gameType")
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getStoreId() {
        return storeId;
    }

    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    public Integer getBoardType() {
        return boardType;
    }

    public void setBoardType(Integer boardType) {
        this.boardType = boardType;
    }

    public List<Integer> getTags() {
        return tags;
    }

    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    public Integer getAttentionState() {
        return attentionState;
    }

    public void setAttentionState(Integer attentionState) {
        this.attentionState = attentionState;
    }

    public Boolean getIsStore() {
        return isStore;
    }

    public void setIsStore(Boolean isStore) {
        this.isStore = isStore;
    }

    public Long getSquadId() {
        return squadId;
    }

    public void setSquadId(Long squadId) {
        this.squadId = squadId;
    }

    public String getSquadName() {
        return squadName;
    }

    public void setSquadName(String squadName) {
        this.squadName = squadName;
    }

    public Long getSquadSupportId() {
        return squadSupportId;
    }

    public void setSquadSupportId(Long squadSupportId) {
        this.squadSupportId = squadSupportId;
    }

    public Long getSquadSupport() {
        return squadSupport;
    }

    public void setSquadSupport(Long squadSupport) {
        this.squadSupport = squadSupport;
    }

    public String getCommentName() {
        return commentName;
    }

    public void setCommentName(String commentName) {
        this.commentName = commentName;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public Long getSquadComment() {
        return squadComment;
    }

    public void setSquadComment(Long squadComment) {
        this.squadComment = squadComment;
    }

    public String getCommentPortrait() {
        return commentPortrait;
    }

    public void setCommentPortrait(String commentPortrait) {
        this.commentPortrait = commentPortrait;
    }

    public String getSquadKey() {
        return squadKey;
    }

    public void setSquadKey(String squadKey) {
        this.squadKey = squadKey;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getIsClassics() {
        return isClassics;
    }

    public void setIsClassics(Integer isClassics) {
        this.isClassics = isClassics;
    }

    public Long getSquadTime() {
        return squadTime;
    }

    public void setSquadTime(Long squadTime) {
        this.squadTime = squadTime;
    }

    public List<Map<String, Object>> getHeroListPlus() {
        return heroListPlus;
    }

    public void setHeroListPlus(List<Map<String, Object>> heroListPlus) {
        this.heroListPlus = heroListPlus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSquadDescribe() {
        return squadDescribe;
    }

    public void setSquadDescribe(String squadDescribe) {
        this.squadDescribe = squadDescribe;
    }

}

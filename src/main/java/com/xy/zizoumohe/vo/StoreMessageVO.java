package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Getter
@Setter
public class StoreMessageVO implements Serializable {

    private Long storeId;

    private Long messageId;

    private String messageTitle;

    private UserVO user;

    private Long storeTime;

    // 收藏的讯息状态，0表示正常（normal） 1表示已删除（deleted）
    private Integer state;

    // 讯息的标签图片
    private String pic;

    // 收藏的类别名称 分为阵容与讯息
    public String storeCategoryName;

    // 点击数或阅读数
    private Long hits;

    // 评论数
    private Integer comments;

    // 媒体类别
    private Integer mediaType;

    // 如果是视频类型，视频链接url链接
    private String videoLinkUrl;

    // 如果是视频类型，视频内容描述
    private String description;
    private int uiPattern;
    private Integer boardType;
    public String[] getImgList() {
        return imgList;
    }

    public void setImgList(String imgList) {
        if (StringUtils.isBlank(imgList)) {
            this.imgList = null;
        } else {
            this.imgList = imgList.split(",");
        }
    }

    private String[] imgList;


}

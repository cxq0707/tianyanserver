package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class UserUploadShareVO implements Serializable {
    // 用户的id
    private Long userId;
    // 是否上传成功分享
    private Boolean isUploadShare;

    private String nickname;

    private String userHeadPortrait;

    private String uploadPicUrl;

    private int inspireNum;

    private Integer fortuneValue;


}

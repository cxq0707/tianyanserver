package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class MessageCommentVO implements Serializable {

    @JsonProperty("commentId")
    private Long messageCommentId;

    private Long messageId;
    @JsonProperty("user")
    private UserVO userVO;

    // 距离1970年1月1日的时间戳
    private Long commentTime;

    private String commentContent;

    @JsonProperty("toComment")
    private MessageCommentSimpleVO toMessageComment;

    private List<String> commentImageList;

    private Integer mediaType;

    private String audioUrl;

    private Integer audioDuration;

    private Integer supports;
    // 当前用户是否点赞
    private Boolean isSupport = false;
    // 是否精选
    private Boolean isExcellent;
    // 回复数
    private Integer replys;

    public MessageCommentVO(){}

    public MessageCommentVO(Long messageCommentId) {
        this.messageCommentId = messageCommentId;
    }

    public MessageCommentVO(Long messageCommentId, Long commentTime) {
        this.messageCommentId = messageCommentId;
        this.commentTime = commentTime;
    }

}

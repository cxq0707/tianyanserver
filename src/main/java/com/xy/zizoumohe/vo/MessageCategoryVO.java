package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Setter
@Getter
public class MessageCategoryVO implements Serializable {

    private Long messageCategoryId;

    private String name;

    public MessageCategoryVO(){}

    public MessageCategoryVO(Long messageCategoryId, String name){
        this.messageCategoryId = messageCategoryId;
        this.name = name;
    }

}

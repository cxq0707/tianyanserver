package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class MessageVO implements Serializable {
    // 讯息id
    private Long messageId;
    private String title;
    // 图片url
    private String pic;
    // 点赞数
    private Integer supports;
    private Boolean isSupport = false;
    // 评论数
    private Integer comments;
    // 点击量
    private Long hits;
    // 所属类别id
    private Long categoryId;
    // 所属类别名称
    private String categoryName;
    // 创建时间
    private Long createTime;
    // 媒体类型
    private Integer mediaType;
    // 视频链接地址
    private String videoLinkUrl;
    // 媒体类型为视频类型，需要提供视频描述
    private String description;

    private Double priority;

    private Boolean isHot;

    private Boolean isRecommend;

    // 精选评论
    private List<MessageCommentSimpleVO> excellentComments;

    private Integer boardType;

    private Long authorId;

    private UserVO user;

    private Integer approveState;

    private Integer actionType;

    private String forwardUrl;

    private String briefIntroduction;

    private Integer attentionState = 0;

    private int uiPattern;

    public String[] getImgList() {
        return imgList;
    }

    public void setImgList(String imgList) {
        if (StringUtils.isBlank(imgList)) {
            this.imgList = null;
        } else {
            this.imgList = imgList.split(",");
        }
    }

    private String[] imgList;


    public MessageVO() {
    }

    public MessageVO(Long messageId) {
        this.messageId = messageId;
    }

    public MessageVO(Long messageId, Long createTime) {
        this.messageId = messageId;
        this.createTime = createTime;
    }

    private Double latitude;
    private Double longitude;
    private String locationString;
}

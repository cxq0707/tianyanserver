package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLoginVO {
    private Long userId;
    private String avatarUrl;
    private String userName;
    private String nickname;
    private String token;
    private String inviteCode;
    private String latitude;
    private String longitude;
    private String gender;
    private String locationString;
    private int newUser;

}
package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftDetailVO {

    private Long userId;

    private Long caloryNum = 0L;

    private Long activityWishId;

    private String giftName;

    private Integer giftNumber;

    private String giftPicUrl;

    private String giftDescription;

    // 开奖时间
    private Long openTime;
    // 当前时间
    private Long currentTime;

    private Long differenceTime;
    // 开奖状态
    private Integer openState;


}

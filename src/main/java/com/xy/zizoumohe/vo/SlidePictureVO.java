package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class SlidePictureVO implements Serializable {

    private Long slidePictureId;
    // 轮播图名称
    private String name;

    private String path;

    private Long linkMessageId;

    private String linkMessageUrl;

    private Long createTime;

    private Boolean isCurrentUse;

    private Integer sort;

    private Integer mediaType;

    private Integer actionType;

    private String forwardUrl;

    public SlidePictureVO(){}

    public SlidePictureVO(Long slidePictureId){
        this.slidePictureId = slidePictureId;
    }

    public SlidePictureVO(Long slidePictureId, Long createTime){
        this.slidePictureId = slidePictureId;
        this.createTime = createTime;
    }

}

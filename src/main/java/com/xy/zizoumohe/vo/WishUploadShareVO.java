package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class WishUploadShareVO implements Serializable {
    // 许愿活动id
    private Long activityWishId;
    // 用户的id
    private Long userId;
    // 礼物名称
    private String giftName;
    // 礼物描述
    private String giftDescription;
    // 礼物图片的地址
    private String giftPicUrl;
    // 上传分享图片的地址
    private String uploadPicUrl;
    private int winningProb;
    // 观看激励视频的次数
    private int inspireNum;
    // 幸运券使用的次数
    private Integer useFortuneTicketsTimes;
    // 用户的幸运券数量
    private Integer fortuneTickets;
    // 用户的幸运值
    private Integer fortuneValue;
    // 当前时间
    private Long currentTime;

    // 开奖状态
    private Integer openState;
}

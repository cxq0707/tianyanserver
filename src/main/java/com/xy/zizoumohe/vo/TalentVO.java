package com.xy.zizoumohe.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class TalentVO {
    private Integer id;
    private Long userId;
    private Integer rank;
    private Integer fans;
    private Integer gameType;
    @JsonProperty("user")
    private UserVO userVO;
    @JsonProperty("details")
    private String describe;
    private Long caloryNum;

    private int attentionState;

    public Long getCaloryNum() {
        return caloryNum;
    }

    public void setCaloryNum(Long caloryNum) {
        this.caloryNum = caloryNum;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    public int getAttentionState() {
        return attentionState;
    }

    public void setAttentionState(int attentionState) {
        this.attentionState = attentionState;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public UserVO getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVO userVO) {
        this.userVO = userVO;
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Integer getRank() {
        return rank;
    }


    public void setRank(Integer rank) {
        this.rank = rank;
    }


    public Integer getGameType() {
        return gameType;
    }


    public void setGameType(Integer gameType) {
        this.gameType = gameType;
    }

}
package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
public class DailyTaskVO implements Serializable {

    private String name;

    private Integer type;

    private Integer finishState;

    private String taskDescription;

    public DailyTaskVO(){}

    public DailyTaskVO(String name, Integer type, Integer finishState, String taskDescription){
        this.name = name;
        this.type = type;
        this.finishState = finishState;
        this.taskDescription = taskDescription;
    }


}

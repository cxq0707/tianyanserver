package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
public class ManageMessageVO implements Serializable {

    private Long manageMessageId;

    //private Long userId;

    private UserVO user;

    private Long toUserId;

    private Long manageMessageCategoryId;

    private String manageMessageCategoryName;

    //private Long entityId;

    private EntityDescriptionVO entityDescription;

    //private Long actionId;

    private ActionDescriptionVO actionDescription;

    // 行为类型，0表示点赞行为，1表示评论行为
    private Integer actionType;

    private Long createTime;


}

package com.xy.zizoumohe.vo;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ActivityWishCommentSimpleVO implements Serializable {

    private Long commentId;

//    private UserVO user;

    private Long userId;

    private String nickname;

    private String userHeadPortrait;

    private String commentContent;

    private List<String> commentImageList;

    public List<String> setImageList(){
        this.commentImageList = new ArrayList<>();
        return commentImageList;
    }


}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.*;
import com.xy.zizoumohe.entity.User;
import com.xy.zizoumohe.service.InviteService;
import com.xy.zizoumohe.service.ManageMessageService;
import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.utils.enumeration.KeyRules;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.LoginByWechatVO;
import com.xy.zizoumohe.vo.request.ManageMessageRequestVO;
import com.xy.zizoumohe.vo.request.MessageRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/api/user")
@RestController
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    private ManageMessageService manageMessageService;
    @Autowired
    InviteService inviteService;
    @Autowired
    AesUtils aesUtils;
    @Autowired
    MessageService messageService;

    @Autowired
    RedisUtils redisUtils;

    private class UserHomePageVO {
        UserBaseStatisticVO user;

        public UserBaseStatisticVO getUser() {
            return user;
        }

        public void setUser(UserBaseStatisticVO user) {
            this.user = user;
        }

        public List<MessageVO> getMessages() {
            return messages;
        }

        public void setMessages(List<MessageVO> messages) {
            this.messages = messages;
        }

        List<MessageVO> messages;
    }

    //微信登录接口
    @RequestMapping(value = "/weChat/Login", method = RequestMethod.POST)
    public PageMsg weChatLogin(@RequestBody @Valid LoginByWechatVO loginVO) {
        UserLoginVO userLoginVO = userService.userWxProgramLogin(loginVO);
        return PageMsg.success().setResult(userLoginVO);
    }

    //用户登录接口，需要传入用户名和密码
    @RequestMapping(value = "/userLogin", method = RequestMethod.POST)
    public PageMsg userLogin(@RequestParam(value = "userName") String userName, @RequestParam(value = "password") String password) {
        UserLoginVO userLoginVO = userService.userLogin(userName, password);
        return PageMsg.success().setResult(userLoginVO).setMessage("登录成功");
    }

    //短信注册或登录接口
    @RequestMapping(value = "/user-register-SMS", method = RequestMethod.POST)
    public PageMsg sendTextMessage(@RequestParam(value = "phoneNum") String encryptString) {
        /* 解密
        String str = aesUtils.decrypt(encryptString, KeyRules.INSPIRE.getValue());
        String[] strings = str.split("`");
        long currentDate = System.currentTimeMillis() / 1000;
        long date = Long.parseLong(strings[1]) + 10;
        String phoneNum = strings[0];
        if (date < currentDate) {
            throw new ApplicationException(500, "请求超时");
        }
        */
        //临时
        String phoneNum = encryptString;
                HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        PhoneVerificationBean pb1 = (PhoneVerificationBean) request.getSession().getAttribute("phoneVerificationBean");
        if (null != pb1 && pb1.getTimeOut() - System.currentTimeMillis() >= 3 * 60 * 1000 && pb1.getPhoneNum().equals(phoneNum)) {
            return PageMsg.failure().setMessage("发送短信过于频繁");
        }
        String verificationCode = SendTextMessageUnit.SendTextMessage(phoneNum);
        if (verificationCode.equals("-1")) {
            return PageMsg.failure().setMessage("发送短信失败");
        }
        long timeOut = System.currentTimeMillis() + 4 * 60 * 1000;
        PhoneVerificationBean pvb = new PhoneVerificationBean(phoneNum, verificationCode, timeOut);
        String pvbKey = "phoneVerificationBean";
        if (redisUtils.get(pvbKey) == null) {
            redisUtils.set(pvbKey, new LinkedHashMap<String, PhoneVerificationBean>());
        }
        LinkedHashMap<String, PhoneVerificationBean> pvbMap = (LinkedHashMap<String, PhoneVerificationBean>)redisUtils.get(pvbKey);
        pvbMap.put(phoneNum, pvb);
        pvbMap.replace(phoneNum, pvb);
        redisUtils.set(pvbKey, pvbMap);
        return PageMsg.success().setMessage("验证码已经通过短信已经发送至你的手机请注意查收");
    }



    //校验验证码
    @RequestMapping(value = "/user-register-code", method = RequestMethod.POST)
    public PageMsg verificationCode(@RequestParam(value = "verificationCode") String verificationCode, @RequestParam(value = "phoneNum") String encryptString) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        String pvbKey = "phoneVerificationBean";
        LinkedHashMap<String, PhoneVerificationBean> pvbMap = (LinkedHashMap<String, PhoneVerificationBean>)redisUtils.get(pvbKey);
        if (pvbMap == null || pvbMap.get(encryptString) == null) {
            return PageMsg.failure().setMessage("验证码失效或过期");
        }
        PhoneVerificationBean pvb = pvbMap.get(encryptString);
        if (System.currentTimeMillis() < (pvb.getTimeOut()) && verificationCode.equals(pvb.getVerificationCode())) {
            String phoneNum = encryptString;
            if (encryptString.length() > 13) {
                String str = aesUtils.decrypt(encryptString, KeyRules.INSPIRE.getValue());
                String[] strings = str.split("`");
                phoneNum = strings[0];
            }
            User user = new User();
            user.setUserName(phoneNum);
            user.setNickname("用户" + phoneNum);
            user.setPhonenum(phoneNum);
            UserLoginVO userLoginVO = userService.sendTextMessageLoginAndRegister(user);
            request.getSession().setAttribute("phoneVerificationBean", null);
            return PageMsg.success().setResult(userLoginVO).setMessage("登录成功");
        }
        return PageMsg.failure().setMessage("验证码失效或过期");
    }

    //刷新token
    @RequestMapping(value = "/refreshToken", method = RequestMethod.POST)
    public PageMsg refreshToken(@RequestParam(value = "userId") Long userId) {
        String token = userService.refreshToken(userId);
        return PageMsg.success().setResult(token).setMessage("刷新成功");


    }

    //获取个人主页（小城事）
    @RequestMapping("/homePage")
    @ResponseBody
    public JsonResponse getHomePage(@RequestBody(required = true) @Valid MessageRequestVO messageRequestVO) {

        try {
            UserBaseStatisticVO userBaseStatisticVO = this.userService.getUserBaseStatistic(null, messageRequestVO.getUserId());

            PageRequest pageRequest = new PageRequest(messageRequestVO.getPageNum(), messageRequestVO.getPageSize());
            List<Integer> boardTypeList = new ArrayList<Integer>();
            boardTypeList.add(messageRequestVO.getBoardType());
            PageResult<MessageVO> pageResult = this.messageService.getMessageListByCategoryIdAndBoardTypeAndMediaType(messageRequestVO.getCategoryId(), boardTypeList, pageRequest, messageRequestVO.getTitle(), messageRequestVO.getUserId(), messageRequestVO.getMediaType());

            UserHomePageVO homePageVO = new UserHomePageVO();
            homePageVO.setMessages(pageResult.getItems());
            homePageVO.setUser(userBaseStatisticVO);

            return JsonResponse.success().setPage(pageResult.getPageInfo()).setResult(homePageVO);
        } catch (Exception ex) {
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }

    //微信登录接口,加入验证
    @RequestMapping(value = "/weChat/LoginOld", method = RequestMethod.POST)
    public PageMsg weChatLogin(@RequestParam(value = "openid") String openid, String unionid, @RequestParam(value = "accessToken") String accessToken, User user) {
        WeChatLogin weChatLogin = new WeChatLogin();
        if (weChatLogin.weChatLogin(openid, accessToken)) {
            UserLoginVO userLoginVO = userService.userWxLogin(openid, user, unionid, null);
            return PageMsg.success().setResult(userLoginVO).setMessage("登录成功");
        } else {
            return PageMsg.failure().setMessage("身份过期");
        }

    }


    //用户修改信息接口
    @RequestMapping(value = "/user-alter", method = RequestMethod.POST)
    public PageMsg userAlter(UserVO user) {
        userService.userAlter(user);
        return PageMsg.success();
    }

    //用户私信
    @RequestMapping(value = "/inbox", method = RequestMethod.POST)
    public PageMsg pushInbox(@RequestParam("userId") Long userId,
                             @RequestParam("targetUserId") Long targetUserId,
                             @RequestParam("content") String content) {
        ManageMessageRequestVO manageMessageRequestVO = new ManageMessageRequestVO(targetUserId, userId, content);
        manageMessageService.pushInbox(userId, targetUserId, content);
        return PageMsg.success();

    }

    //获取用户信息和邀请码
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public PageMsg userDetails(@RequestParam("userId") Long userId
    ) {
        UserVO userVO = userService.userDetails(userId);
        return PageMsg.success().setResult(userVO);
    }


    //用户关注板块批量操作
    @RequestMapping(value = "/attentionTopic/batch", method = RequestMethod.POST)
    public PageMsg attentionTopic(@RequestParam("userId") Long userId, @RequestParam(value = "topicTypes", required = false) List<Integer> topicTypes
    ) {
        userService.batchSetAttentionTopic(userId, topicTypes);
        return PageMsg.success();
    }

    //用户关注取消关注板块单个操作
    @RequestMapping(value = "/attentionTopic/single", method = RequestMethod.POST)
    public PageMsg singleAttentionTopic(@RequestParam("userId") Long userId, @RequestParam(value = "topicType") int topicType, @RequestParam(value = "flag") int flag
    ) {
        userService.singleSetAttentionTopic(userId, topicType, flag);
        return PageMsg.success();
    }

    //获取用户关注的板块列表
    @RequestMapping(value = "/attentionTopic/get", method = RequestMethod.POST)
    public PageMsg getAttentionTopic(@RequestParam("userId") Long userId
    ) {
        int[] results = userService.getAttentionTopic(userId);
        return PageMsg.success().setResults(results);
    }

    //获取所有板块列表
    @RequestMapping(value = "/topic", method = RequestMethod.GET)
    public PageMsg getAllTopic() {
        List<Map<String, Object>> results = userService.getAllTopic();
        return PageMsg.success().setResults(results);
    }
}

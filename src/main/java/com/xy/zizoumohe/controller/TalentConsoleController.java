package com.xy.zizoumohe.controller;


import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.Talent;
import com.xy.zizoumohe.service.TalentConsoleService;
import com.xy.zizoumohe.service.TalentService;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;
import com.xy.zizoumohe.vo.request.TalentRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/console/talent")
@Controller
public class TalentConsoleController {
    @Autowired
    TalentConsoleService talentConsoleService;

    //达人榜列表页面
    @RequestMapping(value = "/list/page", method = RequestMethod.GET)
    public String listPage() {
        return "talent/talentList";
    }

    //达人申请列表页面
    @RequestMapping(value = "/applyForList/page", method = RequestMethod.GET)
    public String applyForListPage() {
        return "talent/talentApplyForList";
    }

    //添加达人页面
    @RequestMapping(value = "/add/page", method = RequestMethod.GET)
    public String addPage() {
        return "talent/addTalent";
    }

    //修改达人页面
    @RequestMapping(value = "/update/page", method = RequestMethod.GET)
    public String updatePage() {
        return "talent/updateTalent";
    }


    //达人列表
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public PageMsg talentList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "50") Integer pageSize, @RequestParam("gameType") Integer gameType) {
        PageResult pageResult = talentConsoleService.talentList(pageNum, pageSize, gameType);
        return PageMsg.success().setResults(pageResult.getResults()).setPage(pageResult.getPage());
    }
    //达人申请列表
    @ResponseBody
    @RequestMapping(value = "/applyForList", method = RequestMethod.GET)
    public PageMsg talentApplyForList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "50") Integer pageSize, @RequestParam("gameType") Integer gameType) {
        PageResult pageResult = talentConsoleService.talentApplyForList(pageNum, pageSize, gameType);
        return PageMsg.success().setResults(pageResult.getResults()).setPage(pageResult.getPage());
    }
    //添加达人
    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public PageMsg add(TalentRequestVO talentRequestVO) {
        talentConsoleService.addTalent(talentRequestVO);
        return PageMsg.success();
    }

    //修改达人
    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public PageMsg update(TalentRequestVO talentRequestVO) {
        talentConsoleService.updateTalent(talentRequestVO);
        return PageMsg.success().setMessage("修改成功");
    }

    //达人详情
    @ResponseBody
    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public PageMsg details(@RequestParam("id") Integer id) {
        Talent talent =  talentConsoleService.details(id);
        return PageMsg.success().setResult(talent);
    }

    //移除达人
    @ResponseBody
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public PageMsg delete(@PathVariable Integer id) {
        talentConsoleService.delete(id);
        return PageMsg.success().setMessage("删除成功");
    }

}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.service.*;
import com.xy.zizoumohe.utils.JsonDefaultResponse;
import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/api/homePage")
public class HomePageController {
    private static Logger logger = LoggerFactory.getLogger(HomePageController.class);
    @Autowired
    UserService userService;
    @Autowired
    MessageService messageService;
    @Autowired
    CaloryService caloryService;
    @Autowired
    SignService signService;
    @Autowired
    ShareService shareService;

    // 获取头条首页的数据 version(2.2.5)
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public JsonDefaultResponse getHomePageData(@RequestParam(value = "boardType") List<Integer> boardType,
                                               @RequestParam(value = "userId", required = false) Long userId) {
        logger.info("【****】request inot /api/homePage/get...");
        try {
            List<SlidePictureVO> slidePictureVOList = this.messageService.getCurrentUseSlidePictureList();
            PageRequest pageRequest = new PageRequest(1, 10, "create_time", "DESC");
            com.xy.zizoumohe.bean.PageResult pageResult = this.messageService.getRecommendMessageListAndBoardType(boardType, userId, pageRequest);
            Map<String, Object> homePageDataMap = new HashMap<>();
            Map<String, Object> slidePictureMap = new HashMap<>();
            Map<String, Object> recommendMessageMap = new HashMap<>();
            slidePictureMap.put("page", null);
            slidePictureMap.put("items", slidePictureVOList);
            recommendMessageMap.put("page", pageResult.getPage());
            recommendMessageMap.put("items", pageResult.getResults());
            homePageDataMap.put("slidePictureData", slidePictureMap);
            homePageDataMap.put("recommendMessageData", recommendMessageMap);
            return JsonDefaultResponse.success().setResults(homePageDataMap);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return JsonDefaultResponse.failure().setMessage(ex.getMessage());
        }
    }

    // 获取讨论首页数据
    @RequestMapping(value = "/discussion/get", method = RequestMethod.POST)
    public JsonResponse getDiscussionHomePageData() {
        logger.info("【****】request into /api/homePage/discussion/get...");
        try {
            // category为1表示视频，2表示赛事，3表示资讯，4表示讨论
            // 获取热门的讯息
            List<MessageVO> messageVOList = this.messageService.getHotMessageList(4L);
            PageRequest pageRequest = new PageRequest(1, 10, "create_time", "DESC");
            PageResult<MessageVO> pageResult = this.messageService.getRecommendMessageList(4L, pageRequest);
            messageVOList.addAll(pageResult.getItems());
            return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(messageVOList);
        } catch (Exception ex) {
            logger.info(ex.getMessage());
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }

    // 获取我的主页数据
    @RequestMapping(value = "/myHomePage/get", method = RequestMethod.POST)
    public JsonResponse getDiscussionHomePageData(@RequestParam("currentLoginUserId") Long currentLoginUserId) {
        logger.info("【****】request for /api/homePage/myHomePage/get");
        try {
            UserBaseStatisticVO userBaseStatisticVO = this.userService.getUserBaseStatistic(null, currentLoginUserId);
            return JsonResponse.success().setResult(userBaseStatisticVO);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }

    /**
     * 获取他的主页数据
     *
     * @param currentLoginUserId 当前登录用户id
     * @param userId             TA的id
     * @return json response
     */
    @RequestMapping(value = "/hisHomePage/get", method = RequestMethod.POST)
    public JsonResponse getDiscussionHomePageData(@RequestParam("currentLoginUserId") Long currentLoginUserId,
                                                  @RequestParam("userId") Long userId) {
        logger.info("【****】request for /api/homePage/myHomePage/get");
        try {
            UserBaseStatisticVO userBaseStatisticVO = this.userService.getUserBaseStatistic(userId, currentLoginUserId);
            return JsonResponse.success().setResult(userBaseStatisticVO);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }

    // 获取任务主页
    @PostMapping("/getTask")
    public JsonResponse getTaskPage(@RequestParam("userId") Long userId) {
        logger.info("【request for /api/homePage/task】");
        try {
            Date today = new Date();
            Map<String, Object> map = new HashMap<>();
            map.put("caloryNum", this.caloryService.getCaloryNum(userId));
            map.put("inviteRegistry", new LongTaskVO("邀请注册", 21, "邀请成功+150卡路里，+5张幸运券"));
            map.put("acquireSupport", new LongTaskVO("获得赞同", 22, "+2卡路里，每日上限80"));

            map.put("openEgg", new LongTaskVO("模拟开蛋", 24, "随机数量卡路里"));

            List<CaloryTradeVO> caloryTradeVOList = this.caloryService.getCaloryTrade(userId, today);
            Map<Integer, Integer> stateMap = new HashMap<>();
            stateMap.put(1, 0); // 签到
            stateMap.put(7, 0); // 发布阵容
            stateMap.put(3, 0); // 发布帖子
            stateMap.put(8, 0); // 分享阵容
            stateMap.put(11, 0); // 分享文章
            stateMap.put(10, 0); // 评论阵容
            stateMap.put(6, 0); // 评论帖子
            stateMap.put(12, 0); // 评论文章
            stateMap.put(9, 0); // 点赞阵容
            stateMap.put(5, 0); // 点赞帖子
            stateMap.put(14, 0); // 点赞文章
            stateMap.put(51, 0); //观看激励视频
            for (CaloryTradeVO caloryTradeVO : caloryTradeVOList) {
                Integer key = caloryTradeVO.getTradeType();
                if(stateMap.get(key)!=null){
                    stateMap.put(key, stateMap.get(key)+1);
                }
            }
            int signTaskComplete = 0;
            int publishSquadTaskComplete = 0;
            int publishDynamicTaskComplete = 0;
            int shareSquadTaskComplete = 0;
            int shareMessageTaskComplete = 0;
            int commentSquadTaskComplete = 0;
            int commentDynamicTaskComplete = 0;
            int commentMessageTaskComplete = 0;
            int supportSquadTaskComplete = 0;
            int supportDynamicTaskComplete = 0;
            int supportMessageTaskComplete = 0;

            if (stateMap.get(1) > 0) { // 签到任务
                signTaskComplete = 1;
            }
            if (stateMap.get(7) > 0) { // 发布阵容任务
                publishSquadTaskComplete = 1;
            }
            if (stateMap.get(3) > 0) { // 发布帖子任务
                publishDynamicTaskComplete = 1;
            }
            if (stateMap.get(8) > 0) { // 分享阵容任务
                shareSquadTaskComplete = 1;
            }
            if (stateMap.get(11) > 0) { // 分享文章任务
                shareMessageTaskComplete = 1;
            }
            if (stateMap.get(10) > 0) { // 评论阵容任务
                commentSquadTaskComplete = 1;
            }
            if (stateMap.get(6) > 0) { // 评论帖子任务
                commentDynamicTaskComplete = 1;
            }
            if (stateMap.get(12) > 0) { // 评论文章任务
                commentMessageTaskComplete = 1;
            }
            if (stateMap.get(9) > 0) { // 点赞阵容任务
                supportSquadTaskComplete = 1;
            }
            if (stateMap.get(5) > 0) { // 点赞帖子任务
                supportDynamicTaskComplete = 1;
            }
            if (stateMap.get(14) > 0) { // 点赞文章任务
                supportMessageTaskComplete = 1;
            }
            Integer timesOfWatchADVideo = stateMap.get(51);
            map.put("watchADVideo", new LongTaskVO("观看视频", 23, "+50卡路里，每日上限150卡路里", 3 - timesOfWatchADVideo));
            List<DailyTaskVO> dailyTaskVOList = new ArrayList<>();
            dailyTaskVOList.add(new DailyTaskVO("签到", 1, signTaskComplete, "+5卡路里，连续签到+6+7+8...+30，中断则从重新计算"));

            dailyTaskVOList.add(new DailyTaskVO("发布阵容", 2, publishSquadTaskComplete, "+5卡路里"));
            dailyTaskVOList.add(new DailyTaskVO("发布帖子", 3, publishDynamicTaskComplete, "+10卡路里"));

            dailyTaskVOList.add(new DailyTaskVO("分享阵容", 4, shareSquadTaskComplete, "+15卡路里"));
            dailyTaskVOList.add(new DailyTaskVO("分享文章", 5, shareMessageTaskComplete, "+15卡路里"));

            dailyTaskVOList.add(new DailyTaskVO("评论阵容", 6, commentSquadTaskComplete, "+3卡路里"));
            dailyTaskVOList.add(new DailyTaskVO("评论帖子", 7, commentDynamicTaskComplete, "+3卡路里"));
            dailyTaskVOList.add(new DailyTaskVO("评论文章", 8, commentMessageTaskComplete, "+3卡路里"));

            dailyTaskVOList.add(new DailyTaskVO("点赞阵容", 9, supportSquadTaskComplete, "+3卡路里"));
            dailyTaskVOList.add(new DailyTaskVO("点赞帖子", 10, supportDynamicTaskComplete, "+3卡路里"));
            dailyTaskVOList.add(new DailyTaskVO("点赞文章", 11, supportMessageTaskComplete, "+3卡路里"));

            return JsonResponse.success().setResult(map).setResults(dailyTaskVOList);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error(ex.getMessage());
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }


}

package com.xy.zizoumohe.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListeningServerController {
    @RequestMapping("/listen")
    public Integer listen(){
        return 1;
    }
}

package com.xy.zizoumohe.controller;


import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.Talent;
import com.xy.zizoumohe.service.TalentService;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/api/talent")
@RestController
public class TalentController {
    @Autowired
    TalentService talentService;
    //达人列表
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public PageMsg talentList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, @RequestParam("gameType") Integer gameType, Long userId) {
        PageResult pageResult = talentService.talentList(pageNum, pageSize, gameType,userId);
        return PageMsg.success().setResults(pageResult.getResults()).setPage(pageResult.getPage());
    }

    //申请达人
    @RequestMapping(value = "/applyFor",method = RequestMethod.POST)
    public PageMsg talentApplyFor(@Valid TalentApplyForRequestVO talentApplyForRequestVO) {
        talentService.talentApplyFor(talentApplyForRequestVO);
        return PageMsg.success().setMessage("申请已提交");
    }
    //卡路里排行榜
    @RequestMapping(value = "/calory/list",method = RequestMethod.GET)
    public PageMsg caloryTalentList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                              @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, Long userId) {
        PageResult pageResult = talentService.caloryTalentList(pageNum, pageSize,userId);
        return PageMsg.success().setResults(pageResult.getResults()).setPage(pageResult.getPage());
    }

    //粉丝数排行
    @RequestMapping(value = "/fans/list",method = RequestMethod.GET)
    public PageMsg fansTalentList(@RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize, Long userId) {
        PageResult pageResult = talentService.fansTalentList(pageNum, pageSize,userId);
        return PageMsg.success().setResults(pageResult.getResults()).setPage(pageResult.getPage());
    }


}

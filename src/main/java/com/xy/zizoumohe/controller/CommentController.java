package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.CommentConsoleBean;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.CommentConsoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/admin/comment")
public class CommentController {

    private static Logger logger = LoggerFactory.getLogger(CommentController.class);
    @Autowired
    CommentConsoleService commentConsoleService;

    @RequestMapping(value = "/message/update", method = RequestMethod.POST)
    public PageMsg updateMessageComment(@RequestBody @Valid CommentConsoleBean commentConsoleBean) {
        commentConsoleService.updateMessageComment(commentConsoleBean);
        return PageMsg.success();
    }

}

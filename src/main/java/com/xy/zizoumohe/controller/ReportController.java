package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.ReportService;
import com.xy.zizoumohe.vo.request.ReportRequestVO;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/api/report")
@RestController
public class ReportController {
    @Autowired
    ReportService reportService;

    //举报用户
    @RequestMapping(value = "/applyFor", method = RequestMethod.POST)
    public PageMsg reportUser(@Valid ReportRequestVO reportRequestVO) {
        reportService.reportUser(reportRequestVO);
        return PageMsg.success().setMessage("举报已提交");
    }

}

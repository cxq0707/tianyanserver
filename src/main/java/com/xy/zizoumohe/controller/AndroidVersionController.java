package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.AndroidVersionService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.AndroidVersionVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/android")
public class AndroidVersionController {

    private static Logger logger = LoggerFactory.getLogger(AndroidVersionController.class);
    @Autowired
    private AndroidVersionService androidVersionService;

    @RequestMapping(value = "/checkNewVersion",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse checkNewVersion(){
        logger.info("【*】request into /api/android/checkNewVersion");
        try {
            AndroidVersionVO androidVersionVO = this.androidVersionService.getLatestVersion();
            return JsonResponse.success().setResult(androidVersionVO).setResults(androidVersionVO.getVersionTypeVOList());
        }catch (ApplicationException ex){
            logger.error(ex.getMessage());
            return JsonResponse.failure().setCode(ex.code).setResult(ex.getMessage());
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return JsonResponse.failure().setCode(2064).setResult("检查新版本异常");
        }
    }




}

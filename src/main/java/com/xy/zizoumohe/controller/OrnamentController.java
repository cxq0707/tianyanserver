package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.Page;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.bean.PagingInfo;
import com.xy.zizoumohe.entity.Ornament;
import com.xy.zizoumohe.service.OrnamentService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.OrnamentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/ornament")
public class OrnamentController {
    @Autowired
    OrnamentService ornamentService;

    //获取饰品首页信息
    @RequestMapping(value = "/homePage", method = RequestMethod.GET)
    public PageMsg homePage(@RequestParam(value = "userId") Long userId,
                            @RequestParam(value = "ornamentType", required = false, defaultValue = "1") int ornamentType, PagingInfo pagingInfo) {
        Map<String, Object> map = ornamentService.homePage(userId, ornamentType, pagingInfo);
        return PageMsg.success().setResults(map.get("results")).setResult(map.get("result")).setPage((Page) map.get("page"));

    }

    //兑换饰品
    @RequestMapping(value = "/convert", method = RequestMethod.POST)
    public PageMsg convert(@RequestParam(value = "userId") Long userId,
                           @RequestParam(value = "ornamentId") int ornamentId) {
        try {
            ornamentService.convert(userId, ornamentId);
            return PageMsg.success().setMessage("兑换成功");
        } catch (ApplicationException ex) {
            return PageMsg.failure().setMessage(ex.getMessage()).setCode(ex.code);
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setMessage("服务器异常！");
        }
    }

    //佩戴饰品
    @RequestMapping(value = "/adorn", method = RequestMethod.POST)
    public PageMsg adorn(@RequestParam(value = "userId") Long userId,
                         @RequestParam(value = "ornamentId") int ornamentId) {
        try {
            ornamentService.adorn(userId, ornamentId);
            return PageMsg.success().setMessage("佩戴成功");
        } catch (ApplicationException ex) {
            return PageMsg.failure().setMessage(ex.getMessage()).setCode(ex.code);
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setMessage("服务器异常！");
        }
    }

    @RequestMapping(value = "/getData", method = RequestMethod.GET)
    public PageMsg getData(Integer id) {
        OrnamentVO ornamentVO = ornamentService.getByKey(id);
        return PageMsg.success().setResult(ornamentVO);
    }

}

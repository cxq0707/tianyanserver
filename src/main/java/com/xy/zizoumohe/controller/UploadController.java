package com.xy.zizoumohe.controller;


import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/api/upload")
@RestController
public class UploadController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UploadController.class);


    @RequestMapping("/{flag}")
    public Map upload(@PathVariable String flag,@RequestParam("file") MultipartFile file,Long userId) {
        Map<String,Object> map = new HashMap<>();
        if (file.isEmpty()) {
            map.put("state",0);
            map.put("message","上传文件不能为空");
            return map;
        }

        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHMMSS");
        String date = df.format(new Date());
        fileName = userId.toString()+date+suffix;
        String filePath = flag+"/";
        String url = filePath+fileName;
        File dest = new File(filePath + fileName);
        try {
            file.transferTo(dest);
            map.put("state",1);
            map.put("url",url);
            map.put("message","上传成功");
            return map;
        } catch (IOException e) {
            LOGGER.error(e.toString(), e);
            map.put("state",-1);
            map.put("message","上传出现异常");
            return map;
        }

    }


}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.entity.Admin;
import com.xy.zizoumohe.service.AdminService;
import com.xy.zizoumohe.utils.AesUtils;
import com.xy.zizoumohe.utils.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/admin")
public class AdminController {



    @Autowired
    AdminService adminService;
    @Autowired
    AesUtils aesUtils;
    @RequestMapping(value = "/login-page", method = RequestMethod.GET)
    public String login(Model model){
        model.addAttribute("key", aesUtils.getKey());
        return "login";
    }
    @ResponseBody
    @RequestMapping(value = "/getKey", method = RequestMethod.GET)
    public PageMsg getKey(Model model){
        Map map = new HashMap();
        map.put("key",aesUtils.getKey());
        return PageMsg.success().setResult(map);
    }


    @RequestMapping(value = "/register-page", method = RequestMethod.GET)
    public String register(Model model){
        model.addAttribute("key", aesUtils.getKey());
        return "register";
    }

    //管理员登录接口，需要传入用户名和密码
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public PageMsg adminLogin(@RequestParam(value = "name") String name, @RequestParam(value = "password") String password,@RequestParam(value = "key") String key) {
        try {

            Admin admin = adminService.Login(name, password,key);
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            request.getSession().setAttribute("admin",admin);
  /*          Cookie adminName = new Cookie("name", URLEncoder.encode(name, "utf-8"));
            Cookie adminPwd = new Cookie("password", URLEncoder.encode(password, "utf-8"));
            adminName.setMaxAge(30*24*60*60);
            adminPwd.setMaxAge(30*24*60*60);
            adminName.setPath(request.getContextPath()+"/");
            adminPwd.setPath(request.getContextPath()+"/");
            res.addCookie(adminName);
            res.addCookie(adminPwd);*/
            //三十分钟过期时间
            request.getSession().setAttribute("outTime",new Date().getTime()+1000*60*30);
            return PageMsg.success().setResult(admin).setMessage("登录成功");
        } catch (ApplicationException ex) {
            return PageMsg.failure().setMessage(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setMessage("服务器异常！");
        }

    }

    //管理员注册接口
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public PageMsg adminRegister(@RequestParam(value = "name") String name, @RequestParam(value = "password") String password,String key) {
        try {
/*
            return PageMsg.success().setMessage("没有权限的操作");
*/
            Admin admin = adminService.register(name, password,key);
            return PageMsg.success().setResult(admin).setMessage("注册成功");
        } catch (ApplicationException ex) {
            return PageMsg.failure().setMessage(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setMessage("服务器异常！");
        }

    }
    //注销登录
    @RequestMapping(value = "/outLogin", method = RequestMethod.GET)
    public String outLogin(Model model){
        model.addAttribute("key", aesUtils.getKey());
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
          request.getSession().setAttribute("admin",null);
        return "login";
    }
}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.LuckyTicketService;
import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.ActivityWishService;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.ActivityConsoleWishRequestVO;
import com.xy.zizoumohe.vo.request.ActivityWishCommentRequestVO;
import com.xy.zizoumohe.vo.request.ActivityWishRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/wish")
public class ActivityWishController {

    private static Logger logger = LoggerFactory.getLogger(ActivityWishController.class);
    @Autowired
    ActivityWishService activityWishService;
    @Autowired
    CaloryService caloryService;
    @Autowired
    LuckyTicketService luckyTicketService;

    // 参与许愿活动
    @RequestMapping(value = "/makeWish", method = RequestMethod.POST)
    public JsonResponse makeWish(@RequestParam("userId") Long userId,
                                 @RequestParam("activityWishId") Long activityWishId,
                                 @RequestParam(value = "type",defaultValue = "0",required = false) Integer type) {
        logger.info("【*】request into /api/wish/makeWish");
        activityWishService.makeWish(userId, activityWishId, type, new Date());
        return JsonResponse.success();
    }

    // 获取我的礼物
    @RequestMapping("/activity/gift/getGiftList")
    public JsonResponse getAwardGiftList(@RequestParam("userId") Long userId,
                         @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                         @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        logger.info("【*】request into /api/wish/activity/gift/getGiftList");
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult pageResult = activityWishService.getAcquireAwardGiftList(userId, pageRequest);
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());
    }

    // 上传图片分享
    @RequestMapping(value = "/uploadShare", method = RequestMethod.POST)
    public JsonResponse uploadPicShare(@RequestParam("userId") Long userId,
                                       @RequestParam("activityWishId") Long activityWishId,
                                       @RequestParam("uploadPicUrl") String uploadPicUrl) {
        logger.info("【*】request into /api/wish/uploadShare");
        activityWishService.uploadWishSharePic(userId, activityWishId, uploadPicUrl);
        return JsonResponse.success();
    }

    // 获取许愿上传分享
    @RequestMapping(value = "/getWishUploadShare", method = RequestMethod.POST)
    public JsonResponse getWishUploadShare(@RequestParam("userId") Long userId,
                                           @RequestParam("activityWishId") Long activityWishId) {
        logger.info("【*】request into /api/wish/getWishUploadShare");
        WishUploadShareVO wishUploadShareVO = activityWishService.getWishUploadShare(activityWishId, userId);
        return JsonResponse.success().setResult(wishUploadShareVO);
    }

    // 获取礼物详情
    @PostMapping("/getGiftDetail")
    public JsonResponse getGiftDetail(@RequestParam(value = "userId",required = false) Long userId,
                                      @RequestParam("activityWishId") Long activityWishId) {
        logger.info("【request for /api/wish/getGiftDetail】");
        GiftDetailVO giftDetailVO = activityWishService.getGiftDetail(userId, activityWishId);
        return JsonResponse.success().setResult(giftDetailVO);
    }


    // 获取许愿池主页
    @RequestMapping("/activity/homePage")
    public JsonResponse getHomePage(@RequestParam(value = "userId", required = false) Long userId,
                                    @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        logger.info("【*】request into /api/wish/uploadShare");
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult pageResult = activityWishService.getActivityWishList(userId, pageRequest);
        Long caloryNum = 0L;
        Integer fortuneTickets = 0;
        if (userId != null) {
            caloryNum = caloryService.getCaloryNum(userId);
            fortuneTickets = luckyTicketService.getLuckyTicketNum(userId);
        }
        Map<String, Object> caloryNumMap = new HashMap<>();
        caloryNumMap.put("caloryNum", caloryNum);
        caloryNumMap.put("fortuneTickets",fortuneTickets);
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResult(caloryNumMap).setResults(pageResult.getItems());
    }

    // 查看许愿活动的开奖结果
    @RequestMapping(value = "/activity/getWishActivityResult", method = RequestMethod.POST)
    public JsonResponse getActivityWishResult(@RequestParam(value = "userId", required = false) Long userId,
                                              @RequestParam("activityWishId") Long activityWishId) {
        logger.info("【*】request into /api/wish/uploadShare");
        // 获取许愿活动中奖用户的列表
        List<UserVO> userVOList = activityWishService.getActivityWishResult(activityWishId);
        // 检查当前用户是否获奖
        WishAwardSimpleVO wishAwardSimpleVO = activityWishService.checkWishAward(userId, activityWishId);
        return JsonResponse.success().setResult(wishAwardSimpleVO).setResults(userVOList);
    }

    // 强制进行开奖
    @RequestMapping(value = "/forceOpenAward", method = RequestMethod.POST)
    public JsonResponse forceOpenAward(@RequestParam("activityWishId") Long activityWishId) {
        logger.info("【*】request into /api/wish/forceOpenAward");
        // 获取许愿活动中奖用户的列表
        activityWishService.foreOpenAward(activityWishId);
        return JsonResponse.success();
    }

    @RequestMapping(value = "/lottery", method = RequestMethod.POST)
    public PageMsg lottery(@RequestParam("id") Long id) {
        ActivityWishVO activityWishVO = activityWishService.getActivityWishById(id);
        return PageMsg.success().setResult(activityWishVO);
    }

    //添加愿望
    @RequestMapping(value = "/addGift", method = RequestMethod.POST)
    public PageMsg addGift(Model model, ActivityConsoleWishRequestVO activityConsoleWishRequestVO, @RequestParam("key") String key) {
        if (!key.equals("a.*f-/io-@axui789")) {
            return PageMsg.failure().setMessage("资源不存在");
        }
        ActivityWishRequestVO activityWishRequestVO = new ActivityWishRequestVO();
        BeanUtils.copyProperties(activityConsoleWishRequestVO, activityWishRequestVO);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH时m分s秒");
        try {
            Date startDate = sdf.parse(activityConsoleWishRequestVO.getStartTime());
            Date openDate = sdf.parse(activityConsoleWishRequestVO.getOpenTime());
            long endDateLong = openDate.getTime() + (1000 * 60 * 60 * 24 * 3);
            Date endDate = new Date(endDateLong);
            activityWishRequestVO.setStartTime(startDate);
            activityWishRequestVO.setEndTime(endDate);
            activityWishRequestVO.setOpenTime(openDate);
            activityWishService.publishActivityWish(activityWishRequestVO);
        } catch (ParseException e) {
            return PageMsg.failure().setCode(500).setMessage("时间格式不正确");
        }
        return PageMsg.success();
    }


    //观看视频增加中奖率
    @RequestMapping(value = "/inspire", method = RequestMethod.POST)
    public PageMsg inspire(@RequestParam("param") String param, @RequestParam("userId") Long userId, @RequestParam("wishId") Long wishId) {
        activityWishService.addInspire(param, userId, wishId);
        return PageMsg.success();
    }

    /**
     * 许愿活动参与消费幸运券
     * @param userId 用户的id
     * @param activityWishId 许愿活动的id
     * @return
     */
    @PostMapping("/fortune/useFortuneTicket")
    public JsonResponse useFortuneTicketToGoodLuck(@RequestParam("userId") Long userId,
                                                   @RequestParam("activityWishId") Long activityWishId){
        logger.info("【request for /api/wish/fortune/useFortuneTicket】");
        luckyTicketService.useLuckyTicketForActivityWish(userId, activityWishId);
        return JsonResponse.success();
    }

    // 发布许愿活动评论
    @PostMapping("/activityWishComment/create")
    public JsonResponse publishActivityWishComment(@RequestBody ActivityWishCommentRequestVO activityWishCommentRequestVO){
       activityWishService.createActivityWishComment(activityWishCommentRequestVO);
       return JsonResponse.success();
    }

    //许愿活动评论点赞
    @PostMapping("/activityWishComment/support/create")
    public JsonResponse ActivityWishCommentSupport(@RequestParam("userId") Long userId,@RequestParam("wishCommentId") Long wishCommentId){
        activityWishService.createActivityWishCommentSupport(userId,wishCommentId);
        return JsonResponse.success();
    }

    /**
     * 获取许愿活动评论列表
     * @param userId 用户的id
     * @param activityWishId 许愿活动的id
     * @param pageNum 页码数字
     * @param pageSize 页码大小
     */
    @PostMapping("/activityWishComment/getList")
    public JsonResponse getActivityWishCommentList(@RequestParam(value = "userId",required = false) Long userId,
                                              @RequestParam("activityWishId") Long activityWishId,
                                              @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                              @RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize){
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult pageResult = activityWishService.getActivityWishCommentList(activityWishId, userId, pageRequest);
        Map<String, Integer> map = new HashMap<>();
        map.put("comments",pageResult.getItems().size());
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResult(map).setResults(pageResult.getItems());
    }


}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.MessageCommentService;
import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.request.MessageCommentRequestVO;
import com.xy.zizoumohe.vo.MessageCommentVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/messageComment")
public class MessageCommentController {
    private static Logger logger = LoggerFactory.getLogger(MessageCommentController.class);
    @Autowired
    MessageCommentService messageCommentService;
    @Autowired
    MessageService messageService;

    // 获取讯息评论列表(分页)
    @PostMapping("/list")
    public JsonResponse getMessageComment(@RequestParam("messageId") Long messageId,
                                   @RequestParam(value = "currentLoginUserId",required = false) Long userId,
                                   @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                   @RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize,
                                   @RequestParam(value = "sort",required = false,defaultValue = "1") int sort){
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult<MessageCommentVO> pageResult = messageCommentService.getMessageComment(messageId, userId, pageRequest, sort);
        return JsonResponse.success().setResults(pageResult.getItems()).setPage(pageResult.getPageInfo());
    }

    // 获取讯息评论列表(不分页) -- 在讨论弹幕评论中用到
    @PostMapping("/listAll")
    public JsonResponse getMessageCommentList(@RequestParam("messageId") Long messageId,
                           @RequestParam(value = "currentLoginUserId",required = false) Long userId){
        List<MessageCommentVO> messageCommentVOList = messageCommentService.getMessageComment(messageId);
        return JsonResponse.success().setResults(messageCommentVOList);
    }

    /**
     * 用户发布评论或回复评论;toMessageCommentId为NULL表示发布评论；否则就是回复评论
     * @param messageCommentRequestVO messageCommentRequestVO
     */
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public JsonResponse createMessageComment(@RequestBody @Valid MessageCommentRequestVO messageCommentRequestVO){
        messageCommentService.createMessageComment(messageCommentRequestVO);
        return JsonResponse.success();
    }

    // 讯息评论点赞
    @PostMapping("/support/create")
    public JsonResponse addMessageCommentSupport(@RequestParam("messageCommentId") Long messageCommentId,
                                                  @RequestParam("currentLoginUserId") Long userId){
        messageService.addMessageCommentSupport(messageCommentId, userId);
        return JsonResponse.success();
    }

}

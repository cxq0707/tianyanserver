package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.service.UserConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/console/user")
@Controller
public class UserConsoleController {
    @Autowired
    UserConsoleService userConsoleService;

    //跳转到用户列表
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String userLogin(@RequestParam(defaultValue = "", required = false) String nickname,
                            @RequestParam(defaultValue = "0", required = false) Long userId,
                            @RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                            @RequestParam(value = "pageSize", required = false, defaultValue = "15") int pageSize,
                            Model model
    ) {
        PageResult pageResult = userConsoleService.userList(pageNum, pageSize, nickname, userId);
        model.addAttribute(pageResult);
        model.addAttribute("nickname", nickname);
        return "user/list";
    }

}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.CaloryTradeVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/calory")
public class CaloryController {
    @Autowired
    CaloryService caloryService;

    // 获取用户卡路里余额 (暂时不用)
    @PostMapping("/get")
    public JsonResponse getCaloryNumber(@RequestParam("currentLoginUserId") Long userId) {
        log.info("【****】request for /api/calory/get...");
        Long caloryNum = caloryService.getCaloryNum(userId);
        Map<String, Long> map = new HashMap<>();
        map.put("caloryNum", caloryNum);
        return JsonResponse.success().setResult(map);
    }

    // 获取用户卡路里交易明细
    @PostMapping("/caloryTrade/list")
    public JsonResponse getCaloryTradeList(@RequestParam("currentLoginUserId") Long userId,
                         @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                         @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        log.info("【****】request for /api/calory/caloryTrade/list...");
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult<CaloryTradeVO> pageResult = caloryService.getCaloryTradeList(userId, pageRequest);
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());
    }

    @PostMapping("/getCaloryPlusDuringPeriod")
    public JsonResponse getCaloryPlusDuringPeriod(@RequestParam("currentLoginUserId") Long currentLoginUserId) {
        // todo
        return JsonResponse.success().setResult(null);
    }

    //观看视频获得卡路里
    @PostMapping("/inspire")
    public PageMsg inspire(@RequestParam("param") String param,@RequestParam("userId") Long userId) {
        int num = caloryService.inspireAddCalory(param,userId);
        Map<String, Integer> map = new HashMap<>();
        map.put("num",num);
        return PageMsg.success().setResult(map);
    }


}

package com.xy.zizoumohe.controller;

import com.tencent.cloud.CosStsClient;
import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.IpHelper;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.MessageVO;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@RequestMapping("/console")
@Controller
public class ConsoleController {
    private static Logger logger = LoggerFactory.getLogger(ConsoleController.class);
    @Autowired
    IpHelper ipHelper;
    @Autowired
    private MessageService messageService;

    //跳转主页面
    @RequestMapping("/console-main")
    public String skipAddPage(HttpServletRequest req,Model model) {

        model.addAttribute("ip",ipHelper.getHostIp());
        logger.info("跳转主页面...");
        return "main";
    }


}

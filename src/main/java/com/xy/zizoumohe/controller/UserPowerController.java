package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.UserResourceService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.request.PowerRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/power")
@RestController
public class UserPowerController {
    @Autowired
    UserResourceService userResourceService;

    @RequestMapping(value = "/remove-user-power", method = RequestMethod.POST)
    public PageMsg removeUserPower(@RequestBody PowerRequestVO powerRequestVO) {
        userResourceService.removeUserPower(powerRequestVO);
        return PageMsg.success().setMessage("设置成功");

    }

    @RequestMapping(value = "/add-user-power", method = RequestMethod.POST)
    public PageMsg addUserPower(@RequestBody PowerRequestVO powerRequestVO) {
        userResourceService.addUserPower(powerRequestVO);
        return PageMsg.success().setMessage("设置成功");
    }
}

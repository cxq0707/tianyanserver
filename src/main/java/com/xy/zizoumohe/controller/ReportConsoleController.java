package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.service.ReportConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/console/report")
@Controller
public class ReportConsoleController {
    @Autowired
    ReportConsoleService reportConsoleService;

    //列表页
    @RequestMapping(value = "/list/page", method = RequestMethod.GET)
    public String listPage() {
        return "other/reportList";
    }


    //举报信息列表
    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public PageMsg list(@RequestParam(defaultValue = "1") int pageNum, @RequestParam(defaultValue = "50") int pageSize) {
        PageResult pageResult = reportConsoleService.list(pageNum,pageSize);
        return PageMsg.success().setResults(pageResult.getResults()).setPage(pageResult.getPage());
    }

}

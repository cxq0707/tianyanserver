package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.ActivityWish;
import com.xy.zizoumohe.entity.ActivityWishExchangeInformation;
import com.xy.zizoumohe.service.ActivityWishService;
import com.xy.zizoumohe.vo.UserUploadShareVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/console/wish")
public class ActivityWishConsoleController {

    @Autowired
    private ActivityWishService activityWishService;


    @RequestMapping(value = "/addGift", method = RequestMethod.GET)
    public String addGift(Model model) {
        return "wishingWell/addGift";
    }

    //礼物列表
    @RequestMapping(value = "/giftList", method = RequestMethod.GET)
    public String list(Model model,
                       @RequestParam(value = "pageNum", required = false, defaultValue = "1") int pageNum,
                       @RequestParam(value = "pageSize", required = false, defaultValue = "15") int pageSize) {
        PageResult pageResult = activityWishService.getActivityWishList(pageNum, pageSize);
        model.addAttribute("pageResult", pageResult);
        return "wishingWell/giftList";
    }


    @RequestMapping(value = "/setPrize", method = RequestMethod.GET)
    public String setPrize(Model model, @RequestParam(value = "id") Long id) {
        List<ActivityWishExchangeInformation> awei = activityWishService.getWishInformation(id);
        model.addAttribute("awei", awei);
        model.addAttribute("giftId", id);
        return "wishingWell/setPrize";
    }

    @ResponseBody
    @RequestMapping(value = "/setPrize", method = RequestMethod.POST)
    public PageMsg setPrize(ActivityWishExchangeInformation awei) {
        activityWishService.updateWishInformation(awei);
        return PageMsg.success();
    }

    @RequestMapping(value = "/winningList", method = RequestMethod.GET)
    public String winningList(@RequestParam("id") Long id, Model model) {
        List<UserUploadShareVO> list = activityWishService.getActivityWishOpenResult(id);
        model.addAttribute("list", list);
        return "wishingWell/winningList";
    }

    //设置轮播图是否使用
    @ResponseBody

    @RequestMapping(value = "/setShow", method = RequestMethod.POST)
    public PageMsg setIsCurrentUse(@RequestParam("isShow") boolean isCurrentUse, @RequestParam("id") Long id) {
        activityWishService.setShow(isCurrentUse ? 1 : 0, id);
        return PageMsg.success();
    }
}

package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.MessageDetailVO;
import com.xy.zizoumohe.vo.MessageSupportVO;
import com.xy.zizoumohe.vo.MessageVO;
import com.xy.zizoumohe.vo.request.MessageRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api/message")
public class MessageController {
    private static Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    MessageService messageService;

    @Autowired
    UserService userService;

    // 按照讯息类别获取讯息列表
    // boardType版块类型
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse getMessageListByCategory(@RequestBody(required = false) MessageRequestVO messageRequestVO) {
        MessageRequestVO rVO = messageRequestVO != null ? messageRequestVO : new MessageRequestVO();
        Long userId = userService.getCurrentUserId();
        rVO.setUserId(userId);
        PageRequest pageRequest = new PageRequest(rVO.getPageNum(), rVO.getPageSize());
        List<Integer> boardTypeList = new ArrayList<Integer>();
        boardTypeList.add(rVO.getBoardType());
        PageResult<MessageVO> pageResult = this.messageService.getMessageListByCategoryIdAndBoardTypeAndMediaType(rVO.getCategoryId(), boardTypeList, pageRequest, rVO.getTitle(), rVO.getUserId(), rVO.getMediaType());
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());

    }

    //发布小视频资讯
    @ResponseBody
    @PostMapping("/add")
    public PageMsg addMessage(@RequestBody MessageRequestVO messageRequestVO) {
        if (null == messageRequestVO.getIsHot()) {
            messageRequestVO.setIsHot(false);
        }
        messageRequestVO.setApproveState(1);
        messageService.addOneMessage(messageRequestVO);
        return PageMsg.success();
    }

    // 获取资讯详情
    @PostMapping("/detail")
    @ResponseBody
    public JsonResponse getMessageDetail(@RequestParam Long messageId) {
        Long userId = userService.getCurrentUserId();
        try {
            MessageDetailVO messageDetailVO = this.messageService.getMessageDetail(messageId, userId != null ? userId : 2l);
            return JsonResponse.success().setResult(messageDetailVO);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }

    // 对讯息进行点赞
    @RequestMapping(value = "/support/create", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse addMessageSupport(@RequestParam("messageId") Long messageId) {
        Long userId = userService.getCurrentUserId();
        if (userId == null) {
            throw new ApplicationException("请登录");
        }
        try {
            MessageSupportVO messageSupportVO = new MessageSupportVO();
            messageSupportVO.setMessageId(messageId);
            messageSupportVO.setUserId(userId);
            this.messageService.addMessageSupport(messageSupportVO);
            return JsonResponse.success();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return JsonResponse.failure().setMessage(ex.getMessage());
        }
    }

    @RequestMapping("/share-page/{messageId}")
    public String sharePage(@PathVariable("messageId") Long messageId, Model model) {
        try {
            MessageDetailVO messageDetailVO = messageService.getMessageDetail(messageId, 2L);
            Integer mediaType = messageService.getMediaType(messageId);
            model.addAttribute("message", messageDetailVO);
            model.addAttribute("mediaType", mediaType);
            return "message/messageShare";
        } catch (ApplicationException ex) {
            return "error";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    // 获取发布的文章
    @RequestMapping("/getPublishList")
    @ResponseBody
    public JsonResponse getPublishList(@RequestParam(value = "currentLoginUserId", required = false) Long currentLoginUserId,
                                       @RequestParam("userId") Long userId,
                                       @RequestParam(value = "pageNum", required = false, defaultValue = "1") Integer pageNum,
                                       @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        logger.info("【request for /api/message/getPublishList】");
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult<MessageVO> pageResult = messageService.getPublishMessageList(currentLoginUserId, userId, pageRequest);
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());

    }


}

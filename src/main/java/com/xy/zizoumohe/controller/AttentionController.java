package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.AttentionService;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/attention")
public class AttentionController {

    private static final Logger logger = LoggerFactory.getLogger(AttentionController.class);
    @Autowired
    AttentionService attentionService;

    /**
     * 添加对用户关注
     * @param currentLoginUserId 当前登录用户id
     * @param toUserId 被关注的用户id
     */
    @PostMapping("/add")
    public JsonResponse addAttention(@RequestParam("currentLoginUserId") Long currentLoginUserId,
                                     @RequestParam("toUserId") Long toUserId){
        Integer attentionState = attentionService.addAttention(currentLoginUserId,toUserId);
        Map<String,Integer> map = new HashMap<>();
        map.put("attentionState",attentionState);
        return JsonResponse.success().setResult(map);
    }

    // 取消关注
    @PostMapping("/cancel")
    public JsonResponse cancelAttention(@RequestParam("currentLoginUserId") Long currentLoginUserId,
                                        @RequestParam("toUserId") Long toUserId){
        Integer attentionState = attentionService.cancelAttention(currentLoginUserId,toUserId);
        Map<String,Integer> map = new HashMap<>();
        map.put("attentionState",attentionState);
        return JsonResponse.success().setResult(map);
    }

    // 获取当前登录用户的关注列表
    @RequestMapping("/list")
    public JsonResponse getAttentionList(@RequestParam("currentLoginUserId") Long currentLoginUserId,
                                     @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                     @RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize){
        PageRequest pageRequest = new PageRequest(pageNum, pageSize);
        PageResult pageResult = attentionService.getAttentionList(currentLoginUserId,pageRequest);
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());
    }

    // 获取当前登录用户的粉丝列表
    @RequestMapping("/fans/list")
    public JsonResponse getFansList(@RequestParam("currentLoginUserId") Long currentLoginUserId,
                                    @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize){
        PageRequest pageRequest = new PageRequest(pageNum,pageSize);
        PageResult pageResult = attentionService.getFansList(currentLoginUserId,pageRequest);
        return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());
    }


}

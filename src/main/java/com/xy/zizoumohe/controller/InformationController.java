package com.xy.zizoumohe.controller;

import com.tencent.cloud.CosStsClient;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.service.MessageCommentService;
import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.service.TopicService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.vo.MessageCategoryVO;
import com.xy.zizoumohe.vo.MessageModelVO;
import com.xy.zizoumohe.vo.request.MessageRequestVO;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RequestMapping("/console/message")
@Controller
public class InformationController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageCommentService messageCommentService;

    @Autowired
    TopicService topicService;

    //跳转到增加资讯页面
    @RequestMapping("/add-message-page")
    public String skipAddPage(Model model) {
        List<MessageCategoryVO> messageCategoryVOList = messageService.getMessageCategoryList();
        model.addAttribute("categorys", messageCategoryVOList);
        model.addAttribute("topicTypes", topicService.getAllTopicConsole());
        return "message/addMessage";
    }

    //跳转到推送消息页面
    @RequestMapping(value = "/push", method = RequestMethod.GET)
    public String pushPage(Model model, @RequestParam("id") Integer id) {
        model.addAttribute("id", id);
        return "message/push";
    }

    //推送消息
    @ResponseBody
    @RequestMapping(value = "/push", method = RequestMethod.POST)
    public PageMsg pushCreate(Model model, @RequestParam("id") Integer id, String title, String subhead, String body) {
        messageService.pushMessage(id, title, subhead, body);
        return PageMsg.success();
    }

    //跳转到资讯列表页面
    @RequestMapping("/message-list-page")
    public String getInformationList(
            @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
            @RequestParam(value = "pageSize", defaultValue = "15", required = false) Integer pageSize,
            @RequestParam(value = "sortColumn", defaultValue = "create_time", required = false) String sortColumn,
            @RequestParam(value = "title", defaultValue = "", required = false) String title,
            @RequestParam(value = "audit", defaultValue = "1", required = false) Integer audit,
            @RequestParam(value = "sortDirection", defaultValue = "desc", required = false) String sortDirection, Model model) {
        PageRequest pageRequest = new PageRequest(pageNum, pageSize, sortColumn, sortDirection);
        PageResult pageResult = this.messageService.messageList(pageRequest, title, audit);
        model.addAttribute("pageResult", pageResult);
        model.addAttribute("title", title);
        model.addAttribute("audit", audit);
        return "message/messageList";
    }

    @RequestMapping("/message-add")
    @ResponseBody
    public PageMsg addMessage(MessageRequestVO messageRequestVO) {
        try {
            if (null == messageRequestVO.getIsHot()) {
                messageRequestVO.setIsHot(false);
            }
            messageService.addOneMessage(messageRequestVO);
            return PageMsg.success();
        } catch (ApplicationException ex) {
            return PageMsg.failure().setResults(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setResults("服务器异常");
        }
    }

    @RequestMapping("/message-delete")
    @ResponseBody
    public PageMsg deleteMessage(Long messageId) {
        try {
            messageService.deleteMessage(Collections.singletonList(messageId));
            return PageMsg.success();
        } catch (ApplicationException ex) {
            return PageMsg.failure().setResults(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setResults("服务器异常");
        }
    }


    @RequestMapping("/message-update-page")
    public String updateMessagePage(Long messageId, Model model) {
        List<MessageCategoryVO> categorys = messageService.getMessageCategoryList();
        model.addAttribute("categorys", categorys);
        MessageModelVO messageModelVO = messageService.getMessageById(messageId);
        model.addAttribute("topicTypes",  topicService.getAllTopicConsole());
        model.addAttribute("message", messageModelVO);
        return "message/updateMessage";

    }

    @RequestMapping("/message-update")
    @ResponseBody
    public PageMsg updateMessage(MessageRequestVO messageRequestVO) {
        try {
            if (null == messageRequestVO.getIsHot()) {
                messageRequestVO.setIsHot(false);
            }
            messageService.updateMessage(messageRequestVO);
            return PageMsg.success();
        } catch (ApplicationException ex) {
            return PageMsg.failure().setResults(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setResults("服务器异常");
        }
    }


    @RequestMapping("/message-comment-list-page")
    public String messageCommentListPage(Long messageId, Model model, @RequestParam(value = "content", defaultValue = "", required = false) String content, @RequestParam(value = "pageNum", defaultValue = "1", required = false) Integer pageNum,
                                         @RequestParam(value = "pageSize", defaultValue = "12", required = false) Integer pageSize) {
        try {

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            if (null != content) {
                request.getSession().setAttribute("content", content);
            } else {
                content = (String) request.getSession().getAttribute("content");
            }
            com.xy.zizoumohe.bean.PageResult pageResult = messageCommentService.getAllComment(messageId, content, pageNum, pageSize);
            model.addAttribute("pageResult", pageResult);
            model.addAttribute("content", content);
            model.addAttribute("messageId", messageId);
            return "message/messageCommentList";
        } catch (ApplicationException ex) {
            return "error";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    //设置该评论是否精选
    @ResponseBody
    @RequestMapping(value = "/is-excellent", method = RequestMethod.POST)
    public PageMsg setIsCurrentUse(@RequestParam("isExcellent") Boolean isExcellent, @RequestParam("id") Long id) {
        try {
            messageCommentService.setIsExcellent(isExcellent, id);
            return PageMsg.success();
        } catch (ApplicationException ex) {
            return PageMsg.failure();
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setMessage(ex);
        }

    }

    @RequestMapping("/getKey")
    @ResponseBody
    public String getKye() {
        TreeMap<String, Object> config = new TreeMap<String, Object>();

        try {
            // 替换为您的 SecretId
            config.put("SecretId", "AKIDuist8FWYUdaS7aomJm4ejdLnyud6ljyl");
            // 替换为您的 SecretKey
            config.put("SecretKey", "h7JGG0lHL9kIYdwylLfmkWq2yfAaoqdA");

            // 临时密钥有效时长，单位是秒
            config.put("durationSeconds", 3000);

            // 换成您的 bucket
            config.put("bucket", "auto-chess-box-1251720259");
            // 换成 bucket 所在地区
            config.put("region", "ap-chengdu");

            // 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的目录，例子：* 或者 doc/* 或者 picture.jpg
            config.put("allowPrefix", "*");

            // 密钥的权限列表。简单上传、表单上传和分片上传需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
            String[] allowActions = new String[]{
                    // 简单上传
                    "name/cos:PutObject",
                    // 表单上传、小程序上传
                    "name/cos:PostObject",
                    // 分片上传
                    "name/cos:InitiateMultipartUpload",
                    "name/cos:ListMultipartUploads",
                    "name/cos:ListParts",
                    "name/cos:UploadPart",
                    "name/cos:CompleteMultipartUpload"
            };
            config.put("allowActions", allowActions);
            JSONObject credential = CosStsClient.getCredential(config);
            String jsonObject = credential.toString();
            //成功返回临时密钥信息，如下打印密钥信息
            System.out.println(credential);
            return jsonObject;

        } catch (Exception e) {
            //失败抛出异常
            throw new IllegalArgumentException("no valid secret !");
        }
    }


}

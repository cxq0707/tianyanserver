package com.xy.zizoumohe.controller;


import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.service.InternalCallsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/console/internalCall")
@RestController

public class InternalCallsController {

    @Autowired
    InternalCallsService internalCallsService;

    //给予称号
    @RequestMapping(value = "/giveTheOrnament", method = RequestMethod.POST)
    public PageMsg giveTheOrnament(@RequestParam("userId") Long userId, @RequestParam("ornamentId") Integer ornamentId) {
        internalCallsService.giveTheOrnament(userId, ornamentId);
        return PageMsg.success();
    }
    //给予卡路里
    @RequestMapping(value = "/giveCalory", method = RequestMethod.POST)
    public PageMsg giveCalory(@RequestParam("userId") Long userId, @RequestParam("num") Integer num,@RequestParam("title") String title) {
        internalCallsService.giveCalory(userId, num,title);
        return PageMsg.success();
    }

}

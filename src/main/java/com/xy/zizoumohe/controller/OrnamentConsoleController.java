package com.xy.zizoumohe.controller;


import com.xy.zizoumohe.bean.Page;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.bean.PagingInfo;
import com.xy.zizoumohe.entity.Ornament;
import com.xy.zizoumohe.service.OrnamentConsoleService;
import com.xy.zizoumohe.service.OrnamentService;
import com.xy.zizoumohe.vo.OrnamentConsoleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/console/ornament")
public class OrnamentConsoleController {
    @Autowired
    OrnamentConsoleService ornamentConsoleService;

    //用户称号信息
    @ResponseBody
    @RequestMapping(value = "/userOrnament", method = RequestMethod.GET)
    public PageMsg userOrnament(@RequestParam(value = "userId") Long userId,
                                @RequestParam(value = "ornamentType", required = false, defaultValue = "1") int ornamentType) {
        List<OrnamentConsoleVO> ornamentList = ornamentConsoleService.userOrnament(userId, ornamentType);
        return PageMsg.success().setResults(ornamentList);

    }


    @RequestMapping(value = "/listPage", method = RequestMethod.GET)
    public String listPage() {
        return "ornament/list";
    }

    @RequestMapping(value = "/addPage", method = RequestMethod.GET)
    public String addPage() {
        return "ornament/add";
    }

    @RequestMapping(value = "/updatePage", method = RequestMethod.GET)
    public String updatePage(Integer id) {
        return "ornament/update";
    }


    @ResponseBody
    @RequestMapping(value = "/listData", method = RequestMethod.GET)
    public PageMsg listData() {
        List<Ornament> ornamentList = ornamentConsoleService.listData();
        return PageMsg.success().setResults(ornamentList).setPage(new PageResult<>(ornamentList, ornamentList).getPage());
    }

    @ResponseBody
    @RequestMapping(value = "/addData", method = RequestMethod.POST)
    public PageMsg addData(Ornament ornament) {
        ornamentConsoleService.add(ornament);
        return PageMsg.success();
    }

    @ResponseBody
    @RequestMapping(value = "/updateData", method = RequestMethod.PUT)
    public PageMsg updateData(Ornament ornament) {
        ornamentConsoleService.update(ornament);
        return PageMsg.success();
    }

}

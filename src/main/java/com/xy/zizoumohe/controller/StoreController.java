package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.StoreService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.request.StoreRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/api/store")
public class StoreController {
    private static Logger logger = LoggerFactory.getLogger(StoreController.class);

    @Autowired
    private StoreService storeService;

    /**
     * 获取收藏列表
     * @param storeCategoryId 收藏类别id；其中1表示阵容，为2表示讯息
     * @param currentLoginUserId 当前登录用户id
     * @param pageNum 页码数字
     * @param pageSize 页面的大小
     * @param sortColumn 排序字段
     * @param sortDirection 排序方向
     * @return json response
     */
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse getStoreList(@RequestParam("storeCategoryId") Long storeCategoryId,
                                     @RequestParam("currentLoginUserId") Long currentLoginUserId,
                                     @RequestParam(value = "type",required = false) Integer type,
                                     @RequestParam(value = "pageNum",required = false,defaultValue = "1") Integer pageNum,
                                     @RequestParam(value = "pageSize",required = false,defaultValue = "10") Integer pageSize,
                                     @RequestParam(value = "sortColumn",required = false,defaultValue = "store_time") String sortColumn,
                                     @RequestParam(value = "sortDirection",required = false,defaultValue = "DESC") String sortDirection
                                     ){
        logger.info("【****】request into /api/store/list...");
        try {
            PageRequest pageRequest = new PageRequest(pageNum,pageSize,sortColumn,sortDirection);
            PageResult pageResult =this.storeService.getStoreList(storeCategoryId, currentLoginUserId, type, pageRequest);
            return JsonResponse.success().setPage(pageResult.getPageInfo()).setResults(pageResult.getItems());
        }catch (ApplicationException ex){
            return JsonResponse.failure().setCode(ex.code).setMessage(ex.getMessage());
        }catch (Exception ex){
            return JsonResponse.failure().setCode(808).setMessage("获取收藏列表异常...");
        }
    }

    /**
     * 添加收藏
     * @param currentLoginUserId 当前登录用户id
     * @param storeCategoryId 收藏类别id 收藏阵容，storeCategoryId为1， 收藏讯息，storeCategoryId为2
     * @param entityId 收藏阵容时，entityId为squadId， 收藏讯息时，entityId为messageId
     * @return json response
     */
    @RequestMapping("/add")
    @ResponseBody
    public JsonResponse createStore(@RequestParam("currentLoginUserId") Long currentLoginUserId,
                                    @RequestParam("storeCategoryId") Long storeCategoryId,
                                    @RequestParam("entityId") Long entityId){
        logger.info("【****】request into /api/store/add...");
        try {
            StoreRequestVO storeRequestVO = new StoreRequestVO(storeCategoryId,currentLoginUserId,entityId);
            this.storeService.addStore(storeRequestVO);
            return JsonResponse.success();
        }catch (ApplicationException ex){
            return JsonResponse.failure().setCode(ex.code).setMessage(ex.getMessage());
        }catch (Exception ex){
            return JsonResponse.failure().setCode(1010).setMessage("添加收藏异常...");
        }
    }

    /**
     * 删除收藏
     * @param storeIds 收藏id的集合，Long[]
     * @return json response
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JsonResponse deleteStore(@RequestParam("storeIds") Long[] storeIds,
                                    @RequestParam("currentLoginUserId") Long currentLoginUserId){
        logger.info("【****】request into /api/store/delete...");
        try {
            List<Long> idList = new ArrayList<>(Arrays.asList(storeIds));
            this.storeService.deleteStore(idList,currentLoginUserId);
            return JsonResponse.success();
        }catch (ApplicationException ex){
            return JsonResponse.failure().setCode(ex.code).setMessage(ex.getMessage());
        }catch (Exception ex){
            return JsonResponse.failure().setCode(924).setMessage("删除收藏异常...");
        }
    }



}

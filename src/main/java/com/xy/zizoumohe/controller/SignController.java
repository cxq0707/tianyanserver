package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.SignService;
import com.xy.zizoumohe.vo.SignVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

@RestController
@RequestMapping("/api/sign")
public class SignController {

    @Autowired
    SignService signService;

    @PostMapping(value = "/create")
    public JsonResponse createSign(@RequestParam("currentLoginUserId") Long userId){
        SignVO signVO = signService.createSign(userId, new Date());
        return JsonResponse.success().setResult(signVO);
    }

}

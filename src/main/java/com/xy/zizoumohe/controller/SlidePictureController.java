package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.bean.PageMsg;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.MessageVO;
import com.xy.zizoumohe.vo.request.SlidePictureRequestVO;
import com.xy.zizoumohe.vo.SlidePictureVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/api/slidePicture")
public class SlidePictureController {
    @Autowired
    private MessageService messageService;

    private static Logger logger = LoggerFactory.getLogger(SlidePictureController.class);

    @ResponseBody
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public JsonResponse getSlidePicture() {
        try {
            List<SlidePictureVO> slidePictureList = this.messageService.getCurrentUseSlidePictureList();
            return JsonResponse.success().setResults(slidePictureList);
        } catch (ApplicationException ex) {
            return JsonResponse.failure().setMessage(ex.getMessage());
        } catch (Exception ex) {
            return JsonResponse.failure().setMessage("获取轮播图异常...");
        }

    }

    @RequestMapping(value = "/console/add-slide-picture-page", method = RequestMethod.GET)
    public String addSlidePicturePage(Model model) {
        List<MessageVO> messageVOS =messageService.getAllMessage();
        model.addAttribute("messages",messageVOS);
        return "message/addSlidePicture";
    }

    //跳转到轮播图列表
    @RequestMapping(value = "/console/slidePicture-list-page", method = RequestMethod.GET)
    public String getSlidePicture(Model model) {
        try {
            List<SlidePictureVO> slidePictureList = this.messageService.getSlidePictureList();
            model.addAttribute("slidePictureList", slidePictureList);
            return "message/slidePictureList";
        } catch (ApplicationException ex) {
            ex.printStackTrace();
            return "error";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    //设置轮播图是否使用
    @ResponseBody
    @RequestMapping(value = "/console/is-current-use", method = RequestMethod.POST)
    public JsonResponse setIsCurrentUse(@RequestParam("isCurrentUse") Boolean isCurrentUse, @RequestParam("id") Long id) {
        try {
            messageService.setIsCurrentUse(isCurrentUse, id);
            return JsonResponse.success();
        } catch (ApplicationException ex) {
            ex.printStackTrace();
            return JsonResponse.failure().setMessage(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return JsonResponse.failure().setMessage("服务器异常");
        }

    }

    //添加轮播图
    @ResponseBody
    @RequestMapping(value = "/console/add-slide-picture", method = RequestMethod.POST)
    public JsonResponse addSlidePicture(SlidePictureRequestVO slidePictureRequestVO) {
        try {
            messageService.addSlidePicture(slidePictureRequestVO);
            return JsonResponse.success();
        } catch (ApplicationException ex) {
            ex.printStackTrace();
            return JsonResponse.failure().setMessage(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return JsonResponse.failure().setMessage("服务器异常");
        }

    }

    @RequestMapping("/console/slide-picture-delete")
    @ResponseBody
    public PageMsg deleteMessage(Long slidePictureId) {
        try {
            messageService.deleteSlidePicture(Collections.singletonList(slidePictureId));
            return PageMsg.success();
        } catch (ApplicationException ex) {
            ex.printStackTrace();
            return PageMsg.failure().setResults(ex.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            return PageMsg.failure().setResults("服务器异常");
        }
    }

}

package com.xy.zizoumohe;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "com.xy.zizoumohe.mapper")
public class ZizoumoheApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZizoumoheApplication.class, args);
    }
}

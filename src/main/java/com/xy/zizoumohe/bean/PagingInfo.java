package com.xy.zizoumohe.bean;

public class PagingInfo {
    private int pageSize;
    private int pageNum;
    private int sort;
    public PagingInfo(){
        this.pageNum=1;
        this.pageSize=10;
        this.sort=1;
    }
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}

package com.xy.zizoumohe.bean;

import com.xy.zizoumohe.utils.enumeration.BehaviorType;
import com.xy.zizoumohe.utils.enumeration.SubjectType;

public class PushBean {
    private String title;
    private Long subjectId;
    private Long contentId;
    private int behaviorType;
    private int subjectType;
    private int gameType;
    private int postType;

    private Integer mediaType;
    private Long categoryId;

    public int getPostType() {
        return postType;
    }

    public PushBean setPostType(int postType) {
        this.postType = postType;
        return this;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public PushBean setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    public Integer getMediaType() {
        return mediaType;
    }

    public PushBean setMediaType(Integer mediaType) {
        this.mediaType = mediaType;
        return this;
    }

    public int getGameType() {
        return gameType;
    }

    public PushBean setGameType(int gameType) {
        this.gameType = gameType;
        return this;
    }

    public static PushBean getInstance() {
        return new PushBean();
    }

    public Long getSubjectId() {
        return subjectId;
    }

    public PushBean setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
        return this;
    }

    public Long getContentId() {
        return contentId;
    }

    public PushBean setContentId(Long contentId) {
        this.contentId = contentId;
        return this;
    }

    public int getBehaviorType() {
        return behaviorType;
    }

    public PushBean setBehaviorType(BehaviorType behaviorType) {
        this.behaviorType = behaviorType.getIndex();
        return this;
    }

    public int getSubjectType() {
        return subjectType;
    }

    public PushBean setSubjectType(SubjectType subjectType) {
        this.subjectType = subjectType.getIndex();
        return this;
    }

    public String getTitle() {
        return title;
    }

    public PushBean setTitle(String userName, BehaviorType behaviorType, SubjectType subjectType) {
        this.title = userName + behaviorType.getName() + subjectType.getName();
        if ((subjectType.getIndex() == 2 || subjectType.getIndex() == 4 || subjectType.getIndex() == 5) && behaviorType.getIndex() == 1)
            this.title = userName + "回复" + subjectType.getName();
        this.behaviorType = behaviorType.getIndex();
        this.subjectType = subjectType.getIndex();
        return this;
    }

    public PushBean setTitle(String title) {
        this.title = title;
        return this;
    }

}

package com.xy.zizoumohe.bean;

import java.util.List;
import java.util.Map;

public class Msg<T> {
    private int code;
    private String message;
    private List<T> results ;
    public Msg(){
        this.code=200;
        this.message="操作成功";
    }
    public Msg(int state){
        if(state<=0){
            this.code=1002;
            this.message="操作失败";
        }else{
            this.code=200;
            this.message="操作成功";
        }
    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<T> getResults() {
        return results;
    }

    public void setResults(List<T> results) {
        this.results = results;
    }
}

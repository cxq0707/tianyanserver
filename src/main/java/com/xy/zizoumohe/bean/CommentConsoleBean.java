package com.xy.zizoumohe.bean;

import com.xy.zizoumohe.utils.ApplicationException;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

public class CommentConsoleBean {
    @NotNull
    @Pattern(regexp = "^xiaoyuan$")
    private String key;
    @NotNull
    private Long userId;
    @NotNull
    private Long commentId;
    private String commentContent;
    private List<String> commentImageList;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public List<String> getCommentImageList() {
        return commentImageList;
    }

    public void setCommentImageList(List<String> commentImageList) {
        this.commentImageList = commentImageList;
    }
}

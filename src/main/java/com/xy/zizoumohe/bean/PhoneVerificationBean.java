package com.xy.zizoumohe.bean;

import java.io.Serializable;

public class PhoneVerificationBean implements Serializable {
    private String phoneNum;
    private String verificationCode;
    private long timeOut;
    public PhoneVerificationBean(String phoneNum,String verificationCode,long timeOut){
        this.phoneNum=phoneNum;
        this.timeOut=timeOut;
        this.verificationCode=verificationCode;
    }
    public PhoneVerificationBean(){
    }
    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(long timeOut) {
        this.timeOut = timeOut;
    }
}

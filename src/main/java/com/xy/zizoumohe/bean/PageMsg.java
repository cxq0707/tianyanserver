package com.xy.zizoumohe.bean;

public class PageMsg<T> {
    private int code;
    private Object message;
    private Object results;
    private Object result;
    private Page page;
    public Page getPage() {
        return page;
    }

    public PageMsg setPage(Page page) {
        this.page = page;
        return this;
    }
    public static PageMsg success() {
        return new PageMsg(200);
    }

    public static PageMsg failure() {
        return new PageMsg(500);
    }

    public Object getResult() {
        return result;
    }

    public PageMsg setResult(Object result) {
        this.result = result;
        return this;
    }

    public PageMsg(){
        this.code=200;
        this.message="操作成功";
    }
    public PageMsg(int code) {
        if (code == 200) {
            this.code = 200;
            this.message = "操作成功";
        } else {
            this.code = code;
            this.message = "操作失败";
        }
    }
    public int getCode() {
        return code;
    }

    public PageMsg setCode(int code) {
        this.code = code;
        return this;
    }

    public Object getMessage() {
        return message;
    }

    public PageMsg setMessage(Object message) {
        this.message = message;
        return this;
    }

    public Object getResults() {
        return results;
    }

    public PageMsg setResults(Object results) {
        this.results = results;
        return this;
    }
}

package com.xy.zizoumohe.bean;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;

import java.util.List;

/***
 *
 * @param <P> 分页集合类型
 * @param <D> 数据集合类型
 */
public class PageResult<P, D> {
    private Page page;
    private List<D> results;

    public PageResult(List<P> page, List<D> data) {
        PageInfo pageInfo = new PageInfo<P>(page);
        this.page = new Page();
        BeanUtils.copyProperties(pageInfo, this.page);
        this.results = data;
    }

    public PageResult() {
    }

    public Page getPage() {
        return page;
    }

    public PageResult<P, D> setPage(List<P> results) {
        PageInfo pageInfo = new PageInfo<P>(results);
        this.page = new Page();
        BeanUtils.copyProperties(pageInfo, this.page);
        return this;
    }

    public List<D> getResults() {
        return results;
    }

    public PageResult<P, D> setResults(List<D> results) {
        this.results = results;
        return this;
    }
}

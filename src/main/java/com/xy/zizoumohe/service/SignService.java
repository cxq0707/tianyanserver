package com.xy.zizoumohe.service;

import com.xy.zizoumohe.vo.SignVO;
import java.util.Date;

public interface SignService {

    SignVO createSign(Long userId, Date signTime);

}

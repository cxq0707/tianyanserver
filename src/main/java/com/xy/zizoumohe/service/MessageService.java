package com.xy.zizoumohe.service;

import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.MessageRequestVO;
import com.xy.zizoumohe.vo.request.SlidePictureRequestVO;

import java.util.Date;
import java.util.List;

public interface MessageService {

    List<MessageCategoryVO> getMessageCategoryList();

    PageResult<MessageVO> getMessageList(PageRequest pageRequest);

    MessageDetailVO getMessageDetail(Long messageId, Long currentLoginUserId);

    PageResult<MessageVO> getRecommendMessageList(Long categoryId,PageRequest pageRequest);

    void updateMessage(MessageRequestVO messageRequestVO);

    void addOneMessage(MessageRequestVO messageRequestVO);

    void deleteMessage(List<Long> messageIdList);

    MessageModelVO getMessageById(Long messageId);

    List<MessageVO> getAllMessage();

    List<MessageVO> getHotMessageList(Long categoryId);

    Integer getMediaType(Long messageId);

    PageResult<MessageVO> getMessageListByCategoryIdAndBoardTypeAndMediaType(Long categoryId, List<Integer> boardType, PageRequest pageRequest, String title, Long userId, Integer mediaType);

    com.xy.zizoumohe.bean.PageResult getRecommendMessageListAndBoardType(List<Integer> boardType, Long userId, PageRequest pageRequest);

    com.xy.zizoumohe.bean.PageResult messageList(PageRequest pageRequest,String title,Integer audit);

    void pushMessage(Integer id,String title,String subhead,String body);

    void modifyApproveState(Long messageId, Integer approveState);

    void modifyAuthorId(Long messageId, Long newAuthorId);

    void addMessageSupport(MessageSupportVO messageSupportVO);

    void addMessageCommentSupport(Long messageCommentId, Long userId);

    List<SlidePictureVO> getCurrentUseSlidePictureList();

    // 获取所有的轮播图
    List<SlidePictureVO> getSlidePictureList();

    void addSlidePicture(SlidePictureRequestVO slidePictureRequestVO);

    void deleteSlidePicture(List<Long> slidePictureIdList);

    void updateSlidePicture(SlidePictureRequestVO slidePictureRequestVO);

    void setIsCurrentUse(Boolean isCurrentUse,Long id);

    Integer checkSupportMessageTaskComplete(Long userId, Date date);

    Integer checkCommentMessageTaskComplete(Long userId, Date date);

    PageResult<MessageVO> getPublishMessageList(Long currentLoginUserId, Long userId, PageRequest pageRequest);


}

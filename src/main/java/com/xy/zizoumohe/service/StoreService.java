package com.xy.zizoumohe.service;

import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.request.StoreRequestVO;

import java.util.List;

public interface StoreService {

    PageResult getStoreList(Long storeCategoryId, Long currentLoginUserId, Integer type, PageRequest pageRequest);

    void addStore(StoreRequestVO storeRequestVO);

    void deleteStore(List<Long> idList,Long currentLoginUserId);

}

package com.xy.zizoumohe.service;

import java.util.Date;

public interface ShareService {

    Integer checkShareMessageTaskComplete(Long userId, Date date);

    Integer checkShareSquadTaskComplete(Long userId, Date date);

}

package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;

public interface TalentService {
    PageResult talentList(Integer pageNum, Integer pageSize, Integer gameType,Long userId);

    void talentApplyFor(TalentApplyForRequestVO talentApplyForRequestVO);

    PageResult caloryTalentList(Integer pageNum, Integer pageSize, Long userId);

    PageResult fansTalentList(Integer pageNum, Integer pageSize, Long userId);
}

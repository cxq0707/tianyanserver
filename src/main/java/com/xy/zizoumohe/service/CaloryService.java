package com.xy.zizoumohe.service;

import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.CaloryTradeVO;
import java.util.Date;
import java.util.List;

public interface CaloryService {

    Long getCaloryNum(Long userId);

    void initCalory(Long userId);

    Integer getCaloryPlusDuringPeriod(Long userId);

    int inspireAddCalory(String param, Long userId);

    int getAvailableTimesForInspireVideo(Long userId);

    List<CaloryTradeVO> getCaloryTrade(Long userId, Date date);

    void processCaloryBusiness(Long userId, Integer tradeType, Integer tradeNum, Date date);

    PageResult<CaloryTradeVO> getCaloryTradeList(Long userId, PageRequest pageRequest);

}

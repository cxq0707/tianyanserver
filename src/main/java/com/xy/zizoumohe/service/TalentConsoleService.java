package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.Talent;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;
import com.xy.zizoumohe.vo.request.TalentRequestVO;

public interface TalentConsoleService {
    PageResult talentList(Integer pageNum, Integer pageSize, Integer gameType);

    void talentApplyFor(TalentApplyForRequestVO talentApplyForRequestVO);

    Talent details(Integer id);

    void updateTalent(TalentRequestVO talentRequestVO);

    void addTalent(TalentRequestVO talentRequestVO);

    void delete(Integer id);

    PageResult talentApplyForList(Integer pageNum, Integer pageSize, Integer gameType);
}

package com.xy.zizoumohe.service;

import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.ManageMessageVO;

public interface ManageMessageService {

    void pushInbox(Long userId,Long targetUserId,String content);

}

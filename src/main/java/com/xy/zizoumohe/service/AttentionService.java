package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.Attention;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.AttentionVO;

import java.util.List;

public interface AttentionService {
    // 添加关注
    Integer addAttention(Long currentLoginUserId, Long toUserId);
    // 取消关注
    Integer cancelAttention(Long currentLoginUserId, Long toUserId);
    // 获取关注列表，带分页
    PageResult<AttentionVO> getAttentionList(Long userId, PageRequest pageRequest);
    // 获取粉丝列表，带分页
    PageResult<AttentionVO> getFansList(Long userId, PageRequest pageRequest);
    // 获取关注数
    Integer getAttentions(Long userId);
    // 获取粉丝数量
    Integer getFans(Long userId);
    // 获取关注状态
    Integer getAttentionState(Long hisUserId, Long myUserId);

    List<Attention> fansTalentList();
}

package com.xy.zizoumohe.service;

import com.xy.zizoumohe.vo.request.PowerRequestVO;


public interface UserResourceService {
    void removeUserPower(PowerRequestVO powerRequestVO);

    void addUserPower(PowerRequestVO powerRequestVO);
}

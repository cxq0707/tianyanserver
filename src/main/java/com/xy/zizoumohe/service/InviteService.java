package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.InviteInfo;

import java.util.List;
import java.util.Map;

public interface InviteService {
    Map<String, Object> inviteDetails(Long userId);

    List<InviteInfo> inviteInfoList(long userId);

    int inviteInfoCountByToUserId(long userId);

    int inviteInfoCountByUserId(long userId);
}

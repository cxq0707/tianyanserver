package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.Topic;

import java.util.List;

public interface TopicService {
    List<Topic> getAllTopicConsole();
}

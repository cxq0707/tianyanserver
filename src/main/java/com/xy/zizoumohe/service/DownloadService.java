package com.xy.zizoumohe.service;

import java.util.List;

public interface DownloadService {
    void create(String type);

    Integer stat(String startTime, String endTime, String type);

    List<String> getType();
}

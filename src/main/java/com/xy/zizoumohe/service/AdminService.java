package com.xy.zizoumohe.service;


import com.xy.zizoumohe.entity.Admin;

public interface AdminService {


    Admin Login(String name, String password,String key);

    Admin register(String name, String password,String key);
}

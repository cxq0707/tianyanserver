package com.xy.zizoumohe.service;

import java.util.List;
import java.util.Map;

public interface ApiService {
    List getLeagueProfileData();
   void initTimeTrigger();

    List getLivescoresData(String leagueId);
}

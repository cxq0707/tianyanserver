package com.xy.zizoumohe.service;

public interface LuckyTicketService {

    void initLuckyTicket(Long userId);

    void useLuckyTicketForActivityWish(Long userId, Long activityWishId);

    Integer getLuckyTicketNum(Long userId);

}

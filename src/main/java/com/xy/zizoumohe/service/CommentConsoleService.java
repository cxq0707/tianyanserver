package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.CommentConsoleBean;
import org.springframework.stereotype.Service;


public interface CommentConsoleService {
    void updateMessageComment(CommentConsoleBean commentConsoleBean);
}

package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.vo.request.ReportRequestVO;

public interface ReportConsoleService {

    PageResult list(int pageNum,int pageSize);
}

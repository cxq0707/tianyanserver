package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.PagingInfo;
import com.xy.zizoumohe.entity.Ornament;
import com.xy.zizoumohe.vo.OrnamentVO;

import java.util.Map;

public interface OrnamentService {
    Map<String, Object> homePage(Long userId, int ornamentType, PagingInfo pagingInfo);

    void convert(Long userId, int ornamentId);

    void adorn(Long userId, int ornamentId);

    OrnamentVO getByKey(Integer id);
}

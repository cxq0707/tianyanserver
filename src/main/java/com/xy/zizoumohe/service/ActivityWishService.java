package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.ActivityWishExchangeInformation;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.ActivityWishCommentRequestVO;
import com.xy.zizoumohe.vo.request.ActivityWishRequestVO;
import java.util.Date;
import java.util.List;

public interface ActivityWishService {

    void uploadWishSharePic(Long userId, Long activityWishId, String uploadPicUrl);

    WishUploadShareVO getWishUploadShare(Long activityWishId, Long userId);

    void makeWish(Long userId, Long activityWishId, Integer type, Date participateTime);

    PageResult<WishAwardGiftVO> getAcquireAwardGiftList(Long userId, PageRequest pageRequest);

    void publishActivityWish(ActivityWishRequestVO activityWishRequestVO);

    PageResult<ActivityWishVO> getActivityWishList(Long userId, PageRequest pageRequest);

    com.xy.zizoumohe.bean.PageResult getActivityWishList(Integer pageNum, Integer pageSize);

    List<UserVO> getActivityWishResult(Long activityWishId);

    List<UserUploadShareVO> getActivityWishOpenResult(Long activityWishId);

    WishAwardSimpleVO checkWishAward(Long userId, Long activityWishId);

    List<UserUploadShareVO> getActivityWishParticipateUserList(Long activityWishId);

    List<Long> randomSelectWinnerUser(List<UserUploadShareVO> userUploadShareVOList, Integer number);

    void handleWishAward(Long activityWishId, Integer number);

    List<ActivityWishExchangeInformation> getWishInformation(Long id);

    void updateWishInformation(ActivityWishExchangeInformation awei);

    void foreOpenAward(Long activityWishId);

    ActivityWishVO getActivityWishById(Long id);

    void addInspire(String param, Long userId,Long wishId);

    void setShow(int isCurrentUse, Long id);

    void createActivityWishComment(ActivityWishCommentRequestVO activityWishCommentRequestVO);

    PageResult<ActivityWishCommentVO> getActivityWishCommentList(Long activityWishId, Long userId, PageRequest pageRequest);

    GiftDetailVO getGiftDetail(Long userId, Long activityWishId);

    void createActivityWishCommentSupport(Long userId, Long wishCommentId);
}

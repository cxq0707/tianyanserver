package com.xy.zizoumohe.service;

public interface InternalCallsService {
    void giveTheOrnament(Long userId,Integer ornamentId);

    void giveCalory(Long userId, Integer num,String title);
}

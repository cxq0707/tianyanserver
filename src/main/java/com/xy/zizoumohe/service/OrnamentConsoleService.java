package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.PagingInfo;
import com.xy.zizoumohe.entity.Ornament;
import com.xy.zizoumohe.vo.OrnamentConsoleVO;

import java.util.List;
import java.util.Map;

public interface OrnamentConsoleService {
    List<Ornament> listData();

    void add(Ornament ornament);

    void update(Ornament ornament);

    Ornament getByKey(Integer id);


    List<OrnamentConsoleVO> userOrnament(Long userId, int ornamentType);
}

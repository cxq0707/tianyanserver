package com.xy.zizoumohe.service;

import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.request.MessageCommentRequestVO;
import com.xy.zizoumohe.vo.MessageCommentSimpleVO;
import com.xy.zizoumohe.vo.MessageCommentVO;

import java.util.List;

public interface MessageCommentService {

    PageResult<MessageCommentVO> getMessageComment(Long messageId, Long userId, PageRequest pageRequest, int sort);

    List<MessageCommentVO> getMessageComment(Long messageId);

    void createMessageComment(MessageCommentRequestVO messageCommentRequestVO);

    // 获取精选评论
    List<MessageCommentSimpleVO> getExcellentMessageComment(Long messageId);

    Long getMessageCommentSupports(Long userId);

    com.xy.zizoumohe.bean.PageResult getAllComment(Long messageId,String comment,int pageNum,int pageSize);

    void setIsExcellent(Boolean isExcellent, Long id);

}

package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.Token;

public interface TokenService {
    int deleteByPrimaryKey(Long tokenId);

    int insert(Token record);

    int insertSelective(Token record);

    Token selectByPrimaryKey(Long tokenId);

    Token selectByTokenString(String tokenString);

    Token selectByUserId(Long userId);

    int updateByPrimaryKeySelective(Token record);

    int updateByPrimaryKey(Token record);
}

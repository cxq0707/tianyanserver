package com.xy.zizoumohe.service;

import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.User;
import com.xy.zizoumohe.vo.GiveTitleVO;
import com.xy.zizoumohe.vo.UserVO;

import java.util.List;

public interface UserConsoleService {
    PageResult<User,GiveTitleVO>  userList(int pageNum, int pageSize, String nickname, Long userId);
}

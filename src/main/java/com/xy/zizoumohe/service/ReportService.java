package com.xy.zizoumohe.service;

import com.xy.zizoumohe.vo.request.ReportRequestVO;

public interface ReportService {
    void reportUser(ReportRequestVO reportRequestVO);
}

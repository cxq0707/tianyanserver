package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.bean.PagingInfo;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.OrnamentService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.TradeTypeConst;
import com.xy.zizoumohe.vo.OrnamentVO;
import com.xy.zizoumohe.vo.UserOrnamentVO;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class OrnamentServiceImpl implements OrnamentService {

    private static Logger logger = LoggerFactory.getLogger(OrnamentServiceImpl.class);
    @Autowired
    OrnamentMapper ornamentMapper;
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    private CaloryMapper caloryMapper;
    @Autowired
    private CaloryTradeMapper caloryTradeMapper;
    @Autowired
    private CaloryService caloryService;


    @Override
    //获取称号首页
    public Map<String, Object> homePage(Long userId, int ornamentType, PagingInfo pagingInfo) {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        PageHelper.startPage(pagingInfo.getPageNum(), pagingInfo.getPageSize());
        //取到所有的饰品
        List<Ornament> ornaments = ornamentMapper.selectByType(ornamentType);
        PageResult pageResult = new PageResult<Ornament, Ornament>().setPage(ornaments);
        //取到用户拥有的饰品
        List<UserOrnament> userOrnaments = userOrnamentMapper.selectByUserId(userId);
        List<OrnamentVO> ornamentVOS = new ArrayList<>();
        List<UserOrnamentVO> userOrnamentVOS = new ArrayList<>();
        for (Ornament ornament : ornaments) {
            OrnamentVO ornamentVO = new OrnamentVO();
            BeanUtils.copyProperties(ornament, ornamentVO);
            ornamentVO.setState(0);
            ornamentVO.setCreateTime(ornament.getCreateTime().getTime());
            //如果当前用户没有称号则不执行
            if (null == userOrnaments) {
                break;
            }
            for (UserOrnament userOrnament : userOrnaments) {
                //如果ID相互匹配则为拥有
                if (userOrnament.getOrnamentId().intValue() == ornament.getId().intValue()) {
                    ornamentVO.setState(1);
                    //如果状态为一则为佩戴
                    if (userOrnament.getState() == 1) {
                        UserOrnamentVO userOrnamentVO = new UserOrnamentVO();
                        BeanUtils.copyProperties(ornament, userOrnamentVO);
                        userOrnamentVO.setCreateTime(ornament.getCreateTime().getTime());
                        userOrnamentVOS.add(userOrnamentVO);
                        ornamentVO.setState(2);
                    }

                }
            }
            if (ornament.getIsStart() > 0 || ornamentVO.getState() > 0) {
                ornamentVOS.add(ornamentVO);
            }
        }
        result.put("userTitle", userOrnamentVOS);
        result.put("userCalorie", caloryService.getCaloryNum(userId));
        map.put("page", pageResult.getPage());
        map.put("results", ornamentVOS);
        map.put("result", result);
        return map;
    }

    //兑换饰品
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void convert(Long userId, int ornamentId) {
        User user = userMapper.selectByPrimaryKey(userId);
        if (null == user) {
            throw new ApplicationException(800, "当前用户不存在");
        }
        //判断用户是否已经有该饰品
        UserOrnament isOwn = userOrnamentMapper.selectByUserIdAndOrnamentId(userId, ornamentId);
        if (null != isOwn) {
            throw new ApplicationException(1103, "你已经拥有该饰品了");
        }
        //通过ID查询兑换这件饰品所需要的卡路里数
        Ornament ornament = ornamentMapper.selectByPrimaryKey(ornamentId);
        if (null == ornament || ornament.getIsStart() == 0) {
            throw new ApplicationException(1104, "饰品已经下架");
        }
        if (ornament.getStock() < 1) {
            throw new ApplicationException(1102, "饰品库存不足");
        }
        //获取用户当前拥有的卡路里,需要朱圆提供获取卡路里接口
        //Integer userCalorie = 9999;
        this.caloryService.initCalory(userId);
        Long userCalorie = this.caloryMapper.selectByUserIdForUpdate(userId).getCaloryNum();
        //得到购买后剩余的卡路里
        long surplusCalorie = userCalorie - ornament.getCalorie();
        //如果剩余卡路里小于0则代表卡路里余额不足
        if (surplusCalorie < 0) {
            throw new ApplicationException(1101, "卡路里余额不足");
        }
        //给用户添加这件饰品
        UserOrnament userOrnament = new UserOrnament();
        userOrnament.setOrnamentId(ornamentId);
        userOrnament.setUserId(userId);
        userOrnament.setState(0);
        userOrnamentMapper.insertSelective(userOrnament);
        logger.info("【****】用户:" + userId + "  购买饰品成功!");

        /*
         * 购买徽章成功，生成一笔卡路里财富交易账单
         */
        CaloryTrade caloryTrade = new CaloryTrade();
        caloryTrade.setAccountId(userId);  // 账单的户主
        caloryTrade.setIsIncome(false);
        caloryTrade.setTradeDescription(TradeTypeConst.TRADE_TYPE_THIRTEEN);
        caloryTrade.setTradeNum(-ornament.getCalorie());  // 账单的交易卡路里数额
        caloryTrade.setCreateTime(new Date());
        caloryTrade.setTradeType(13); // 13表示购买徽章
        this.caloryTradeMapper.insertSelective(caloryTrade);
        logger.info("【****】生成卡路里交易账单成功！");
        /*
         * 数据库t_calory表中，扣除该用户的卡路里余额
         */
        Calory calory = new Calory();
        calory.setUserId(userId);
        calory.setCaloryNum(surplusCalorie);
        this.caloryMapper.updateByUserId(calory);
        logger.info("【****】扣除用户卡路里余额成功！");
        //饰品库存-1
        ornamentMapper.stockReduce(ornamentId);
        logger.info("【****】扣除饰品库存成功！");
    }

    @Override
    @Transactional
    public void adorn(Long userId, int ornamentId) {
        User user = userMapper.selectByPrimaryKey(userId);
        if (null == user) {
            throw new ApplicationException(800, "当前用户不存在");
        }
        //判断用户是否已经有该饰品
        UserOrnament userOrnament = userOrnamentMapper.selectByUserIdAndOrnamentId(userId, ornamentId);
        if (null == userOrnament) {
            throw new ApplicationException(1103, "你暂未拥有该饰品");
        }
        //查询要装备的这件饰品
        Ornament ornament = ornamentMapper.selectByPrimaryKey(ornamentId);
        //获取用户当前佩戴着的这个类型的饰品
        List<UserOrnament> userOrnaments = userOrnamentMapper.selectByUserIdAndType(userId, ornament.getOrnamentType());
        if (userOrnaments.size() > 0) {
            List<Long> userOrnamentIdList = new ArrayList<>();
            for (UserOrnament bean : userOrnaments) {
                userOrnamentIdList.add(bean.getId());
            }
            //取下用户佩戴的同类饰品
            userOrnamentMapper.updateStartByIds(userOrnamentIdList);
        }
        //让用户佩戴饰品
        userOrnament.setState(1);
        userOrnamentMapper.updateByPrimaryKeySelective(userOrnament);
    }

    @Override
    public OrnamentVO getByKey(Integer id) {
        Ornament ornament = ornamentMapper.selectByPrimaryKey(id);
        OrnamentVO ornamentVO = new OrnamentVO();
        BeanUtils.copyProperties(ornament, ornamentVO);
        return ornamentVO;
    }
}

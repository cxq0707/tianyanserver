package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.bean.PushBean;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.notification.Notification;
import com.xy.zizoumohe.service.ManageMessageService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.utils.enumeration.BehaviorType;
import com.xy.zizoumohe.utils.enumeration.SubjectType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ManageMessageServiceImpl implements ManageMessageService {
    @Autowired
    ManageMessageMapper manageMessageMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserService userService;

    @Autowired
    MessageCommentMapper messageCommentMapper;

    @Autowired
    MessageMapper messageMapper;
    @Autowired
    MessageDetailMapper messageDetailMapper;
    @Autowired
    Helper helper;
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    /**
     * 管理员发布管理消息，推给接收的用户 version(2.2.5)
     * @param
     */
    @Override
    public void pushInbox(Long userId,Long targetUserId,String content){
        User user = userMapper.selectByPrimaryKey(userId);
        // 检查是否存在该用户
        if (user == null ||
                this.userMapper.selectByPrimaryKey(targetUserId) == null){
            throw new ApplicationException(800,"用户不存在");
        }
        // 1表示点赞，2表示评论，1000表示私信
        ManageMessage manageMessage = new ManageMessage(userId, targetUserId, null, null, 1000L, 1000);
        manageMessage.setMsgBody(content);
        this.manageMessageMapper.insertSelective(manageMessage);
        Notification notification = new Notification();
        notification.sendIOSUnicast(targetUserId, PushBean.getInstance().setContentId(null).setSubjectId(null).setTitle(user.getNickname()+"对你说:"+content).setSubjectType(SubjectType.INBOX).setBehaviorType(BehaviorType.OTHER));
    }


}

package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.entity.Calory;
import com.xy.zizoumohe.entity.CaloryTrade;
import com.xy.zizoumohe.mapper.CaloryMapper;
import com.xy.zizoumohe.mapper.CaloryTradeMapper;
import com.xy.zizoumohe.mapper.PageVisitMapper;
import com.xy.zizoumohe.mapper.UserMapper;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.utils.enumeration.KeyRules;
import com.xy.zizoumohe.vo.CaloryTradeVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CaloryServiceImpl implements CaloryService {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static Logger logger = LoggerFactory.getLogger(CaloryServiceImpl.class);
    @Autowired
    AesUtils aesUtils;
    @Autowired
    CaloryMapper caloryMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    CaloryTradeMapper caloryTradeMapper;
    @Autowired
    PageVisitMapper pageVisitMapper;

    // 获取用户的卡路里余额
    @Override
    public Long getCaloryNum(Long userId) {
        if (userId == null) {
            throw new ApplicationException(400, "参数异常");
        }
        Calory calory = caloryMapper.selectByUserId(userId);
        if (calory != null){
            return calory.getCaloryNum();
        }else {
            caloryMapper.insertSelective(new Calory(userId));
            return 0L;
        }
    }

    // 初始化用户的卡路里财富值
    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void initCalory(Long userId) {
        if (userId != null){
            if (caloryMapper.selectByUserId(userId) == null) {
                caloryMapper.insertSelective(new Calory(userId));
            }
        }
    }

    // 获取用户某个时间段卡路里的增量值
    @Override
    public Integer getCaloryPlusDuringPeriod(Long userId) {
        Date endTime = new Date();
        //Date startTime =
        Date startTime = pageVisitMapper.selectByUserId(userId).getCreateTime();
        List<CaloryTrade> caloryTradeList = caloryTradeMapper.selectByAccountIdAndIsIncomeAndDuringDate(
                                            userId, true, startTime, endTime);
        Integer totalCalory = 0;
        for (CaloryTrade caloryTrade : caloryTradeList) {
            totalCalory = totalCalory + caloryTrade.getTradeNum();
        }
        return totalCalory;
    }

    @Override
    @Transactional
    public int inspireAddCalory(String param, Long userId) {
        if (userMapper.selectByPrimaryKey(userId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        Date currentDate = new Date();
        Long overTime = Long.valueOf(aesUtils.decrypt(param, KeyRules.INSPIRE.getValue())) + 30;
        if (overTime < currentDate.getTime()/1000) {
            throw new ApplicationException(500, "超时");
        }

        List<CaloryTrade> caloryTradeList = caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(
                userId, 51, sdf.format(currentDate)); //tradeType为7表示发布阵容
        // 生成阵容作者的卡路里交易账单，只有生成交易单，才能增加用户的卡路里余额
        int num = 3-caloryTradeList.size();
        if (num > 0) {
            CaloryTrade caloryTrade = new CaloryTrade(userId, true, TradeTypeConst.TRADE_TYPE_FIFTY_ONE,
                    50, currentDate, 51, null);
            caloryTradeMapper.insertSelective(caloryTrade);
            // 增加阵容作者的卡路里财富值
            caloryMapper.increaseByUserId(userId, 50);
            num--;
        }else{
            num=-1;
        }
        return num;
    }

    @Override
    public int getAvailableTimesForInspireVideo(Long userId){
        Date currentDate = new Date();
        List<CaloryTrade> caloryTradeList = caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(
                userId, 51, sdf.format(currentDate)); //tradeType为51表示观看激励视频
        return 3-caloryTradeList.size();
    }

    @Override
    public List<CaloryTradeVO> getCaloryTrade(Long userId, Date date){
        checkUser(userId);
        List<CaloryTrade> caloryTradeList = caloryTradeMapper.selectByAccountIdAndDate(userId, sdf.format(date));
        return caloryTradeList.stream().map(e ->{
            CaloryTradeVO caloryTradeVO = new CaloryTradeVO(e.getId(), e.getCreateTime().getTime());
            BeanUtils.copyProperties(e, caloryTradeVO);
            return caloryTradeVO; }).collect(Collectors.toList());
    }

    @Override
    public void processCaloryBusiness(Long userId, Integer tradeType, Integer tradeNum, Date date){
        if (userId == null || tradeType == null || tradeNum == null || date == null){
            throw new ApplicationException("参数异常");
        }
        // 初始化用户卡路里
        if (caloryMapper.selectByUserId(userId) == null) {
            caloryMapper.insertSelective(new Calory(userId));
        }
        List<CaloryTrade> tradeList = caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(userId, tradeType, sdf.format(date)); // 某天某种卡路里交易的数量
        switch (tradeType){
            case 1 :
                break;
            case 2 :
                break;
            case 3 :

                break;
            case 4 :
                break;
            case 5 :
                break;
            case 6 :
                break;
            case 7 :
                break;
            case 8 :
                break;
            case 9 :
                break;
            case 10 :
                break;
            case 11 :
                break;
            case 12 : // 讯息评论
                if (tradeList.size() < 1){
                    CaloryTrade caloryTrade = new CaloryTrade(userId, true, TradeTypeConst.TRADE_TYPE_TWELVE, 3, date, 12);
                    caloryTradeMapper.insertSelective(caloryTrade);
                    caloryMapper.increaseByUserId(userId, 3);
                }
                break;
            case 13 :
                break;
            case 14 :
                break;
            case 15 :
                break;
            case 16 :
                break;
            case 17 :
                break;
            case 18 :
                break;
            case 19 :
                break;
            case 20 :
                break;
            default:
                break;
        }

    }

    @Override
    public PageResult<CaloryTradeVO> getCaloryTradeList(Long userId, PageRequest pageRequest) {
        checkUser(userId);
        Integer pageNum = pageRequest.getPageNum(); // 请求页码数字
        Integer pageSize = pageRequest.getPageSize(); // 请求的页面大小
        Integer totalRecords = caloryTradeMapper.selectByAccountId(userId).size(); // 总记录数
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);

        if (pageRequest.getPageNum()>pageResponse.getTotalPages()){ // 如果请求的页码数字 > 总的页数
            return new PageResult<>(new PageInfo(pageNum, pageSize, pageResponse.getTotalPages(), totalRecords), new ArrayList<>());
        }
        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<CaloryTrade> caloryTradeList = caloryTradeMapper.selectByAccountId(userId);
        // caloryTradeList -> caloryTradeVOList
        List<CaloryTradeVO> caloryTradeVOList = caloryTradeList.stream().map(e ->{
            CaloryTradeVO caloryTradeVO = new CaloryTradeVO(e.getId(), e.getCreateTime().getTime());
            BeanUtils.copyProperties(e,caloryTradeVO);
            return caloryTradeVO;
        }).collect(Collectors.toList());
        return new PageResult<>(new PageInfo(pageNum,pageSize, pageResponse.getTotalPages(),totalRecords,
                pageResponse.getFirstPage(), pageResponse.getLastPage()), caloryTradeVOList);
    }

    private void checkUser(Long userId){
        if (userId == null ){
            throw new ApplicationException(400,"参数异常");
        }
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null){
            throw new ApplicationException(800,"用户不存在");
        }
    }


}

package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.bean.PagingInfo;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.OrnamentConsoleService;
import com.xy.zizoumohe.service.OrnamentService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.TradeTypeConst;
import com.xy.zizoumohe.vo.OrnamentConsoleVO;
import com.xy.zizoumohe.vo.OrnamentVO;
import com.xy.zizoumohe.vo.UserOrnamentVO;
import org.aspectj.weaver.ast.Or;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class OrnamentConsoleServiceImpl implements OrnamentConsoleService {
    @Autowired
    OrnamentMapper ornamentMapper;
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    private CaloryService caloryService;

    @Override
    public List<Ornament> listData() {
        return ornamentMapper.selectAll();
    }

    @Override
    public void add(Ornament ornament) {
        ornamentMapper.insertSelective(ornament);
    }

    @Override
    public void update(Ornament ornament) {
        ornamentMapper.updateByPrimaryKeySelective(ornament);
    }

    @Override
    public Ornament getByKey(Integer id) {
        return ornamentMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<OrnamentConsoleVO> userOrnament(Long userId, int ornamentType) {
        List<Ornament> ornamentList = ornamentMapper.selectByType(ornamentType);
        List<UserOrnament> userOrnamentList = userOrnamentMapper.selectByUserId(userId);
        List<OrnamentConsoleVO> ornamentConsoleVOList = new ArrayList<>();
        for (Ornament ornament : ornamentList) {
            OrnamentConsoleVO ornamentConsoleVO = new OrnamentConsoleVO();
            ornamentConsoleVO.setId(ornament.getId());
            ornamentConsoleVO.setUserId(userId);
            ornamentConsoleVO.setName(ornament.getName());
            for (UserOrnament userOrnament : userOrnamentList) {
                if (ornament.getId().equals(userOrnament.getOrnamentId())) {
                    ornamentConsoleVO.setFlag(true);
                    break;
                }
            }
            ornamentConsoleVOList.add(ornamentConsoleVO);

        }
        return ornamentConsoleVOList;
    }


}

package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.notification.DrawPush;
import com.xy.zizoumohe.service.ActivityWishService;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.utils.enumeration.KeyRules;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.ActivityWishCommentRequestVO;
import com.xy.zizoumohe.vo.request.ActivityWishRequestVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import java.util.*;

@Service
public class ActivityWishServiceImpl implements ActivityWishService {

    private static Logger logger = LoggerFactory.getLogger(ActivityWishServiceImpl.class);
    @Autowired
    ActivityWishExchangeInformationMapper activityWishExchangeInformationMapper;
    @Autowired
    ActivityWishMapper activityWishMapper;
    @Autowired
    ActivityWishParticipateMapper activityWishParticipateMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    CaloryMapper caloryMapper;
    @Autowired
    WishAwardMapper wishAwardMapper;
    @Autowired
    UploadShareMapper uploadShareMapper;
    @Autowired
    UserService userService;
    @Autowired
    CaloryService caloryService;
    @Autowired
    AesUtils aesUtils;
    @Autowired
    ActivityWishCommentMapper activityWishCommentMapper;
    @Autowired
    LuckyTicketMapper luckyTicketMapper;
    @Autowired
    ActivityWishCommentSupportMapper activityWishCommentSupportMapper;

    /**
     * 上传许愿分享图片
     * @param userId         用户的id
     * @param activityWishId 许愿活动的id
     * @param uploadPicUrl   上传的图片的地址
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void uploadWishSharePic(Long userId, Long activityWishId, String uploadPicUrl) {
        if (userId == null || activityWishId == null || uploadPicUrl == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        // 判断用户之前是否参与过
        ActivityWishParticipate activityWishParticipate = activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
        if (activityWishParticipate == null) {
            throw new ApplicationException(2028, "您还未参与许愿活动，无法上传许愿分享图片!");
        }
        // 判断是否过了开奖时间，过了开奖时间，分享是无效的
        Date current = new Date();
        Date openTime = activityWish.getOpenTime();
        if (current.compareTo(openTime) == 1) {
            throw new ApplicationException(2032, "已经过了开奖时间，当前上传分享无效!");
        }
        // 判断之前是否分享过，如果分享过，此处做修改上传分享
        UploadShare uploadShare = uploadShareMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
        if (uploadShare == null) { //新增
            // 保存结果至t_upload_share
            uploadShare = new UploadShare();
            uploadShare.setActivityWishId(activityWishId);
            uploadShare.setUserId(userId);
            uploadShare.setUploadPicUrl(uploadPicUrl);
            uploadShare.setCreateTime(current);
            uploadShareMapper.insertSelective(uploadShare);
            // 修改表t_activity_wish_participate中is_upload_share的状态值
            //int flag = this.activityWishParticipateMapper.updateUploadShareState(activityWishId, userId, true);
            activityWishParticipate.setIsUploadShare(true);
            activityWishParticipate.setFortuneValue(activityWishParticipate.getFortuneValue()+10);
            int flag = activityWishParticipateMapper.updateByPrimaryKeySelective(activityWishParticipate);
            if (flag != 1) {
                throw new ApplicationException(2074, "上传许愿分享图片异常");
            }
        } else { // 修改
            uploadShare.setUploadPicUrl(uploadPicUrl);
            int flag = uploadShareMapper.updateByPrimaryKeySelective(uploadShare);
            if (flag != 1) {
                throw new ApplicationException(2074, "上传许愿分享图片异常");
            }
        }
    }

    @Override
    //获取上传分享
    public WishUploadShareVO getWishUploadShare(Long activityWishId, Long userId) {
//        int count = 1;
        if (activityWishId == null || userId == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        ActivityWishParticipate activityWishParticipate = activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
        if (activityWishParticipate == null){
            throw new ApplicationException("未参与该许愿活动");
        }
        WishUploadShareVO wishUploadShareVO = new WishUploadShareVO();
        wishUploadShareVO.setActivityWishId(activityWishId);
        wishUploadShareVO.setUserId(userId);
        wishUploadShareVO.setGiftName(activityWish.getGiftName());
        wishUploadShareVO.setGiftDescription(activityWish.getGiftDescription());
        wishUploadShareVO.setGiftPicUrl(activityWish.getGiftPicUrl());
        wishUploadShareVO.setUseFortuneTicketsTimes(activityWishParticipate.getFortuneTicketTimes());
        LuckyTicket luckyTicket = luckyTicketMapper.selectByUserId(userId);
        wishUploadShareVO.setFortuneTickets(luckyTicket == null?0:luckyTicket.getNum());
        wishUploadShareVO.setFortuneValue(activityWishParticipate.getFortuneValue()/10);
        wishUploadShareVO.setInspireNum(activityWishParticipate.getInspireNum());
        if (activityWishParticipate.getIsUploadShare()) {
            UploadShare uploadShare = uploadShareMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
            wishUploadShareVO.setUploadPicUrl(uploadShare.getUploadPicUrl());
//            count++;
        }
//        count += activityWishParticipate.getInspireNum();
//        wishUploadShareVO.setWinningProb(count);
        wishUploadShareVO.setWinningProb(activityWishParticipate.getFortuneValue()/10);
        Date currentTime = new Date();
        wishUploadShareVO.setCurrentTime(currentTime.getTime());
        // 设置 开奖状态 1未开奖 2已开奖
        Date openTime = activityWish.getOpenTime();
        Integer openState = 1;
        if (currentTime.compareTo(openTime) == 1) {
            openState = 2;
        }
        wishUploadShareVO.setOpenState(openState);
        return wishUploadShareVO;
    }

    /**
     * 获取许愿活动的列表
     * @param userId      用户的id，未登录时为null
     * @param pageRequest 请求分页参数
     * @return
     */
    @Override
    public PageResult<ActivityWishVO> getActivityWishList(Long userId, PageRequest pageRequest) {
        // 记录当前时间
        Date current = new Date();
        // 如何用户处于登录状态，判断用户是否存在
        if (userId != null) {
            if (userMapper.selectByPrimaryKey(userId) == null) {
                throw new ApplicationException(800, "用户不存在");
            }
        }
        // 总记录数
        Integer totalRecords = activityWishMapper.selectByTimeBetween(current).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<ActivityWishVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "create_time desc");
        List<ActivityWish> activityWishList = activityWishMapper.selectByTimeBetween(current);
        // activityWishList --> activityWishVOList
        List<ActivityWishVO> activityWishVOList = new ArrayList<>();
        for (ActivityWish activityWish : activityWishList) {
            ActivityWishVO activityWishVO = new ActivityWishVO();
            BeanUtils.copyProperties(activityWish, activityWishVO);
            activityWishVO.setActivityWishId(activityWish.getId());
            // 设置发布时间
            activityWishVO.setCreateTime(activityWish.getCreateTime().getTime());
            // 设置开奖时间
            activityWishVO.setOpenTime(activityWish.getOpenTime().getTime());
            // 设置当前时间
            activityWishVO.setCurrentTime(current.getTime());
            // 当前时间距离开奖时间的时间差值
            activityWishVO.setDifferenceTime(current.getTime() - activityWish.getOpenTime().getTime());
            // 设置礼物
            activityWishVO.setGiftName(activityWish.getGiftName());
            activityWishVO.setGiftDescription(activityWish.getGiftDescription());
            activityWishVO.setGiftPicUrl(activityWish.getGiftPicUrl());
            // 设置 开奖状态 1未开奖 2已开奖
            Date openTime = activityWish.getOpenTime();
            Integer openState = 1;
            if (current.compareTo(openTime) == 1) {
                openState = 2;
            }
            activityWishVO.setOpenState(openState);
            // 设置 用户参与状态 0未知，1未参与，2已参与
            Integer participateState = 0;
            if (userId != null) {
                ActivityWishParticipate activityWishParticipate = activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, activityWish.getId());
                if (activityWishParticipate == null) {
                    participateState = 1; // 用户未参与
                } else {
                    participateState = 2; // 用户已参与
                }
            }
            activityWishVO.setParticipateState(participateState);
            // 上传分享状态 0未知，1未上传，2已上传
            Integer uploadShareState = 0;
            if (userId != null) {
                UploadShare uploadShare = uploadShareMapper.selectByUserIdAndActivityWishId(userId, activityWish.getId());
                if (uploadShare == null) {
                    uploadShareState = 1;
                } else {
                    uploadShareState = 2;
                }
            }
            activityWishVO.setUploadShareState(uploadShareState);
            // 添加到列表中
            activityWishVOList.add(activityWishVO);
        }
        return new PageResult<>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), activityWishVOList);
    }

    @Override
    public com.xy.zizoumohe.bean.PageResult<ActivityWish,ActivityWish> getActivityWishList(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<ActivityWish> activityWish = activityWishMapper.selectAll();
        return new com.xy.zizoumohe.bean.PageResult<>(activityWish,activityWish);
    }

    /**
     * 获取用户{userId}在许愿活动{activityWishId}中的中奖情况
     * @param userId         用户的id
     * @param activityWishId 许愿活动的id
     * @return wishAwardSimpleVO
     */
    @Override
    public WishAwardSimpleVO checkWishAward(Long userId, Long activityWishId) {
        // 判断用户是否存在
        if (userId != null) {
            if (userMapper.selectByPrimaryKey(userId) == null) {
                throw new ApplicationException(800, "用户不存在");
            }
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        // acquireAwardDescription 获奖描述信息
        String acquireAwardDescription = null;
        // state 0未知，1未参与，2未中奖，3已中奖
        Integer state = 0;
        // 用户已登录
        if (userId != null) {
            state = 1;
            // 判断用户是否参与
            ActivityWishParticipate activityWishParticipate = activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
            if (activityWishParticipate != null) { // 用户已参与
                state = 2;
                WishAward wishAward = wishAwardMapper.selectByActivityWishIdAndUserId(activityWishId, userId);
                if (wishAward != null) { // 已中奖
                    state = 3;
                    acquireAwardDescription = wishAward.getDescription();
                }
            }
        }
        WishAwardSimpleVO wishAwardSimpleVO = new WishAwardSimpleVO();
        BeanUtils.copyProperties(activityWish, wishAwardSimpleVO);
        wishAwardSimpleVO.setState(state);
        wishAwardSimpleVO.setAcquireAwardDescription(acquireAwardDescription);
        return wishAwardSimpleVO;
    }

    /**
     * 获取许愿活动的中奖结果
     * @param activityWishId 许愿活动的id
     * @return 中奖人员列表
     */
    @Override
    public List<UserVO> getActivityWishResult(Long activityWishId) {
        if (activityWishId == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        List<WishAward> wishAwardList = wishAwardMapper.selectByActivityWishId(activityWishId);
        // wishAwardList --> userVOList
        List<UserVO> userVOList = new ArrayList<>();
        for (WishAward wishAward : wishAwardList) {
            Long userId = wishAward.getUserId();
            UserVO userVO = userService.getUserByUserId(userId);
            userVOList.add(userVO);
        }
        return userVOList;
    }

    @Override
    public List<UserUploadShareVO> getActivityWishOpenResult(Long activityWishId) {
        if (activityWishId == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        List<WishAward> wishAwardList = wishAwardMapper.selectByActivityWishId(activityWishId);
        // wishAwardList --> userUploadShareVOList
        List<UserUploadShareVO> userUploadShareVOList = new ArrayList<>();
        for (WishAward wishAward : wishAwardList) {
            Long userId = wishAward.getUserId();
            UserVO userVO = userService.getUserByUserId(userId);
            UserUploadShareVO userUploadShareVO = new UserUploadShareVO();
            BeanUtils.copyProperties(userVO, userUploadShareVO);
            UploadShare uploadShare = uploadShareMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
            userUploadShareVO.setUploadPicUrl(uploadShare == null ? null : uploadShare.getUploadPicUrl());
            userUploadShareVOList.add(userUploadShareVO);
        }
        return userUploadShareVOList;
    }


    /**
     * 用户参与许愿活动
     * @param userId          用户的id
     * @param activityWishId  许愿活动的id
     * @param type 参与方式，0表示消耗卡路里，1表示观看视频
     * @param participateTime 许愿活动参加时间
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void makeWish(Long userId, Long activityWishId, Integer type, Date participateTime) {
        if (userId == null || activityWishId == null || participateTime == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        // 判断用户之前是否参与过
        if (activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, activityWishId) != null) {
            throw new ApplicationException(2018, "您已参与许愿活动，请勿重复参加!");
        }
        // 判断参加时间是否介于'开始时间'到'结束时间'之间
        // 当前需求，简单做法，参加时间<许愿活动的开奖时间，即为合理
        int compareResult = participateTime.compareTo(activityWish.getOpenTime());
        if (compareResult == 1 || compareResult == 0) {
            throw new ApplicationException(2006, "已过开奖时间，您无法参加许愿活动！");
        }

        // 保存记录至t_activity_wish_participate
        ActivityWishParticipate activityWishParticipate = new ActivityWishParticipate();
        if (type == 0){ // 消费卡路里参与
            // 判断用户的卡路里余额是否充足
            if (caloryService.getCaloryNum(userId) < activityWish.getCostCalory()) {
                throw new ApplicationException(2009, "您的卡路里余额不足，无法参与许愿活动！");
            }
            // 扣除该用户的卡路里
            int res = caloryMapper.decreaseByUserId(userId, activityWish.getCostCalory());
            if (res != 1) {
                throw new ApplicationException(983, "扣除用户卡路里余额异常");
            }
            activityWishParticipate.setParticipateType(0);
        }else { // 观看视频参与
            activityWishParticipate.setParticipateType(1);
        }

        activityWishParticipate.setActivityWishId(activityWishId);
        activityWishParticipate.setUserId(userId);
        activityWishParticipate.setCreateTime(participateTime);
        activityWishParticipateMapper.insertSelective(activityWishParticipate);

        // t_activity_wish表中参加人数+1
        int flag = activityWishMapper.increaseParticipateNumber(activityWishId, 1);
        if (flag != 1) {
            throw new ApplicationException(2048, "许愿活动增加人数异常!");
        }
    }

    /**
     * 获取用户历次许愿活动中奖的礼物列表
     * @param userId      用户的id
     * @param pageRequest 分页请求对象
     * @return 分页结果
     */
    @Override
    public PageResult<WishAwardGiftVO> getAcquireAwardGiftList(Long userId, PageRequest pageRequest) {
        if (userId == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        Integer totalRecords = wishAwardMapper.selectByUserId(userId).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "create_time desc");
        List<WishAward> wishAwardList = wishAwardMapper.selectByUserId(userId);
        // wishAwardList --> wishAwardGiftVOList
        List<WishAwardGiftVO> wishAwardGiftVOList = new ArrayList<>();
        for (WishAward wishAward : wishAwardList) {
            WishAwardGiftVO wishAwardGiftVO = new WishAwardGiftVO();
            BeanUtils.copyProperties(wishAward, wishAwardGiftVO);
            // 许愿活动的id
            Long activityWishId = wishAward.getActivityWishId();
            ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
            BeanUtils.copyProperties(activityWish, wishAwardGiftVO);
            wishAwardGiftVO.setDescription(wishAward.getDescription());
            wishAwardGiftVO.setOpenTime(activityWish.getOpenTime().getTime());
            //
            wishAwardGiftVOList.add(wishAwardGiftVO);
        }
        return new PageResult<>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), wishAwardGiftVOList);
    }

    /**
     * 从参与许愿活动的用户中随机抽取@Code(number)名幸运观众，用户分享上传成功，增加100%的中奖率
     * @param userUploadShareVOList userUploadShareVO,记录用户的id与是否分享成功的状态
     * @param number                奖品礼物的数量
     * @return 中奖的用户
     */
    @Override
    public List<Long> randomSelectWinnerUser(List<UserUploadShareVO> userUploadShareVOList, Integer number) {
        List<Long> winnerUserIdList = new ArrayList<>();
        // 如果参与许愿活动的人数 < 礼物的数量 ，则参与的人都中奖
        if (userUploadShareVOList.size() <= number) {
            for (UserUploadShareVO userUploadShareVO : userUploadShareVOList) {
                winnerUserIdList.add(userUploadShareVO.getUserId());
            }
            return winnerUserIdList;
        }
        // 参与的人数远远大于奖品的数量
        winnerUserIdList = randomSelectOneWinner(userUploadShareVOList, number);
        // 列表中移除该中奖用户
        return winnerUserIdList;
    }

    /**
     * 随机挑选一名中奖用户，若该用户分享上传成功，中奖率提高100%
     * @param userUploadShareVOList userUploadShareVO
     * @return 中奖用户的信息
     */
    public UserUploadShareVO randomSelectOneWinner(List<UserUploadShareVO> userUploadShareVOList) {
        Map<String, UserUploadShareVO> userRankMap = new HashMap<>();
        Integer count = 0;
        for (UserUploadShareVO userUploadShareVO : userUploadShareVOList) {
            String key = count.toString();
            if (userUploadShareVO.getIsUploadShare()) {
                count++;
            }
            key = key + "," + count.toString();
            userRankMap.put(key, userUploadShareVO);
            count++;
        }
        // 生成随机数，范围是[0,count)的整数
        Integer r = MathUtils.random(0, count - 1);
        // 判断哪个用户拥有该随机数，拥有的，即为中奖用户
        // 中奖用户的key值可能为"r", "r-1,r", "r,r+1"
        String[] keyArr = new String[]{r + "," + r, (r - 1) + "," + r, r + "," + (r + 1)};
        UserUploadShareVO winnerUserUploadShareVO = null;
        for (int i = 0; i < 3; i++) {
            UserUploadShareVO userUploadShareVO = userRankMap.get(keyArr[i]);
            if (userUploadShareVO != null) {
                winnerUserUploadShareVO = userUploadShareVO;
                break;
            }
        }
        return winnerUserUploadShareVO;
    }


    public List<Long> randomSelectOneWinner(List<UserUploadShareVO> userUploadShareVOList, Integer num) {
        Map<Integer, UserUploadShareVO> map = new HashMap<Integer, UserUploadShareVO>();
        List<Long> userIdList = new ArrayList<>();
        int i = 0;
        for (UserUploadShareVO u : userUploadShareVOList) {
            i++;
            map.put(i, u);
/*            if (u.getUserId().intValue() == 24210) {
                for (int k = 0; k < 200; k++) {
                    i++;
                    map.put(i, u);

                }
            }*/

            for (int k = 0; k < u.getFortuneValue(); k++) {
                i++;
                map.put(i, u);

            }
/*            for (int k = 0; k < u.getInspireNum(); k++) {
                i++;
                map.put(i, u);

            }
            if (u.getIsUploadShare() != null && u.getIsUploadShare()) {
                i++;
                map.put(i, u);
            }*/
        }
        while (true) {
            int random = new Random().nextInt(i) + 1;
            if (!userIdList.contains(map.get(random).getUserId())) {
                userIdList.add(map.get(random).getUserId());
                if (num.equals(userIdList.size())) {
                    break;
                }
            }

        }
        return userIdList;
    }


    /**
     * 获取参与许愿活动的用户
     * @param activityWishId 许愿活动的id
     * @return 结果列表
     */
    @Override
    public List<UserUploadShareVO> getActivityWishParticipateUserList(Long activityWishId) {
        List<ActivityWishParticipate> activityWishParticipateList = activityWishParticipateMapper.selectByActivityWishId(activityWishId);
        // activityWishParticipateList --> userUploadShareVOList
        List<UserUploadShareVO> userUploadShareVOList = new ArrayList<>();
        for (ActivityWishParticipate activityWishParticipate : activityWishParticipateList) {
            UserUploadShareVO userUploadShareVO = new UserUploadShareVO();
            userUploadShareVO.setUserId(activityWishParticipate.getUserId());
            userUploadShareVO.setIsUploadShare(activityWishParticipate.getIsUploadShare());
            userUploadShareVO.setFortuneValue(activityWishParticipate.getFortuneValue());
            userUploadShareVOList.add(userUploadShareVO);
        }
        return userUploadShareVOList;
    }

    /**
     * 发布许愿活动
     * @param activityWishRequestVO activityWishRequestVO
     */
    @Override
    @Transactional
    public void publishActivityWish(ActivityWishRequestVO activityWishRequestVO) {
        Date activityWishOpenTime = activityWishRequestVO.getOpenTime();
        if (activityWishOpenTime == null) {
            throw new ApplicationException(2060, "许愿活动开奖时间不得为空!");
        }
        ActivityWish activityWish = new ActivityWish();
        BeanUtils.copyProperties(activityWishRequestVO, activityWish);
        int flag = activityWishMapper.insertSelective(activityWish);
        for (int i = 0; i < activityWish.getGiftNumber(); i++) {
            ActivityWishExchangeInformation activityWishExchangeInformation = new ActivityWishExchangeInformation();
            activityWishExchangeInformation.setActivityWishId(activityWish.getId());
            activityWishExchangeInformationMapper.insertSelective(activityWishExchangeInformation);
        }
        if (flag != 1) {
            throw new ApplicationException(2021, "发布许愿活动异常");
        }
        // 开启定时任务，在许愿活动开奖时间触发任务，计算中奖用户
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                logger.info("【定时任务，处理许愿中奖过程...】");
                handleWishAward(activityWish.getId(), activityWishRequestVO.getGiftNumber());
            }
        }, activityWishOpenTime);
    }

    @Transactional
    @Override
    public void handleWishAward(Long activityWishId, Integer number) {
        // 参与该许愿活动的用户
        List<UserUploadShareVO> userUploadShareVOList = getActivityWishParticipateUserList(activityWishId);
        // 无用户参与
        if (userUploadShareVOList.size() == 0) {
            return;
        }
        // 抽取中奖用户，数量为number
        List<Long> winnerUserIdList = randomSelectWinnerUser(userUploadShareVOList, number);
        // 取出该许愿活动的所有的兑换信息
        List<ActivityWishExchangeInformation> activityWishExchangeInformationList = activityWishExchangeInformationMapper.
                selectByActivityWishId(activityWishId);
        if (activityWishExchangeInformationList.size() < number) {
            throw new ApplicationException(2056, "许愿活动的兑奖信息数量不足!");
        }
        // 保存结果至t_wish_award中
        int i = 0;
        for (Long winnerUserId : winnerUserIdList) {
            WishAward wishAward = new WishAward();
            // 设置兑奖码
            wishAward.setExchangeCode(activityWishExchangeInformationList.get(i).getExchangeCode());
            // 设置获奖描述信息
            wishAward.setDescription(activityWishExchangeInformationList.get(i).getExchangeInformation());
            wishAward.setUserId(winnerUserId);
            wishAward.setActivityWishId(activityWishId);
            wishAward.setCreateTime(new Date());
            wishAwardMapper.insertSelective(wishAward);
            i++;
        }

        //获取这些用户ID的集合
        List<Long> participateUserIdList = new ArrayList<>();
        for (UserUploadShareVO userUploadShareVO : userUploadShareVOList) {
            participateUserIdList.add(userUploadShareVO.getUserId());
        }
        // 许愿活动
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        DrawPush drawPush = new DrawPush();
        drawPush.push(participateUserIdList, activityWishId, activityWish.getGiftName());
    }

    /**
     * 手动的强制开奖（提供测试阶段使用）
     * 如果某个活动已有中奖结果，禁止重新进行开奖重选新运用户
     * @param activityWishId 许愿活动的id
     */
    @Override
    public void foreOpenAward(Long activityWishId) {
        if (activityWishId == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 许愿活动是否之前已有开奖结果
        List<WishAward> wishAwardList = wishAwardMapper.selectByActivityWishId(activityWishId);
        if (wishAwardList.size() > 0) {
            return;
        }
        // 许愿活动是否存在
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null) {
            throw new ApplicationException(2003, "许愿活动不存在");
        }
        Integer giftNumber = activityWish.getGiftNumber();
        this.handleWishAward(activityWishId, giftNumber);
    }

    @Override
    public ActivityWishVO getActivityWishById(Long id) {
        ActivityWish activityWish = activityWishMapper.selectByPrimaryKey(id);
        ActivityWishVO activityWishVO = new ActivityWishVO();
        BeanUtils.copyProperties(activityWish, activityWishVO);
        return activityWishVO;
    }

    @Override
    public void addInspire(String param, Long userId, Long wishId) {
        Date currentDate = new Date();
        Long overTime = Long.valueOf(aesUtils.decrypt(param, KeyRules.INSPIRE.getValue())) + 3000;
        if (overTime < currentDate.getTime() / 1000) {
            throw new ApplicationException(500, "超时");
        }
        ActivityWishParticipate activityWishParticipate = activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, wishId);
        if (activityWishParticipate.getInspireNum() > 0) {
            throw new ApplicationException(1201, "已经增加过概率了");
        }
//        activityWishParticipateMapper.addInspire(userId, wishId);
        activityWishParticipate.setInspireNum(activityWishParticipate.getInspireNum()+1);
        activityWishParticipate.setFortuneValue(activityWishParticipate.getFortuneValue()+10);
        activityWishParticipateMapper.updateByPrimaryKeySelective(activityWishParticipate);
    }

    //设置愿望是否在首页展示
    @Override
    public void setShow(int isCurrentUse, Long id) {
        activityWishMapper.setShow(isCurrentUse,id);
    }

    /**
     * @param id 传入礼物ID
     * @return 返回兑奖信息集合
     */
    @Override
    public List<ActivityWishExchangeInformation> getWishInformation(Long id) {
        return activityWishExchangeInformationMapper.selectByActivityWishId(id);
    }

    @Override
    public void updateWishInformation(ActivityWishExchangeInformation awei) {
        activityWishExchangeInformationMapper.updateByPrimaryKeySelective(awei);
    }

    /**
     * 发布许愿活动评论
     * @param activityWishCommentRequestVO activityWishCommentRequestVO
     */
    public void createActivityWishComment(ActivityWishCommentRequestVO activityWishCommentRequestVO){
        if (activityWishCommentRequestVO == null || activityWishCommentRequestVO.getActivityWishId() == null
                || activityWishCommentRequestVO.getUserId() == null){
            throw new ApplicationException("请求参数异常");
        }
        Long activityWishId = activityWishCommentRequestVO.getActivityWishId();
        Long userId = activityWishCommentRequestVO.getUserId(); // 评论人的id
        Long toCommentId = activityWishCommentRequestVO.getToCommentId(); // 关联的父评论的id
        if (toCommentId != null){
            ActivityWishComment toComment = this.activityWishCommentMapper.selectByPrimaryKey(toCommentId);
            if (toComment == null){
                throw new ApplicationException("您回复的原评论不存在");
            }
        }
        String commentContent = activityWishCommentRequestVO.getCommentContent();
        List<String> commentImageList = activityWishCommentRequestVO.getCommentImageList();
        String commentImage = null;
        if (!CollectionUtils.isEmpty(commentImageList)){
            commentImage = commentImageList.get(0);
        }
        ActivityWishComment activityWishComment = new ActivityWishComment();
        activityWishComment.setActivityWishId(activityWishId);
        activityWishComment.setUserId(userId);
        activityWishComment.setCommentContent(commentContent);
        activityWishComment.setCommentImageList(commentImage);
        activityWishComment.setCreateTime(new Date());
        activityWishComment.setToCommentId(toCommentId);
        this.activityWishCommentMapper.insertSelective(activityWishComment);
    }

    // 获取许愿活动的评论列表
    @Override
    public PageResult<ActivityWishCommentVO> getActivityWishCommentList(Long activityWishId, Long userId, PageRequest pageRequest) {
        Integer totalRecords = this.activityWishCommentMapper.selectByActivityWishId(activityWishId).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }

        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "create_time desc");
        List<ActivityWishComment> activityWishCommentList = this.activityWishCommentMapper.selectByActivityWishId(activityWishId);

        // activityWishCommentList -> activityWishCommentVOList
        List<ActivityWishCommentVO> activityWishCommentVOList = new ArrayList<>();
        for(ActivityWishComment activityWishComment:activityWishCommentList){
            ActivityWishCommentVO activityWishCommentVO = new ActivityWishCommentVO();
            BeanUtils.copyProperties(activityWishComment, activityWishCommentVO);
            activityWishCommentVO.setCommentId(activityWishComment.getId());
            if (!StringUtils.isBlank(activityWishComment.getCommentImageList())){
                activityWishCommentVO.setImageList().add(activityWishComment.getCommentImageList());
            }
            if (userId != null) {
                int i = activityWishCommentSupportMapper.selectCountByUserIdAndCommentId(userId, activityWishComment.getId());
                activityWishCommentVO.setIsSupport(i);
            }
            activityWishCommentVO.setUser(this.userService.getUserByUserId(activityWishComment.getUserId()));
            activityWishCommentVO.setCommentTime(activityWishComment.getCreateTime().getTime());
            if (activityWishComment.getToCommentId() != null){
                ActivityWishComment toComment = this.activityWishCommentMapper.selectByPrimaryKey(activityWishComment.getToCommentId());
                ActivityWishCommentSimpleVO activityWishCommentSimpleVO = new ActivityWishCommentSimpleVO();
                activityWishCommentSimpleVO.setCommentId(toComment.getId());
                UserVO userVO = this.userService.getUserByUserId(toComment.getUserId());
                activityWishCommentSimpleVO.setUserId(userVO.getUserId());
                activityWishCommentSimpleVO.setNickname(userVO.getNickname());
                activityWishCommentSimpleVO.setUserHeadPortrait(userVO.getUserHeadPortrait());
                activityWishCommentSimpleVO.setCommentContent(toComment.getCommentContent());
                if (!StringUtils.isBlank(toComment.getCommentImageList())){
                    activityWishCommentSimpleVO.setImageList().add(toComment.getCommentImageList());
                }
                activityWishCommentVO.setToComment(activityWishCommentSimpleVO);
            }
            activityWishCommentVOList.add(activityWishCommentVO);
        }

        return new PageResult<>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), activityWishCommentVOList);
    }

    public GiftDetailVO getGiftDetail(Long userId, Long activityWishId){
        GiftDetailVO giftDetailVO = new GiftDetailVO();
        if (userId != null){
            giftDetailVO.setUserId(userId);
            giftDetailVO.setCaloryNum(this.caloryService.getCaloryNum(userId));
        }
        ActivityWish activityWish = this.activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null){
            throw new ApplicationException("许愿活动不存在");
        }
        Date currentTime = new Date();
        giftDetailVO.setActivityWishId(activityWishId);
        giftDetailVO.setCurrentTime(currentTime.getTime());
        giftDetailVO.setOpenTime(activityWish.getOpenTime().getTime());
        giftDetailVO.setDifferenceTime(currentTime.getTime() - activityWish.getOpenTime().getTime());
        giftDetailVO.setGiftName(activityWish.getGiftName());
        giftDetailVO.setGiftDescription(activityWish.getGiftDescription());
        giftDetailVO.setGiftNumber(activityWish.getGiftNumber());
        giftDetailVO.setGiftPicUrl(activityWish.getGiftPicUrl());
        // 设置 开奖状态 1未开奖 2已开奖
        Date openTime = activityWish.getOpenTime();
        Integer openState = 1;
        if (currentTime.compareTo(openTime) == 1) {
            openState = 2;
        }
        giftDetailVO.setOpenState(openState);
        return giftDetailVO;
    }

    //许愿活动评论点赞
    @Override
    @Transactional
    public void createActivityWishCommentSupport(Long userId, Long wishCommentId) {
        int count = activityWishCommentSupportMapper.selectCountByUserIdAndCommentId(userId, wishCommentId);
        if (count >= 1) {
            throw new ApplicationException("你已经点过赞了");
        }
        ActivityWishCommentSupport activityWishCommentSupport = new ActivityWishCommentSupport();
        activityWishCommentSupport.setActivityCommentId(wishCommentId);
        activityWishCommentSupport.setUserId(userId);
        int start1 = activityWishCommentMapper.addSupportsByCommentId(wishCommentId);
        int start2 = activityWishCommentSupportMapper.insertSelective(activityWishCommentSupport);
        if (start1 <= 0 || start2 <= 0) {
            throw new ApplicationException("点赞失败");
        }
    }

}

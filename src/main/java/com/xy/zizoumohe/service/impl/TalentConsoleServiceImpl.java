package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.Talent;
import com.xy.zizoumohe.entity.TalentApplyFor;
import com.xy.zizoumohe.entity.UserOrnament;
import com.xy.zizoumohe.mapper.TalentApplyForMapper;
import com.xy.zizoumohe.mapper.TalentMapper;
import com.xy.zizoumohe.service.TalentConsoleService;
import com.xy.zizoumohe.service.TalentService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.TalentVO;
import com.xy.zizoumohe.vo.TitleVO;
import com.xy.zizoumohe.vo.UserVO;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;
import com.xy.zizoumohe.vo.request.TalentRequestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class TalentConsoleServiceImpl implements TalentConsoleService {
    @Autowired
    TalentMapper talentMapper;
    @Autowired
    TalentApplyForMapper talentApplyForMapper;

    @Override
    public PageResult talentList(Integer pageNum, Integer pageSize, Integer gameType) {
        PageHelper.startPage(pageNum, pageSize, "`rank` asc");
        List<Talent> talentList = talentMapper.selectByGameType(gameType);
        return new PageResult<>(talentList, talentList);
    }

    @Override
    public void talentApplyFor(TalentApplyForRequestVO talentApplyForRequestVO) {
        TalentApplyFor talentApplyFor = new TalentApplyFor();
        BeanUtils.copyProperties(talentApplyForRequestVO, talentApplyFor);
        talentApplyForMapper.insertSelective(talentApplyFor);
    }

    @Override
    public Talent details(Integer id) {
        return talentMapper.selectByPrimaryKey(id);
    }

    //更新排行榜
    @Override
    @Transactional
    public void updateTalent(TalentRequestVO talentRequestVO) {
        Talent talent = talentMapper.selectByPrimaryKey(talentRequestVO.getId());
        int oldRank = talent.getRank();
        int newRank = talentRequestVO.getRank();
        //如果是将排名往后移动
        if (oldRank < newRank) {
            for (int i = oldRank + 1; i <= newRank; i++) {
                talentMapper.updateRank(i, talentRequestVO.getGameType(), i - 1);
            }
        }
        //如果是将排名往前移动
        if (oldRank > newRank) {
            for (int i = oldRank - 1; i >= newRank; i--) {
                talentMapper.updateRank(i, talentRequestVO.getGameType(), i + 1);
            }
        }
        Talent talentData = new Talent();
        BeanUtils.copyProperties(talentRequestVO, talentData);
        talentMapper.updateByPrimaryKeySelective(talentData);
    }

    //添加达人
    @Transactional
    @Override
    public void addTalent(TalentRequestVO talentRequestVO) {
        //在该排名之后的全部往后移
        int isExist = talentMapper.isExist(talentRequestVO.getUserId(), talentRequestVO.getGameType());
        if (isExist > 0) {
            throw new ApplicationException(500, "该用户已经存在该排行榜中了");
        }
        Talent talent = talentMapper.selectMinRank(talentRequestVO.getGameType());
        if (talent != null) {
            int minRank = talent.getRank();
            for (int i = minRank; i >= talentRequestVO.getRank(); i--) {
                talentMapper.updateRank(i, talentRequestVO.getGameType(), i + 1);
            }
        }
        Talent talentData = new Talent();
        BeanUtils.copyProperties(talentRequestVO, talentData);
        talentMapper.insertSelective(talentData);
    }

    @Override
    //删除达人
    @Transactional
    public void delete(Integer id) {
        Talent talent = talentMapper.selectByPrimaryKey(id);
        Talent minRankTalent = talentMapper.selectMinRank(talent.getGameType());
        int minRank = minRankTalent.getRank();
        for (int i = talent.getRank(); i <= minRank; i++) {
            talentMapper.updateRank(i, talent.getGameType(), i - 1);
        }
        talentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public PageResult talentApplyForList(Integer pageNum, Integer pageSize, Integer gameType) {
        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<TalentApplyFor> talentApplyForList = talentApplyForMapper.selectByGameType(gameType);
        return new PageResult<>(talentApplyForList, talentApplyForList);
    }
}

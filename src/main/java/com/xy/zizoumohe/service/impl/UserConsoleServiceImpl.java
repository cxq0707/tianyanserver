package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.User;
import com.xy.zizoumohe.entity.UserOrnament;
import com.xy.zizoumohe.mapper.UserMapper;
import com.xy.zizoumohe.mapper.UserOrnamentMapper;
import com.xy.zizoumohe.service.UserConsoleService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.utils.Helper;
import com.xy.zizoumohe.vo.GiveTitleVO;
import com.xy.zizoumohe.vo.TitleVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserConsoleServiceImpl implements UserConsoleService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    @Autowired
    Helper helper;
    @Autowired
    UserService userService;

    @Override
    public PageResult<User,GiveTitleVO> userList(int pageNum, int pageSize, String nickname, Long userId) {
        PageHelper.startPage(pageNum, pageSize);
        List<User> userList = new ArrayList<>();
        if (userId == null || userId.intValue() == 0) {
            userList = userMapper.selectByNickname(nickname);
        } else {
            User user = userMapper.selectByPrimaryKey(userId);
            if (user != null) {
                userList.add(user);
            }
        }
        List<GiveTitleVO> giveTitleVOList = new ArrayList<>();
        List<Long> userIdList = new ArrayList<>();
        for (User user : userList) {
            userIdList.add(user.getUserId());
        }
        List<UserOrnament> userOrnaments = new ArrayList<>();
        if (userIdList.size() > 0) {
            userOrnaments = userOrnamentMapper.selectAllTitleByUserIds(userIdList);
        }
        for (User user : userList) {
            GiveTitleVO giveTitleVO = new GiveTitleVO();
            BeanUtils.copyProperties(user, giveTitleVO);
            List<TitleVO> titleVOS = new ArrayList<>();
            helper.titleAppend(titleVOS, userOrnaments, user.getUserId());
            if (titleVOS.size() > 0) {
                giveTitleVO.setTitleList(titleVOS);
                for (TitleVO titleVO :
                        titleVOS) {
                    if (titleVO.getId().equals(3)) {
                        giveTitleVO.setIsTitle(1);
                    }

                }
            }
            giveTitleVOList.add(giveTitleVO);
        }
        return new PageResult<User,GiveTitleVO>().setResults(giveTitleVOList).setPage(userList);
    }
}

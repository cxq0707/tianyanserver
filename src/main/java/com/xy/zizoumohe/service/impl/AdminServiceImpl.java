package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.Admin;
import com.xy.zizoumohe.mapper.AdminMapper;
import com.xy.zizoumohe.service.AdminService;
import com.xy.zizoumohe.utils.AesUtils;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.Encrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminMapper adminMapper;

    @Autowired
    AesUtils aesUtils;
    @Override
    public Admin Login(String name, String password, String key) {
        Admin admin = adminMapper.getAdminByName(name);
        if (null == admin) {
            throw new ApplicationException("用户名不存在");
        }
        String decryptPassword = aesUtils.decrypt(password, key);
        String encryptPassword = Encrypt.SHA(decryptPassword, admin.getSalt());
        if (!encryptPassword.equals(admin.getPassword())) {
            throw new ApplicationException("密码错误");
        }
        return admin;
    }

    @Override
    @Transactional
    public Admin register(String name, String password, String key) {
        Admin admin = adminMapper.getAdminByName(name);
        if (null != admin) {
            throw new ApplicationException("该用户名已经被注册");
        }
        admin = new Admin();
        String salt = password;
        admin.setSalt(salt);
        String decryptPassword = aesUtils.decrypt(password, key);
        admin.setName(name);
        admin.setPassword(Encrypt.SHA(decryptPassword, admin.getSalt()));
        int state = adminMapper.insertSelective(admin);
        admin = adminMapper.selectByPrimaryKey(admin.getId());
        admin.setSalt("");
        if (state <= 0) {
            throw new ApplicationException("注册失败");
        }
        return admin;
    }
}

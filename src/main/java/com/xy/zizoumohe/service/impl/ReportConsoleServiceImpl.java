package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.Report;
import com.xy.zizoumohe.mapper.ReportMapper;
import com.xy.zizoumohe.service.ReportConsoleService;
import com.xy.zizoumohe.service.ReportService;
import com.xy.zizoumohe.vo.request.ReportRequestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportConsoleServiceImpl implements ReportConsoleService {
    @Autowired
    ReportMapper reportMapper;

    @Override
    public PageResult list(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize, "create_time desc");
        List<Report> reportList = reportMapper.selectAll();
        return new PageResult<>(reportList, reportList);
    }
}

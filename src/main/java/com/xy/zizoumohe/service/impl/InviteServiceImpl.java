package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.InviteInfo;
import com.xy.zizoumohe.mapper.InviteInfoMapper;
import com.xy.zizoumohe.service.InviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class InviteServiceImpl implements InviteService {
    @Autowired
    InviteInfoMapper inviteInfoMapper;
    @Override
    public Map<String, Object> inviteDetails(Long userId) {
        List<InviteInfo> inviteInfoList;
        inviteInfoList = inviteInfoMapper.selectByUserId(userId);
        Map<String, Object> result = new HashMap<>();
        int peopleNum = inviteInfoList.size();
        int calorie = 0;
        int luckyTicket = 0;
        for (InviteInfo inviteInfo : inviteInfoList) {
            calorie += inviteInfo.getCalorieNum();
            luckyTicket += inviteInfo.getLuckyTicketNum();
        }
        result.put("peopleNum", peopleNum);
        result.put("calorie", calorie);
        result.put("luckyTicket", luckyTicket);
        return result;
    }

    @Override
    public List<InviteInfo> inviteInfoList(long userId) {
        return inviteInfoMapper.selectByUserId(userId);
    }

    @Override
    public int inviteInfoCountByToUserId(long userId) {
        return inviteInfoMapper.selectCountByToUserId(userId);
    }

    @Override
    public int inviteInfoCountByUserId(long userId) {
        return inviteInfoMapper.selectCountByUserId(userId);
    }
}

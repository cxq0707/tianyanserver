package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.ActivityWish;
import com.xy.zizoumohe.entity.ActivityWishParticipate;
import com.xy.zizoumohe.entity.LuckyTicket;
import com.xy.zizoumohe.entity.LuckyTicketInfo;
import com.xy.zizoumohe.mapper.ActivityWishMapper;
import com.xy.zizoumohe.mapper.ActivityWishParticipateMapper;
import com.xy.zizoumohe.mapper.LuckyTicketInfoMapper;
import com.xy.zizoumohe.mapper.LuckyTicketMapper;
import com.xy.zizoumohe.service.LuckyTicketService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.enumeration.LuckyTicketSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class LuckyTicketServiceImpl implements LuckyTicketService {

    @Autowired
    LuckyTicketMapper luckyTicketMapper;
    @Autowired
    LuckyTicketInfoMapper luckyTicketInfoMapper;
    @Autowired
    ActivityWishParticipateMapper activityWishParticipateMapper;
    @Autowired
    ActivityWishMapper activityWishMapper;

    @Override
    public void initLuckyTicket(Long userId) {
        if (userId != null){
            LuckyTicket luckyTicket = this.luckyTicketMapper.selectByUserId(userId);
            if (luckyTicket == null){
                luckyTicket = new LuckyTicket();
                luckyTicket.setUserId(userId);
                this.luckyTicketMapper.insertSelective(luckyTicket);
            }
        }
    }

    /**
     * 消费幸运券给参与的许愿活动增加幸运值
     * @param userId 用户的id
     * @param activityWishId 许愿活动的id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void useLuckyTicketForActivityWish(Long userId, Long activityWishId){
        Date currentDate = new Date();
        ActivityWishParticipate activityWishParticipate = this.activityWishParticipateMapper.selectByUserIdAndActivityWishId(userId, activityWishId);
        ActivityWish activityWish = this.activityWishMapper.selectByPrimaryKey(activityWishId);
        if (activityWish == null){
            throw new ApplicationException("许愿活动不存在");
        }
        // 判断是否过了开奖时间，过了开奖时间，无法使用幸运券
        Date openTime = activityWish.getOpenTime();
        if (currentDate.compareTo(openTime) > 0) {
            throw new ApplicationException( "许愿活动已开奖，无法使用幸运券!");
        }
        if (activityWishParticipate == null){
            throw new ApplicationException("您未参与该许愿活动，无法消费幸运券");
        }
        if (activityWishParticipate.getFortuneTicketTimes()>=3){
            throw new ApplicationException("您已经消费三次幸运券");
        }
        this.luckyTicketMapper.updateNumByUserId(userId, -1);
        LuckyTicketInfo luckyTicketInfo = new LuckyTicketInfo(userId, LuckyTicketSource.GIFT_EXPEND.getName(),
                LuckyTicketSource.GIFT_EXPEND.getIndex(),LuckyTicketSource.GIFT_EXPEND.getNum());
        this.luckyTicketInfoMapper.insertSelective(luckyTicketInfo);
        //
        activityWishParticipate.setFortuneValue(activityWishParticipate.getFortuneValue()+10);
        activityWishParticipate.setFortuneTicketTimes(activityWishParticipate.getFortuneTicketTimes()+1);
        this.activityWishParticipateMapper.updateByPrimaryKeySelective(activityWishParticipate);

    }

    @Override
    public Integer getLuckyTicketNum(Long userId){
        initLuckyTicket(userId);
        return this.luckyTicketMapper.selectByUserId(userId).getNum();
    }

}

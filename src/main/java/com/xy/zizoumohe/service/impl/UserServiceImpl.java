package com.xy.zizoumohe.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.service.*;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.utils.enumeration.LuckyTicketSource;
import com.xy.zizoumohe.utils.enumeration.Wechat;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.LoginByWechatVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    @Autowired
    TokenMapper tokenMapper;
    @Autowired
    WxLoginMapper wxLoginMapper;

    @Autowired
    TokenService tokenService;

    @Autowired
    private MessageCommentService messageCommentService;
    @Autowired
    private AttentionService attentionService;
    @Autowired
    private UserOrnamentMapper userOrnamentMapper;
    @Autowired
    Helper helper;
    @Autowired
    private CaloryService caloryService;

    @Autowired
    private InviteCodeUtils inviteCodeUtils;
    @Autowired
    private CaloryMapper caloryMapper;
    @Autowired
    private CaloryTradeMapper caloryTradeMapper;
    @Autowired
    private LuckyTicketMapper luckyTicketMapper;
    @Autowired
    private LuckyTicketInfoMapper luckyTicketInfoMapper;
    @Autowired
    private InviteInfoMapper inviteInfoMapper;
    @Autowired
    private TopicLabelMapper topicLabelMapper;
    @Autowired
    TopicMapper topicMapper;
    //用户登录
    @Override
    public UserLoginVO userLogin(String userName, String password) {
        User user = userMapper.getUserByUserName(userName);
        if (null == user) {
            throw new ApplicationException("用户名不存在");
        }
        String encryptPassword = Encrypt.SHA(password, user.getSalt());
        if (!encryptPassword.equals(user.getPassword())) {
            throw new ApplicationException("密码错误");
        }
        UserLoginVO userLoginVO = new UserLoginVO();
        BeanUtils.copyProperties(user, userLoginVO);
        userLoginVO.setInviteCode(inviteCodeUtils.userIdChangeInbox(userLoginVO.getUserId()));
        userLoginVO.setToken(tokenHelp(user));
        return userLoginVO;
    }

    @Override
    public Long getCurrentUserId() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String headerToken = request.getHeader("SmallTownToken");
        if (headerToken == null) {
            return null;
        }
        Token token = tokenService.selectByTokenString(headerToken);

        if (token == null || token.getUserId() == null) {
            return null;
        }
        return token.getUserId();
    }

    //小程序微信登录
    @Transactional
    public UserLoginVO userWxProgramLogin(LoginByWechatVO loginByWechatVO) {
        RestTemplate template=new RestTemplate();

        String uri = "https://api.weixin.qq.com/sns/jscode2session?appid=" + Wechat.SMALL_PROGRAM_APPID.getValue() + "&secret=" + Wechat.SMALL_PROGRAM_APPKEY.getValue() + "&js_code=" + loginByWechatVO.getCode() + "&grant_type=authorization_code";
        ResponseEntity<String> response = template.getForEntity(uri, String.class);
        if (response != null) {
            User user = new User();
            user.setPhonenum("");
            user.setUserSex(loginByWechatVO.getGender().toString());
            user.setUserHeadPortrait(loginByWechatVO.getAvatarUrl());
            user.setUserAvatar(loginByWechatVO.getAvatarUrl());
            user.setNickname(loginByWechatVO.getNickName());
            user.setLatitude(loginByWechatVO.getLatitude());
            user.setLongitude(loginByWechatVO.getLongitude());
            user.setLocationString(loginByWechatVO.getLocationString());
            String bodyString = response.getBody();
            Map<String,String> wechatVO = (Map<String, String>) JSONUtils.parse(bodyString);
            if (wechatVO == null || wechatVO.get("openid") == null) {
                throw new ApplicationException(10002,"微信返回错误");
            }
            return userWxLogin(wechatVO.get("openid"), user, wechatVO.get("unionid"), loginByWechatVO.getInviteCode());
        } else {
            throw new ApplicationException(10001,"微信登录错误");
        }

    }


    //微信登录openId/unionId
    @Transactional
    public UserLoginVO userWxLogin(String openid, User user, String unionid, String inviteCode) {
        UserLoginVO userLoginVO = new UserLoginVO();
        User userTemp = userMapper.getUserByUniqueId(unionid, openid);
        if (null == userTemp) {
            userLoginVO = generateNewUser(openid, user, unionid, inviteCode);
        } else {
            //已经登录过则直接查询用户信息
            user.setUserId(userTemp.getUserId());
            userTemp = userMapper.selectByPrimaryKey(userTemp.getUserId());
            if (null == userTemp)
                throw new ApplicationException("登录失败");
            BeanUtils.copyProperties(userTemp, userLoginVO);
            userLoginVO.setToken(tokenHelp(userTemp));
        }
        userLoginVO.setInviteCode(inviteCodeUtils.userIdChangeInbox(userLoginVO.getUserId()));
        return userLoginVO;
    }

    @Transactional
    public UserLoginVO generateNewUser(String openid, User user, String unionid, String inviteCode) {
        UserLoginVO userLoginVO = new UserLoginVO();
        user.setRegisterTime(new Date());
        userMapper.insertSelective(user);
        Long i = user.getUserId();
        if (i < 1)
            throw new ApplicationException("登录失败");
        //插入成功后再关联到微信表
        WxLogin wxLogin = new WxLogin();
        wxLogin.setOpenid(openid);
        wxLogin.setUnionid(unionid);
        wxLogin.setUserId(i);
        int state = wxLoginMapper.insertSelective(wxLogin);
        if (state < 1)
            throw new ApplicationException("登录失败");
        User userTemp = userMapper.selectByPrimaryKey(i);
        if (null == userTemp)
            throw new ApplicationException("登录失败");
        BeanUtils.copyProperties(userTemp, userLoginVO);
        userLoginVO.setNewUser(1);
        userLoginVO.setToken(tokenHelp(userTemp));
        if (inviteCode != null) {
            applyInviteCode(userLoginVO, inviteCode);
        }
        return userLoginVO;
    }

    private void applyInviteCode(UserLoginVO userVO, String inviteCode) {
        if (inviteCode == null) {
            return;
        }
        Long inviteUserId = inviteCodeUtils.InboxChangeUserId(inviteCode);
        User inviteUser = userMapper.selectByPrimaryKey(inviteUserId);
        if (inviteUser == null) {
            System.out.println("邀请人不存在");
            return;
        }

        //被邀请人（注册用户）
        User toUser = userMapper.selectByPrimaryKey(userVO.getUserId());
        Date date = new Date();
        //如果填写了邀请码并且是刚注册不到一天的用户
        if (inviteUserId != null && toUser.getRegisterTime().getTime() >= date.getTime() - 1000 * 60 * 60 * 24) {
            int count = inviteInfoMapper.selectCountByToUserId(userVO.getUserId());
            if (count > 0) {
                System.out.println("已经被邀请过了");
                return;
            }

            //增加用户邀请记录

            InviteInfo inviteInfo = InviteInfo.deposit(inviteUserId, userVO.getUserId(), 0, 0);
            inviteInfoMapper.insertSelective(inviteInfo);
        }
    }

    //短信登录
    @Override
    @Transactional
    public UserLoginVO sendTextMessageLoginAndRegister(User user) {
        UserLoginVO userLoginVO = new UserLoginVO();
        User u = userMapper.getUserByUserName(user);
        if (null == u) {
            userMapper.insertSelective(user);
            user = userMapper.selectByPrimaryKey(user.getUserId());
            if (null == user)
                throw new ApplicationException("登录失败");
            BeanUtils.copyProperties(user, userLoginVO);
            userLoginVO.setToken(tokenHelp(user));
            userLoginVO.setInviteCode(inviteCodeUtils.userIdChangeInbox(userLoginVO.getUserId()));
            userLoginVO.setNewUser(1);
            return userLoginVO;
        }
        BeanUtils.copyProperties(u, userLoginVO);
        userLoginVO.setToken(tokenHelp(u));
        userLoginVO.setInviteCode(inviteCodeUtils.userIdChangeInbox(userLoginVO.getUserId()));
        return userLoginVO;


    }

    //用户修改信息
    @Override
    @Transactional
    public void userAlter(UserVO userVO) {
        Long inviteUserId = null;
        if (userVO.getInviteCode() != null) {
            inviteUserId = inviteCodeUtils.InboxChangeUserId(userVO.getInviteCode());
            User inviteUser = userMapper.selectByPrimaryKey(inviteUserId);
            if (inviteUser == null) {
                throw new ApplicationException(400, "邀请人不存在");
            }
        }
        User toUser = userMapper.selectByPrimaryKey(userVO.getUserId());
        User user = new User();
        BeanUtils.copyProperties(userVO, user);
        if (userVO.getNickname() != null || userVO.getUserHeadPortrait() != null) {
            userMapper.updateByPrimaryKeySelective(user);
        }
        Date date = new Date();
        //如果填写了邀请码并且是刚注册不到一天的用户
        if (inviteUserId != null && toUser.getRegisterTime().getTime() >= date.getTime() - 1000 * 60 * 60 * 24) {
            int count = inviteInfoMapper.selectCountByToUserId(userVO.getUserId());
            if (count > 0) {
                throw new ApplicationException(400, "您已经被邀请过了");
            }
            //初始化用户卡路里
            caloryService.initCalory(inviteUserId);
            //生成卡路里记录
            CaloryTrade caloryTrade = new CaloryTrade(inviteUserId, true, TradeTypeConst.TRADE_TYPE_SEVENTEEN, 150, new Date(), 17);
            caloryTradeMapper.insertSelective(caloryTrade);
            //给用户增加卡路里
            caloryMapper.increaseByUserId(inviteUserId, 150);
            //判断是否已经存在该用户的幸运券记录
            LuckyTicket luckyTicket = luckyTicketMapper.selectByUserId(inviteUserId);
            //如果不存在则初始化该用户的幸运券记录
            if (luckyTicket == null) {
                luckyTicket = new LuckyTicket(inviteUserId);
                luckyTicketMapper.insertSelective(luckyTicket);
            }
            //增加用户幸运券
            luckyTicketMapper.add(LuckyTicketSource.INVITE.getNum(), inviteUserId);
            //增加用户幸运券获取记录
            LuckyTicketInfo luckyTicketInfo = new LuckyTicketInfo(inviteUserId, LuckyTicketSource.INVITE.getName(),
                    LuckyTicketSource.INVITE.getIndex(), LuckyTicketSource.INVITE.getNum());
            luckyTicketInfoMapper.insertSelective(luckyTicketInfo);
            //增加用户邀请记录
            InviteInfo inviteInfo = new InviteInfo(inviteUserId, userVO.getUserId(), 150, LuckyTicketSource.INVITE.getNum());
            inviteInfoMapper.insertSelective(inviteInfo);
        }
    }

    @Override
    public List<WxLogin> getOpenIdList() {
        List<WxLogin> wxLoginList = wxLoginMapper.selectAllOpenids();
        return wxLoginList;
    }


    @Transactional
    public String tokenHelp(User user) {
        Token token = tokenMapper.selectByUserId(user.getUserId());
        //为生成Token准备
        String TokenStr;
        long nowtime = new Date().getTime();
        //生成Token
        TokenStr = CreatToken.creatToken(user);
        if (null == token) {
            //第一次登陆
            token = new Token();
            token.setToken(TokenStr);
            token.setBuildtime(nowtime);
            token.setUserId(user.getUserId());
            tokenMapper.insertSelective(token);
        } else {
            //登陆就更新Token信息
            TokenStr = CreatToken.creatToken(user);
            token.setToken(TokenStr);
            token.setBuildtime(nowtime);
            tokenMapper.updateByPrimaryKeySelective(token);
        }
        return TokenStr;
    }


    /**
     * 通过用户的id查询用户信息
     *
     * @param userId 用户的id
     * @return userVO
     */
    @Override
    public UserVO getUserByUserId(Long userId) {
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            return null;
        }
        UserVO userVO = new UserVO();
        userVO.setLatitude(user.getLatitude());
        userVO.setLongitude(user.getLongitude());
        userVO.setLocationString(user.getLocationString());
        userVO.setUserId(user.getUserId());
        userVO.setUserName(user.getUserName());
        userVO.setNickname(user.getNickname());
        userVO.setUserSex(user.getUserSex());
        userVO.setUserHeadPortrait(user.getUserHeadPortrait());
        List<UserOrnament> userOrnaments = userOrnamentMapper.selectTitle(userId);
        if (null != userOrnaments && userOrnaments.size() > 0) {
            List<TitleVO> titleVOS = new ArrayList<>();
            helper.titleAppend(titleVOS, userOrnaments, userId);
            userVO.setTitleList(titleVOS);
        }
        return userVO;
    }

    /**
     * 获取用户的基本统计信息，包含被点赞数，粉丝数，关注数
     *
     * @param userId             TA的id 如果userId为NULL，表示查看的是我个人的统计信息
     * @param currentLoginUserId 当前登录用户的id
     * @return userBaseStatisticVO
     */
    @Override
    public UserBaseStatisticVO getUserBaseStatistic(Long userId, Long currentLoginUserId) {
        UserBaseStatisticVO userBaseStatisticVO = new UserBaseStatisticVO();
        if (userId == null) {  // 查看的是我个人的主页
            // 判断currentLoginUserId是否存在
            userBaseStatisticVO.setCalorie(caloryService.getCaloryNum(currentLoginUserId));
            if (this.userMapper.selectByPrimaryKey(currentLoginUserId) == null) {
                throw new ApplicationException(800, "当前登录用户不存在");
            }
            userBaseStatisticVO.setUserId(currentLoginUserId);
            UserVO vo = this.getUserByUserId(currentLoginUserId);
            BeanUtils.copyProperties(vo, userBaseStatisticVO);
//            userBaseStatisticVO.setUser(this.getUserByUserId(currentLoginUserId));
            // 获取我的被点赞数
            // 目前2.1的需求，普通用户不能发布讯息
            // 包括我的阵容被点赞数，我发布的评论被点赞数，我的帖子被点赞数，我发布的帖子评论的被点赞数，我的讯息评论被点赞数
            // 1.获取我发布的阵容的点赞数量

            // 5.获取我发布的讯息评论的点赞数量
            Long totalMessageCommentSupports = this.messageCommentService.getMessageCommentSupports(currentLoginUserId);
            // 计算总的点赞数量
            Long totalSupports = totalMessageCommentSupports;
            // 设置被点赞数量
            userBaseStatisticVO.setToSupports(totalSupports);
            // 获取我的粉丝数量
            // 即关注我的人数
            Integer fans = this.attentionService.getFans(currentLoginUserId);
            // 设置粉丝数
            userBaseStatisticVO.setFans(fans);
            // 获取我的关注数量
            Integer attentions = this.attentionService.getAttentions(currentLoginUserId);
            // 设置关注数
            userBaseStatisticVO.setAttentions(attentions);
        } else {  // 查看的是TA的主页
            // 判断userId 与 currentLoginUserId是否真实存在
            if (this.userMapper.selectByPrimaryKey(userId) == null) {
                throw new ApplicationException(928, "被查看的用户不存在");
            }
            if (this.userMapper.selectByPrimaryKey(currentLoginUserId) == null) {
                throw new ApplicationException(800, "当前登录用户不存在");
            }
            userBaseStatisticVO.setUserId(userId);
            UserVO vo = this.getUserByUserId(currentLoginUserId);
            BeanUtils.copyProperties(vo, userBaseStatisticVO);
//            userBaseStatisticVO.setUser(this.getUserByUserId(userId));
            // 获取TA的被点赞数
            // 目前2.1的需求，普通用户不能发布讯息
            // 包括TA的阵容被点赞数，TA发布的评论被点赞数，TA的帖子被点赞数，TA发布的帖子评论的被点赞数，TA的讯息评论被点赞数
            // 1.获取TA发布的阵容的点赞数量

            Long totalMessageCommentSupports = this.messageCommentService.getMessageCommentSupports(userId);
            // 计算总的点赞数量
            Long totalSupports = totalMessageCommentSupports;
            // 设置被点赞数量
            userBaseStatisticVO.setToSupports(totalSupports);
            // 获取TA的粉丝数量
            // 即关注TA的人数
            Integer fans = this.attentionService.getFans(userId);
            // 设置粉丝数
            userBaseStatisticVO.setFans(fans);
            // 获取TA的关注数量
            Integer attentions = this.attentionService.getAttentions(userId);
            // 设置关注数
            userBaseStatisticVO.setAttentions(attentions);
            // 获取TA的关注状态，即当前登录用户对TA的关注状况
            // attentionState为0表示未关注，为1表示已关注，为2表示相互关注
            Integer attentionState = this.attentionService.getAttentionState(userId, currentLoginUserId);
            // 设置关注状态
            userBaseStatisticVO.setAttentionState(attentionState);
        }
        Long id = userId == null ? currentLoginUserId : userId;
        List<UserOrnament> userOrnaments = userOrnamentMapper.selectTitle(id);
        if (null != userOrnaments && userOrnaments.size() > 0) {
            List<TitleVO> titleVOS = new ArrayList<>();
            helper.titleAppend(titleVOS, userOrnaments, id);
            UserVO vo = this.getUserByUserId(id);
            vo.setTitleList(titleVOS);
        }
        userBaseStatisticVO.setInviteCode(inviteCodeUtils.userIdChangeInbox(userBaseStatisticVO.getUserId()));
        return userBaseStatisticVO;
    }

    /**
     * 更新token
     *
     * @param userId 用户的id
     * @return token字符串
     */
    @Override
    @Transactional
    public String refreshToken(Long userId) {
        User user = new User();
        user.setUserId(userId);
        Token token = tokenMapper.selectByUserId(userId);
        if (token == null) {
            return "";
        }
        //更新Token信息
        String tokenStr = CreatToken.creatToken(user);
        token.setToken(tokenStr);
        token.setBuildtime(new Date().getTime());
        tokenMapper.updateByPrimaryKeySelective(token);
        return tokenStr;
    }

    @Override
    public UserVO userDetails(Long userId) {
        User user = userMapper.selectByPrimaryKey(userId);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        userVO.setInviteCode(inviteCodeUtils.userIdChangeInbox(userVO.getUserId()));
        return userVO;
    }

    /**
     * 批量修改用户关注的话题
     *
     * @param userId     用户ID
     * @param topicTypes 话题ID集合
     */
    public void batchSetAttentionTopic(Long userId, List<Integer> topicTypes) {
        User user = new User();
        user.setUserId(userId);
        if (topicTypes == null || topicTypes.isEmpty()) {
            user.setAttentionTopic("");
        } else {
            StringBuilder topicTypeSb = new StringBuilder();
            for (int i : topicTypes) {
                topicTypeSb.append(i).append(",");
            }
            user.setAttentionTopic(topicTypeSb.toString());
        }
        userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * 修改用户关注的话题
     *
     * @param userId    用户ID
     * @param topicType 话题ID
     * @param flag      1关注/2取消关注
     */
    public void singleSetAttentionTopic(Long userId, int topicType, int flag) {
        int[] topicArray = getAttentionTopic(userId);
        List<Integer> topics;
        if (flag == 1) {
            topics = new ArrayList<>(topicArray.length + 1);
            for (int i : topicArray) {
                if (i == topicType) return;
                topics.add(i);
            }
            topics.add(topicType);
        } else {
            topics = new ArrayList<>(topicArray.length);
            for (int i : topicArray) {
                if (i != topicType) {
                    topics.add(i);
                }
            }
            if (topics.size() == topicArray.length) return;
        }
        batchSetAttentionTopic(userId, topics);
    }

    /**
     * 获取用户关注的话题ID数组
     *
     * @param userId 用户ID
     * @return 返回用户关注的ID数组
     */
    public int[] getAttentionTopic(long userId) {
        User user = userMapper.selectByPrimaryKey(userId);
        if (StringUtils.isBlank(user.getAttentionTopic())) {
            return new int[0];
        }
        String[] string = user.getAttentionTopic().split(",");
        int[] topicArray = new int[string.length];
        for (int i = 0; i < topicArray.length; i++) {
            topicArray[i] = Integer.parseInt(string[i]);
        }
        return topicArray;
    }

    public  List<Map<String,Object>> getAllTopic() {
        List<TopicLabel>  topicLabelList = topicLabelMapper.selectAll();
        List<Topic> topicList = topicMapper.selectAll();
        List<Map<String,Object>> results = new ArrayList<>();
        for (TopicLabel topicLabel:topicLabelList){
            Map<String,Object> entity = new HashMap<>();
            List<Topic> topicList1 = new ArrayList<>();

            for (Topic topic:topicList){
                String[] strings = topic.getLabel().split(",");
                for (String string:strings){
                    if(string.equals(topicLabel.getId().toString())){
                        topicList1.add(topic);
                    }
                }
            }
            entity.put("title",topicLabel.getTitle());
            entity.put("topicTypeList",topicList1);
            results.add(entity);
        }
        return results;
    }
}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.ShareService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.TradeTypeConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ShareServiceImpl implements ShareService {

    private static Logger logger = LoggerFactory.getLogger(ShareServiceImpl.class);
    @Autowired
    private ShareMapper shareMapper;
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private CaloryTradeMapper caloryTradeMapper;
    @Autowired
    private CaloryMapper caloryMapper;
    @Autowired
    private CaloryService caloryService;
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");


    private int processShare(Long userId, Integer shareType, Long entityId, Date currentDate){
        // 今日已分享的次数
        int sharedTimes = this.shareMapper.selectByUserIdAndShareTypeAndEntityIdAndDate
                (userId, shareType, entityId, simpleDateFormat.format(currentDate)).size();
        // 保存share到DB
        Share share = new Share(userId, shareType, entityId, currentDate);
        this.shareMapper.insertSelective(share);
        return sharedTimes;
    }

    @Override
    public Integer checkShareMessageTaskComplete(Long userId, Date date){
        String dateStr = simpleDateFormat.format(date); // signDate签到日期字符串，格式2019-06-19
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(userId, 11, dateStr);
        if (caloryTradeList.size()<1){
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public Integer checkShareSquadTaskComplete(Long userId, Date date){
        String dateStr = simpleDateFormat.format(date); // signDate签到日期字符串，格式2019-06-19
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(userId, 8, dateStr);
        if (caloryTradeList.size()<1){
            return 0;
        }else {
            return 1;
        }
    }

}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.AndroidVersion;
import com.xy.zizoumohe.entity.VersionType;
import com.xy.zizoumohe.mapper.AndroidVersionMapper;
import com.xy.zizoumohe.mapper.VersionTypeMapper;
import com.xy.zizoumohe.service.AndroidVersionService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.AndroidVersionVO;
import com.xy.zizoumohe.vo.VersionTypeVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AndroidVersionServiceImpl implements AndroidVersionService {

    private static Logger logger = LoggerFactory.getLogger(AndroidVersionServiceImpl.class);
    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss");
    @Autowired
    private AndroidVersionMapper androidVersionMapper;
    @Autowired
    private VersionTypeMapper versionTypeMapper;

    /**
     * 检查最新的安卓版本
     * @return androidVersionVO
     */
    @Override
    public AndroidVersionVO getLatestVersion(){
        AndroidVersion androidVersion = this.androidVersionMapper.selectByLatestVersion();
        List<VersionType> versionTypeList = this.versionTypeMapper.selectByAndroidVersionId(androidVersion.getId());
        // --> androidVersionVO
        AndroidVersionVO androidVersionVO = new AndroidVersionVO();
        androidVersionVO.setAndroidVersionId(androidVersion.getId());
        androidVersionVO.setVersion(androidVersion.getVersion());
        androidVersionVO.setSerial(androidVersion.getSerial());
        androidVersionVO.setDescription(androidVersion.getDescription());
        androidVersionVO.setPublishTime(androidVersion.getPublishTime().getTime());
        // 构造 versionTypeVO
        for (VersionType versionType:versionTypeList){
            if (versionType.getType().equals(0)){
                androidVersionVO.setDownloadUrl(versionType.getDownloadUrl());
            }
            VersionTypeVO versionTypeVO = new VersionTypeVO();
            versionTypeVO.setAndroidVersionId(androidVersion.getId());
            versionTypeVO.setType(versionType.getType());
            versionTypeVO.setDownloadUrl(versionType.getDownloadUrl());
            versionTypeVO.setSourceName(versionType.getSourceName());
            androidVersionVO.getVersionTypeVOList().add(versionTypeVO);
        }
        return androidVersionVO;
    }

    /**
     * 版本号更新
     * @param androidVersion
     */
    @Override
    @Transactional
    public void updateAndroidVersion(AndroidVersion androidVersion){
        androidVersion.setId(1l);
        this.androidVersionMapper.updateByPrimaryKeySelective(androidVersion);
    }

    /**
     * 获取安卓版本列表
     * @return
     */
    @Override
    public List<AndroidVersionVO> getAndroidVersionList(){

        List<AndroidVersion> androidVersionList = this.androidVersionMapper.selectAll();
        List<AndroidVersionVO> androidVersionVOList = new ArrayList<>();
        if (androidVersionList.size()>0){
            for (AndroidVersion androidVersion:androidVersionList){
                AndroidVersionVO androidVersionVO = new AndroidVersionVO();
                androidVersionVO.setVersion(androidVersion.getVersion());
                androidVersionVO.setSerial(androidVersion.getSerial());
                androidVersionVO.setPublishTime(androidVersion.getPublishTime().getTime());
                androidVersionVO.setDescription(androidVersion.getDescription());
                androidVersionVO.setAndroidVersionId(androidVersion.getId());
                List<VersionType> versionTypeList = this.versionTypeMapper.selectByAndroidVersionId(androidVersion.getId());
                for (VersionType versionType:versionTypeList){
                    VersionTypeVO versionTypeVO = new VersionTypeVO();
                    versionTypeVO.setType(versionType.getType());
                    versionTypeVO.setSourceName(versionType.getSourceName());
                    versionTypeVO.setDownloadUrl(versionType.getDownloadUrl());
                    versionTypeVO.setAndroidVersionId(versionType.getAndroidVersionId());
                    androidVersionVO.getVersionTypeVOList().add(versionTypeVO);
                }
            }
        }
        return androidVersionVOList;
    }



}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.Token;
import com.xy.zizoumohe.mapper.TokenMapper;
import com.xy.zizoumohe.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {
    @Autowired
    private TokenMapper tokenMapper;
    public int deleteByPrimaryKey(Long tokenId) {
        return tokenMapper.deleteByPrimaryKey(tokenId);
    }

    @Override
    public int insert(Token record) {
        return tokenMapper.insert(record);
    }

    @Override
    public int insertSelective(Token record) {
        return tokenMapper.insertSelective(record);
    }

    @Override
    public Token selectByPrimaryKey(Long tokenId) {
        return tokenMapper.selectByPrimaryKey(tokenId);
    }

    @Override
    public Token selectByTokenString(String tokenString) {
        return tokenMapper.selectByTokenString(tokenString);
    }

    @Override
    public Token selectByUserId(Long userId) {
        return tokenMapper.selectByUserId(userId);
    }

    @Override
    public int updateByPrimaryKeySelective(Token record) {
        return tokenMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Token record) {
        return updateByPrimaryKey(record);
    }
}

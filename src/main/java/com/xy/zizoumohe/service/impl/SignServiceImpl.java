package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.LuckyTicketService;
import com.xy.zizoumohe.service.SignService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.TradeTypeConst;
import com.xy.zizoumohe.utils.enumeration.LuckyTicketSource;
import com.xy.zizoumohe.vo.SignVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class SignServiceImpl implements SignService {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static final int MIN_AWARD_FOR_SIGN = 5;
    private static final int MAX_AWARD_FOR_SIGN = 30;

    @Autowired
    SignMapper signMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    CaloryMapper caloryMapper;
    @Autowired
    CaloryService caloryService;
    @Autowired
    CaloryTradeMapper caloryTradeMapper;
    @Autowired
    LuckyTicketInfoMapper luckyTicketInfoMapper;
    @Autowired
    LuckyTicketMapper luckyTicketMapper;
    @Autowired
    LuckyTicketService luckyTicketService;

    // 用户签到
    @Override
    @Transactional(rollbackFor = Exception.class)
    public SignVO createSign(Long userId, Date signTime) {
        if (userId == null || signTime == null){
            throw new ApplicationException(400,"参数异常");
        }
        // 判断用户是否存在
        User user = userMapper.selectByPrimaryKey(userId);
        if (user == null ){
            throw new ApplicationException(800,"用户不存在");
        }
        // 判断该用户今日是否已签到过; 签到日期字符串，格式2019-06-19
        Sign sign = signMapper.selectByUserIdAndCreateTime(userId, sdf.format(signTime));
        if (sign != null){
            throw new ApplicationException(980,"您今日已签到！");
        }
        // 未签到的情况
        sign = new Sign(userId, 1, MIN_AWARD_FOR_SIGN);
        // 判断当前登录用户昨天是否签到过
        Sign sign2 = signMapper.selectByUserIdAndCreateTime(userId, sdf.format(signTime.getTime()-24*60*60*1000));
        if (sign2 != null){ // 昨天已签到
            sign.setLastForDays(sign2.getLastForDays() + 1); //今日签到连续天数设置为昨天的值+1
            sign.setAward(sign2.getAward() < MAX_AWARD_FOR_SIGN ? sign2.getAward() + 1 : MAX_AWARD_FOR_SIGN); // 应奖励的卡路里值
        }
        signMapper.insertSelective(sign);
        caloryService.initCalory(userId);
        // 记录一笔卡路里财富交易单
        CaloryTrade caloryTrade = new CaloryTrade(userId, true, TradeTypeConst.TRADE_TYPE_ONE, sign.getAward(), signTime, 1);
        caloryTradeMapper.insertSelective(caloryTrade);
        caloryMapper.increaseByUserId(userId, sign.getAward());
        // 返回SignVO
        SignVO signVO = new SignVO(sign.getId(), signTime.getTime());
        BeanUtils.copyProperties(sign, signVO);
        // 检查用户是否领域过大礼包
        if (!user.getHasReceivePacket()){ // 未领取过
            signVO.setFortuneTickets(1);
            signVO.setCaloryNum(150);
            // 给用户加卡路里
            CaloryTrade caloryTrade2 = new CaloryTrade(userId, true, TradeTypeConst.TRADE_TYPE_EIGHTEEN, 150, signTime, 18);
            caloryTradeMapper.insertSelective(caloryTrade2);
            caloryMapper.increaseByUserId(userId, 150);
            // 给用户加幸运券
            luckyTicketService.initLuckyTicket(userId);
            luckyTicketMapper.updateNumByUserId(userId, 1);
            LuckyTicketInfo luckyTicketInfo = new LuckyTicketInfo(userId, LuckyTicketSource.BIG_PACKET.getName(),
                    LuckyTicketSource.BIG_PACKET.getIndex(), 1);
            luckyTicketInfoMapper.insertSelective(luckyTicketInfo);
            // 更新用户表中hasReceivePacket的状态
            user.setHasReceivePacket(true);
            userMapper.updateByPrimaryKeySelective(user);
        }else {
            signVO.setHasReceivePacket(1);
        }
        return signVO;
    }

}

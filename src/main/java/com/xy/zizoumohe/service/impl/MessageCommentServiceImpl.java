package com.xy.zizoumohe.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PushBean;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.utils.enumeration.BehaviorType;
import com.xy.zizoumohe.utils.enumeration.SubjectType;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.notification.Notification;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.service.MessageCommentService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.utils.enumeration.Wechat;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.MessageCommentRequestVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class MessageCommentServiceImpl implements MessageCommentService {
    // 允许的最大评论长度
    private final static int COMMENT_ALLOW_LENGTH = 300;
    private static Logger logger = LoggerFactory.getLogger(MessageCommentServiceImpl.class);
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Autowired
    MessageCommentMapper messageCommentMapper;
    @Autowired
    MessageCommentSupportMapper messageCommentSupportMapper;
    @Autowired
    MessageSupportCommentHitStatMapper messageSupportCommentHitStatMapper;
    @Autowired
    UserService userService;
    @Autowired
    UserMapper userMapper;
    @Autowired
    MessageMapper messageMapper;
    @Autowired
    ManageMessageMapper manageMessageMapper;
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    @Autowired
    CaloryTradeMapper caloryTradeMapper;
    @Autowired
    CaloryMapper caloryMapper;
    @Autowired
    CaloryService caloryService;

    // 获取讯息评论列表
    @Override
    public PageResult<MessageCommentVO> getMessageComment(Long messageId, Long currentLoginUserId, PageRequest pageRequest, int sort) {
        if (messageId == null || pageRequest == null) {
            throw new ApplicationException(400, "请求参数异常...");
        }
        // 判断该讯息是否存在
        if (this.messageMapper.selectByPrimaryKey(messageId) == null) {
            throw new ApplicationException(803, "讯息不存在");
        }
        // 总记录数量
        Integer totalRecords = this.messageCommentMapper.selectCountByMessageId(messageId);
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<MessageCommentVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        String orderBy = "is_excellent Desc";
        switch (sort) {
            case 1:
                orderBy = orderBy + ",comment_time Desc";
                break;
            case 2:
                orderBy = orderBy + ",supports Desc";
                break;
        }
        // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), orderBy);
        List<MessageComment> messageCommentList = this.messageCommentMapper.selectByMessageId(messageId);
        // messageCommentList -> messageCommentVOList
        List<MessageCommentVO> messageCommentVOList = new ArrayList<>();
        for (MessageComment messageComment : messageCommentList) {
            MessageCommentVO messageCommentVO = new MessageCommentVO(messageComment.getId(), messageComment.getCommentTime().getTime());
            BeanUtils.copyProperties(messageComment, messageCommentVO);
            messageCommentVO.setCommentImageList(convertCommentImageStringToList(messageComment.getCommentImageList()));
            // 设置评论人的信息
            List<UserOrnament> userOrnaments = userOrnamentMapper.selectTitle(messageComment.getUserId());
            messageCommentVO.setUserVO(this.userService.getUserByUserId(messageComment.getUserId()));
            if (null != userOrnaments && userOrnaments.size() > 0) {
                List<TitleVO> titleVOS = new ArrayList<>();
                for (UserOrnament userOrnament : userOrnaments) {
                    TitleVO titleVO = new TitleVO();
                    BeanUtils.copyProperties(userOrnament.getOrnament(), titleVO);
                    titleVOS.add(titleVO);
                }
                messageCommentVO.getUserVO().setTitleList(titleVOS);
            }
            // 设置父评论的信息
            if (messageComment.getToMessageCommentId() != null) {
                MessageCommentSimpleVO messageCommentSimpleVO = new MessageCommentSimpleVO(messageComment.getToMessageCommentId());
                MessageComment toComment = this.messageCommentMapper.selectByPrimaryKey(messageComment.getToMessageCommentId());
                if (toComment == null) {
                    messageCommentSimpleVO.setCommentContent("该评论已删除");
                } else {
                    BeanUtils.copyProperties(toComment, messageCommentSimpleVO);
                    UserVO toUserVO = this.userService.getUserByUserId(toComment.getUserId());
                    messageCommentSimpleVO.setNickname(toUserVO.getNickname());
                    messageCommentSimpleVO.setUserHeadPortrait(toUserVO.getUserHeadPortrait());
                    messageCommentSimpleVO.setCommentImageList(convertCommentImageStringToList(toComment.getCommentImageList()));
                }
                messageCommentVO.setToMessageComment(messageCommentSimpleVO);
            }
            // 判断当前用户是否讯息评论进行点赞过
            if (currentLoginUserId != null) {  // 用户已登录
                if (this.userMapper.selectByPrimaryKey(currentLoginUserId) == null) {
                    throw new ApplicationException(800, "当前用户不存在");
                }
                if (this.messageCommentSupportMapper.selectByMessageCommentIdAndUserId(
                        messageComment.getId(), currentLoginUserId) != null) {  // 当前登录用户点赞过
                    messageCommentVO.setIsSupport(true);
                }
            }
            messageCommentVOList.add(messageCommentVO);
        }
        return new PageResult<MessageCommentVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), messageCommentVOList);
    }

    /**
     * 获取讯息评论列表
     *
     * @param messageId 讯息id
     * @return 讯息评论列表
     */
    @Override
    public List<MessageCommentVO> getMessageComment(Long messageId) {
        if (messageId == null) {
            throw new ApplicationException(400, "请求参数异常...");
        }
        // 判断该讯息是否存在
        if (messageMapper.selectByPrimaryKey(messageId) == null) {
            throw new ApplicationException(803, "讯息不存在");
        }
        List<MessageComment> messageCommentList = messageCommentMapper.selectByMessageId(messageId);
        List<MessageCommentVO> messageCommentVOList = new ArrayList<>();
        for (MessageComment messageComment : messageCommentList) {
            MessageCommentVO messageCommentVO = new MessageCommentVO(messageComment.getId(), messageComment.getCommentTime().getTime());
            BeanUtils.copyProperties(messageComment, messageCommentVO);
            // 设置评论图片列表
            messageCommentVO.setCommentImageList(convertCommentImageStringToList(messageComment.getCommentImageList()));
            // 设置评论人的信息
            messageCommentVO.setUserVO(userService.getUserByUserId(messageComment.getUserId()));
            messageCommentVOList.add(messageCommentVO);
        }
        return messageCommentVOList;
    }

    // 发表讯息评论
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void createMessageComment(MessageCommentRequestVO messageCommentRequestVO) {
        Date currentDate = new Date(); //当前时间
        Long userId = messageCommentRequestVO.getUserId(); // 发布人的id
        Long messageId = messageCommentRequestVO.getMessageId(); // 讯息的id
        Long toMessageCommentId = messageCommentRequestVO.getToMessageCommentId(); // 关联的父评论id
        String commentContent = messageCommentRequestVO.getCommentContent(); // 评论的内容
        String audioUrl = messageCommentRequestVO.getAudioUrl(); // 评论的音频内容
        List<String> commentImageList = messageCommentRequestVO.getCommentImageList(); // 评论的图片
        if (commentContent == null && commentImageList == null && audioUrl == null) {
            throw new ApplicationException(840, "评论的内容或者图片不能都为空");
        }
        if (commentContent != null && commentContent.length() > COMMENT_ALLOW_LENGTH) {
            throw new ApplicationException("评论的字数太长");
        }
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            throw new ApplicationException("当前用户不存在");
        }
        // 判断讯息是否存在
        Message message = this.messageMapper.selectByPrimaryKey(messageId);
        if (message == null) {
            throw new ApplicationException(803, "讯息不存在");
        }
        Long toUserId = message.getAuthorId(); // 如果1.发布评论，toUserId表示资讯的作者；2.回复评论，toUserId表示父评论的作者
        if (toMessageCommentId != null) { // 回复评论
            MessageComment toMessageComment = this.messageCommentMapper.selectByPrimaryKey(toMessageCommentId);
            if (toMessageComment == null) {
                throw new ApplicationException(806, "您回复的原评论不存在");
            }
            toUserId = toMessageComment.getUserId();
        }

        int checkCode = checkContent(commentContent);

        if (checkCode != 0) {
            throw new ApplicationException(checkCode, "微信安全检查有问题");
        }

        MessageComment messageComment = new MessageComment();
        BeanUtils.copyProperties(messageCommentRequestVO, messageComment);
        if (commentImageList != null) {
            messageComment.setCommentImageList(commentImageList.get(0));
        }
        this.messageCommentMapper.insertSelective(messageComment);
        // 表t_message_support_comment_hits_stat中评论数加1
        this.messageSupportCommentHitStatMapper.updateCommentsAutoIncrement(messageId);
        // A评论了B的讯息评论;处理A的卡路里业务
        if (!userId.equals(toUserId)) {
            caloryService.processCaloryBusiness(userId, 12, 3, currentDate);
        }
        // 消息中心与友盟推送
        if (toMessageCommentId != null) {
            if (!userId.equals(toUserId)) {
                // 记录到消息中心
                ManageMessage manageMessage = new ManageMessage(userId, toUserId, toMessageCommentId, messageComment.getId(), 6L, 1);
                this.manageMessageMapper.insertSelective(manageMessage);
                // 友盟推送
                pushHelper(toUserId, messageComment.getId(), messageComment.getMessageId(), BehaviorType.COMMENT,
                        SubjectType.MESSAGE_COMMENT, user.getNickname(), message.getMediaType(), message.getCategoryId());
            }
        }
    }

    private int checkContent(String content) {
        RestTemplate template=new RestTemplate();

        String uri = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + Wechat.SMALL_PROGRAM_APPID.getValue() + "&secret=" + Wechat.SMALL_PROGRAM_APPKEY.getValue();
        ResponseEntity<String> response = template.getForEntity(uri, String.class);
        if (response != null) {
            String bodyString = response.getBody();
            Map<String,String> wechatVO = (Map<String, String>) JSONUtils.parse(bodyString);
            if (wechatVO == null || wechatVO.get("access_token") == null) {
                throw new ApplicationException("微信返回错误");
            }
            String access_token = wechatVO.get("access_token");

            String checkUri = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + access_token;
            LinkedHashMap postData = new LinkedHashMap();
            postData.put("content", content);
            String checkResponseString = template.postForEntity(checkUri, postData, String.class).getBody();

            Map<String,Object> checkResponse = (Map<String, Object>) JSONUtils.parse(checkResponseString);
            if (checkResponse == null || checkResponse.get("errcode") == null) {
                throw new ApplicationException("微信返回错误");
            }
            Integer errcode = Integer.parseInt(checkResponse.get("errcode").toString());
            return errcode;
        } else {
            throw new ApplicationException("内容检查有问题");
        }
    }

    //友盟推送
    public void pushHelper(Long id, Long contentId, Long subjectId, BehaviorType behaviorType, SubjectType subjectType, String nickName, Integer mediaType, Long categoryId) {
        Notification notification = new Notification();
        notification.sendIOSUnicast(id, PushBean.getInstance().setContentId(contentId).setSubjectId(subjectId).setTitle(nickName, behaviorType, subjectType).setMediaType(mediaType).setCategoryId(categoryId));
    }

    // 获取精选评论
    @Override
    public List<MessageCommentSimpleVO> getExcellentMessageComment(Long messageId) {
        // 判断messageId是否存在
        if (this.messageMapper.selectByPrimaryKey(messageId) == null) {
            throw new ApplicationException(803, "讯息不存在");
        }
        List<MessageComment> messageCommentList = this.messageCommentMapper.selectByIsExcellentAndMessageId(messageId);
        List<MessageCommentSimpleVO> messageCommentSimpleVOList = new ArrayList<>();
        for (MessageComment messageComment : messageCommentList) {
            MessageCommentSimpleVO messageCommentSimpleVO = new MessageCommentSimpleVO(messageComment.getId());
            BeanUtils.copyProperties(messageComment, messageCommentSimpleVO);
            UserVO userVO = this.userService.getUserByUserId(messageComment.getUserId());
            if (userVO == null) {
                throw new ApplicationException(700, "评论人不存在");
            }
            messageCommentSimpleVO.setUserId(userVO.getUserId());
            messageCommentSimpleVO.setUserId(userVO.getUserId());
            messageCommentSimpleVO.setUserHeadPortrait(userVO.getUserHeadPortrait());
            messageCommentSimpleVO.setNickname(userVO.getNickname());
            messageCommentSimpleVO.setCommentContent(messageComment.getCommentContent());
            // 设置评论内容图片列表
            if (!StringUtils.isBlank(messageComment.getCommentImageList())) {
                messageCommentSimpleVO.setCommentImageList(new ArrayList<>(Arrays.asList(messageComment.getCommentImageList().split(","))));
            }
            messageCommentSimpleVOList.add(messageCommentSimpleVO);
        }
        return messageCommentSimpleVOList;
    }

    /**
     * 获取讯息评论的点赞数
     *
     * @param userId 用户id
     * @return 点赞数
     */
    @Override
    public Long getMessageCommentSupports(Long userId) {
        // 查询用户是否存在
        if (this.userMapper.selectByPrimaryKey(userId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        List<MessageComment> messageCommentList = this.messageCommentMapper.selectByUserId(userId);
        Long totalSupports = 0L;
        for (MessageComment messageComment : messageCommentList) {
            totalSupports = totalSupports + messageComment.getSupports();
        }
        return totalSupports;
    }

    @Override
    public com.xy.zizoumohe.bean.PageResult getAllComment(Long messageId, String content, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize, "is_excellent desc,comment_time desc");
        content = "%" + content + "%";
        List<MessageComment> messageComments = messageCommentMapper.selectMessageCommentByMessageIdAndContent(messageId, content);
        return new com.xy.zizoumohe.bean.PageResult<>(messageComments, messageComments);
    }

    @Override
    public void setIsExcellent(Boolean isExcellent, Long id) {
        int state = messageCommentMapper.setIsExcellent(isExcellent, id);
        if (state < 1) {
            throw new ApplicationException("修改失败");
        }
    }

    private List<String> convertCommentImageStringToList(String commentImageString) {
        List<String> commentImageList = null;
        if (StringUtils.isNotBlank(commentImageString)) {
            String[] arr = commentImageString.split(",");
            commentImageList = new ArrayList<String>(Arrays.asList(arr));
        }
        return commentImageList;
    }

}

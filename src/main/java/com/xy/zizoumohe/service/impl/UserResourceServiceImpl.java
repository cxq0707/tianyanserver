package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.UserResource;
import com.xy.zizoumohe.mapper.UserResourceMapper;
import com.xy.zizoumohe.service.UserResourceService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.vo.request.PowerRequestVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserResourceServiceImpl implements UserResourceService {
    @Autowired
    UserResourceMapper userResourceMapper;
    @Override
    public void removeUserPower(PowerRequestVO powerRequestVO) {
        if(!powerRequestVO.getKey().equals("xiaoyuan")){
            throw new ApplicationException("缺少必要参数");
        }
/*       List<UserResource> userResources =  userResourceMapper.selectUserPower(powerRequestVO.getUserId(),powerRequestVO.getPowerIds());
        for (UserResource userResource: userResources) {
            powerRequestVO.getPowerIds().remove(userResource.getResourceId());
        }
        if(powerRequestVO.getPowerIds().size()<=0) {
           throw new ApplicationException("没有任何修改");
        }*/
        int line = userResourceMapper.bulkInsert(powerRequestVO.getUserId());
    }

    @Override
    public void addUserPower(PowerRequestVO powerRequestVO) {
        if(!powerRequestVO.getKey().equals("xiaoyuan")){
            throw new ApplicationException("缺少必要参数");
        }
        userResourceMapper.bulkDelete(powerRequestVO.getUserId());
    }
}

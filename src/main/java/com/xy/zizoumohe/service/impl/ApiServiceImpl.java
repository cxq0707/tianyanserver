package com.xy.zizoumohe.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.xy.zizoumohe.service.ApiService;
import com.xy.zizoumohe.utils.RTS;
import com.xy.zizoumohe.utils.RedisUtils;
import com.xy.zizoumohe.utils.trigger.OnceADayApi;
import com.xy.zizoumohe.utils.trigger.Second20Api;
import com.xy.zizoumohe.vo.GiveTitleVO;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Service
public class ApiServiceImpl implements ApiService {
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    RTS rts;
    private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();
    private static Scheduler scheduler;

    private static Scheduler getScheduler() {
        if (scheduler == null) {
            try {
                scheduler = schedulerFactory.getScheduler();
            } catch (SchedulerException e) {
                e.printStackTrace();
            }
        }
        return scheduler;
    }

    public void initTimeTrigger() {
        try {
            realTime();
            scheduler.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List getLivescoresData(String leagueId) {
        List<Map> livescoresData = (List) redisUtils.get("livescoresData");
        List<Map> teamProfileData = (List) redisUtils.get("teamProfileData");
        List<Map> data = new ArrayList<>();
        for (Map ld : livescoresData) {
            if (ld.get("leagueId").equals(leagueId)) {
                data.add(ld);
                for (Map td : teamProfileData) {
                    if(ld.get("homeId").equals(td.get("teamId"))){
                        ld.put("homeLogo",td.get("logo"));
                    }
                    if(ld.get("awayId").equals(td.get("teamId"))){
                        ld.put("awayLogo",td.get("logo"));
                    }

                }
            }
        }
        return data;
    }


    @Override
    public List getLeagueProfileData() {
        List<Map> data = (List) redisUtils.get("leagueProfileData");
        return data;
    }

    //每天获取一次数据任务
    private void dailyTask() {
        //创建一个JobDetail
        JobDetail jobDetail = JobBuilder.newJob(OnceADayApi.class)
                .withIdentity("dailyTask", "dailyTask")
                .build();
        jobDetail.getJobDataMap().put("redisUtils", redisUtils);
        //创建一个trigger触发规则
        Trigger trigger = TriggerBuilder.newTrigger()
                .startAt(new Date())
                .withIdentity("dailyTask", "dailyTask")
                .withSchedule(SimpleScheduleBuilder.repeatHourlyForever(24))
                .build();
        try {
            //将Job和Trigger注册到scheduler容器中
            getScheduler().scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    //20秒一次调用接口
    private void realTime() {
        JobDetail jobDetail = JobBuilder.newJob(Second20Api.class)
                .withIdentity("realTime", "realTime")
                .build();
        jobDetail.getJobDataMap().put("redisUtils", redisUtils);
        //创建一个trigger触发规则
        Trigger trigger = TriggerBuilder.newTrigger()
                .startAt(new Date())
                .withIdentity("realTime", "realTime")
                .withSchedule(SimpleScheduleBuilder.repeatSecondlyForever(20))
                .build();
        try {
            //将Job和Trigger注册到scheduler容器中
            getScheduler().scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}

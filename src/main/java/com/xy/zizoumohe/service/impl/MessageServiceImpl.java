package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PushBean;
import com.xy.zizoumohe.component.AcquireSupportHelper;
import com.xy.zizoumohe.config.PropertyConfig;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.notification.DrawPush;
import com.xy.zizoumohe.service.*;
import com.xy.zizoumohe.utils.enumeration.BehaviorType;
import com.xy.zizoumohe.utils.enumeration.SubjectType;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.model.MessageModel;
import com.xy.zizoumohe.notification.Notification;
import com.xy.zizoumohe.notification.PushAll;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.vo.*;
import com.xy.zizoumohe.vo.request.MessageRequestVO;
import com.xy.zizoumohe.vo.request.SlidePictureRequestVO;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MessageServiceImpl implements MessageService {

    private static Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private SlidePictureMapper slidePictureMapper;
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private MessageDetailMapper messageDetailMapper;
    @Autowired
    private MessageSupportCommentHitStatMapper messageSupportCommentHitStatMapper;
    @Autowired
    private MessageCommentMapper messageCommentMapper;
    @Autowired
    private MessageCommentService messageCommentService;
    @Autowired
    private MessageSupportMapper messageSupportMapper;
    @Autowired
    private MessageCommentSupportMapper messageCommentSupportMapper;
    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private CaloryTradeMapper caloryTradeMapper;
    @Autowired
    private CaloryMapper caloryMapper;
    @Autowired
    private CaloryService caloryService;
    @Autowired
    private PropertyConfig propertyConfig;
    @Autowired
    DrawPush drawPush;
    @Autowired
    private AttentionMapper attentionMapper;
    @Autowired
    private AcquireSupportHelper acquireSupportHelper;
    @Autowired
    AttentionService attentionService;
    @Autowired
    SearchRecordMapper searchRecordMapper;


    /**
     * 小程序资讯
     * 按照讯息类别id与版块类型来查询.
     *
     * @param categoryId  讯息类别id
     * @param boardType   版块类型
     * @param pageRequest 请求参数
     * @return MessageVO
     */
    @Override
    public PageResult<MessageVO> getMessageListByCategoryIdAndBoardTypeAndMediaType(Long categoryId, List<Integer> boardType, PageRequest pageRequest, String title, Long userId, Integer mediaType) {
        List<Integer> finalBoardType = boardType;
        if (!boardType.isEmpty() && (boardType.get(0) == 0 || boardType.get(0) == 100)) {
            finalBoardType = null;
        }
        // 总记录数量
        Integer approveState = 1;
        Integer totalRecords = this.messageMapper.selectByCategoryIdAndApproveStateAndBoardTypeAndMediaType(categoryId, approveState, finalBoardType, title, mediaType).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        try {
            if (!StringUtils.isBlank(title)) {
                new Thread(() -> {
                    SearchRecord searchRecord = new SearchRecord(userId, title, 1);
                    searchRecordMapper.insertSelective(searchRecord);
                }).start();
            }
        } catch (Exception e) {
            System.out.println("添加查找记录异常");
        }
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize());
        //List<MessageModel> messageModelList = this.messageMapper.selectByCategoryIdAndBoardType(categoryId,boardType);

        List<MessageModel> messageModelList = this.messageMapper.selectByCategoryIdAndApproveStateAndBoardTypeAndMediaType(categoryId, approveState, finalBoardType, title, mediaType);
        // messageModelList -> messageVOList
        List<MessageVO> messageVOList = new ArrayList<>();
        for (MessageModel messageModel : messageModelList) {
            MessageVO messageVO = new MessageVO(messageModel.getId(), messageModel.getCreateTime().getTime());
            // 设置所属类别名称
            messageVO.setCategoryName(this.categoryMapper.selectByPrimaryKey(categoryId == null ? messageModel.getCategoryId() : categoryId).getName());
            BeanUtils.copyProperties(messageModel, messageVO);
            messageVO.setImgList(messageModel.getImgList());
            // 设置视频链接地址，如果媒体类型为“图文”，则为空字符串；如果媒体类型为“视频”，则是一个具体的视频地址
            Integer mediaTypeInMessageModel = messageModel.getMediaType();
            if (mediaTypeInMessageModel.equals(1)) {
//                messageVO.setVideoLinkUrl(this.messageDetailMapper.selectVideoLinkUrlByMessageId(message.getId()));
                messageVO.setDescription(this.messageDetailMapper.selectByMessageId(messageModel.getId()).getContent());
            }
            MessageSupport messageSupport = this.messageSupportMapper.selectByMessageIdAndUserId(messageModel.getId(), userId);
            messageVO.setIsSupport(messageSupport != null);
            messageVO.setUser(this.userService.getUserByUserId(messageModel.getAuthorId()));
            if (userId != null) {
                messageVO.setAttentionState(attentionService.getAttentionState(messageModel.getAuthorId(), userId));
            }
            messageVOList.add(messageVO);
        }
        return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), messageVOList);
    }


    // 获取所有的讯息类别
    @Override
    public List<MessageCategoryVO> getMessageCategoryList() {
        List<Category> categoryList = this.categoryMapper.selectAll();
        return categoryList.stream().map(e -> new MessageCategoryVO(e.getId(), e.getName())).collect(Collectors.toList());
    }

    // 获取讯息类别映射
    public Map<Long, MessageCategoryVO> getMessageCategoryMap() {
        List<MessageCategoryVO> messageCategoryVOList = getMessageCategoryList();
        Map<Long, MessageCategoryVO> map = new HashMap<>();
        if (messageCategoryVOList.size() > 0) {
            for (MessageCategoryVO messageCategoryVO : messageCategoryVOList) {
                map.put(messageCategoryVO.getMessageCategoryId(), messageCategoryVO);
            }
        }
        return map;
    }

    /**
     * 获取讯息详情
     *
     * @param messageId          讯息id
     * @param currentLoginUserId 当前登录用户id，未登录为NULL
     * @return messageDetailVO
     */
    @Override
    public MessageDetailVO getMessageDetail(Long messageId, Long currentLoginUserId) {
        if (messageId == null) {
            throw new ApplicationException("参数异常");
        }
        Message message = this.messageMapper.selectByPrimaryKey(messageId);
        MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(messageId);
        MessageSupportCommentHitStat messageSupportCommentHitStat = this.messageSupportCommentHitStatMapper.selectByMessageId(messageId);
        if (message == null || messageDetail == null || messageSupportCommentHitStat == null) {
            throw new ApplicationException(803, "讯息不存在");
        }
        MessageDetailVO messageDetailVO = new MessageDetailVO();
        BeanUtils.copyProperties(message, messageDetailVO);
        BeanUtils.copyProperties(messageDetail, messageDetailVO);
        BeanUtils.copyProperties(messageSupportCommentHitStat, messageDetailVO);
        // 设置messageDetailVO
        messageDetailVO.setUser(this.userService.getUserByUserId(message.getAuthorId())); // 讯息作者
        messageDetailVO.setCategoryId(message.getCategoryId());
        messageDetailVO.setCategoryName(this.categoryMapper.selectByPrimaryKey(message.getCategoryId()).getName());
        messageDetailVO.setMessageId(messageId);
        messageDetailVO.setCreateTime(message.getCreateTime().getTime());
        if (currentLoginUserId != null) {  // 用户已登录
            // 判断当前登录用户是否存在
            if (this.userMapper.selectByPrimaryKey(currentLoginUserId) == null) {
                throw new ApplicationException(800, "用户不存在");
            }
            // 检查当前登录用户是否对讯息点赞过
            MessageSupport messageSupport = this.messageSupportMapper.selectByMessageIdAndUserId(messageId, currentLoginUserId);
            // 检查当前登录用户是否对讯息收藏过
            Store store = this.storeMapper.selectByStoreCategoryIdAndUserIdAndEntityId(2L, currentLoginUserId, messageId);
            messageDetailVO.setIsSupport(messageSupport != null);
            messageDetailVO.setIsStore(store != null);
            messageDetailVO.setAttentionState(attentionService.getAttentionState(message.getAuthorId(), currentLoginUserId));
        }
        // 访问该接口后，讯息的点击数+1
        this.messageSupportCommentHitStatMapper.updateHitsRandomIncrement(messageId);
        return messageDetailVO;
    }

    /**
     * 获取推荐的讯息列表
     *
     * @param categoryId  类别id；为NULL表示所有类别下的推荐
     * @param pageRequest 请求参数
     * @return pagerResult
     */
    @Override
    public PageResult<MessageVO> getRecommendMessageList(Long categoryId, PageRequest pageRequest) {
        checkMessageCategory(categoryId);
        // 总记录数量
        Integer totalRecords = this.messageMapper.selectByRecommendAndCategoryId(true, categoryId).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        // 执行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "priority desc, create_time desc");
        List<Message> messageList = this.messageMapper.selectByRecommendAndCategoryId(true, categoryId);
        // messageList -> messageVOList
        List<MessageVO> messageVOList = new ArrayList<MessageVO>();
        Map<Long, MessageCategoryVO> messageCategoryVOMap = this.getMessageCategoryMap();
        for (Message message : messageList) {
            MessageVO messageVO = new MessageVO(message.getId(), message.getCreateTime().getTime());
            BeanUtils.copyProperties(message, messageVO);
            // 媒体类型是“视频”类型，设置连接地址与视频描述
            if (message.getMediaType().equals(1)) {
                MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(message.getId());
                messageVO.setVideoLinkUrl(messageDetail.getVideoLinkUrl());
                messageVO.setDescription(messageDetail.getContent());
            }
            // 通过messageId获取messageSupportCommentHitStat
            MessageSupportCommentHitStat messageSupportCommentHitStat = messageSupportCommentHitStatMapper.selectByMessageId(message.getId());
            if (messageSupportCommentHitStat == null) {
                throw new ApplicationException(822, "讯息点赞评论点击量异常...");
            }
            // 复制hits，supports，comments的属性值
            BeanUtils.copyProperties(messageSupportCommentHitStat, messageVO);
            // 设置所属类别名称
            messageVO.setCategoryName(messageCategoryVOMap.get(message.getCategoryId()).getName());
            messageVOList.add(messageVO);
        }
        return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), messageVOList);
    }

    /**
     * 获取热门的讯息列表；讯息类型为讨论；当前需求下，热门只有一条
     *
     * @param categoryId 类别id；4为讨论
     * @return result list
     */
    @Override
    public List<MessageVO> getHotMessageList(Long categoryId) {
        checkMessageCategory(categoryId);
        // 如果categoryId为null，表示取所有类别下热门讯息；
        List<Message> messageList = messageMapper.selectByHotAndCategoryId(true, categoryId);
        // messageList -> messageVOList
        List<MessageVO> messageVOList = new ArrayList<MessageVO>();
        for (Message message : messageList) {
            MessageVO messageVO = new MessageVO(message.getId(), message.getCreateTime().getTime());
            BeanUtils.copyProperties(message, messageVO);
            messageVO.setUser(userService.getUserByUserId(message.getAuthorId()));
            // 设置视频链接地址与视频描述
            if (message.getMediaType().equals(1)) {
                messageVO.setVideoLinkUrl(message.getVideoLinkUrl());
                messageVO.setDescription(messageDetailMapper.selectByMessageId(message.getId()).getContent());
            }
            // 通过messageId获取messageSupportCommentHitStat
            MessageSupportCommentHitStat mschs = messageSupportCommentHitStatMapper.selectByMessageId(message.getId());
            // 复制hits，supports，comments的属性值
            BeanUtils.copyProperties(mschs, messageVO);
            // 设置所属类别名称
            Category c = categoryMapper.selectByPrimaryKey(message.getCategoryId());
            messageVO.setCategoryName(c == null ? "讯息类别不存在" : c.getName());
            // 设置精选评论
            List<MessageCommentSimpleVO> messageCommentSimpleVOList = messageCommentService.getExcellentMessageComment(message.getId());
            messageVO.setExcellentComments(messageCommentSimpleVOList);
            messageVOList.add(messageVO);
        }
        return messageVOList;
    }

    // 添加一条讯息
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addOneMessage(MessageRequestVO messageRequestVO) {
        User user = userMapper.selectByPrimaryKey(messageRequestVO.getUserId());
        if (user == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        Message msg = new Message();
        BeanUtils.copyProperties(messageRequestVO, msg);
        MessageDetail msgD = new MessageDetail();
        msg.setCreateTime(new Date());
        msgD.setContent(messageRequestVO.getContent());
        msgD.setVideoLinkUrl(messageRequestVO.getVideoLinkUrl());
        messageMapper.insertSelective(msg);
        msgD.setMessageId(msg.getId());
        MessageSupportCommentHitStat messageSupportCommentHitStat = new MessageSupportCommentHitStat();
        messageSupportCommentHitStat.setMessageId(msg.getId());
        if (null != msg.getId() || msg.getId() != 0) {
            messageDetailMapper.insertSelective(msgD);
            messageSupportCommentHitStatMapper.insertSelective(messageSupportCommentHitStat);
        } else {
            throw new ApplicationException("新增失败");
        }
        List<Long> userIdList = attentionMapper.selectIdListByToUserId(user.getUserId());
        String subtitle = StringUtils.isBlank(msg.getTitle()) ? "点击查看" : "《" + msg.getTitle() + "》 点击查看";
//        drawPush.fansPush(userIdList, SubjectType.MESSAGE_COMMENT, msg.getId(), user.getNickname() + "发布了新文章", subtitle, msg.getMediaType());
    }

    @Override
    public void modifyApproveState(Long messageId, Integer approveState) {
        Message message = new Message();
        message.setId(messageId);
        message.setApproveState(approveState);
        int res = this.messageMapper.updateByPrimaryKeySelective(message);
        if (res != 1) {
            throw new ApplicationException(885, "修改讯息审核状态异常");
        }
    }

    @Override
    public void modifyAuthorId(Long messageId, Long newAuthorId) {
        Message message = new Message();
        message.setId(messageId);
        message.setAuthorId(newAuthorId);
        int res = this.messageMapper.updateByPrimaryKeySelective(message);
        if (res != 1) {
            throw new ApplicationException(885, "修改讯息审核状态异常");
        }
    }

    // 讯息修改 可以对讯息的标题，内容，标签图片，所属类别，是否推荐，媒体类型，优先级，视频链接地址（video_link_url）
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateMessage(MessageRequestVO messageRequestVO) {
        logger.info("update message ...");
        if (messageRequestVO == null) {
            throw new ApplicationException(400, "入参错误");
        }
        if (messageRequestVO.getMessageId() == null) {
            throw new ApplicationException(852, "讯息id不能为空");
        }
        if (StringUtils.isBlank(messageRequestVO.getTitle())) {
            throw new ApplicationException(856, "讯息标题不得为空");
        }
        if (StringUtils.isBlank(messageRequestVO.getContent())) {
            throw new ApplicationException(860, "讯息内容不得为空");
        }
        if (StringUtils.isBlank(messageRequestVO.getPic())) {
            throw new ApplicationException(864, "讯息标签图片不得为空");
        }
        if (messageRequestVO.getMediaType() > 1) {
            throw new ApplicationException(868, "讯息的媒体类别为空");
        }
        if (!messageRequestVO.getMediaType().equals(0) && !messageRequestVO.getMediaType().equals(1)) {
            throw new ApplicationException(872, "讯息的媒体类型必须为图文或视频");
        }
        if (messageRequestVO.getPriority() == null) {
            throw new ApplicationException(874, "讯息的优先级不得为空");
        }
        if (messageRequestVO.getPriority() <= 0.0) {
            throw new ApplicationException(878, "讯息的优先级必须为大于0的数");
        }
        if (messageRequestVO.getIsRecommend() == null) {
            throw new ApplicationException(886, "讯息的是否推荐不得为NULL");
        }
        if (messageRequestVO.getCategoryId() == null) {
            throw new ApplicationException(828, "讯息类别id不得为空");
        }
        if (this.categoryMapper.selectByPrimaryKey(messageRequestVO.getCategoryId()) == null) {
            throw new ApplicationException(820, "讯息类别不存在");
        }
        if (messageRequestVO.getVideoLinkUrl() == null) {
            throw new ApplicationException(882, "视频链接地址不得为NULL");
        }
        if (messageRequestVO.getUserId() == null) {
            throw new ApplicationException(883, "讯息作者不得为NULL");
        }
        if (messageRequestVO.getApproveState() == null) {
            throw new ApplicationException(884, "审核状态不得为NULL");
        }
        // 修改表t_message
        Message message = new Message();
        BeanUtils.copyProperties(messageRequestVO, message);
        message.setId(messageRequestVO.getMessageId());
        this.messageMapper.updateByPrimaryKeySelective(message);
        MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(messageRequestVO.getMessageId());
        if (messageDetail == null) {
            throw new ApplicationException(890, "该讯息的讯息详情不存在");
        }
        BeanUtils.copyProperties(messageRequestVO, messageDetail);
        this.messageDetailMapper.updateByPrimaryKeySelective(messageDetail);
    }

    // 删除讯息，支持批量删除
    @Override
    @Transactional
    public void deleteMessage(List<Long> messageIdList) {
        // 删除t_message
        this.messageMapper.deleteByPrimaryKeyIn(messageIdList);
        // 删除t_message_detail
        this.messageDetailMapper.deleteByMessageIdIn(messageIdList);
        // 删除t_message_support_comment_hit_stat
        this.messageSupportCommentHitStatMapper.deleteByMessageIdIn(messageIdList);
        // 删除t_message_support
        this.messageSupportMapper.deleteByMessageIdIn(messageIdList);
        // 删除t_messagecomment_support
        List<MessageComment> messageCommentList = this.messageCommentMapper.selectByMessageIdIn(messageIdList);
        List<Long> messageCommentIdList = messageCommentList.stream().map(MessageComment::getId).collect(Collectors.toList());
        if (!messageCommentIdList.isEmpty()) {
            this.messageCommentSupportMapper.deleteByMessageCommentIdIn(messageCommentIdList);
        }
        // 删除t_message_comment
        if (!messageIdList.isEmpty()) {
            this.messageCommentMapper.deleteByMessageIdIn(messageIdList);
        }
    }

    @Override
    public MessageModelVO getMessageById(Long messageId) {
        Message message = this.messageMapper.selectByPrimaryKey(messageId);
        MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(messageId);
        MessageModelVO messageModelVO = new MessageModelVO();
        BeanUtils.copyProperties(message, messageModelVO);
        BeanUtils.copyProperties(messageDetail, messageModelVO);
        messageModelVO.setMessageId(messageId);
        return messageModelVO;
    }

    @Override
    public List<MessageVO> getAllMessage() {
        List<Message> messages = messageMapper.getAllMessage();
        List<MessageVO> messageVOS = new ArrayList<>();
        for (Message message : messages) {
            MessageVO messageVO = new MessageVO();
            messageVO.setMessageId(message.getId());
            messageVO.setTitle(message.getTitle());
            messageVOS.add(messageVO);
        }
        return messageVOS;
    }

    @Override
    public PageResult<MessageVO> getMessageList(PageRequest pageRequest) {
        // 总记录数量
        Integer totalRecords = this.messageMapper.selectCount();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "create_time desc");
        List<Message> messageList = this.messageMapper.selectAll();
        // 取出所有的messageID
        List<Long> messageIdList = new ArrayList<Long>();
        for (Message message : messageList) {
            messageIdList.add(message.getId());
        }
        // 构建map容器，messageId <-> messageSupportCommentHitStat
        List<MessageSupportCommentHitStat> messageSupportCommentHitStatList = this.messageSupportCommentHitStatMapper.selectByMessageIdIn(messageIdList);
        Map<Long, MessageSupportCommentHitStat> messageSupportCommentHitStatMap = new HashMap<>();
        for (MessageSupportCommentHitStat messageSupportCommentHitStat : messageSupportCommentHitStatList) {
            messageSupportCommentHitStatMap.put(messageSupportCommentHitStat.getMessageId(), messageSupportCommentHitStat);
        }
        List<MessageVO> messageVOList = new ArrayList<MessageVO>();
        Map<Long, MessageCategoryVO> messageCategoryVOMap = this.getMessageCategoryMap();
        // 构造messageVO
        for (Message message : messageList) {
            MessageVO messageVO = new MessageVO();
            BeanUtils.copyProperties(message, messageVO);
            messageVO.setMessageId(message.getId());
            // 设置视频链接地址，如果媒体类型为“图文”，则为空字符串；如果媒体类型为“视频”，则是一个具体的视频地址
            Integer mediaType = message.getMediaType();
            if (mediaType.equals(1)) {
                messageVO.setVideoLinkUrl(this.messageDetailMapper.selectVideoLinkUrlByMessageId(message.getId()));
                messageVO.setDescription(this.messageDetailMapper.selectByMessageId(message.getId()).getContent());
            }
            MessageSupportCommentHitStat messageSupportCommentHitStat = messageSupportCommentHitStatMap.get(message.getId());
            // 复制hits，supports，comments的属性值
            BeanUtils.copyProperties(messageSupportCommentHitStat, messageVO);
            // 设置所属类别名称
            messageVO.setCategoryName(messageCategoryVOMap.get(message.getCategoryId()).getName());
            // 设置时间戳
            messageVO.setCreateTime(message.getCreateTime().getTime());
            messageVOList.add(messageVO);
        }
        return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), messageVOList);
    }

    /***
     * @author 黄良樑
     * @param pageRequest 分页请求参数
     * @param title 标题
     * @return
     */
    @Override
    public com.xy.zizoumohe.bean.PageResult<Message, MessageVO> messageList(PageRequest pageRequest, String title, Integer audit) {
        // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "create_time desc");
        List<Message> messageList = this.messageMapper.selectByTitle("%" + title + "%", audit);
        com.github.pagehelper.PageInfo pageInfo = new com.github.pagehelper.PageInfo(messageList);
        // 取出所有的messageID
        List<Long> messageIdList = new ArrayList<Long>();
        for (Message message : messageList) {
            messageIdList.add(message.getId());
        }
        List<MessageSupportCommentHitStat> messageSupportCommentHitStatList = new ArrayList<>();
        // 构建map容器，messageId <-> messageSupportCommentHitStat
        if (messageIdList.size() > 0) {
            messageSupportCommentHitStatList = this.messageSupportCommentHitStatMapper.selectByMessageIdIn(messageIdList);
        }
        Map<Long, MessageSupportCommentHitStat> messageSupportCommentHitStatMap = new HashMap<Long, MessageSupportCommentHitStat>();
        for (MessageSupportCommentHitStat messageSupportCommentHitStat : messageSupportCommentHitStatList) {
            messageSupportCommentHitStatMap.put(messageSupportCommentHitStat.getMessageId(), messageSupportCommentHitStat);
        }
        List<MessageVO> messageVOList = new ArrayList<MessageVO>();
        Map<Long, MessageCategoryVO> messageCategoryVOMap = this.getMessageCategoryMap();
        // 构造messageVO
        for (Message message : messageList) {
            MessageVO messageVO = new MessageVO();
            BeanUtils.copyProperties(message, messageVO);
            messageVO.setMessageId(message.getId());
            // 设置视频链接地址，如果媒体类型为“图文”，则为空字符串；如果媒体类型为“视频”，则是一个具体的视频地址
            if (message.getMediaType().equals(1)) {
                MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(message.getId());
                messageVO.setVideoLinkUrl(messageDetail.getVideoLinkUrl());
                messageVO.setDescription(messageDetail.getContent());
            }
            MessageSupportCommentHitStat messageSupportCommentHitStat = messageSupportCommentHitStatMap.get(message.getId());
            // 复制hits，supports，comments的属性值
            BeanUtils.copyProperties(messageSupportCommentHitStat, messageVO);
            // 设置所属类别名称
            messageVO.setCategoryName(messageCategoryVOMap.get(message.getCategoryId()).getName());
            messageVO.setCreateTime(message.getCreateTime().getTime());
            messageVOList.add(messageVO);
        }
        //return new PageResult<MessageVO>(totalRecords,totalPages,pageRequest.getPageNum(),messageVOList);
        return new com.xy.zizoumohe.bean.PageResult<>(messageList, messageVOList);
    }

    @Override
    //推送消息
    public void pushMessage(Integer id, String title, String subhead, String body) {
        Message message = messageMapper.selectByPrimaryKey(id.longValue());
        PushAll pushAll = new PushAll();
        pushAll.sendBroadcast(PushBean.getInstance().setSubjectId(id.longValue()).setTitle("", BehaviorType.SUPPORT, SubjectType.MESSAGE_COMMENT).setMediaType(message.getMediaType()).setCategoryId(message.getCategoryId()), title, subhead, body);
    }

    @Override
    public Integer getMediaType(Long messageId) {
        Message message = messageMapper.selectByPrimaryKey(messageId);
        return message.getMediaType();
    }

    /**
     * 获取推荐的讯息列表，区分版块 version(2.2.5)
     *
     * @param boardType   版块类别
     * @param userId      当前登录用户的id
     * @param pageRequest 请求参数
     * @return pageResult
     */
    @Override
    public com.xy.zizoumohe.bean.PageResult<MessageModel, MessageVO> getRecommendMessageListAndBoardType(List<Integer> boardType, Long userId, PageRequest pageRequest) {
        if (boardType == null || pageRequest == null) {
            throw new ApplicationException(400, "参数异常");
        }
        // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize());
        List<MessageModel> messageList = this.messageMapper.selectByIsRecommendAndApproveStateAndBoardType(1, boardType);
        // messageModelList -> messageVOList
        List<MessageVO> messageVOList = new ArrayList<MessageVO>();
        Map<Long, MessageCategoryVO> messageCategoryVOMap = this.getMessageCategoryMap(); // 所有的讯息类别；id <-> MessageCategoryVO
        for (MessageModel messageModel : messageList) {
            MessageVO messageVO = new MessageVO(messageModel.getId(), messageModel.getCreateTime().getTime());
            BeanUtils.copyProperties(messageModel, messageVO);
            messageVO.setUser(this.userService.getUserByUserId(messageModel.getAuthorId()));
            if (messageModel.getMediaType().equals(1)) {
                MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(messageModel.getId());
                messageVO.setVideoLinkUrl(messageDetail.getVideoLinkUrl());
                messageVO.setDescription(messageDetail.getContent());
            }
            // 设置所属类别名称
            messageVO.setCategoryName(messageCategoryVOMap.get(messageModel.getCategoryId()).getName()); // 设置讯息类别
            if (userId != null) {
                messageVO.setAttentionState(attentionService.getAttentionState(messageModel.getAuthorId(), userId));
            }
            messageVOList.add(messageVO);
        }
        return new com.xy.zizoumohe.bean.PageResult<>(messageList, messageVOList);
    }

    /**
     * 讯息点赞
     *
     * @param messageSupportVO messageSupportVO
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addMessageSupport(MessageSupportVO messageSupportVO) {
        if (messageSupportVO == null || messageSupportVO.getMessageId() == null || messageSupportVO.getUserId() == null) {
            throw new ApplicationException(400, "请求参数异常");
        }
        // 判断用户是否存在
        Long userId = messageSupportVO.getUserId(); // 点赞人的id
        User user = this.userMapper.selectByPrimaryKey(userId); // 点赞人
        if (user == null) {
            throw new ApplicationException(800, "当前用户不存在");
        }
        // 判断讯息是否存在
        if (this.messageMapper.selectByPrimaryKey(messageSupportVO.getMessageId()) == null) {
            throw new ApplicationException(803, "讯息不存在");
        }
        // 检查该用户之前是否对该条讯息进行点赞过
        MessageSupport messageSupport = this.messageSupportMapper.selectByMessageIdAndUserId(
                messageSupportVO.getMessageId(), messageSupportVO.getUserId());
        if (messageSupport != null) {
            throw new ApplicationException(1003, "您已经点赞过，请勿重复点赞");
        }
        messageSupport = new MessageSupport();
        BeanUtils.copyProperties(messageSupportVO, messageSupport);
        messageSupport.setSupportTime(new Date());
        // 讯息点赞表添加一条记录
        this.messageSupportMapper.insertSelective(messageSupport);
        // 讯息点赞评论点击量统计表更新一条记录
        this.messageSupportCommentHitStatMapper.updateSupportsAutoIncrement(messageSupportVO.getMessageId());
        // A 点赞 B的讯息，A的卡路里+5，最多加10次  (当前需求改成1次); B的讯息被赞，当前需求，B是系统管理员，无需增加卡路里
        this.caloryService.initCalory(userId);
        Date currentDate = new Date();
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(
                userId, 14, simpleDateFormat.format(currentDate)); // tradeType为14点赞讯息
        // 生成A的卡路里交易账单，只有生成交易单，才能增加用户的卡路里余额
        if (caloryTradeList.size() < 1) { // 10改为1
            CaloryTrade caloryTrade = new CaloryTrade(userId, true,
                    TradeTypeConst.TRADE_TYPE_FOURTEEN, 3, currentDate, 14);
            this.caloryTradeMapper.insertSelective(caloryTrade);
            // 增加A的卡路里财富值
            this.caloryMapper.increaseByUserId(userId, 3);
        }
    }

    /**
     * 讯息评论点赞
     *
     * @param messageCommentId 讯息评论id
     * @param userId           点赞人的id
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addMessageCommentSupport(Long messageCommentId, Long userId) {
        if (messageCommentId == null || userId == null) {
            throw new ApplicationException("请求参数异常");
        }
        User user = this.userMapper.selectByPrimaryKey(userId); // 点赞人
        if (user == null) {
            throw new ApplicationException(800, "用户不存在");
        }
        MessageComment messageComment = this.messageCommentMapper.selectByPrimaryKey(messageCommentId);
        if (messageComment == null) {
            throw new ApplicationException(843, "讯息评论不存在");
        }
        // 检查该用户之前是否对该条讯息进行点赞过
        MessageCommentSupport messageCommentSupport = this.messageCommentSupportMapper.selectByMessageCommentIdAndUserId(
                messageCommentId, userId);
        if (messageCommentSupport != null) {
            throw new ApplicationException("您已经点赞过，请勿重复点赞");
        }
        messageCommentSupport = new MessageCommentSupport(messageCommentId, userId);
        // 讯息评论点赞表添加一条记录
        this.messageCommentSupportMapper.insertSelective(messageCommentSupport);
        // 讯息评论表中点赞数加1
        this.messageCommentMapper.updateSupportsAutoIncrement(messageCommentId);
        Message message = messageMapper.selectByPrimaryKey(messageComment.getMessageId());
        if (!messageComment.getUserId().equals(userId)) { // 判断讯息评论的作者与点赞人是否为同一人
            // 讯息评论的作者获赞，增加卡路里+2
            this.caloryService.initCalory(messageComment.getUserId());
            this.acquireSupportHelper.handlerCaloryForAcquireSupport(messageComment.getUserId(), new Date(),
                    this.propertyConfig.acquireSupportsLimits);
            // 友盟推送
            pushHelper(messageComment.getUserId(), messageCommentId, messageComment.getMessageId(), BehaviorType.SUPPORT,
                    SubjectType.MESSAGE_COMMENT, user.getNickname(), message.getMediaType(), message.getCategoryId());
        }

    }

    //友盟推送
    private void pushHelper(Long id, Long contentId, Long subjectId, BehaviorType behaviorType, SubjectType subjectType, String nickName, Integer mediaType, Long categoryId) {
        Notification notification = new Notification();
        notification.sendIOSUnicast(id, PushBean.getInstance().setContentId(contentId).
                setSubjectId(subjectId).setTitle(nickName, behaviorType, subjectType).setMediaType(mediaType).setCategoryId(categoryId));
    }

    // 获取当前正在使用的轮播图列表
    @Override
    public List<SlidePictureVO> getCurrentUseSlidePictureList() {
        List<SlidePicture> slidePictureList = this.slidePictureMapper.selectByCurrentUse();
        // slidePictureList -> slidePictureVOList
        return slidePictureList.stream().map(e -> {
            SlidePictureVO slidePictureVO = new SlidePictureVO(e.getId(), e.getCreateTime().getTime());
            BeanUtils.copyProperties(e, slidePictureVO);
            // 设置媒体类型
            Message linkMessage = this.messageMapper.selectByPrimaryKey(e.getLinkMessageId()); //链接的讯息
            slidePictureVO.setMediaType(linkMessage.getMediaType());
            return slidePictureVO;
        }).collect(Collectors.toList());
    }

    // 添加轮播图;如果前端不设置isCurrentUse，默认为false;如果前端不设置sort的值，默认为100
    @Override
    public void addSlidePicture(SlidePictureRequestVO slidePictureRequestVO) {
        if (slidePictureRequestVO == null) {
            throw new ApplicationException(400, "请求参数异常...");
        }
        if (StringUtils.isBlank(slidePictureRequestVO.getPath())) {
            throw new ApplicationException(810, "轮播图的图片地址为空");
        }
        if (slidePictureRequestVO.getLinkMessageId() == null) {
            throw new ApplicationException(811, "轮播图链接的messageId不得为空");
        }
        SlidePicture slidePicture = new SlidePicture();
        BeanUtils.copyProperties(slidePictureRequestVO, slidePicture);
        if (slidePictureRequestVO.getIsCurrentUse() == null) {
            slidePicture.setIsCurrentUse(false);
        }
        if (slidePictureRequestVO.getLinkMessageUrl() == null) {
            slidePicture.setLinkMessageUrl("");
        }
        slidePicture.setCreateTime(new Date()); // 设置时间
        this.slidePictureMapper.insertSelective(slidePicture);
    }

    // 删除轮播图
    @Override
    public void deleteSlidePicture(List<Long> slidePictureIdList) {
        this.slidePictureMapper.deleteByPrimaryKeyIn(slidePictureIdList);
    }

    // 修改轮播图;修改轮播图的名称，sort的值，isCurrentUse,linkMessageUrl等;轮播图创建时间不能修改
    @Override
    public void updateSlidePicture(SlidePictureRequestVO slidePictureRequestVO) {
        if (slidePictureRequestVO.getSlidePictureId() == null) {
            throw new ApplicationException(400, "请求参数异常...");
        }
        SlidePicture slidePicture = new SlidePicture();
        BeanUtils.copyProperties(slidePictureRequestVO, slidePicture);
        slidePicture.setId(slidePictureRequestVO.getSlidePictureId());
        this.slidePictureMapper.updateByPrimaryKeySelective(slidePicture);
    }

    // 获取所有的轮播图
    @Override
    public List<SlidePictureVO> getSlidePictureList() {
        List<SlidePicture> slidePictureList = this.slidePictureMapper.selectAll();
        // slidePictureList -> slidePictureVOList
        return slidePictureList.stream().map(e -> {
            SlidePictureVO slidePictureVO = new SlidePictureVO();
            BeanUtils.copyProperties(e, slidePictureVO);
            slidePictureVO.setSlidePictureId(e.getId());
            slidePictureVO.setCreateTime(e.getCreateTime().getTime());
            return slidePictureVO;
        }).collect(Collectors.toList());
    }

    //修改是否启用
    @Override
    public void setIsCurrentUse(Boolean isCurrentUse, Long id) {
        int state = slidePictureMapper.setIsCurrentUse(isCurrentUse, id);
        if (state < 1) {
            throw new ApplicationException("修改失败");
        }
    }

    /**
     * 检查讯息类别是否存在
     *
     * @param messageCategoryId 讯息类别id
     */
    private void checkMessageCategory(Long messageCategoryId) {
        if (messageCategoryId != null) {
            Category category = this.categoryMapper.selectByPrimaryKey(messageCategoryId);
            if (category == null) {
                throw new ApplicationException(820, "讯息类别不存在");
            }
        }
    }

    @Override
    public Integer checkSupportMessageTaskComplete(Long userId, Date date) {
        String dateStr = simpleDateFormat.format(date); // signDate签到日期字符串，格式2019-06-19
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(userId, 14, dateStr);
        if (caloryTradeList.size() < 1) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public Integer checkCommentMessageTaskComplete(Long userId, Date date) {
        String dateStr = simpleDateFormat.format(date); // signDate签到日期字符串，格式2019-06-19
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndDate(userId, 12, dateStr);
        if (caloryTradeList.size() < 1) {
            return 0;
        } else {
            return 1;
        }
    }


    // 获取发布的讯息列表
    @Override
    public PageResult<MessageVO> getPublishMessageList(Long currentLoginUserId, Long userId, PageRequest pageRequest) {

        if (userId == null) {
            throw new ApplicationException("参数异常");
        }

        List<Message> messageList = null;
        PageUtils.PageResponse pageResponse = null;
        Integer totalRecords = 0;
        if (currentLoginUserId != null) {
            if (currentLoginUserId.equals(userId)) {// 获取我发布文章
                totalRecords = messageMapper.selectByAuthorId(userId).size();
                pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
                // 请求的页码数字 > 总的页数
                if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
                    return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                            pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
                }
                // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
                PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize());
                messageList = messageMapper.selectByAuthorId(userId);
            } else { // 获取TA发布的文章
                totalRecords = messageMapper.selectByAuthorIdAndApproveState(userId, 1).size();
                pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
                // 请求的页码数字 > 总的页数
                if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
                    return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                            pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
                }
                // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
                PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize());
                messageList = messageMapper.selectByAuthorIdAndApproveState(userId, 1);
            }
        } else { // 获取TA发布的文章
            totalRecords = messageMapper.selectByAuthorIdAndApproveState(userId, 1).size();
            pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
            // 请求的页码数字 > 总的页数
            if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
                return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                        pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
            }
            // 执行分页，pageHelper默认对紧跟在后面的sql语句进行分页
            PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize());
            messageList = messageMapper.selectByAuthorIdAndApproveState(userId, 1);
        }

        // transfer from messageList to messageVOList
        List<MessageVO> messageVOList = new ArrayList<MessageVO>();
        Map<Long, MessageCategoryVO> messageCategoryVOMap = this.getMessageCategoryMap();
        // 构造messageVO
        for (Message message : messageList) {
            MessageVO messageVO = new MessageVO();
            BeanUtils.copyProperties(message, messageVO);
            messageVO.setMessageId(message.getId());
            // 设置视频链接地址，如果媒体类型为“图文”，则为空字符串；如果媒体类型为“视频”，则是一个具体的视频地址
            if (message.getMediaType().equals(1)) {
                MessageDetail messageDetail = this.messageDetailMapper.selectByMessageId(message.getId());
                messageVO.setVideoLinkUrl(messageDetail.getVideoLinkUrl());
                messageVO.setDescription(messageDetail.getContent());
            }

            // 设置所属类别名称
            messageVO.setCategoryName(messageCategoryVOMap.get(message.getCategoryId()).getName());
            messageVO.setCreateTime(message.getCreateTime().getTime());
            messageVOList.add(messageVO);
        }

        return new PageResult<MessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), messageVOList);
    }


}

package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.mapper.CaloryMapper;
import com.xy.zizoumohe.mapper.TalentApplyForMapper;
import com.xy.zizoumohe.mapper.TalentMapper;
import com.xy.zizoumohe.mapper.UserOrnamentMapper;
import com.xy.zizoumohe.service.AttentionService;
import com.xy.zizoumohe.service.TalentService;
import com.xy.zizoumohe.utils.Helper;
import com.xy.zizoumohe.vo.TalentVO;
import com.xy.zizoumohe.vo.TitleVO;
import com.xy.zizoumohe.vo.UserVO;
import com.xy.zizoumohe.vo.request.TalentApplyForRequestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TalentServiceImpl implements TalentService {
    @Autowired
    TalentMapper talentMapper;
    @Autowired
    TalentApplyForMapper talentApplyForMapper;
    @Autowired
    Helper helper;
    @Autowired
    private UserOrnamentMapper userOrnamentMapper;
    @Autowired
    AttentionService attentionService;
    @Autowired
    CaloryMapper caloryMapper;

    @Override
    public PageResult talentList(Integer pageNum, Integer pageSize, Integer gameType, Long userId) {
        PageHelper.startPage(pageNum, pageSize, "`rank` asc");
        List<Talent> talentList = talentMapper.selectByGameType(gameType);
        return talentListBase(pageNum, pageSize, userId, talentList);
    }

    private PageResult talentListBase(Integer pageNum, Integer pageSize, Long userId, List<Talent> talentList) {
        List<TalentVO> talentVOList = new ArrayList<>(pageSize);
        List<Long> userIdList = new ArrayList<>(pageSize);
        talentList.forEach(talent -> {
            userIdList.add(talent.getUserId());
        });
        List<UserOrnament> userOrnaments = new ArrayList<>();
        if (userIdList.size() > 0) {
            userOrnaments = userOrnamentMapper.selectTitleByUserIds(userIdList);
        }

        for (Talent talent : talentList) {
            List<TitleVO> titleVOS = new ArrayList<>();
            TalentVO talentVO = new TalentVO();
            talentVO.setAttentionState(userId == null ? 0 : attentionService.getAttentionState(talent.getUserId(), userId));
            UserVO userVO = new UserVO();
            talentVO.setUserVO(userVO);
            talentVO.setCaloryNum(talent.getCaloryNum());
            BeanUtils.copyProperties(talent, talentVO);
            talentVO.setFans(talent.getFans() != null ? talent.getFans() : attentionService.getFans(talent.getUserId()));
            helper.titleAppend(titleVOS, userOrnaments, talent.getUserId());
            if (talent.getUser() != null) {
                BeanUtils.copyProperties(talent.getUser(), talentVO.getUserVO());
            }
            if (titleVOS.size() > 0) {
                talentVO.getUserVO().setTitleList(titleVOS);
            }
            talentVOList.add(talentVO);
        }
        return new PageResult<>(talentList, talentVOList);
    }

    @Override
    public void talentApplyFor(TalentApplyForRequestVO talentApplyForRequestVO) {
        TalentApplyFor talentApplyFor = new TalentApplyFor();
        BeanUtils.copyProperties(talentApplyForRequestVO, talentApplyFor);
        talentApplyForMapper.insertSelective(talentApplyFor);
    }

    @Override
    public PageResult caloryTalentList(Integer pageNum, Integer pageSize, Long userId) {
        PageHelper.startPage(pageNum, pageSize, "calory_num desc");
        List<Calory> caloryList = caloryMapper.selectAllLinkUser();
        List<Talent> talents = new ArrayList<>(caloryList.size());
        for (Calory calory : caloryList) {
            Talent talent = new Talent();
            talent.setUserId(calory.getUserId());
            talent.setCreateTime(calory.getCreateTime());
            talent.setUser(calory.getUser());
            talent.setCaloryNum(calory.getCaloryNum());
            talents.add(talent);
        }
        return talentListBase(pageNum, pageSize, userId, talents);
    }

    @Override
    public PageResult fansTalentList(Integer pageNum, Integer pageSize, Long userId) {
        PageHelper.startPage(pageNum, pageSize, "fansNum DESC ");
        List<Attention> attentionList = attentionService.fansTalentList();
        List<Talent> talents = new ArrayList<>(attentionList.size());
        for (Attention attention : attentionList) {
            Talent talent = new Talent();
            talent.setUserId(attention.getUserId());
            talent.setUser(attention.getUser());
            talent.setFans(attention.getFansNum());
            talents.add(talent);
        }
        return talentListBase(pageNum, pageSize, userId, talents);
    }
}

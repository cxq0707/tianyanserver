package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.Topic;
import com.xy.zizoumohe.mapper.TopicMapper;
import com.xy.zizoumohe.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {
    @Autowired
    TopicMapper topicMapper;
    @Override
    public List<Topic> getAllTopicConsole() {
        return topicMapper.selectAll();
    }
}

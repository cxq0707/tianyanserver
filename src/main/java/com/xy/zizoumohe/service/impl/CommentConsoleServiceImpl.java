package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.bean.CommentConsoleBean;
import com.xy.zizoumohe.entity.MessageComment;
import com.xy.zizoumohe.mapper.MessageCommentMapper;
import com.xy.zizoumohe.service.CommentConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentConsoleServiceImpl implements CommentConsoleService {
    @Autowired
    private MessageCommentMapper messageCommentMapper;

    @Override
    public void updateMessageComment(CommentConsoleBean commentConsoleBean) {
        MessageComment comment = new MessageComment();
        comment.setId(commentConsoleBean.getCommentId());
        comment.setCommentContent(commentConsoleBean.getCommentContent());
        comment.setCommentImageList(arrayHelper(commentConsoleBean.getCommentImageList()));
        messageCommentMapper.updateComment(comment);

    }
    private String arrayHelper(List<String> list) {
        if (list == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (String string : list) {
            sb.append(string).append(",");
        }
        return sb.toString();
    }
}

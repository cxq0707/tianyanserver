package com.xy.zizoumohe.service.impl;


import com.xy.zizoumohe.entity.Download;
import com.xy.zizoumohe.mapper.DownloadMapper;
import com.xy.zizoumohe.service.DownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DownloadServiceImpl implements DownloadService {

    @Autowired
    DownloadMapper downloadMapper;

    @Override
    public void create(String type) {
        Download download = new Download();
        download.setType(type);
        downloadMapper.insertSelective(download);
    }

    @Override
    public Integer stat(String startTime, String endTime, String type) {
        if (startTime == null || endTime == null) {
            return 0;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH时m分s秒");
            Date startDate = sdf.parse(startTime);
            Date endDate = sdf.parse(endTime);
            return downloadMapper.selectByTimeInterval(startDate, endDate, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<String> getType() {
        return downloadMapper.selectGroupByType();
    }
}

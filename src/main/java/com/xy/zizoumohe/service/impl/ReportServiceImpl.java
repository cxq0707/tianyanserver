package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.Report;
import com.xy.zizoumohe.mapper.ReportMapper;
import com.xy.zizoumohe.service.ReportService;
import com.xy.zizoumohe.vo.request.ReportRequestVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    ReportMapper reportMapper;
    @Override
    public void reportUser(ReportRequestVO reportRequestVO) {
        Report report = new Report();
        BeanUtils.copyProperties(reportRequestVO,report);
        reportMapper.insertSelective(report);
    }
}

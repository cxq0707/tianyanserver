package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.entity.Message;
import com.xy.zizoumohe.entity.MessageSupportCommentHitStat;
import com.xy.zizoumohe.entity.Store;
import com.xy.zizoumohe.entity.User;
import com.xy.zizoumohe.mapper.*;
import com.xy.zizoumohe.model.StoreModel;
import com.xy.zizoumohe.service.StoreService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.vo.StoreMessageVO;
import com.xy.zizoumohe.vo.UserVO;
import com.xy.zizoumohe.vo.request.StoreRequestVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class StoreServiceImpl implements StoreService {

    private static Logger logger = LoggerFactory.getLogger(StoreServiceImpl.class);
    @Autowired
    StoreMapper storeMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    MessageMapper messageMapper;
    @Autowired
    MessageDetailMapper messageDetailMapper;
    @Autowired
    MessageSupportCommentHitStatMapper messageSupportCommentHitStatMapper;

    // 添加收藏
    @Override
    public void addStore(StoreRequestVO storeRequestVO) {
        // 判断当前用户是否存在
        if (this.userMapper.selectByPrimaryKey(storeRequestVO.getUserId()) == null) {
            throw new ApplicationException(800, "当前用户不存在");
        }
        // 判断阵容是否存在/信息是否存在
        if (storeRequestVO.getStoreCategoryId() == 2) {
            // 类别为讯息
            if (this.messageMapper.selectByPrimaryKey(storeRequestVO.getEntityId()) == null) {
                throw new ApplicationException(803, "讯息不存在");
            }
        } else {
            throw new ApplicationException(801, "收藏的类别不存在");
        }
        // 判断是否重复收藏
        if (this.storeMapper.selectByStoreCategoryIdAndUserIdAndEntityId(storeRequestVO.getStoreCategoryId(),
                storeRequestVO.getUserId(), storeRequestVO.getEntityId()) != null) {
            throw new ApplicationException(804, "您已收藏过，请勿重复收藏");
        }
        Store store = new Store();
        BeanUtils.copyProperties(storeRequestVO, store);
        store.setStoreTime(new Date());
        // 保存到表t_store中
        this.storeMapper.insertSelective(store);
    }

    /**
     * 删除收藏.
     *
     * @param idList             收藏id的集合
     * @param currentLoginUserId 当前登录用户的id
     */
    @Override
    @Transactional
    public void deleteStore(List<Long> idList, Long currentLoginUserId) {
        if (idList.size() == 0) {
            return;
        }
        //判断当前登录用户是否存在
        if (this.userMapper.selectByPrimaryKey(currentLoginUserId) == null) {
            throw new ApplicationException(800, "用户不存在");
        }

        for (Long id : idList) {
            if (this.storeMapper.selectByPrimaryKey(id) == null) {
                throw new ApplicationException(960, "您删除的收藏已不存在");
            }
            // 当前用户是否持有此收藏
            if (this.storeMapper.selectByPrimaryKeyAndUserId(id, currentLoginUserId) == null) {
                throw new ApplicationException(964, "该用户没有此收藏");
            }
            int result = this.storeMapper.deleteByPrimaryKey(id);
            if (result == 1) {
                return;
            } else {
                throw new ApplicationException(962, "删除收藏异常");
            }
        }
    }

    /**
     * 获取收藏列表
     *
     * @param storeCategoryId    收藏类别id，其中1表示阵容，2表示讯息
     * @param currentLoginUserId 当前登录的用户id
     * @param type               type为游戏类型，收藏阵容时，有游戏类型的区分
     * @param pageRequest        分页请求对象
     * @return pageResult分页结果
     */
    @Override
    public PageResult getStoreList(Long storeCategoryId, Long currentLoginUserId, Integer type, PageRequest pageRequest) {
        // 判断用户是否存在
        if (this.userMapper.selectByPrimaryKey(currentLoginUserId) == null) {
            throw new ApplicationException(800, "当前用户不存在");
        }
        // 获取收藏的阵容
        if (storeCategoryId == 2) {
            PageResult<StoreMessageVO> pageResult = this.getStoreMessageList(storeCategoryId, currentLoginUserId, pageRequest);
            return pageResult;
        } else {
            throw new ApplicationException(801, "收藏类别不存在");
        }
    }
    // 获取收藏的讯息列表
    private PageResult<StoreMessageVO> getStoreMessageList(Long storeCategoryId, Long currentLoginUserId, PageRequest pageRequest) {
        Integer totalRecords = this.storeMapper.selectByStoreCategoryIdAndUserId(storeCategoryId, currentLoginUserId).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()) {
            return new PageResult<StoreMessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                    pageResponse.getTotalPages(), totalRecords, false, true), new ArrayList<>());
        }
        PageHelper.startPage(pageRequest.getPageNum(), pageRequest.getPageSize(), "store_time desc");
        List<Store> storeList = this.storeMapper.selectByStoreCategoryIdAndUserId(storeCategoryId, currentLoginUserId);
        // 遍历storeList，将storeList -> storeMessageVOList
        //PageResult<StoreSquadVO> pageResult;
        List<StoreMessageVO> storeMessageVOList = new ArrayList<>();
        for (Store store : storeList) {
            StoreMessageVO storeMessageVO = new StoreMessageVO();
            // 设置时间戳
            storeMessageVO.setStoreTime(store.getStoreTime().getTime());
            storeMessageVO.setStoreId(store.getId());
            Long onUserId = store.getUserId();
            // 收藏的讯息的原作者被删除了
            User user = this.userMapper.selectByPrimaryKey(onUserId);
            if (user == null) {
                throw new ApplicationException(918, "收藏的讯息的原作者不存在");
            } else {
                // 收藏的讯息的原作者未被删除
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(user, userVO);
                storeMessageVO.setUser(userVO);
            }
            // 收藏的讯息被原作者删除了，即讯息现在已不存在
            Message message = this.messageMapper.selectByPrimaryKey(store.getEntityId());
            if (message == null) {
                storeMessageVO.setMessageId(null);
                storeMessageVO.setMessageTitle(null);
                // 状态state 0表示正常，1表示删除
                storeMessageVO.setState(1);
                storeMessageVO.setPic("");
                storeMessageVO.setStoreCategoryName("");
            } else {
                // 收藏的讯息现在还存在
                storeMessageVO.setMessageId(message.getId());
                storeMessageVO.setMessageTitle(message.getTitle());
                // 状态state 0表示正常，1表示删除
                storeMessageVO.setState(0);
                storeMessageVO.setBoardType(message.getBoardType());
                storeMessageVO.setPic(message.getPic());
                storeMessageVO.setImgList(message.getImgList());
                storeMessageVO.setUiPattern(message.getUiPattern());
                MessageSupportCommentHitStat messageSupportCommentHitStat = this.messageSupportCommentHitStatMapper.
                        selectByMessageId(message.getId());
                if (messageSupportCommentHitStat == null) {
                    throw new ApplicationException(958, "讯息点赞评论点击量统计不存在");
                }
                storeMessageVO.setStoreCategoryName("讯息");
                // 媒体类型
                int mediaType = message.getMediaType();
                storeMessageVO.setMediaType(mediaType);
                // 媒体类型为视频类型，显示视频链接url与描述内容
                if (mediaType == 1) {
                    storeMessageVO.setVideoLinkUrl(message.getVideoLinkUrl());
                    storeMessageVO.setDescription(this.messageDetailMapper.selectByMessageId(message.getId()).getContent());
                }
                storeMessageVO.setHits(messageSupportCommentHitStat.getHits());
                storeMessageVO.setComments(messageSupportCommentHitStat.getComments());
            }
            storeMessageVOList.add(storeMessageVO);
        }
        return new PageResult<StoreMessageVO>(new PageInfo(pageRequest.getPageNum(), pageRequest.getPageSize(),
                pageResponse.getTotalPages(), totalRecords, pageResponse.getFirstPage(), pageResponse.getLastPage()), storeMessageVOList);
    }

}

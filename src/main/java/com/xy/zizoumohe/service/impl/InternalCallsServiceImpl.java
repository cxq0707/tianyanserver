package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.entity.Calory;
import com.xy.zizoumohe.entity.CaloryTrade;
import com.xy.zizoumohe.entity.UserOrnament;
import com.xy.zizoumohe.mapper.CaloryMapper;
import com.xy.zizoumohe.mapper.CaloryTradeMapper;
import com.xy.zizoumohe.mapper.UserOrnamentMapper;
import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.service.InternalCallsService;
import com.xy.zizoumohe.utils.ApplicationException;
import com.xy.zizoumohe.utils.TradeTypeConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class InternalCallsServiceImpl implements InternalCallsService {
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    @Autowired
    CaloryTradeMapper caloryTradeMapper;
    @Autowired
    CaloryMapper caloryMapper;
    @Autowired
    CaloryService caloryService;

    @Transactional
    @Override
    public void giveTheOrnament(Long userId,Integer ornamentId) {
        //判断用户是否已经有该饰品
        UserOrnament isOwn =  userOrnamentMapper.selectByUserIdAndOrnamentId(userId,ornamentId);
        if(null!=isOwn){
            throw new ApplicationException(1103,"你已经拥有该饰品了");
        }
        //给用户添加这件饰品
        UserOrnament userOrnament = new UserOrnament();
        userOrnament.setOrnamentId(ornamentId);
        userOrnament.setUserId(userId);
        userOrnament.setState(0);
        userOrnamentMapper.insertSelective(userOrnament);
    }

    @Override
    @Transactional
    public void giveCalory(Long userId, Integer num,String title) {
        CaloryTrade caloryTrade = new CaloryTrade();
        if(num<0){
            Calory calory = caloryMapper.selectByUserId(userId);
            caloryTrade.setIsIncome(false);
            if(calory.getCaloryNum()<Math.abs(num)){
                throw new ApplicationException("这个用户太穷了没这么多卡路里呢@_@");
            }
        }
        if(num>1000){
            throw new ApplicationException("你是不是手抖了呀！！！！！请重新赠送呢");
        }
    // 生成阵容作者的卡路里交易账单，只有生成交易单，才能增加用户的卡路里余额
            caloryService.initCalory(userId);
            caloryTrade.setAccountId(userId);  // 账单的户主
            caloryTrade.setIsIncome(true);
            caloryTrade.setTradeDescription(title);
            caloryTrade.setTradeNum(num);  // 账单的交易卡路里数额
            caloryTrade.setCreateTime(new Date());
            caloryTrade.setTradeType(100); //100表示其他
            this.caloryTradeMapper.insertSelective(caloryTrade);
            // 增加阵容作者的卡路里财富值
            Long userCaloryNum = this.caloryMapper.selectByUserIdForUpdate(userId).getCaloryNum();
            Long userRemainCaloryNum = userCaloryNum + caloryTrade.getTradeNum();
            Calory calory = new Calory();
            calory.setUserId(userId);
            calory.setCaloryNum(userRemainCaloryNum);
            this.caloryMapper.updateByUserId(calory);
    }
}

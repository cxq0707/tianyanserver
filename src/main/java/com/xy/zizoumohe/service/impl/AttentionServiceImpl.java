package com.xy.zizoumohe.service.impl;

import com.github.pagehelper.PageHelper;
import com.xy.zizoumohe.entity.Attention;
import com.xy.zizoumohe.entity.UserOrnament;
import com.xy.zizoumohe.mapper.AttentionMapper;
import com.xy.zizoumohe.mapper.UserMapper;
import com.xy.zizoumohe.mapper.UserOrnamentMapper;
import com.xy.zizoumohe.service.AttentionService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.utils.*;
import com.xy.zizoumohe.vo.AttentionVO;
import com.xy.zizoumohe.vo.TitleVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class AttentionServiceImpl implements AttentionService {
    private static Logger logger = LoggerFactory.getLogger(AttentionServiceImpl.class);
    @Autowired
    AttentionMapper attentionMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    UserService userService;
    @Autowired
    UserOrnamentMapper userOrnamentMapper;
    @Autowired
    Helper helper;

    /**
     * 添加关注. userId 关注 toUserId
     * @param userId 关注人的id
     * @param toUserId 被关注的用户id
     * @return 关注状态，0表示未关注，1表示已关注，2表示互相关注
     */
    @Override
    public Integer addAttention(Long userId, Long toUserId) {
        if (userId == null || toUserId == null){
            throw new ApplicationException("参数异常");
        }
        // 判断您关注的用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null || userMapper.selectByPrimaryKey(toUserId) == null){
            throw new ApplicationException("用户不存在");
        }
        // 是否是对自己进行关注
        if (userId.equals(toUserId)){
            throw new ApplicationException(904,"不能对自己进行关注");
        }
        // 判断之前我是否已经关注过他，如果已经关注过，就不能重复关注
        if (attentionMapper.selectByUserIdAndToUserId(userId,toUserId) != null){
            throw new ApplicationException(906,"您已经关注过他，请勿重复关注");
        }
        attentionMapper.insertSelective(new Attention(userId, toUserId));
        return getAttentionState(toUserId, userId);
    }

    /**
     * 取消关注.
     * @param currentLoginUserId 当前登录用户id
     * @param toUserId 被关注的用户id
     * @return 关注状态，0表示未关注，1表示已关注，2表示互相关注
     */
    @Override
    public Integer cancelAttention(Long currentLoginUserId, Long toUserId) {
        if (userMapper.selectByPrimaryKey(currentLoginUserId)==null || userMapper.selectByPrimaryKey(toUserId)==null){
            throw new ApplicationException("用户不存在");
        }
        // 是否将取消对自己的关注
        if (currentLoginUserId.equals(toUserId)){
            throw new ApplicationException(912,"不能取消对自己的关注");
        }
        // 判断之前我是否已经关注过他，如果已经关注过，可以取消对其的关注，如果未关注过，则取消关注失败
        if (attentionMapper.selectByUserIdAndToUserId(currentLoginUserId,toUserId) == null){
            throw new ApplicationException(914,"您之前未关注过他，无法取消对其的关注");
        }
        // 取消关注
        attentionMapper.deleteByUserIdAndToUserId(currentLoginUserId,toUserId);
        return 0;
    }

    // 获取当前用户的关注列表
    public PageResult<AttentionVO> getAttentionList(Long userId, PageRequest pageRequest){
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null){
            throw new ApplicationException(800,"当前用户不存在");
        }
        // 总记录数量
        Integer totalRecords = attentionMapper.selectCountByUserId(userId);
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum()>pageResponse.getTotalPages()){
            return new PageResult<AttentionVO>(new PageInfo(pageRequest.getPageNum(),pageRequest.getPageSize(),
                    pageResponse.getTotalPages(),totalRecords,false,true),new ArrayList<>());
        }
        PageHelper.startPage(pageRequest.getPageNum(),pageRequest.getPageSize(),"attention_time desc");
        List<Attention> attentionList = attentionMapper.selectByUserId(userId);
        // attentionList -> attentionVOList
        List<AttentionVO> attentionVOList = new ArrayList<>();
        for (Attention attention:attentionList){
            AttentionVO attentionVO = new AttentionVO(attention.getId(), attention.getAttentionTime().getTime());
            BeanUtils.copyProperties(attention,attentionVO);
            attentionVO.setToUser(userService.getUserByUserId(attention.getToUserId()));
            List<UserOrnament> userOrnaments = userOrnamentMapper.selectTitle(attention.getToUserId());
            if( null!=userOrnaments&&userOrnaments.size()>0){
                List<TitleVO> titleVOS = new ArrayList<>();
               helper.titleAppend(titleVOS,userOrnaments,attentionVO.getToUser().getUserId());
                attentionVO.getToUser().setTitleList(titleVOS);
            }
            // 设置attentionState
            attentionVO.setAttentionState(1);
            if (attentionMapper.selectByUserIdAndToUserId(attention.getToUserId(),userId) != null){
                attentionVO.setAttentionState(2); // 设置状态为“已关注”
            }
            attentionVOList.add(attentionVO);
        }
        return new PageResult<>(new PageInfo(pageRequest.getPageNum(),pageRequest.getPageSize(),
                pageResponse.getTotalPages(),totalRecords,pageResponse.getFirstPage(),pageResponse.getLastPage()),attentionVOList);
    }

    // 获取当前用户的粉丝列表
    public PageResult<AttentionVO> getFansList(Long userId, PageRequest pageRequest){
        // 判断用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null){
            throw new ApplicationException(800,"当前用户不存在");
        }
        // 总记录数量
        Integer totalRecords = attentionMapper.selectByToUserId(userId).size();
        PageUtils.PageResponse pageResponse = PageUtils.getPageResponse(totalRecords, pageRequest);
        // 请求的页码数字 > 总的页数
        if (pageRequest.getPageNum() > pageResponse.getTotalPages()){
            return new PageResult<>(new PageInfo(pageRequest.getPageNum(),pageRequest.getPageSize(),
                    pageResponse.getTotalPages(),totalRecords,false,true),new ArrayList<>());
        }
        PageHelper.startPage(pageRequest.getPageNum(),pageRequest.getPageSize(),"attention_time desc");
        List<Attention> attentionList = attentionMapper.selectByToUserId(userId);
        // attentionList -> attentionVOList
        List<AttentionVO> attentionVOList = new ArrayList<>();
        for (Attention attention:attentionList){
            AttentionVO attentionVO = new AttentionVO(attention.getId(), attention.getAttentionTime().getTime());
            BeanUtils.copyProperties(attention,attentionVO);
            // 注意：在粉丝列表中，toUser表示关注我的用户，即我的粉丝
            attentionVO.setToUser(userService.getUserByUserId(attention.getUserId()));
            List<UserOrnament> userOrnaments = userOrnamentMapper.selectTitle(attention.getUserId());
            if( null!=userOrnaments&&userOrnaments.size()>0 ){
                List<TitleVO> titleVOS = new ArrayList<>();
                helper.titleAppend(titleVOS,userOrnaments,attentionVO.getToUser().getUserId());
                attentionVO.getToUser().setTitleList(titleVOS);
            }
            // 设置attentionState
            attentionVO.setAttentionState(getAttentionState(attention.getUserId(), userId));
            attentionVOList.add(attentionVO);
        }
        return new PageResult<>(new PageInfo(pageRequest.getPageNum(),pageRequest.getPageSize(),
                pageResponse.getTotalPages(),totalRecords,pageResponse.getFirstPage(),pageResponse.getLastPage()),attentionVOList);
    }

    /**
     * 获取userId所关注的人数
     * @param userId userId
     * @return 关注的人数
     */
    @Override
    public Integer getAttentions(Long userId){
        // 查询用户是否存在
        if (userMapper.selectByPrimaryKey(userId) == null){
            throw new ApplicationException(800,"用户不存在");
        }
        return attentionMapper.selectByUserId(userId).size();
    }

    /**
     * 获取userId的粉丝数量
     * @param userId userId
     * @return 粉丝数量
     */
    @Override
    public Integer getFans(Long userId){
        // 查询用户是否存在
        if (userMapper.selectByPrimaryKey(userId)==null){
            throw new ApplicationException(800,"用户不存在");
        }
        return attentionMapper.selectByToUserId(userId).size();
    }

    /**
     * 获取关注状态，参数1表示TA的userId，参数2表示我的userId，
     * @param hisUserId TA的userId
     * @param myUserId 我的userId
     * @return 关注状态 0表示“未关注”，1表示“已关注”，2表示“相互关注”
     * “未关注”表示：a.TA没有关注我，我没有关注TA   b.TA关注过我，我没有关注TA
     * “已关注”表示：c.TA没有关注我，我已经关注过TA
     * “相关关注”表示：d.TA关注过我，我已经关注过TA
     */
    @Override
    public Integer getAttentionState(Long hisUserId, Long myUserId){
        if (hisUserId == null || myUserId == null){
            throw new ApplicationException("参数异常");
        }
        Integer attentionState = 0;
        Attention attention = attentionMapper.selectByUserIdAndToUserId(myUserId, hisUserId);
        if (attention != null){
            attentionState = 1;
            Attention attention2 = attentionMapper.selectByUserIdAndToUserId(hisUserId, myUserId);
            if (attention2 != null){
                attentionState = 2;
            }
        }
        return attentionState;
    }

    @Override
    public List<Attention> fansTalentList() {
        return attentionMapper.fansTalentList();
    }

}

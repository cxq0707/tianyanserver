package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.AndroidVersion;
import com.xy.zizoumohe.vo.AndroidVersionVO;

import java.util.List;

public interface AndroidVersionService {

    AndroidVersionVO getLatestVersion();

    void updateAndroidVersion(AndroidVersion androidVersion);

    List<AndroidVersionVO> getAndroidVersionList();

}

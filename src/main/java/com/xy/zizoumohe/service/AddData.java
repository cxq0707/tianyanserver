package com.xy.zizoumohe.service;

public interface AddData {
    public  void addFetter();
    public  void addFetterMobile();
    public void addUser();
    public void addHero();
    public void addHeroMobile();
    public void addSquad();
    public void addDynamic();
    void addComment();
}

package com.xy.zizoumohe.service;

import com.xy.zizoumohe.entity.User;
import com.xy.zizoumohe.bean.PageResult;
import com.xy.zizoumohe.entity.WxLogin;
import com.xy.zizoumohe.vo.UserBaseStatisticVO;
import com.xy.zizoumohe.vo.UserLoginVO;
import com.xy.zizoumohe.vo.UserVO;
import com.xy.zizoumohe.vo.request.LoginByWechatVO;

import java.util.List;
import java.util.Map;

public interface UserService {
    UserLoginVO userLogin(String userName, String password);

    Long getCurrentUserId();

    void userAlter(UserVO user);

    UserLoginVO userWxProgramLogin(LoginByWechatVO loginByWechatVO);

    UserLoginVO userWxLogin(String openid, User user, String unionid, String inviteCode);

    UserLoginVO sendTextMessageLoginAndRegister(User user);

    List<WxLogin> getOpenIdList();

    UserVO getUserByUserId(Long userId);

    // 获取用户基本统计信息，包括被赞数，粉丝数，关注数，
    UserBaseStatisticVO getUserBaseStatistic(Long userId, Long currentLoginUserId);

    String refreshToken(Long userId);

    UserVO userDetails(Long userId);

    void batchSetAttentionTopic(Long userId, List<Integer> topicTypes);

    void singleSetAttentionTopic(Long userId, int topicType, int flag);

    int[] getAttentionTopic(long userId);

    List<Map<String,Object>> getAllTopic();
}

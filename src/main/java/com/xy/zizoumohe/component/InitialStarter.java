package com.xy.zizoumohe.component;

import com.xy.zizoumohe.entity.ActivityWish;
import com.xy.zizoumohe.mapper.ActivityWishMapper;
import com.xy.zizoumohe.service.ActivityWishService;
import com.xy.zizoumohe.service.ApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Max;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author zachary zhu
 * @date 2019/8/2  9:52
 * @corporation UESTC
 */
@Component
public class InitialStarter implements InitializingBean {

    private static Logger logger = LoggerFactory.getLogger(InitialStarter.class);
    @Autowired
    private ActivityWishMapper activityWishMapper;
    @Autowired
    private ActivityWishService activityWishService;
    @Autowired
    ApiService apiService;
    @Override
    public void afterPropertiesSet() throws Exception {
        apiService.initTimeTrigger();
        logger.info("【初始化】initial starter ...");
        // 从数据库中取出未开奖的许愿活动
        List<ActivityWish> activityWishList = this.activityWishMapper.selectByOpenTimeLess(new Date());
        for (ActivityWish activityWish:activityWishList){
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    logger.info("【定时任务，处理许愿中奖过程...许愿活动id为：】"+activityWish.getId()+"【开奖时间为：】"+activityWish.getOpenTime());
                    activityWishService.handleWishAward(activityWish.getId(), activityWish.getGiftNumber());
                }
            },activityWish.getOpenTime());
        }
    }
}

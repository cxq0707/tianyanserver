package com.xy.zizoumohe.component;

import com.xy.zizoumohe.entity.CaloryTrade;
import com.xy.zizoumohe.mapper.CaloryMapper;
import com.xy.zizoumohe.mapper.CaloryTradeMapper;
import com.xy.zizoumohe.utils.TradeTypeConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author zachary zhu
 * @date 2019/8/9  11:11
 * @corporation UESTC
 */
@Component
public class AcquireSupportHelper {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Autowired
    private CaloryMapper caloryMapper;
    @Autowired
    private CaloryTradeMapper caloryTradeMapper;

    /**
     * 处理用户获赞而获取卡路里
     * @param userId 用户的id
     * @param currentDate 当前日期
     * @param limits 卡路里新增上限(150)
     */
    public void handlerCaloryForAcquireSupport(Long userId, Date currentDate, Integer limits){
        List<CaloryTrade> bCaloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndDateForUpdate(userId, 2, simpleDateFormat.format(currentDate));
        // 生成B的卡路里交易账单
        if (bCaloryTradeList.size() == 0) { // 今日第一次被赞
            CaloryTrade caloryTrade = new CaloryTrade(userId, true,TradeTypeConst.TRADE_TYPE_TWO,2,currentDate,2 );
            this.caloryTradeMapper.insertSelective(caloryTrade);
            // 增加B的卡路里财富
            this.caloryMapper.increaseByUserId(userId, 2);
        } else { // 今日非第一次获赞
            CaloryTrade bCaloryTrade = bCaloryTradeList.get(0);
            Integer acquireSupportCaloryNum = bCaloryTrade.getTradeNum();
            if (acquireSupportCaloryNum < limits) {
                bCaloryTrade.setCreateTime(currentDate);
                bCaloryTrade.setTradeNum(acquireSupportCaloryNum + 2); // 2表示获赞
                this.caloryTradeMapper.updateByPrimaryKeySelective(bCaloryTrade);
                // 增加B的卡路里财富
                this.caloryMapper.increaseByUserId(userId, 2);
            }
        }
    }


}

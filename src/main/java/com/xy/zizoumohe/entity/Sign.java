package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class Sign {
    private Long id;
    // 用户的id
    private Long userId;
    // 连续签到的天数
    private Integer lastForDays;
    // 奖励的卡路里值
    private Integer award;
    // 创建时间，也即签到时间
    private Date createTime;
    // 更新时间
    private Date updateTime;

    public Sign(){}

    public Sign(Long userId){
        this.userId = userId;
    }

    public Sign(Long userId, Integer lastForDays, Integer award){
        this.userId = userId;
        this.lastForDays = lastForDays;
        this.award = award;
    }

}
package com.xy.zizoumohe.entity;

import java.util.Date;

public class Talent {
    private Integer id;
    private Long userId;
    private Integer rank;
    private Integer gameType;
    private Date createTime;
    private Date updateTime;
    private User user;
    private String describe;
    private long caloryNum;
    private Integer fans;
    public long getCaloryNum() {
        return caloryNum;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    public void setCaloryNum(long caloryNum) {
        this.caloryNum = caloryNum;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Integer getRank() {
        return rank;
    }
    public void setRank(Integer rank) {
        this.rank = rank;
    }
    public Integer getGameType() {
        return gameType;
    }
    public void setGameType(Integer gameType) {
        this.gameType = gameType;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
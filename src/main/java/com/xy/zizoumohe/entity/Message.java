package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class Message {
    private Long id;
    // 标题
    private String title;
    // 封面图的地址
    private String pic;
    // 讯息类别id
    private Long categoryId;

    private Date createTime;
    // 媒体类型，视频和图文
    private Integer mediaType;

    private Date updateTime;
    // 是否推荐
    private Boolean isRecommend;
    // 优先级系数
    private Double priority;
    // 视频链接的地址
    private String videoLinkUrl;
    // 是否热门
    private Boolean isHot;

    private Integer boardType;
    // 作者的id
    private Long authorId;
    // 审核状态
    private Integer approveState;
    // 跳转行为类型
    private Integer actionType;
    // 跳转的url
    private String forwardUrl;
    // 简介
    private String briefIntroduction;

    private int uiPattern;
    private String imgList;

    private Double latitude;
    private Double longitude;
    private String locationString;

}
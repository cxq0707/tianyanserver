package com.xy.zizoumohe.entity;

import java.util.Date;

public class ActivityWishCommentSupport {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_activity_wish_comment_support.id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_activity_wish_comment_support.user_id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    private Long userId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_activity_wish_comment_support.activity_comment_id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    private Long activityCommentId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_activity_wish_comment_support.create_time
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_activity_wish_comment_support.update_time
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    private Date updateTime;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_activity_wish_comment_support.id
     *
     * @return the value of t_activity_wish_comment_support.id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_activity_wish_comment_support.id
     *
     * @param id the value for t_activity_wish_comment_support.id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_activity_wish_comment_support.user_id
     *
     * @return the value of t_activity_wish_comment_support.user_id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_activity_wish_comment_support.user_id
     *
     * @param userId the value for t_activity_wish_comment_support.user_id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_activity_wish_comment_support.activity_comment_id
     *
     * @return the value of t_activity_wish_comment_support.activity_comment_id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public Long getActivityCommentId() {
        return activityCommentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_activity_wish_comment_support.activity_comment_id
     *
     * @param activityCommentId the value for t_activity_wish_comment_support.activity_comment_id
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public void setActivityCommentId(Long activityCommentId) {
        this.activityCommentId = activityCommentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_activity_wish_comment_support.create_time
     *
     * @return the value of t_activity_wish_comment_support.create_time
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_activity_wish_comment_support.create_time
     *
     * @param createTime the value for t_activity_wish_comment_support.create_time
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_activity_wish_comment_support.update_time
     *
     * @return the value of t_activity_wish_comment_support.update_time
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_activity_wish_comment_support.update_time
     *
     * @param updateTime the value for t_activity_wish_comment_support.update_time
     *
     * @mbg.generated Fri Sep 06 16:38:08 CST 2019
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
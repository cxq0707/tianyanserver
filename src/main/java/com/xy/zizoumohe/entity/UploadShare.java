package com.xy.zizoumohe.entity;

import java.util.Date;

public class UploadShare {
    
    private Long id;

    
    private Long userId;

    
    private Long activityWishId;

    
    private String uploadPicUrl;


    private Date createTime;

    
    private Date updateTime;


    public Long getId() {
        return id;
    }

    
    public void setId(Long id) {
        this.id = id;
    }

    
    public Long getUserId() {
        return userId;
    }

    
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    
    public Long getActivityWishId() {
        return activityWishId;
    }

    
    public void setActivityWishId(Long activityWishId) {
        this.activityWishId = activityWishId;
    }

    
    public String getUploadPicUrl() {
        return uploadPicUrl;
    }

    
    public void setUploadPicUrl(String uploadPicUrl) {
        this.uploadPicUrl = uploadPicUrl == null ? null : uploadPicUrl.trim();
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public Date getUpdateTime() {
        return updateTime;
    }

    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
package com.xy.zizoumohe.entity;

import com.xy.zizoumohe.vo.TitleVO;

public class UserOrnament {
    
    private Long id;

    private Long userId;

    private Integer ornamentId;

    private Integer state;

    private Ornament ornament;

    public Ornament getOrnament() {
        return ornament;
    }

    public void setOrnament(Ornament ornament) {
        this.ornament = ornament;
    }

    public Integer getOrnamentId() {
        return ornamentId;
    }

    public void setOrnamentId(Integer ornamentId) {
        this.ornamentId = ornamentId;
    }


    public Long getId() {
        return id;
    }

    
    public void setId(Long id) {
        this.id = id;
    }

    
    public Long getUserId() {
        return userId;
    }

    
    public void setUserId(Long userId) {
        this.userId = userId;
    }



    
    public Integer getState() {
        return state;
    }

    
    public void setState(Integer state) {
        this.state = state;
    }
}
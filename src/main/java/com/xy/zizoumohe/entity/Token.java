package com.xy.zizoumohe.entity;

public class Token {
    private Long tokenId;

    private Long userId;

    private String token;

    private Long buildtime;

    public Long getTokenId() {
        return tokenId;
    }

    public void setTokenId(Long tokenId) {
        this.tokenId = tokenId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public Long getBuildtime() {
        return buildtime;
    }

    public void setBuildtime(Long buildtime) {
        this.buildtime = buildtime;
    }
}
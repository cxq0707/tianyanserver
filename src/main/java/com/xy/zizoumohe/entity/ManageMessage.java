package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ManageMessage {

    private Long id;

    private Long userId;

    private Long toUserId;

    private Long manageMessageCategoryId;

    private Long entityId;

    private Long actionId;

    private Integer actionType;

    private Date createTime;

    private Date updateTime;

    private String msgBody;

    public ManageMessage(){}

    public ManageMessage(Long userId, Long toUserId, Long entityId, Long actionId, Long manageMessageCategoryId, Integer actionType){
        this.userId = userId;
        this.toUserId = toUserId;
        this.entityId = entityId;
        this.actionId = actionId;
        this.manageMessageCategoryId = manageMessageCategoryId;
        this.actionType = actionType;
    }

}
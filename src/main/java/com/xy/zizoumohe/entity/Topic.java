package com.xy.zizoumohe.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Topic {
    @JsonProperty("topicId")
    private Integer id;

    private String name;

    private String ico;

    private String pic;

    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    
    public String getIco() {
        return ico;
    }

    
    public void setIco(String ico) {
        this.ico = ico == null ? null : ico.trim();
    }

    
    public String getPic() {
        return pic;
    }

    
    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }
}
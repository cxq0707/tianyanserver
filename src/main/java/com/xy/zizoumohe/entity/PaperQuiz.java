package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaperQuiz {
    private Long id;
    private Long voucherId;
    private int isValid = 1; // is_valid

    private Long paperId;

    private Long quizId;
}
package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MessageSupport {

    private Long id;

    private Long messageId;

    private Long userId;

    private String userName;

    private Date supportTime;


}
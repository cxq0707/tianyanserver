package com.xy.zizoumohe.entity;

import java.util.Date;

public class Report {
    private Integer id;
    private User user;
    private Long userId;
    private Long beReportId;
    private User beReportUser;
    private Integer type;
    private String content;

    
    private Date createTime;

    
    private Date updateTime;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getBeReportUser() {
        return beReportUser;
    }

    public void setBeReportUser(User beReportUser) {
        this.beReportUser = beReportUser;
    }

    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public Long getUserId() {
        return userId;
    }

    
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    
    public Long getBeReportId() {
        return beReportId;
    }

    
    public void setBeReportId(Long beReportId) {
        this.beReportId = beReportId;
    }

    
    public Integer getType() {
        return type;
    }

    
    public void setType(Integer type) {
        this.type = type;
    }

    
    public String getContent() {
        return content;
    }

    
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public Date getUpdateTime() {
        return updateTime;
    }

    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
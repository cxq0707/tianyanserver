package com.xy.zizoumohe.entity;

import java.util.Date;

public class LuckyTicket {

    public LuckyTicket() {
    }

    public LuckyTicket(Long userId) {
        this.userId = userId;
    }

    public LuckyTicket(Long userId, Integer num) {
        this.userId = userId;
        this.num = num;
    }

    private Integer id;

    private Long userId;

    private Integer num;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
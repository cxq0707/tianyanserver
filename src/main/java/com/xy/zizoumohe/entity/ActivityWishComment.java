package com.xy.zizoumohe.entity;

import java.util.Date;

public class ActivityWishComment {
    
    private Long id;

    
    private Long activityWishId;

    
    private Long userId;

    
    private Long toCommentId;

    
    private String commentContent;

    
    private String commentImageList;

    
    private Integer supports;

    
    private Date createTime;

    
    private Date updateTime;

    
    public Long getId() {
        return id;
    }

    
    public void setId(Long id) {
        this.id = id;
    }

    
    public Long getActivityWishId() {
        return activityWishId;
    }

    
    public void setActivityWishId(Long activityWishId) {
        this.activityWishId = activityWishId;
    }

    
    public Long getUserId() {
        return userId;
    }

    
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    
    public Long getToCommentId() {
        return toCommentId;
    }

    
    public void setToCommentId(Long toCommentId) {
        this.toCommentId = toCommentId;
    }

    
    public String getCommentContent() {
        return commentContent;
    }

    
    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent == null ? null : commentContent.trim();
    }

    
    public String getCommentImageList() {
        return commentImageList;
    }

    
    public void setCommentImageList(String commentImageList) {
        this.commentImageList = commentImageList == null ? null : commentImageList.trim();
    }

    
    public Integer getSupports() {
        return supports;
    }

    
    public void setSupports(Integer supports) {
        this.supports = supports;
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public Date getUpdateTime() {
        return updateTime;
    }

    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
package com.xy.zizoumohe.entity;

public class MessageDetail {

    private Long id;

    private Long messageId;

    private String videoLinkUrl;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content ;
    }

    public String getVideoLinkUrl() {
        return videoLinkUrl;
    }

    public void setVideoLinkUrl(String videoLinkUrl) {
        this.videoLinkUrl = videoLinkUrl;
    }
}
package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class SlidePicture {

    private Long id;

    private String name;

    private String path;

    private Long linkMessageId;

    private String linkMessageUrl;

    private Date createTime;

    private Boolean isCurrentUse;

    private Integer sort;

    private Integer actionType;

    private String forwardUrl;

}
package com.xy.zizoumohe.entity;

/**
 * @author zachary zhu
 * @date 2019/5/21  18:06
 * @corporation UESTC
 */
public class StoreCategory {

    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class Attention {

    private Long id;
    // 关注人的id
    private Long userId;
    // 被关注人的id
    private Long toUserId;
    // 关注时间
    private Date attentionTime;

    private Date updateTime;
    private int fansNum;
    private User user;
    public Attention(){}

    public Attention(Long userId, Long toUserId){
        this.userId = userId;
        this.toUserId = toUserId;
    }

}

package com.xy.zizoumohe.entity;

import java.util.Date;

public class Share {

    private Long id;

    private Long userId;

    private Integer shareType;

    private Long entityId;

    private Date createTime;

    private Date updateTime;

    public Share(){}

    public Share(Long userId, Integer shareType, Long entityId, Date createTime){
        this.userId = userId;
        this.shareType = shareType;
        this.entityId = entityId;
        this.createTime = createTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getShareType() {
        return shareType;
    }

    public void setShareType(Integer shareType) {
        this.shareType = shareType;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
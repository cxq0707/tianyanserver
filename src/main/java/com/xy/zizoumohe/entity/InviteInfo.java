package com.xy.zizoumohe.entity;

import java.util.Date;

public class InviteInfo {

    public InviteInfo() {
    }

    public InviteInfo(Long userId, Long toUserId, Integer calorieNum, Integer luckyTicketNum) {
        this(userId, toUserId, calorieNum, luckyTicketNum, 0, 0, 0, 0);
    }

    public static InviteInfo deposit(Long userId, Long toUserId, Integer power, Integer deposit) {
        InviteInfo inviteInfo = new InviteInfo(userId, toUserId, 0, 0, 0, 0, power, deposit);
        return inviteInfo;
    }

    public InviteInfo(Long userId, Long toUserId, Integer calorieNum, Integer luckyTicketNum, Integer toCalorieNum, Integer toLuckyTicketNum, Integer power, Integer deposit) {
        this.userId = userId;
        this.toUserId = toUserId;
        this.calorieNum = calorieNum;
        this.luckyTicketNum = luckyTicketNum;
        this.power = power;
        this.deposit = deposit;
    }

    private Integer power;
    private Integer deposit;

    private Integer id;


    private Long userId;


    private Long toUserId;


    private Integer calorieNum;


    private Integer luckyTicketNum;


    private Integer toCalorieNum;


    private Integer toLuckyTicketNum;


    private Date createTime;


    private Date updateTime;


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Long getToUserId() {
        return toUserId;
    }


    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }


    public Integer getCalorieNum() {
        return calorieNum;
    }


    public void setCalorieNum(Integer calorieNum) {
        this.calorieNum = calorieNum;
    }


    public Integer getLuckyTicketNum() {
        return luckyTicketNum;
    }


    public void setLuckyTicketNum(Integer luckyTicketNum) {
        this.luckyTicketNum = luckyTicketNum;
    }


    public Integer getToCalorieNum() {
        return toCalorieNum;
    }


    public void setToCalorieNum(Integer toCalorieNum) {
        this.toCalorieNum = toCalorieNum;
    }


    public Integer getToLuckyTicketNum() {
        return toLuckyTicketNum;
    }


    public void setToLuckyTicketNum(Integer toLuckyTicketNum) {
        this.toLuckyTicketNum = toLuckyTicketNum;
    }


    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Date getUpdateTime() {
        return updateTime;
    }


    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getDeposit() {
        return deposit;
    }

    public void setDeposit(Integer deposit) {
        this.deposit = deposit;
    }
}
package com.xy.zizoumohe.entity;

import java.util.Date;

/**
 * @author zachary zhu
 * @date 2019/5/21  18:03
 * @corporation UESTC
 */
public class Store {

    private Long id;
    // 收藏类别id
    private Long storeCategoryId;
    // 收藏人的id
    private Long userId;
    // 收藏的实体id，如阵容id或者讯息id
    private Long entityId;
    // 收藏时间
    private Date storeTime;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public Long getStoreCategoryId() {
        return storeCategoryId;
    }
    public void setStoreCategoryId(Long storeCategoryId) {
        this.storeCategoryId = storeCategoryId;
    }

    public Long getUserId() {
        return userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getEntityId() {
        return entityId;
    }
    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Date getStoreTime() {
        return storeTime;
    }
    public void setStoreTime(Date storeTime) {
        this.storeTime = storeTime;
    }

}

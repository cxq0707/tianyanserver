package com.xy.zizoumohe.entity;

public class PropItems {
    
    private Integer id;

    
    private Integer propId;

    
    private Integer materialId;

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public Integer getPropId() {
        return propId;
    }

    
    public void setPropId(Integer propId) {
        this.propId = propId;
    }

    
    public Integer getMaterialId() {
        return materialId;
    }

    
    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }
}
package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class MessageComment {

    private Long id;

    private Long messageId;

    private Long userId;

    private Date commentTime;

    private String commentContent;

    private Long toMessageCommentId;

    private String commentImageList;

    private Integer mediaType;

    private String audioUrl;

    private Integer audioDuration;

    private Integer supports;

    private Integer replys;

    private Date updateTime;
    // 是否精选
    private Boolean isExcellent;
    // 标志时间
    private Date flagTime;


}
package com.xy.zizoumohe.entity;

import java.util.Date;

public class Ornament {

    private Integer id;

    private String name;

    
    private String ico;

    
    private String details;

    
    private Integer ornamentType;

    
    private Integer calorie;

    
    private Integer stock;

    private Integer isStart;

    public Integer getIsStart() {
        return isStart;
    }

    public void setIsStart(Integer isStart) {
        this.isStart = isStart;
    }

    private Date createTime;

    
    private Date updateTime;

    
    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    
    public String getIco() {
        return ico;
    }

    
    public void setIco(String ico) {
        this.ico = ico == null ? null : ico.trim();
    }

    
    public String getDetails() {
        return details;
    }

    
    public void setDetails(String details) {
        this.details = details == null ? null : details.trim();
    }

    
    public Integer getOrnamentType() {
        return ornamentType;
    }

    
    public void setOrnamentType(Integer ornamentType) {
        this.ornamentType = ornamentType;
    }

    
    public Integer getCalorie() {
        return calorie;
    }

    
    public void setCalorie(Integer calorie) {
        this.calorie = calorie;
    }

    
    public Integer getStock() {
        return stock;
    }

    
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public Date getUpdateTime() {
        return updateTime;
    }

    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
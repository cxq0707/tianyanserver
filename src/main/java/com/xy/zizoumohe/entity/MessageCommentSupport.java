package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class MessageCommentSupport {

    private Long id;
    // 讯息评论id
    private Long messageCommentId;
    // 点赞人的id
    private Long userId;
    // 点赞时间
    private Date supportTime;

    public MessageCommentSupport(){}

    public MessageCommentSupport(Long messageCommentId, Long userId) {
        this.messageCommentId = messageCommentId;
        this.userId = userId;
    }

}
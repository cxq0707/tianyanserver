package com.xy.zizoumohe.entity;

import java.util.Date;

public class LuckyTicketInfo {


    public LuckyTicketInfo() {
    }

    public LuckyTicketInfo(Long userId, String explain, Integer type,Integer num) {
        this.userId = userId;
        this.explain = explain;
        this.type = type;
        this.num=num;
    }

    private Long id;


    private Long userId;


    private String explain;


    private Integer type;


    private Date createTime;


    private Date updateTime;

    private int num;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Long getUserId() {
        return userId;
    }


    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public String getExplain() {
        return explain;
    }


    public void setExplain(String explain) {
        this.explain = explain == null ? null : explain.trim();
    }


    public Integer getType() {
        return type;
    }


    public void setType(Integer type) {
        this.type = type;
    }


    public Date getCreateTime() {
        return createTime;
    }


    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }


    public Date getUpdateTime() {
        return updateTime;
    }


    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
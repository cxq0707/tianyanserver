package com.xy.zizoumohe.entity;

import java.util.Date;

public class Prop {
    
    private Integer id;

    
    private String name;

    
    private String details;

    
    private String pic;

    
    private Date createTime;

    
    private Date updateTime;

    
    private Integer gameType;
    private String stuff;
    private Integer state;
    private int score;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getStuff() {
        return stuff;
    }

    public void setStuff(String stuff) {
        this.stuff = stuff;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    
    public void setId(Integer id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    
    public String getDetails() {
        return details;
    }

    
    public void setDetails(String details) {
        this.details = details == null ? null : details.trim();
    }

    
    public String getPic() {
        return pic;
    }

    
    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    
    public Date getCreateTime() {
        return createTime;
    }

    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    
    public Date getUpdateTime() {
        return updateTime;
    }

    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    
    public Integer getGameType() {
        return gameType;
    }

    
    public void setGameType(Integer gameType) {
        this.gameType = gameType;
    }
}
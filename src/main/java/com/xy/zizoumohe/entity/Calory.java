package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Calory {

    private Long id;

    private Long userId;

    private Long caloryNum;

    private Date createTime;

    private Date updateTime;
    private User user;

    public Calory() {
    }

    public Calory(Long userId) {
        this.userId = userId;
    }

}
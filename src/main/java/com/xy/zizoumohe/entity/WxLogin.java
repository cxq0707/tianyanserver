package com.xy.zizoumohe.entity;

import com.xy.zizoumohe.vo.UserVO;

public class WxLogin {
    private Long wxLoginId;

    private String openid;
    private String unionid;

    private Long userId;

    private UserVO user;

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Long getWxLoginId() {
        return wxLoginId;
    }

    public void setWxLoginId(Long wxLoginId) {
        this.wxLoginId = wxLoginId;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }
}
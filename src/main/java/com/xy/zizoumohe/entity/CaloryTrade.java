package com.xy.zizoumohe.entity;

import java.util.Date;

public class CaloryTrade {

    private Long id;

    private Long accountId;

    private Integer tradeNum;

    private Boolean isIncome;

    private String tradeDescription;

    private Integer tradeType;

    private Date createTime;

    private Date updateTime;

    private Long entityId;

    public CaloryTrade(){}

    public CaloryTrade(Long accountId, Boolean isIncome, String tradeDescription, Integer tradeNum, Date createTime, Integer tradeType){
        this.accountId = accountId;
        this.isIncome = isIncome;
        this.tradeDescription = tradeDescription;
        this.tradeNum = tradeNum;
        this.createTime = createTime;
        this.tradeType = tradeType;
    }

    public CaloryTrade(Long accountId, Boolean isIncome, String tradeDescription, Integer tradeNum, Date createTime, Integer tradeType, Long entityId){
        this.accountId = accountId;
        this.isIncome = isIncome;
        this.tradeDescription = tradeDescription;
        this.tradeNum = tradeNum;
        this.createTime = createTime;
        this.tradeType = tradeType;
        this.entityId = entityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getTradeNum() {
        return tradeNum;
    }

    public void setTradeNum(Integer tradeNum) {
        this.tradeNum = tradeNum;
    }

    public Boolean getIsIncome() {
        return isIncome;
    }

    public void setIsIncome(Boolean isIncome) {
        this.isIncome = isIncome;
    }

    public String getTradeDescription() {
        return tradeDescription;
    }

    public void setTradeDescription(String tradeDescription) {
        this.tradeDescription = tradeDescription == null ? null : tradeDescription.trim();
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }
}
package com.xy.zizoumohe.entity;

import java.util.Date;

/**
 * @author zachary zhu
 * @date 2019/6/4  14:40
 * @corporation UESTC
 */
public class ManageMessageCategory {

    private Long id;

    private String name;

    private Date createTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}

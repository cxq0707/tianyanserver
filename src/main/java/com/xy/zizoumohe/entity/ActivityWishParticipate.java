package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class ActivityWishParticipate {

    private Long id;

    private Long activityWishId;

    private Long userId;

    private Date createTime;

    private Date updateTime;

    // 是否上传分享
    private Boolean isUploadShare;

    private int inspireNum;

    private Integer fortuneValue;

    private Integer fortuneTicketTimes;

    // 参与方式；0消费卡路里，1观看视频
    private Integer participateType;


}
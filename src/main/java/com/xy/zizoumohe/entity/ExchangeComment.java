package com.xy.zizoumohe.entity;

import com.xy.zizoumohe.vo.UserVO;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class ExchangeComment {

    private Long id;

    private Long userId;

    private UserVO user;

    private String title;

    private String content;

    private Date createdTime;

    private Date updatedTime;

    private String imgList;

}
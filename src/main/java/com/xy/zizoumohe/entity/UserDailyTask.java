package com.xy.zizoumohe.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserDailyTask {

    public UserDailyTask() {
    }

    public UserDailyTask(Long itemId, Long userId, String title, int target, int current, int type, int itemType, String imgUrl, Integer gain, Date validTime) {
        this.itemId = itemId;
        this.userId = userId;
        this.title = title;
        this.target = target;
        this.current = current;
        this.type = type;
        this.itemType = itemType;
        this.imgUrl = imgUrl;
        this.gain = gain;
        this.validTime = validTime;
        this.isComplete = 0;
    }
    private Long id;

    private int isComplete;

    private Long itemId;

    private Long userId;

    private String title;

    private int target;

    private int current;

    private int type;

    private int itemType;

    private String imgUrl;

    private Integer gain;

    private Date validTime;

    private Date ripeTime;

}
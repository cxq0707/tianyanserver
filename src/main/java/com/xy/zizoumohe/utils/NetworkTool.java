package com.xy.zizoumohe.utils;

import com.alibaba.druid.support.json.JSONUtils;
import com.xy.zizoumohe.utils.enumeration.Wechat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedHashMap;
import java.util.Map;

public class NetworkTool {

    public static String WxBaseUrl = "https://api.weixin.qq.com/cgi-bin/";

    public static Map<String,Object> post(String url, LinkedHashMap param) {
        RestTemplate template=new RestTemplate();
        String resultString = template.postForEntity(url, param, String.class).getBody();
        Map<String,Object> checkResponse = (Map<String, Object>) JSONUtils.parse(resultString);
        if (checkResponse == null) {
            throw new ApplicationException("网络返回错误");
        }
        return checkResponse;
    }

    public static Map<String,Object> get(String url) {
        RestTemplate template=new RestTemplate();
        String resultString = template.getForEntity(url, String.class).getBody();
        Map<String,Object> checkResponse = (Map<String, Object>) JSONUtils.parse(resultString);
        if (checkResponse == null) {
            throw new ApplicationException("网络返回错误");
        }
        return checkResponse;
    }

    public static String getWxAccessToken() {
        String uri = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + Wechat.SMALL_PROGRAM_APPID.getValue() + "&secret=" + Wechat.SMALL_PROGRAM_APPKEY.getValue();
        Map<String,Object> resultMap = get(uri);
        Object token = resultMap.get("access_token");
        if (token == null) {
            throw new ApplicationException("网络返回错误");
        }
        String accessToken = token.toString();
        return accessToken;
    }
}

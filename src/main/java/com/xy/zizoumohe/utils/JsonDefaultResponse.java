package com.xy.zizoumohe.utils;

import java.io.Serializable;

public class JsonDefaultResponse implements Serializable {

    private int code;
    private String message;

    private Object result;

    private Object results;

    public JsonDefaultResponse(int code, String message) {
        this.code = code;
        this.message = message;
        this.results = null;
    }

    public static JsonDefaultResponse success() {
        return new JsonDefaultResponse(200, "操作成功");
    }

    public static JsonDefaultResponse failure() {
        return new JsonDefaultResponse(0, "failure");
    }

    public int getCode() {
        return this.code;
    }

    public JsonDefaultResponse setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return this.message;
    }

    public JsonDefaultResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getResults() {
        return this.results;
    }

    public JsonDefaultResponse setResults(Object results) {
        this.results = results;
        return this;
    }

    public JsonDefaultResponse with(int code, String message) {
        this.code = code;
        this.message = message;
        return this;
    }

    public Object getResult() {
        return result;
    }

    public JsonDefaultResponse setResult(Object result) {
        this.result = result;
        return this;
    }
}

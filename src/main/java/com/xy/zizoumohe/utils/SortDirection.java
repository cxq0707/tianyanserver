package com.xy.zizoumohe.utils;

public enum SortDirection {

    ASC("升序"),
    DESC("降序");

    private String value;

    SortDirection(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

}

package com.xy.zizoumohe.utils;


import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;

public class Encrypt {
    public static String SHA(String password,String salt) {
            password = salt+password;
            try {
                MessageDigest messageDigest = MessageDigest
                        .getInstance("SHA-512");
                // 传入要加密的字符串
                messageDigest.reset();
                byte [] bytes = password.getBytes("UTF-8");
                byte [] result= messageDigest.digest(bytes);
                for (int i=0;i<512;i++){
                    result=messageDigest.digest(result);
                 }
                Base64 base64 = new Base64();
                return  base64.encodeToString(result);
            } catch (Exception e) {
                e.printStackTrace();
                return e.toString();
            }

    }


}

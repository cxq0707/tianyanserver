package com.xy.zizoumohe.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageInfo {

    private Integer pageNum;

    private Integer pageSize;

    @JsonProperty("pages")
    private Integer totalPages;

    @JsonProperty("total")
    private Integer totalRecords;

    private Boolean isFirstPage;

    private Boolean isLastPage;

    public PageInfo(){}

    public PageInfo(Integer pageNum,Integer pageSize,Integer totalPages,Integer totalRecords){
        this(pageNum,pageSize,totalPages,totalRecords,false,true);
    }

    public PageInfo(Integer pageNum,Integer pageSize,Integer totalPages,Integer totalRecords,Boolean isFirstPage,Boolean isLastPage){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.totalPages = totalPages;
        this.totalRecords = totalRecords;
        this.isFirstPage = isFirstPage;
        this.isLastPage = isLastPage;
    }

}

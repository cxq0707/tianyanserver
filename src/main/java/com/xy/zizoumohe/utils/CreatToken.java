package com.xy.zizoumohe.utils;

import com.xy.zizoumohe.entity.User;

import java.util.Random;


public class CreatToken {
    //分别对应数字大写字母小写字母的ASCII码
    private final static int[] MIN = {48,65,97};
    private final static int[] MAX = {57,90,122};
    //token的长度为60个字符
    private final static int length= 60;
    public static String creatToken(User user) {
        char[] chars = new char[length];
        for (int i = 0; i < chars.length; i++) {
            int site = new Random().nextInt(3);
            int random = new Random().nextInt(MAX[site]-MIN[site])+MIN[site];
            if(random==92){
                continue;
            }
            chars[i] =(char) random;
        }
        return new String(chars);
    }

}

package com.xy.zizoumohe.utils;

import org.springframework.stereotype.Component;

@Component
public class InviteCodeUtils {
    //定义长度为36的字符数组
    private static final char[] CODE_ARRAY = {'Z', '0', 'Y', '1', 'X', '2', 'W', '3', 'V', '4', 'U', '5', 'T', '6',
            'S', '7', 'R', '8', 'Q', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
            'L', 'M', 'N', 'O', 'P',};
    //字符数组的长度为36
    private static final int SIZE = 36;
    //验证码的长度为6
    private static final int INBOX_LENGTH = 6;

    //用户ID转邀请码
    public String userIdChangeInbox(Long userId) {
        int num = userId.intValue();
        char[] inboxArray = {CODE_ARRAY[0], CODE_ARRAY[0], CODE_ARRAY[0], CODE_ARRAY[0], CODE_ARRAY[0], CODE_ARRAY[0],};
        for (int i = 0; i < INBOX_LENGTH; i++) {
            inboxArray[i] = CODE_ARRAY[num % SIZE ];
            if ((num = num / SIZE) <= 0) {
                break;
            }
        }
        return new String(inboxArray);
    }

    //邀请码转用户ID
    public Long InboxChangeUserId(String inbox) {
        char[] inboxArray = inbox.toCharArray();
        int num = 0;
        for (int i = INBOX_LENGTH - 1; i >= 0; i--) {

            if (inboxArray[i] == CODE_ARRAY[0]) {
                continue;
            }

            for (int j = 0; j < SIZE; j++) {
                if (CODE_ARRAY[j] == inboxArray[i]) {
                    num = num + (int) Math.pow(SIZE, i) * j;
                    break;
                }
            }
        }
        return (long) num;
    }

    public static void main(String[] args) {
        long userId = 1111;
        while (true) {
            InviteCodeUtils inviteCodeUtils = new InviteCodeUtils();
            String str = inviteCodeUtils.userIdChangeInbox(userId);
            System.out.println(str);
            long userId1 = inviteCodeUtils.InboxChangeUserId(str);
            System.out.println(userId + "," + userId1);
            if (userId != userId1) {
                break;
            }

            userId +=1000;
        }
    }
}

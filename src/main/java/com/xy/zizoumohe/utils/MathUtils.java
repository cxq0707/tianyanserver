package com.xy.zizoumohe.utils;

public class MathUtils {

    /**
     * 产生一个随机整数，范围从min到max，边界值可以取到
     * @param min 最小值
     * @param max 最大值
     * @return
     */
    public static int random(int min, int max){
        if (min > max){
            throw new RuntimeException("最小值必须小于最大值");
        }
        double r = Math.random();
        double rp = r*(max-1+min) + min;
        return (int) Math.floor(rp);
    }


}

package com.xy.zizoumohe.utils.trigger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.xy.zizoumohe.utils.RTS;
import com.xy.zizoumohe.utils.RedisUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.HashMap;
import java.util.Map;

public class Second20Api extends BaseApi {
    private static String API_KEY = "qmrR1IxJzrtjzekt";
    private static String LEAGUE_PROFILE_API = "http://api.isportsapi.com/sport/football/league/basic?api_key=" + API_KEY;
    private static String LIVESCORES_FOR_TODAY = "http://api.isportsapi.com/sport/football/livescores?api_key=" + API_KEY;
    private static String TEAM_PROFILE = "http://api.isportsapi.com/sport/football/team?api_key=" + API_KEY;

    @Override
    public void getApi(RedisUtils redisUtils){
        reqApi(LEAGUE_PROFILE_API,"leagueProfileData",redisUtils);
        reqApi(LIVESCORES_FOR_TODAY,"livescoresData",redisUtils);
        reqApi(TEAM_PROFILE,"teamProfileData",redisUtils);
    }
}

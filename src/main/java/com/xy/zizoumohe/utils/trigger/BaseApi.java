package com.xy.zizoumohe.utils.trigger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.xy.zizoumohe.utils.RedisUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseApi implements Job {
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
        RedisUtils redisUtils = (RedisUtils) dataMap.get("redisUtils");
        try {
            getApi(redisUtils);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  abstract  void getApi(RedisUtils redisUtils);
    public void reqApi(String url, String key, RedisUtils redisUtils) {
        HttpGet httpGet = new HttpGet(url);
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();//设置请求和传输超时时间
            httpGet.setConfig(requestConfig);
            HttpResponse response = httpClient.execute(httpGet);
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    String str = EntityUtils.toString(resEntity, "utf-8");
                    ObjectMapper mapper = new ObjectMapper();
                    TypeFactory factory = TypeFactory.defaultInstance();
                    MapType type = factory.constructMapType(HashMap.class, Object.class, Object.class);
                    Map result = mapper.readValue(str, type);
                    if (result.get("code").equals(0)) {
                        redisUtils.set(key, result.get("data"));
                        System.out.println("刷新成功");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            httpGet.releaseConnection();
        }
    }
}

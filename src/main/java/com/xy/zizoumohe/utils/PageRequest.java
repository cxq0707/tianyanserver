package com.xy.zizoumohe.utils;

public class PageRequest {

    private Integer pageNum;

    private Integer pageSize;

    private String sortColumn;

    private String sortDirection;

    public PageRequest(){}

    public PageRequest(Integer pageNum,Integer pageSize){
        this.pageNum = pageNum;
        this.pageSize =pageSize;
    }

    public PageRequest(Integer pageNum,Integer pageSize,String sortColumn, String sortDirection){
        this.pageNum = pageNum;
        this.pageSize =pageSize;
        this.sortColumn = sortColumn;
        this.sortDirection = sortDirection;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }
}

package com.xy.zizoumohe.utils.enumeration;

public enum BehaviorType {
    COMMENT(1,"评论"),
    SUPPORT(2,"赞"),
    OTHER(3,"其他"),;
    BehaviorType(int index, String name) {
        this.index = index;
        this.name = name;
    }
    private int index;
    private String name;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

package com.xy.zizoumohe.utils.enumeration;

public enum Wechat {

    SMALL_PROGRAM_APPID(0,"wx5e15906e4c27ba15"),
    SMALL_PROGRAM_APPKEY(1,"ddf419935f1f8a9d13a23e6b25189549");

    Wechat(int index, String value) {
        this.index = index;
        this.value = value;
    }
    private int index;
    private String value;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

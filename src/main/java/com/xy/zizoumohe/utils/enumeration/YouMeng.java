package com.xy.zizoumohe.utils.enumeration;

public enum YouMeng {
    ANDROID_APP_KEY(0,"5cc69f100cafb2e3b4000037"),
    ANDROID_APP_MASTER_SECRET(1,"ak1siw31csmrvj5h8b9zgit6gn4ivyah"),
    IOS_APP_KEY(2,"5cc6a0523fc195b2b100108d"),
    IOS_APP_MASTER_SECRET(3,"nhdcktwo8fjwlxwvty9vraqmvlceaxqh");
    YouMeng(int index, String value) {
        this.index = index;
        this.value = value;
    }
    private int index;
    private String value;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

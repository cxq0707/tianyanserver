package com.xy.zizoumohe.utils.enumeration;

public enum LuckyTicketSource {

    INVITE(1, "邀请有礼", 5),
    OPEN_EGG(2, "开金蛋", 1),
    GIFT_EXPEND(3, "魔盒礼包消耗", -1),
    BIG_PACKET(4,"新人大礼包",1);

    LuckyTicketSource(int index, String name, int num) {
        this.index = index;
        this.name = name;
        this.num = num;
    }

    private int index;
    private String name;
    private int num;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}

package com.xy.zizoumohe.utils.enumeration;

public enum GameType {
    DOTA(1, "刀塔自走棋", "pc"),
    DUODUO(2, "多多自走棋", "mobile"),
    UNDERLORDS(3, "刀塔霸业", "underlords"),
    BG(9, "酒馆战棋", "bg"),
    BL(100, "部落自走棋", "bl"),
    SITW(101, "方块世界", "sitw"),
    LOL(4, "云顶之弈", "lol"),
    CHICHAO(5, "赤潮自走棋", "chichao"),
    RK(6, "皇家骑士", "rk"),
    KOSW(7, "王者模拟战", "kosw"),
    CR(8, "战歌竞技场", "cr");

    GameType(int index, String name, String type) {
        this.index = index;
        this.name = name;
        this.type = type;
    }

    private int index;
    private String name;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Integer getGameType(String type) {
        for (GameType ele : values()) {
            if (ele.getType().equals(type)) {
                return ele.getIndex();
            }
        }
        return 0;
    }

    public static String getType(Integer gameType) {
        for (GameType ele : values()) {
            if (ele.getIndex() == gameType) return ele.getType();
        }
        return "";
    }
}

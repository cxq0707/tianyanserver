package com.xy.zizoumohe.utils.enumeration;

public enum KeyRules {
    ADMIN_KEY(0,"xiaoyuan"),
    INSPIRE(1,"x-y+@cxq.com2508");
    KeyRules(int index, String value) {
        this.index = index;
        this.value = value;
    }
    private int index;
    private String value;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

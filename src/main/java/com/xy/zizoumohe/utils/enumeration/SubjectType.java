package com.xy.zizoumohe.utils.enumeration;

public enum SubjectType {
    SQUAD(1,"了你的阵容"),
    SQUAD_COMMENT(2,"了你的评论"),
    POST(3,"了你的贴子"),
    POST_COMMENT(4,"了你的评论"),
    MESSAGE_COMMENT(5,"了你的评论"),
    HERO_COMMENT(6,"了你的评论"),
    VOW_TO(7,"开奖了"),
    INBOX(8,"对你说");

    SubjectType(int index, String name) {
        this.index = index;
        this.name = name;
    }
    private int index;
    private String name;
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

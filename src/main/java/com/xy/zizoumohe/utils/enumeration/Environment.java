package com.xy.zizoumohe.utils.enumeration;

public enum Environment {
    YOUMENG(true, "友盟推送");

    Environment(boolean value, String name) {
        this.value = value;
        this.name = name;
    }

    private boolean value;
    private String name;

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

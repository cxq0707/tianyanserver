package com.xy.zizoumohe.utils;

import com.alibaba.druid.util.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.net.*;
import java.util.Enumeration;

@Component
public class IpHelper {


    public static final String LOCAL_IP = "127.0.0.1";//本地ip地址
    public static final String DEFAULT_IP = "0:0:0:0:0:0:0:1";//默认ip地址
    public static final int DEFAULT_IP_LENGTH = 15;//默认ip地址长度


    public String getIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");//squid 服务代理
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");//apache服务代理
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");//weblogic 代理
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");//有些代理
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP"); //nginx代理
        }

        /*
         * 如果此时还是获取不到ip地址，那么最后就使用request.getRemoteAddr()来获取
         * */
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            if (StringUtils.equals(ip, LOCAL_IP) || StringUtils.equals(ip, DEFAULT_IP)) {
                //根据网卡取本机配置的IP
                InetAddress iNet = null;
                try {
                    iNet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    System.out.println("InetAddress getLocalHost error In HttpUtils getRealIpAddress: "+e.getMessage());
                }
                ip = iNet.getHostAddress();
            }
        }
        return ip;
    }

    public String getHostIp()
    {
        try{

            Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            while ( allNetInterfaces.hasMoreElements() )
            {
                NetworkInterface        netInterface    = (NetworkInterface) allNetInterfaces.nextElement();
                Enumeration<InetAddress>    addresses   = netInterface.getInetAddresses();
                while ( addresses.hasMoreElements() )
                {
                    InetAddress ip = (InetAddress) addresses.nextElement();
                    if ( ip != null
                            && ip instanceof Inet4Address
                            && !ip.isLoopbackAddress() /* loopback地址即本机地址，IPv4的loopback范围是127.0.0.0 ~ 127.255.255.255 */
                            && ip.getHostAddress().indexOf( ":" ) == -1 )
                    {
                        return(ip.getHostAddress() );
                    }
                }
            }
        }catch ( Exception e ) {
            e.printStackTrace();
        }
        return(null);
    }
}

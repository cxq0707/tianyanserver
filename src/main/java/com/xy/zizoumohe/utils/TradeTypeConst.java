package com.xy.zizoumohe.utils;

public final class TradeTypeConst {

    public final static String TRADE_TYPE_ONE = "签到";

    public final static String TRADE_TYPE_TWO = "获赞";

    public final static String TRADE_TYPE_THREE = "发布帖子";

    public final static String TRADE_TYPE_FOUR = "分享帖子";

    public final static String TRADE_TYPE_FIVE = "点赞帖子";

    public final static String TRADE_TYPE_SIX = "评论帖子";

    public final static String TRADE_TYPE_SEVEN = "发布阵容";

    public final static String TRADE_TYPE_EIGHT = "分享阵容";

    public final static String TRADE_TYPE_NINE = "点赞阵容";

    public final static String TRADE_TYPE_TEN = "评论阵容";

    public final static String TRADE_TYPE_ELEVEN = "分享文章";

    public final static String TRADE_TYPE_TWELVE = "评论文章";

    public final static String TRADE_TYPE_THIRTEEN = "购买称号";

    public final static String TRADE_TYPE_FOURTEEN = "点赞文章";

    public final static String TRADE_TYPE_FIFTEEN = "评论棋子";

    public final static String TRADE_TYPE_SIXTEEN = "设置阵容本周热门";

    public final static String TRADE_TYPE_SEVENTEEN = "邀请有礼";

    public final static String TRADE_TYPE_EIGHTEEN = "新人大礼包";

    public final static String TRADE_TYPE_NINETEEN= "开金蛋中奖";
    public final static String TRADE_TYPE_TWENTY= "购买开金蛋次数";

    public final static String TRADE_TYPE_FIFTY_ONE = "观看激励视频";

}

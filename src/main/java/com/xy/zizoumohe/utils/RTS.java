package com.xy.zizoumohe.utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Component
public class RTS {
    public String get(String url, Map<String,String> param) {
        StringBuffer sb = new StringBuffer();
        sb.append(url);
        if(param!=null){
            sb.append("?");
            Set set = param.entrySet();
            for (Iterator iterator = set.iterator();iterator.hasNext();){
                Map.Entry entry = (Map.Entry)iterator.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();
                sb.append(key);
                sb.append("=");
                sb.append(value);
                sb.append("&");
            }
        }
        HttpGet httpGet = new HttpGet(sb.toString());
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(10000).setConnectTimeout(10000).build();//设置请求和传输超时时间
            httpGet.setConfig(requestConfig);
            HttpResponse response  = httpClient.execute(httpGet);
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    String str = EntityUtils.toString(resEntity, "utf-8");
                    return str;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            httpGet.releaseConnection();
        }
        return null;
    }
}

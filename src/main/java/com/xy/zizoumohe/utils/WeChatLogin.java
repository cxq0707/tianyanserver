package com.xy.zizoumohe.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.utils.RTS;

import java.util.HashMap;
import java.util.Map;

public class WeChatLogin {
    /**
     * Post发送json数据
     */
    private final String APP_ID = "wxc0493ea636ea614f";
    private final String SECRET = "30b41ce22be24e0d2f8cb0a2558b2ec9";

    public Boolean weChatLogin(String openId, String accessToken) {
        //通过code请求接口得到refresh_token
        String examineTokenUrl = "https://api.weixin.qq.com/sns/auth";
        Map<String, String> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("openid", openId);
        RTS rts = new RTS();
        String message = rts.get(examineTokenUrl, param);
        Map<String, Object> result = jsonToMap(message);
        //判断token否有效
        if (result.get("errcode").equals(0) && result.get("errmsg").equals("ok")) {
            return true;
        }
        return false;
    }

    public Map<String, Object> jsonToMap(String data) {
        Map<String,Object> map = new HashMap();
        try {
            ObjectMapper mapper = new ObjectMapper();
            map = mapper.readValue(data, Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
}

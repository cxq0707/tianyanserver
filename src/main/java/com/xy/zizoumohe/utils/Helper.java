package com.xy.zizoumohe.utils;

import com.xy.zizoumohe.entity.*;
import com.xy.zizoumohe.vo.TitleVO;
import com.xy.zizoumohe.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Configuration;

import java.util.*;

@Configuration
public class Helper {
    //绑定称号
    public void titleAppend(List<TitleVO> titleVOS, List<UserOrnament> userOrnaments, Long id) {
        if (id == null) {
            return;
        }
        for (UserOrnament userOrnament : userOrnaments) {
            if (userOrnament.getUserId() != null && userOrnament.getUserId().longValue() == id.longValue()) {
                TitleVO titleVO = new TitleVO();
                BeanUtils.copyProperties(userOrnament.getOrnament(), titleVO);
                titleVOS.add(titleVO);
            }
        }
    }
}

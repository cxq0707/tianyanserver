package com.xy.zizoumohe.utils;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import org.json.JSONException;

import java.io.IOException;
import java.util.Random;

public class SendTextMessageUnit {
    // 短信应用 SDK AppID
    private static final int  APP_ID= 1400207474; // 1400开头

    // 短信应用 SDK AppKey
    private static final String APP_KEY = "2cca33e969364d6120b60663e48c52ed";

    // 短信模板 ID，需要在短信应用中申请
    private static final int  TEMPLATE_ID= 327378; // NOTE: 这里的模板 ID`7839`只是一个示例，真实的模板 ID 需要在短信控制台中申请
    // templateId7839对应的内容是"您的验证码是: {1}"
// 签名
    private static final String  SMS_SIGN= "深圳市晓愿科技有限公司"; // NOTE: 签名参数使用的是`签名内容`，而不是`签名ID`。这里的签名"腾讯云"只是一个示例，真实的签名需要在短信控制台申请。


    public static  String SendTextMessage( String phoneNumbers) {
        String verificationCode = randomUnits();
        try {
            String[] params = {verificationCode,"3"};// 数组具体的元素个数和模板中变量个数必须一致，例如示例中 templateId:5678 对应一个变量，参数数组中元素个数也必须是一个
            SmsSingleSender ssender = new SmsSingleSender(APP_ID, APP_KEY);
            SmsSingleSenderResult result = ssender.sendWithParam("86", phoneNumbers,
                    TEMPLATE_ID, params, SMS_SIGN, "", "");  // 签名参数未提供或者为空时，会使用默认签名发送短信
            System.out.println(result);
        } catch (Exception e) {
            // 异常
            e.printStackTrace();
            return "-1";
        }
        return verificationCode;
    }

/*    public static void main(String[] args) {
        System.out.println(randomUnits());
    }*/
    //生成六位随机数验证码
    public  static String randomUnits(){
        StringBuffer sb = new StringBuffer();
        Random random = new Random();
        for(int i=0;i<4;i++){
            sb.append(random.nextInt(9));
        }
        return sb.toString();
    }
}

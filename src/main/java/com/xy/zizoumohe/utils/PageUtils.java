package com.xy.zizoumohe.utils;

public class PageUtils {

    public static PageResponse getPageResponse(Integer totalRecords, PageRequest pageRequest){
        Integer totalPages = totalRecords % pageRequest.getPageSize() == 0 ?
                totalRecords/pageRequest.getPageSize() : totalRecords/pageRequest.getPageSize()+1;
        // 判断分页是首页还是尾页
        Boolean isFirstPage = pageRequest.getPageNum() == 1;
        Boolean isLastPage = pageRequest.getPageNum().equals(totalPages) ;
        return new PageResponse(totalRecords,totalPages,isFirstPage,isLastPage);
    }

    public static class PageResponse{
        private Integer totalRecords;
        private Integer totalPages;
        private Boolean isFirstPage;
        private Boolean isLastPage;

        private PageResponse(){}

        private PageResponse(Integer totalRecords, Integer totalPages, Boolean isFirstPage, Boolean isLastPage){
            this.totalRecords = totalRecords;
            this.totalPages = totalPages;
            this.isFirstPage = isFirstPage;
            this.isLastPage = isLastPage;
        }

        public Integer getTotalRecords() {
            return totalRecords;
        }

        public void setTotalRecords(Integer totalRecords) {
            this.totalRecords = totalRecords;
        }

        public Integer getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        public Boolean getFirstPage() {
            return isFirstPage;
        }

        public void setFirstPage(Boolean firstPage) {
            isFirstPage = firstPage;
        }

        public Boolean getLastPage() {
            return isLastPage;
        }

        public void setLastPage(Boolean lastPage) {
            isLastPage = lastPage;
        }
    }

}

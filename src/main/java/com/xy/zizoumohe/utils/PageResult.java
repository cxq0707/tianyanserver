package com.xy.zizoumohe.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class PageResult<T> {

    @JsonProperty("page")
    private PageInfo pageInfo;

    private List<T> items;

    public PageResult(){}

    public PageResult(PageInfo pageInfo, List<T> items){
        this.pageInfo=pageInfo;
        this.items=items;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

}

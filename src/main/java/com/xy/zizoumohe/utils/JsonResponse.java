package com.xy.zizoumohe.utils;

import java.io.Serializable;

public class JsonResponse implements Serializable {

    private int code;
    private String message;

    private PageInfo page;

    private Object result;

    private Object results;

    public JsonResponse(int code, String message) {
        this.code = code;
        this.message = message;
        this.results = null;
    }

    public static JsonResponse success() {
        return new JsonResponse(200, "操作成功");
    }

    public static JsonResponse failure() {
        return new JsonResponse(0, "failure");
    }

    public int getCode() {
        return this.code;
    }

    public JsonResponse setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return this.message;
    }

    public JsonResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getResults() {
        return this.results;
    }

    public JsonResponse setResults(Object results) {
        this.results = results;
        return this;
    }

    public JsonResponse with(int code, String message) {
        this.code = code;
        this.message = message;
        return this;
    }

    public PageInfo getPage() {
        return page;
    }

    public JsonResponse setPage(PageInfo page) {
        this.page = page;
        return this;
    }

    public Object getResult() {
        return result;
    }

    public JsonResponse setResult(Object result) {
        this.result = result;
        return this;
    }
}

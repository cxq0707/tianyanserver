package com.xy.zizoumohe.model;

public class StuffModel {

    private Integer id;


    private String name;


    private String pic;


    private String rate;


    private Integer proportion;

    private int isRelevance;

    public int getIsRelevance() {
        return isRelevance;
    }

    public void setIsRelevance(int isRelevance) {
        this.isRelevance = isRelevance;
    }

    private String describe;

    private Integer level;
    private Integer gameType;

    public Integer getGameType() {
        return gameType;
    }

    public void setGameType(Integer gameType) {
        this.gameType = gameType;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }


    public String getPic() {
        return pic;
    }


    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }


    public String getRate() {
        return rate;
    }


    public void setRate(String rate) {
        this.rate = rate == null ? null : rate.trim();
    }


    public Integer getProportion() {
        return proportion;
    }


    public void setProportion(Integer proportion) {
        this.proportion = proportion;
    }


    public String getDescribe() {
        return describe;
    }


    public void setDescribe(String describe) {
        this.describe = describe == null ? null : describe.trim();
    }
}
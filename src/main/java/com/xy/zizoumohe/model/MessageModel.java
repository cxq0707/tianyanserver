package com.xy.zizoumohe.model;

import lombok.Getter;
import lombok.Setter;
import java.util.Date;

@Getter
@Setter
public class MessageModel {
    private Long id;
    // 标题
    private String title;
    // 封面图的地址
    private String pic;
    // 类别的id
    private Long categoryId;
    private Date createTime;
    // 媒体类型，视频和图文
    private Integer mediaType;

    private Date updateTime;
    // 是否推荐
    private Boolean isRecommend;
    // 优先级系数
    private Double priority;
    // 视频链接地址
    private String videoLinkUrl;
    // 是否热门
    private Boolean isHot;
    // 版块类型，0综合,1刀塔自走棋,2多多自走棋,3云顶之弈,4刀塔霸业,5赤潮自走棋
    private Integer boardType;
    // 点赞数
    private Integer supports;
    // 评论数
    private Integer comments;
    // 点击量
    private Long hits;
    private Double factor;
    // 作者的id
    private Long authorId;
    // 审核状态
    private Integer approveState;
    // 调整行为
    private Integer actionType;
    // 调整的url
    private String forwardUrl;
    // 简介
    private String briefIntroduction;

    private int uiPattern;

    private String imgList;

    private Double latitude;
    private Double longitude;
    private String locationString;


}

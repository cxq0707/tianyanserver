package com.xy.zizoumohe.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class StoreModel {

    private Long id;

    // 收藏类别id
    private Long storeCategoryId;

    // 收藏人的id
    private Long userId;

    // 收藏的实体id，如阵容id或者讯息id
    private Long entityId;

    // 收藏时间
    private Date storeTime;

    // 游戏类型
    private Integer type;



}

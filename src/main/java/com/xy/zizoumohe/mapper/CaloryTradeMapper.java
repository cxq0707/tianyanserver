package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.CaloryTrade;
import org.apache.ibatis.annotations.Param;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

public interface CaloryTradeMapper {

    int deleteByPrimaryKey(Long id);

    int insert(CaloryTrade record);

    int insertSelective(CaloryTrade record);

    CaloryTrade selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(CaloryTrade record);

    int updateByPrimaryKey(CaloryTrade record);

    List<CaloryTrade> selectByAccountId(Long accountId);

    List<CaloryTrade> selectByAccountIdAndTradeTypeAndDate(@Param("accountId") Long accountId,
                                                           @Param("tradeType") int tradeType, @Param("dateString") String dateString);

    List<CaloryTrade> selectByAccountIdAndTradeTypeAndDateForUpdate(@Param("accountId") Long accountId,
                                                           @Param("tradeType") int tradeType, @Param("dateString") String dateString);

    List<CaloryTrade> selectByAccountIdAndIsIncomeAndDuringDate(@Param("accountId") Long accountId,
                                                                @Param("isIncome") Boolean isIncome,
                                                                @Param("startTime") Date startTime,
                                                                @Param("endTime") Date endTime);

    List<CaloryTrade> selectByAccountIdAndTradeTypeAndEntityId(@Param("accountId") Long accountId,
                                                               @Param("tradeType") int tradeType,
                                                               @Param("entityId") Long entityId);

    List<CaloryTrade> selectByAccountIdAndDate(@Param("accountId") Long accountId,
                                               @Param("dateString") String dateString);


}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Message;
import com.xy.zizoumohe.model.MessageModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageMapper {

    int deleteByPrimaryKey(Long id);

    int insertSelective(Message record);

    Message selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Message record);

    List<Message> selectByCategoryId(Long categoryId);

    List<Message> selectByHotAndCategoryId(@Param("isHot") Boolean isHot,
                                           @Param("categoryId") Long categoryId);

    /**
     * 按是否推荐与类别查询讯息
     * @param isRecommend 是否推荐
     * @param categoryId 类别的id；为NULL表示所有的类别
     * @return result list
     */
    List<Message> selectByRecommendAndCategoryId(@Param("isRecommend") Boolean isRecommend,
                                                 @Param("categoryId") Long categoryId);

    // 获取所有的讯息数量
    Integer selectCount();

    // 获取所有的讯息记录
    List<Message> selectAll();

    // 删除讯息
    int deleteByPrimaryKeyIn(List<Long> messageIdList);

    Integer selectCountByCategoryId(Long categoryId);

    List<Message> getAllMessage();

    List<MessageModel> selectByCategoryIdAndBoardType(@Param("categoryId") Long categoryId,
                                                      @Param("boardType") Integer boardType);
    List<MessageModel> selectByCategoryIdAndApproveStateAndBoardType(@Param("categoryId") Long categoryId,
                                                                     @Param("approveState") Integer approveState,
                                                                     @Param("boardTypes")  List<Integer>  boardTypes,
                                                                     @Param("title") String title);
    List<MessageModel> selectByCategoryIdAndApproveStateAndBoardTypeAndMediaType(@Param("categoryId") Long categoryId,
                                                                                 @Param("approveState") Integer approveState,
                                                                                 @Param("boardTypes")  List<Integer>  boardTypes,
                                                                                 @Param("title") String title, @Param("mediaType") Integer mediaType);
    List<MessageModel> selectByIsRecommendAndBoardType(Integer boardType);

    List<Message> selectByTitle(@Param("title") String title,
                                @Param("audit")Integer audit);

    List<MessageModel> selectByIsRecommendAndApproveStateAndBoardType(@Param("approveState") Integer approveState,
                                                                      @Param("boardTypes") List<Integer> boardTypes);

    List<Message> selectByAuthorId(Long authorId);

    List<Message> selectByAuthorIdAndApproveState(@Param("authorId") Long authorId,
                                                  @Param(("approveState")) Integer approveState);

}
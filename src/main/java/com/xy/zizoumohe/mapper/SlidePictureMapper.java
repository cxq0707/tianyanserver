package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.SlidePicture;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SlidePictureMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SlidePicture record);

    int insertSelective(SlidePicture record);

    SlidePicture selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SlidePicture record);

    int updateByPrimaryKey(SlidePicture record);

    // 获取当前正在使用的轮播图列表
    List<SlidePicture> selectByCurrentUse();

    // 获取所有的轮播图
    List<SlidePicture> selectAll();

    void deleteByPrimaryKeyIn(List<Long> slidePictureIdList);

    int setIsCurrentUse(@Param("isCurrentUse") Boolean isCurrentUse, @Param("id") Long id);
}
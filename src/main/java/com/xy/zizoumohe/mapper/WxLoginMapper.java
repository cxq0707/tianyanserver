package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.WxLogin;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WxLoginMapper {

    int deleteByPrimaryKey(Long wxLoginId);

    int insert(WxLogin record);

    int insertSelective(WxLogin record);


    WxLogin selectByPrimaryKey(Long wxLoginId);

    int updateByPrimaryKeySelective(WxLogin record);

    int updateByPrimaryKey(WxLogin record);

    WxLogin selectOpenidLineByUnionid(String unionid);

    WxLogin selectByOpenid(String openid);

    List<WxLogin> selectAllOpenids();
}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.ManageMessage;

import java.util.List;

public interface ManageMessageMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ManageMessage record);

    int insertSelective(ManageMessage record);

    ManageMessage selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ManageMessage record);

    int updateByPrimaryKey(ManageMessage record);

    List<ManageMessage> selectByToUserId(Long toUserId);

}
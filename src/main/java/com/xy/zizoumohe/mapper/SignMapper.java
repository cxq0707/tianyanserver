package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Sign;
import org.apache.ibatis.annotations.Param;

public interface SignMapper {

    int deleteByPrimaryKey(Long id);

    int insertSelective(Sign record);

    Sign selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Sign record);

    int updateByPrimaryKey(Sign record);

    /**
     * 按用户id与签到时间进行查找
     * @param userId 用户id
     * @param signDate 签到时间，格式为yyyy-MM-dd
     * @return Sign
     */
    Sign selectByUserIdAndCreateTime(@Param("userId") Long userId, @Param("signDate") String signDate);



}
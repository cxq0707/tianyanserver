package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.UserResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserResourceMapper {
    UserResource isPower(@Param("userId") Long userId);
    List<UserResource> selectUserPower(@Param("userId") Long userId,@Param("powerIds")List<Integer> powerIds);
    int bulkInsert(@Param("userId") Long userId);
    void bulkDelete(@Param("userId") Long userId);
}

package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Calory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CaloryMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Calory record);

    int insertSelective(Calory record);

    Calory selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Calory record);

    int updateByPrimaryKey(Calory record);

    Calory selectByUserId(Long userId);
    
    Calory selectByUserIdForUpdate(Long userId);

    /**
     * 按照userId去更新记录
     * @param calory calory.calory中userId不可更新，primary key也不可更改
     * @return 发生改变的记录数
     */
    int updateByUserId(Calory calory);

    /**
     * 扣除某个用户的卡路里
     * @param userId 用户的id
     * @param num 扣除的数量
     * @return 更新的记录个数
     */
    int decreaseByUserId(@Param("userId") Long userId,
                         @Param("num") int num);

    /**
     * 增加{userId}的卡路里财富值
     * @param userId 用户的id
     * @param num 增加的余额
     * @return 更新的记录个数
     */
    int increaseByUserId(@Param("userId") Long userId,
                         @Param("num") int num);

    List<Calory> selectAllLinkUser();
}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.MessageCommentSupport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageCommentSupportMapper {

    int deleteByPrimaryKey(Long id);

    int insert(MessageCommentSupport record);

    int insertSelective(MessageCommentSupport record);

    MessageCommentSupport selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MessageCommentSupport record);

    int updateByPrimaryKey(MessageCommentSupport record);

    MessageCommentSupport selectByMessageCommentIdAndUserId(@Param("messageCommentId") Long messageCommentId,@Param("userId") Long userId);

    List<MessageCommentSupport> selectByUserId(Long userId);

    int deleteByMessageCommentIdIn(List<Long> messageCommentIdList);

}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Store;
import com.xy.zizoumohe.model.StoreModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zachary zhu
 * @date 2019/5/21  18:01
 * @corporation UESTC
 */
public interface StoreMapper {

    Store selectByPrimaryKey(Long id);

    Store selectByStoreCategoryIdAndUserIdAndEntityId(@Param("storeCategoryId") Long storeCategoryId,
                                                      @Param("userId") Long userId, @Param("entityId") Long entityId);

    List<Store> selectByStoreCategoryIdAndUserId(@Param("storeCategoryId") Long storeCategoryId,
                                                 @Param("userId") Long userId);

    Integer selectCountByStoreCategoryIdAndUserId(@Param("storeCategoryId") Long storeCategoryId,
                                                  @Param("userId") Long userId);

    int insertSelective(Store record);

    int deleteByPrimaryKeyIn(List<Long> idList);

    int deleteByPrimaryKey(Long id);

    Store selectByPrimaryKeyAndUserId(@Param("id") Long id,@Param("userId") Long userId);

    List<Store> selectByStoreCategoryIdAndEntityIdAndUserId(@Param("storeCategoryId") Long storeCategoryId,
                                                            @Param("entityId") Long entityId,@Param("userId") Long userId);

    List<StoreModel> selectByStoreCategoryIdAndTypeAndUserId(@Param("storeCategoryId") Long storeCategoryId,
                                                             @Param("type") Integer type,
                                                             @Param("currentLoginUserId") Long currentLoginUserId);

}

package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Token;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

public interface TokenMapper {
    int deleteByPrimaryKey(Long tokenId);

    int insert(Token record);

    int insertSelective(Token record);

    Token selectByPrimaryKey(Long tokenId);
    Token selectByTokenString(String tokenString);


    int updateByPrimaryKeySelective(Token record);

    int updateByPrimaryKey(Token record);

    Token selectByUserId(Long userId);
}
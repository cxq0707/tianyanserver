package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.MessageDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageDetailMapper {

    int deleteByPrimaryKey(Long id);

    int insert(MessageDetail record);

    int insertSelective(MessageDetail record);

    MessageDetail selectByPrimaryKey(Long id);

    String selectVideoLinkUrlByPrimaryKey(Long id);

    String selectVideoLinkUrlByMessageId(Long messageId);

    int updateByPrimaryKeySelective(MessageDetail record);

    int updateByPrimaryKeyWithBLOBs(MessageDetail record);

    int updateByPrimaryKey(MessageDetail record);

    MessageDetail selectByMessageId(Long messageId);

    int deleteByMessageIdIn(List<Long> messageIdList);

}
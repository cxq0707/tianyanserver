package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Share;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShareMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Share record);

    int insertSelective(Share record);

    Share selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Share record);

    int updateByPrimaryKey(Share record);

    List<Share> selectByUserIdAndShareTypeAndEntityIdAndDate(@Param("userId") Long userId,
                                                  @Param("shareType") Integer shareType,
                                                  @Param("entityId") Long entityId,
                                                  @Param("dateString") String dateString);


}
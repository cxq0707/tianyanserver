package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.InviteInfo;

import java.util.List;

public interface InviteInfoMapper {
    
    int deleteByPrimaryKey(Integer id);

    
    int insert(InviteInfo record);

    
    int insertSelective(InviteInfo record);

    
    InviteInfo selectByPrimaryKey(Integer id);

    
    int updateByPrimaryKeySelective(InviteInfo record);

    
    int updateByPrimaryKey(InviteInfo record);

    int selectCountByToUserId(Long toUserId);

    int selectCountByUserId(Long userId);



    List<InviteInfo> selectByUserId(Long userId);
}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Resource;

import java.util.List;

public interface ResourceMapper {
    List<Resource> selectAll();
}

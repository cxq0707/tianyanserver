package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Admin;

public interface AdminMapper {

    Admin getAdminByName(String name);

    int insertSelective(Admin admin);

    Admin selectByPrimaryKey(Integer id);
}

package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Talent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TalentMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Talent record);

    int insertSelective(Talent record);

    Talent selectByPrimaryKey(Integer id);


    int updateByPrimaryKeySelective(Talent record);


    int updateByPrimaryKey(Talent record);

    List<Talent> selectByGameType(Integer gameType);

    void updateRank(@Param("oldRank") int oldRank, @Param("gameType") Integer gameType,  @Param("newRank")int newRank);

    Talent selectMinRank(int gameType);

    int isExist(@Param("userId") Long userId, @Param("gameType") Integer gameType);
}
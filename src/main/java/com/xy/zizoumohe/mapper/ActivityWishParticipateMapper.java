package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.ActivityWishParticipate;
import org.apache.ibatis.annotations.Param;

import javax.validation.constraints.Max;
import java.util.List;

public interface ActivityWishParticipateMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ActivityWishParticipate record);

    int insertSelective(ActivityWishParticipate record);

    ActivityWishParticipate selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ActivityWishParticipate record);

    int updateByPrimaryKey(ActivityWishParticipate record);

    ActivityWishParticipate selectByUserIdAndActivityWishId(@Param("userId") Long userId,
                                                            @Param("activityWishId") Long activityWishId);

    List<ActivityWishParticipate> selectByActivityWishId(Long activityWishId);

    int updateUploadShareState(@Param("activityWishId") Long activityWishId,
                               @Param("userId") Long userId,
                               @Param("isUploadShare") Boolean isUploadShare);

    void addInspire(@Param("userId") Long userId,@Param("activityWishId") Long activityWishId);
}
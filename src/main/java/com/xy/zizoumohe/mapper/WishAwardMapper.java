package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.WishAward;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WishAwardMapper {

    int deleteByPrimaryKey(Long id);

    int insert(WishAward record);

    int insertSelective(WishAward record);

    WishAward selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WishAward record);

    int updateByPrimaryKey(WishAward record);

    List<WishAward> selectByUserId(Long userId);

    List<WishAward> selectByActivityWishId(Long activityWishId);

    WishAward selectByActivityWishIdAndUserId(@Param("activityWishId") Long activityWishId,
                                              @Param("userId") Long userId);

}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Stuff;

import java.util.List;

public interface StuffMapper {
    
    int deleteByPrimaryKey(Integer id);


    int insertSelective(Stuff record);

    
    Stuff selectByPrimaryKey(Integer id);

    
    int updateByPrimaryKeySelective(Stuff record);


    List<Stuff> selectAllByGameType(Integer gameType);
}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.User;

import java.util.List;

import com.xy.zizoumohe.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Long userId);

    int insertSelective(User record);

    User selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(User record);

    User getUserByUniqueId(@Param("unionId")String unionId, @Param("openId")String openId);

    User getUserByUserName(User user);
    User getUserByUserName(String userName);

    List<User> selectByIdIn(List<Long> idList);

    List<User> selectByNickname(String nickname);

    int updateUserNameByKey(@Param("userId") Long userId,@Param("userName")String userName);
}
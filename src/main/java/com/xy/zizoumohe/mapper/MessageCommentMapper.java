package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.MessageComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageCommentMapper {

    int deleteByPrimaryKey(Long id);

    int insert(MessageComment record);

    int insertSelective(MessageComment record);

    MessageComment selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MessageComment record);

    int updateByPrimaryKey(MessageComment record);

    List<MessageComment> selectByMessageId(Long messageId);

    List<MessageComment> selectByMessageIdIn(List<Long> messageIdList);

    List<MessageComment> selectByToMessageCommentId(Long toMessageCommentId);

    Integer selectCountByMessageId(Long messageId);

    Integer selectCountByToMessageCommentId(Long toMessageCommentId);

    List<Object[]> selectReplysForMessageCommentIdIn(List<Long> toMessageCommentIdList);

    void updateSupportsAutoIncrement(Long messageCommentId);

    void updateReplysAutoIncrement(Long messageCommentId);

    List<Long> selectUserIdByMessageId(Long messageId);

    List<MessageComment> selectByMessageIdAndToMessageCommentId(@Param("messageId") Long messageId,
                                                             @Param("toMessageCommentId") Long toMessageCommentId);

    int deleteByMessageIdIn(List<Long> messageIdList);

    // 按messageId获取精选的讯息评论
    List<MessageComment> selectByIsExcellentAndMessageId(Long messageId);

    // 获取userId所发表的所有评论
    List<MessageComment> selectByUserId(Long userId);

    List<MessageComment> selectMessageCommentByMessageIdAndContent(@Param("messageId") Long messageId,@Param("content") String content);

    int setIsExcellent(@Param("isExcellent") Boolean isExcellent,@Param("id") Long id);

    void updateComment(MessageComment comment);
}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.UploadShare;
import org.apache.ibatis.annotations.Param;

public interface UploadShareMapper {

    int deleteByPrimaryKey(Long id);

    int insert(UploadShare record);

    int insertSelective(UploadShare record);

    UploadShare selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(UploadShare record);

    int updateByPrimaryKey(UploadShare record);

    UploadShare selectByUserIdAndActivityWishId(@Param("userId") Long userId,
                                                @Param("activityWishId") Long activityWishId);


}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Ornament;

import java.util.List;

public interface OrnamentMapper {
    
    int deleteByPrimaryKey(Integer id);

    
    int insert(Ornament record);

    
    int insertSelective(Ornament record);

    
    Ornament selectByPrimaryKey(Integer id);

    
    int updateByPrimaryKeySelective(Ornament record);

    
    int updateByPrimaryKey(Ornament record);

    List<Ornament> selectByType(int ornamentType);

    int selectCalorieByKey(int ornamentId);

    void stockReduce(int id);

    List<Ornament> selectAll();
}
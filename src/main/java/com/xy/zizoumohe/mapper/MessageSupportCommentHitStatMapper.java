package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.MessageSupportCommentHitStat;

import java.util.List;

public interface MessageSupportCommentHitStatMapper {

    int deleteByPrimaryKey(Long id);

    int insert(MessageSupportCommentHitStat record);

    int insertSelective(MessageSupportCommentHitStat record);

    MessageSupportCommentHitStat selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MessageSupportCommentHitStat record);

    int updateSupportsAutoIncrement(Long messageId);

    int updateCommentsAutoIncrement(Long messageId);

    int updateHitsAutoIncrement(Long messageId);

    int updateHitsRandomIncrement(Long messageId);

    List<MessageSupportCommentHitStat> selectByMessageIdIn(List<Long> messageIdList);

    MessageSupportCommentHitStat selectByMessageId(Long messageId);

    // 删除
    int deleteByMessageIdIn(List<Long> messageIdList);


}
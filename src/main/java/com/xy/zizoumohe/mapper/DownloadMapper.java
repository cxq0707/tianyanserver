package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Download;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface DownloadMapper {
    int insertSelective(Download record);

    Integer selectByTimeInterval(@Param("startDate") Date startDate,@Param("endDate") Date endDate, @Param("type")String type);

    List<String> selectGroupByType();
}

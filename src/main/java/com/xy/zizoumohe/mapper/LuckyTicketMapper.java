package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.LuckyTicket;
import org.apache.ibatis.annotations.Param;

public interface LuckyTicketMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(LuckyTicket record);

    int insertSelective(LuckyTicket record);

    LuckyTicket selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(LuckyTicket record);

    int updateByPrimaryKey(LuckyTicket record);

    LuckyTicket selectByUserId(long userId);

    void add(@Param("num") int num, @Param("userId")long userId);

    int updateNumByUserId(@Param("userId") Long userId, @Param("delta") Integer delta);

}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.MessageSupport;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageSupportMapper {

    int deleteByPrimaryKey(Long id);

    int insert(MessageSupport record);

    int insertSelective(MessageSupport record);

    MessageSupport selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(MessageSupport record);

    int updateByPrimaryKey(MessageSupport record);

    MessageSupport selectByMessageIdAndUserId(@Param("messageId") Long messageId, @Param("userId") Long userId);

    int deleteByMessageIdIn(List<Long> messageIdList);

    List<MessageSupport> selectByUserIdAndSupportTime(@Param("userId") Long userId, @Param("date") String date);

}
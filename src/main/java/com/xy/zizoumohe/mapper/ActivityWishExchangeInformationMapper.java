package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.ActivityWishExchangeInformation;

import java.util.List;

public interface ActivityWishExchangeInformationMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ActivityWishExchangeInformation record);

    int insertSelective(ActivityWishExchangeInformation record);

    ActivityWishExchangeInformation selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ActivityWishExchangeInformation record);

    int updateByPrimaryKey(ActivityWishExchangeInformation record);

    List<ActivityWishExchangeInformation> selectByActivityWishId(Long activityWishId);

}
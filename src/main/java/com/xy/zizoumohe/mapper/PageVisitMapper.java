package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.PageVisit;

public interface PageVisitMapper {

    int deleteByPrimaryKey(Long id);

    int insert(PageVisit record);

    int insertSelective(PageVisit record);

    PageVisit selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(PageVisit record);

    int updateByPrimaryKey(PageVisit record);

    PageVisit selectByUserId(Long userId);

}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Paper;

public interface PaperMapper {

    int deleteByPrimaryKey(Long paperId);

    int insert(Paper record);

    int insertSelective(Paper record);

    Paper selectByPrimaryKey(Long paperId);

    int updateByPrimaryKeySelective(Paper record);

    int updateByPrimaryKey(Paper record);
}
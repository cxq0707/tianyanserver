package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.TalentApplyFor;

import java.util.List;

public interface TalentApplyForMapper {
    
    int deleteByPrimaryKey(Integer id);

    
    int insert(TalentApplyFor record);

    
    int insertSelective(TalentApplyFor record);

    
    TalentApplyFor selectByPrimaryKey(Integer id);

    
    int updateByPrimaryKeySelective(TalentApplyFor record);

    
    int updateByPrimaryKey(TalentApplyFor record);

    List<TalentApplyFor> selectByGameType(Integer gameType);
}
package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Attention;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AttentionMapper {

    int deleteByPrimaryKey(Long id);

    int deleteByUserIdAndToUserId(@Param("userId") Long userId,
                                  @Param("toUserId") Long toUserId);

    int insertSelective(Attention record);

    Attention selectByPrimaryKey(Long id);

    Attention selectByUserIdAndToUserId(@Param("userId") Long userId, @Param("toUserId") Long toUserId);

    List<Attention> selectByUserId(Long userId);

    List<Attention> selectByToUserId(Long toUserId);

    List<Long> selectIdListByToUserId(Long toUserId);

    Integer selectCountByUserId(Long userId);

    int updateByPrimaryKeySelective(Attention record);

    int updateByPrimaryKey(Attention record);


    List<Attention> fansTalentList();
}

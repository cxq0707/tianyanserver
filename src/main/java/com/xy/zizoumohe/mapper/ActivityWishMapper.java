package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.ActivityWish;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ActivityWishMapper {

    int deleteByPrimaryKey(Long id);

    int insert(ActivityWish record);

    int insertSelective(ActivityWish record);

    ActivityWish selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ActivityWish record);

    int updateByPrimaryKey(ActivityWish record);

    List<ActivityWish> selectAll();

    int increaseParticipateNumber(@Param("id") Long id,
                                  @Param("delta") int delta);

    /**
     * 查询未开奖的许愿活动
     * @param current 当前时间
     * @return result list
     */
    List<ActivityWish> selectByOpenTimeLess(Date current);

    /**
     * 查询未过期的许愿活动
     * @param current 当前时间
     * @return result list
     */
    List<ActivityWish> selectByEndTimeLess(Date current);

    /**
     * 查询时间区间在开始时间到结束时间的许愿活动列表
     * @param current 当前时间
     * @return result list
     */
    List<ActivityWish> selectByTimeBetween(Date current);

    //查询在阵容列表展示的许愿活动
    List<ActivityWish> selectByShow();

    void setShow(@Param("isShow") int isShow, @Param("id") Long id);
}
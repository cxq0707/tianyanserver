package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.UserOrnament;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserOrnamentMapper {

    int deleteByPrimaryKey(Long id);


    int insertSelective(UserOrnament record);


    UserOrnament selectByPrimaryKey(Long id);


    int updateByPrimaryKeySelective(UserOrnament record);


    List<UserOrnament> selectByUserId(Long userId);

    UserOrnament selectByUserIdAndOrnamentId(@Param("userId") Long userId, @Param("ornamentId")int ornamentId);

    List<UserOrnament> selectByUserIdAndType(@Param("userId")Long userId, @Param("ornamentType") Integer ornamentType);

    void updateStartByIds(List<Long> userOrnamentIdList);

    List<UserOrnament>  selectTitle(Long userId);

    List<UserOrnament> selectTitleByUserIds(List<Long> userIds);

    List<UserOrnament> selectAllTitleByUserIds(List<Long> userIdList);
}
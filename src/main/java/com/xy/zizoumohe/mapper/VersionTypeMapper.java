package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.VersionType;
import org.apache.catalina.LifecycleState;

import java.util.List;

public interface VersionTypeMapper {

    int deleteByPrimaryKey(Long id);

    int insert(VersionType record);

    int insertSelective(VersionType record);

    VersionType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(VersionType record);

    int updateByPrimaryKey(VersionType record);

    List<VersionType> selectByAndroidVersionId(Long androidVersionId);

}
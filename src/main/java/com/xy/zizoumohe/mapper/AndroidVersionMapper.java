package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.AndroidVersion;

import java.util.List;

public interface AndroidVersionMapper {

    int deleteByPrimaryKey(Long id);

    int insert(AndroidVersion record);

    int insertSelective(AndroidVersion record);

    AndroidVersion selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(AndroidVersion record);

    int updateByPrimaryKey(AndroidVersion record);

    AndroidVersion selectByLatestVersion();

    List<AndroidVersion> selectAll();

}
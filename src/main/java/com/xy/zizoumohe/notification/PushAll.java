package com.xy.zizoumohe.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.bean.PushBean;
import com.xy.zizoumohe.utils.enumeration.Environment;
import com.xy.zizoumohe.utils.enumeration.YouMeng;
//import push.android.AndroidBroadcast;
//import push.ios.IOSBroadcast;

import java.util.HashMap;
import java.util.Map;

public class PushAll {
    public void sendBroadcast(PushBean pushBean, String title, String subhead, String body) {
        try {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ObjectMapper mapper = new ObjectMapper();
                                Map map = new HashMap();
                                map.put("title", title);
                                map.put("subtitle", subhead);
                                map.put("body", body);
                                String json = mapper.writeValueAsString(pushBean);
                                sendIOSBroadcast(pushBean, json, map);
                                sendAndroidBroadcast(pushBean, json, map);
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println(e);
                            }
                        }
                    }
            ).start();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void sendAndroidBroadcast(PushBean pushBean, String json, Map map) throws Exception {
//        AndroidBroadcast broadcast = new AndroidBroadcast();
//        broadcast.setAppMasterSecret(YouMeng.ANDROID_APP_MASTER_SECRET.getValue());
//        broadcast.setPredefinedKeyValue("appkey", YouMeng.ANDROID_APP_KEY.getValue());
//        broadcast.setPredefinedKeyValue("timestamp", Integer.toString((int) (System.currentTimeMillis() / 1000L)));
//        broadcast.setPredefinedKeyValue("ticker", json);
//        broadcast.setPredefinedKeyValue("title", map.get("title"));
//        broadcast.setPredefinedKeyValue("text", map.get("subtitle"));
//        broadcast.setPredefinedKeyValue("after_open", "go_app");
//        broadcast.setPredefinedKeyValue("display_type", "notification");
//        broadcast.setPredefinedKeyValue("production_mode", Environment.YOUMENG.getValue());
//        broadcast.send();
    }

    public void sendIOSBroadcast(PushBean pushBean, String json, Map map) throws Exception {
//        IOSBroadcast broadcast = new IOSBroadcast();
//        broadcast.setAppMasterSecret(YouMeng.IOS_APP_MASTER_SECRET.getValue());
//        broadcast.setPredefinedKeyValue("appkey", YouMeng.IOS_APP_KEY.getValue());
//        broadcast.setPredefinedKeyValue("type", "broadcast");
//        broadcast.setPredefinedKeyValue("timestamp", Integer.toString((int) (System.currentTimeMillis() / 1000L)));
//        broadcast.setPredefinedKeyValue("alert", map);
//        broadcast.setPredefinedKeyValue("badge", 0);
//        broadcast.setPredefinedKeyValue("sound", "chime");
//        broadcast.setPredefinedKeyValue("production_mode", Environment.YOUMENG.getValue());
//        broadcast.setCustomizedField("message", json);
//        broadcast.send();
    }
}

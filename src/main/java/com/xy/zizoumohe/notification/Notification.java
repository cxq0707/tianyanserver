package com.xy.zizoumohe.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.bean.PushBean;
import com.xy.zizoumohe.utils.enumeration.Environment;
import com.xy.zizoumohe.utils.enumeration.YouMeng;
//import push.android.AndroidCustomizedcast;
//import push.android.AndroidUnicast;
//import push.ios.IOSCustomizedcast;

public class Notification {
    public void sendIOSUnicast(Long userId, PushBean pushBean) {
        try {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ObjectMapper mapper = new ObjectMapper();
                                String json = mapper.writeValueAsString(pushBean);
                                sendIOSCustomizedcast(userId, pushBean, json);
                                sendAndroidCustomizedcast(userId, pushBean, json);
                            } catch (Exception e) {
                                System.out.println(e);
                            }
                        }
                    }
            ).start();

        } catch (Exception e) {
            System.out.println(e);
        }

    }


    public void sendAndroidCustomizedcast(Long userId, PushBean pushBean, String json) throws Exception {
//        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast();
//        customizedcast.setAppMasterSecret(YouMeng.ANDROID_APP_MASTER_SECRET.getValue());
//        customizedcast.setPredefinedKeyValue("appkey", YouMeng.ANDROID_APP_KEY.getValue());
//        customizedcast.setPredefinedKeyValue("timestamp", Integer.toString((int) (System.currentTimeMillis() / 1000L)));
//        customizedcast.setPredefinedKeyValue("alias", userId);
//        customizedcast.setPredefinedKeyValue("alias_type", "Android");
//        customizedcast.setPredefinedKeyValue("ticker", json);
//        customizedcast.setPredefinedKeyValue("title", pushBean.getTitle());
//        customizedcast.setPredefinedKeyValue("text", "");
//        customizedcast.setPredefinedKeyValue("after_open", "go_app");
//        customizedcast.setPredefinedKeyValue("display_type", "notification");
//        customizedcast.setPredefinedKeyValue("production_mode", Environment.YOUMENG.getValue());
//        customizedcast.send();
    }

    public void sendIOSCustomizedcast(Long userId, PushBean pushBean, String json) throws Exception {
//        IOSCustomizedcast customizedcast = new IOSCustomizedcast();
//        customizedcast.setAppMasterSecret(YouMeng.IOS_APP_MASTER_SECRET.getValue());
//        customizedcast.setPredefinedKeyValue("appkey", YouMeng.IOS_APP_KEY.getValue());
//        customizedcast.setPredefinedKeyValue("timestamp", Integer.toString((int) (System.currentTimeMillis() / 1000L)));
//        customizedcast.setPredefinedKeyValue("alias", userId);
//        customizedcast.setPredefinedKeyValue("alias_type", "iOS");
//        customizedcast.setPredefinedKeyValue("alert", pushBean.getTitle());
//        customizedcast.setPredefinedKeyValue("badge", 0);
//        customizedcast.setPredefinedKeyValue("sound", "chime");
//        customizedcast.setPredefinedKeyValue("content-available", 1);
//        customizedcast.setCustomizedField("message", json);
//        customizedcast.setPredefinedKeyValue("production_mode", Environment.YOUMENG.getValue());
//        customizedcast.send();
    }


}

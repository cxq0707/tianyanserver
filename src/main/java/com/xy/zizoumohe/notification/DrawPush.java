package com.xy.zizoumohe.notification;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.bean.PushBean;
import com.xy.zizoumohe.utils.enumeration.BehaviorType;
import com.xy.zizoumohe.utils.enumeration.Environment;
import com.xy.zizoumohe.utils.enumeration.SubjectType;
import com.xy.zizoumohe.utils.enumeration.YouMeng;
import org.springframework.stereotype.Component;
//import push.android.AndroidCustomizedcast;
//import push.ios.IOSCustomizedcast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DrawPush {
    /**
     * @param userIdList     参与的用户ID集合
     * @param activityWishId 活动ID
     * @param giftName       礼物名称
     */
    public void push(List<Long> userIdList, Long activityWishId, String giftName) {
        try {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                PushBean pushBean = PushBean.getInstance().setSubjectId(activityWishId).setBehaviorType(BehaviorType.OTHER).setSubjectType(SubjectType.VOW_TO);
                                ObjectMapper mapper = new ObjectMapper();
                                Map map = new HashMap();
                                map.put("title", "开奖啦");
                                map.put("subtitle", "你许愿的" + giftName + "开奖了，快来看看你有没有中奖吧!");
                                String json = mapper.writeValueAsString(pushBean);
                                StringBuffer sb = new StringBuffer();
                                for (int i = 0; i < userIdList.size(); i++) {
                                    sb.append(userIdList.get(i) + ",");
                                    //因为友盟一次最多对500个用户推送，所以每过450个推送一次并清空之前的人
                                    if (i % 450 == 0 && i != 0) {
                                        sendIOSBroadcast(json, map, sb.toString());
                                        sendAndroidBroadcast(json, map, sb.toString());
                                        sb = new StringBuffer();
                                    }
                                }
                                //推送剩余的
                                sendIOSBroadcast(json, map, sb.toString());
                                sendAndroidBroadcast(json, map, sb.toString());

                            } catch (Exception e) {
                                System.out.println(e);
                            }
                        }
                    }
            ).start();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void fansPush(List<Long> userIdList, SubjectType subjectType, long subjectId, String title,String subtitle,int postType) {
        fansPush(userIdList, subjectType, subjectId, title,subtitle, null, null,postType);
    }

    public void fansPush(List<Long> userIdList, SubjectType subjectType, long subjectId, String title,String subtitle) {
        fansPush(userIdList, subjectType, subjectId, title,subtitle, null, null,1);
    }

    public void fansPush(List<Long> userIdList, SubjectType subjectType, long subjectId, int gameType, String title,String subtitle) {
        fansPush(userIdList, subjectType, subjectId, title,subtitle, gameType, null,1);
    }

    public void fansPush(List<Long> userIdList, SubjectType subjectType, long subjectId, String title ,String subtitle,Integer mediaType) {
        fansPush(userIdList, subjectType, subjectId, title, null,null, mediaType,1);
    }

    public void fansPush(List<Long> userIdList, SubjectType subjectType, long subjectId, String title,String subtitle, Integer gameType, Integer mediaType,int postType) {
        try {
            new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                PushBean pushBean = PushBean.getInstance().setSubjectId(subjectId).setBehaviorType(BehaviorType.OTHER).setSubjectType(subjectType);
                                ObjectMapper mapper = new ObjectMapper();
                                Map map = new HashMap();
                                map.put("title", title);
                                map.put("subtitle", subtitle);
                                String json = mapper.writeValueAsString(pushBean);
                                StringBuffer sb = new StringBuffer();
                                for (int i = 0; i < userIdList.size(); i++) {
                                    sb.append(userIdList.get(i) + ",");
                                    //因为友盟一次最多对500个用户推送，所以每过450个推送一次并清空之前的人
                                    if (i % 450 == 0 && i != 0) {
                                        sendIOSBroadcast(json, map, sb.toString());
                                        sendAndroidBroadcast(json, map, sb.toString());
                                        sb = new StringBuffer();
                                    }
                                }
                                //推送剩余的
                                sendIOSBroadcast(json, map, sb.toString());
                                sendAndroidBroadcast(json, map, sb.toString());

                            } catch (Exception e) {
                                System.out.println(e);
                            }
                        }
                    }
            ).start();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void sendAndroidBroadcast(String json, Map map, String idList) throws Exception {
//        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast();
//        customizedcast.setAppMasterSecret(YouMeng.ANDROID_APP_MASTER_SECRET.getValue());
//        customizedcast.setPredefinedKeyValue("appkey", YouMeng.ANDROID_APP_KEY.getValue());
//        customizedcast.setPredefinedKeyValue("timestamp", Integer.toString((int) (System.currentTimeMillis() / 1000L)));
//        customizedcast.setPredefinedKeyValue("alias", idList);
//        customizedcast.setPredefinedKeyValue("alias_type", "Android");
//        customizedcast.setPredefinedKeyValue("ticker", json);
//        customizedcast.setPredefinedKeyValue("title", map.get("title"));
//        customizedcast.setPredefinedKeyValue("text", map.get("subtitle"));
//        customizedcast.setPredefinedKeyValue("after_open", "go_app");
//        customizedcast.setPredefinedKeyValue("display_type", "notification");
//        customizedcast.setPredefinedKeyValue("production_mode", Environment.YOUMENG.getValue());
//        customizedcast.send();
    }

    private void sendIOSBroadcast(String json, Map map, String idList) throws Exception {
//        IOSCustomizedcast customizedcast = new IOSCustomizedcast();
//        customizedcast.setAppMasterSecret(YouMeng.IOS_APP_MASTER_SECRET.getValue());
//        customizedcast.setPredefinedKeyValue("appkey", YouMeng.IOS_APP_KEY.getValue());
//        customizedcast.setPredefinedKeyValue("timestamp", Integer.toString((int) (System.currentTimeMillis() / 1000L)));
//        customizedcast.setPredefinedKeyValue("alias", idList);
//        customizedcast.setPredefinedKeyValue("alias_type", "iOS");
//        customizedcast.setPredefinedKeyValue("alert", map);
//        customizedcast.setPredefinedKeyValue("badge", 0);
//        customizedcast.setPredefinedKeyValue("sound", "chime");
//        customizedcast.setPredefinedKeyValue("content-available", 1);
//        customizedcast.setCustomizedField("message", json);
//        customizedcast.setPredefinedKeyValue("production_mode", Environment.YOUMENG.getValue());
//        customizedcast.send();
    }
}

package com.xy.zizoumohe.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author zachary zhu
 * @date 2019/6/27  17:09
 * @corporation UESTC
 */
@Component
public class PropertyConfig {

    @Value("${acquire.supports.limits}")
    public int acquireSupportsLimits;

    public int getAcquireSupportsLimits() {
        return acquireSupportsLimits;
    }

    public void setAcquireSupportsLimits(int acquireSupportsLimits) {
        this.acquireSupportsLimits = acquireSupportsLimits;
    }



}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.MessageCommentService;
import com.xy.zizoumohe.vo.request.MessageCommentRequestVO;
import com.xy.zizoumohe.vo.MessageCommentSimpleVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageCommentServiceTest {

    @Autowired
    private MessageCommentService messageCommentService;

    @Test
    public void getExcellentMessageComment(){
        List<MessageCommentSimpleVO> messageCommentSimpleVOList = this.messageCommentService.getExcellentMessageComment(20l);
        messageCommentSimpleVOList.stream().map(e ->{
            System.out.println(e.getMessageCommentId()+ "  "+ e.getUserId() + "  "+ e.getNickname() + "  "+ e.getUserHeadPortrait() +
            "  "+ e.getCommentContent());
            return null;
        }).collect(Collectors.toList());
    }

    @Test
    public void getMessageCommentSupports(){
        Long totalSupports = this.messageCommentService.getMessageCommentSupports(213l);
        System.out.println(totalSupports);
    }

    @Test
    public void createMessageComment(){
        MessageCommentRequestVO messageCommentRequestVO = new MessageCommentRequestVO();
        messageCommentRequestVO.setToMessageCommentId(62L);
        messageCommentRequestVO.setMessageId(48L);
        messageCommentRequestVO.setUserId(213L);
        messageCommentRequestVO.setCommentContent("我来测试11:45");
        messageCommentRequestVO.setCommentImageList(null);
        this.messageCommentService.createMessageComment(messageCommentRequestVO);

    }



}

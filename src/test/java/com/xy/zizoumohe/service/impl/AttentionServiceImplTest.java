package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.AttentionService;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.AttentionVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AttentionServiceImplTest {

    @Autowired
    private AttentionService attentionService;


    @Test
    public void addAttention(){

//        Integer attentionState = this.attentionService.addAttention(new Long(1),new Long(13));
//        System.out.println(attentionState);
//        System.out.println("test end...");


//        Integer attentionState2 = this.attentionService.addAttention(new Long(1),new Long(1339900));
//        System.out.println(attentionState2);
//        System.out.println("test end...");

//        Integer attentionState3 = this.attentionService.addAttention(new Long(13399),new Long(1));
//        System.out.println(attentionState3);
//        System.out.println("test end...");

        Integer attentionState4 = this.attentionService.addAttention(new Long(1),new Long(21));
        System.out.println(attentionState4);
        System.out.println("test end...");

        Integer attentionState5 = this.attentionService.addAttention(new Long(1),new Long(22));
        System.out.println(attentionState5);
        System.out.println("test end...");

        Integer attentionState6 = this.attentionService.addAttention(new Long(1),new Long(23));
        System.out.println(attentionState6);
        System.out.println("test end...");

    }


    @Test
    public void cancelAttention(){

//        Integer attentionState = this.attentionService.cancelAttention(new Long(1),new Long(1));
//        System.out.println(attentionState);
//        System.out.println("test end");

//        Integer attentionState2 = this.attentionService.cancelAttention(new Long(1),new Long(180));
//        System.out.println(attentionState2);
//        System.out.println("test end");

//        Integer attentionState3 = this.attentionService.cancelAttention(new Long(1),new Long(189000));
//        System.out.println(attentionState3);
//        System.out.println("test end");


        Integer attentionState4 = this.attentionService.cancelAttention(new Long(1),new Long(150));
        System.out.println(attentionState4);
        System.out.println("test end");

    }


    @Test
    public void getAttentionList( ){
        PageRequest pageRequest = new PageRequest(1,10,"attention_time","DESC");
        PageResult<AttentionVO> pageResult = this.attentionService.getAttentionList(new Long(1),pageRequest);

        pageResult.getItems().stream().map(e ->{
            System.out.println(e.getAttentionId() + "  "+ e.getUserId()+ "  "+ e.getToUser().getUserId()+ "  "+ e.getAttentionState());
            return null;
        }).collect(Collectors.toList());

    }

    @Test
    public void getAttentions(){
        Integer attentions = this.attentionService.getAttentions(1l);
        System.out.println(attentions);
    }

    @Test
    public void getFans(){
        Integer fans = this.attentionService.getFans(13l);
        System.out.println(fans);
    }

    @Test
    public void getAttentionState(){
        Integer attentionState1 = this.attentionService.getAttentionState(1l,13399l);
        Integer attentionState2 = this.attentionService.getAttentionState(1l,22l);
        Integer attentionState3 = this.attentionService.getAttentionState(21l,1l);
        System.out.println(attentionState1);
        System.out.println(attentionState2);
        System.out.println(attentionState3);

    }


}

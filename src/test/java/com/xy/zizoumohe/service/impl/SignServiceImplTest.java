package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.SignService;
import com.xy.zizoumohe.vo.SignVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SignServiceImplTest {
    @Autowired
    private SignService signService;

    @Test
    public void createSign(){
        SignVO signVO = this.signService.createSign(10L,new Date());
        System.out.println(signVO.getSignId());
    }

}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.LuckyTicketService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LuckyTicketServiceImplTest {

    @Autowired
    LuckyTicketService luckyTicketService;

    @Test
    public void initLuckyTicket(){
        Long userId = 20L;
        this.luckyTicketService.initLuckyTicket(userId);
        System.out.println("test end ...");
    }

}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.ActivityWishService;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.UserUploadShareVO;
import com.xy.zizoumohe.vo.UserVO;
import com.xy.zizoumohe.vo.WishAwardGiftVO;
import com.xy.zizoumohe.vo.WishAwardSimpleVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivityWishServiceImplTest {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private ActivityWishService activityWishService;

    @Test
    public void makeWish() throws Exception{
        Long userId = 1L;
        Long activityWishId = 1L;
        Integer type = 0;
        Date participateTime = simpleDateFormat.parse("2019-07-23 17:30:30");
        this.activityWishService.makeWish(userId, activityWishId, type, participateTime);
        System.out.println("test end ...");
    }

    @Test
    public void uploadWishSharePic(){
        Long userId = 1L;
        Long activityWishId = 1L ;
        String uploadPicUrl = "http://www.ps.21.png";
        this.activityWishService.uploadWishSharePic(userId, activityWishId, uploadPicUrl);
        System.out.println("test end ...");
    }

    @Test
    public void randomSelectOneWinner(){


    }

    @Test
    public void randomSelectWinnerUser(){
        Long activityWishId = 1L;
        List<UserUploadShareVO> userUploadShareVOList = this.activityWishService.getActivityWishParticipateUserList(activityWishId);
        List<Long> winnerUserIdList = this.activityWishService.randomSelectWinnerUser(userUploadShareVOList, 3);
        System.out.println("test end ...");
    }

    @Test
    public void getActivityWishParticipateUserList(){
        Long activityWishId = 1L;
        List<UserUploadShareVO> userUploadShareVOList= this.activityWishService.getActivityWishParticipateUserList(activityWishId);
        System.out.println("test end ...");
    }

    @Test
    public void handleWishAward(){
        Long activityWishId = 39L;
        this.activityWishService.handleWishAward(activityWishId, 3);
        System.out.println("test end ...");
    }

    @Test
    public void getActivityWishResult(){
        Long activityWishId = 1L;
        List<UserVO> userVOList = this.activityWishService.getActivityWishResult(activityWishId);
        System.out.println("test end ...");
    }

    @Test
    public void checkWishAward(){
        Long userId = 10L;
        Long activityWishId = 1L;
        WishAwardSimpleVO wishAwardSimpleVO = this.activityWishService.checkWishAward(userId, activityWishId);
        System.out.println("test end ...");
    }

    @Test
    public void getAcquireAwardGiftList(){
        Long userId = 1939L;
        PageRequest pageRequest = new PageRequest(1,10);
        PageResult pageResult = this.activityWishService.getAcquireAwardGiftList(userId, pageRequest);
        System.out.println("test end ...");
    }

    @Test
    public void getActivityWishOpenResult(){
        Long activityWishId = 1L;
        List<UserUploadShareVO> userUploadShareVOList = this.activityWishService.getActivityWishOpenResult(activityWishId);
        System.out.println("test end ...");
    }

}

package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.MessageService;
import com.xy.zizoumohe.utils.PageRequest;
import com.xy.zizoumohe.utils.PageResult;
import com.xy.zizoumohe.vo.request.MessageRequestVO;
import com.xy.zizoumohe.vo.MessageVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageServiceImplTest {

    @Autowired
    private MessageService messageService;

    @Test
    public void deleteMessage(){
        List<Long> messageIdList = new ArrayList<>();
        messageIdList.add(new Long("16"));
        this.messageService.deleteMessage(messageIdList);

        System.out.println("test end");
    }

    @Test
    public void updateMessage(){
        MessageRequestVO messageRequestVO =new MessageRequestVO();
        messageRequestVO.setMessageId(new Long(30));
        messageRequestVO.setCategoryId(new Long("3"));
        messageRequestVO.setTitle("兽人法运营策略详解");
        messageRequestVO.setContent("<p><divmargin: 0px=\"\" 6px;=\"\" padding:=\"\" 56px;=\"\" border:=\"\" 0px;=\"\" font-size:=\"\" 16px;=\"\" font-weight:=\"\" 400;=\"\" vertical-align:=\"\" baseline;=\"\" outline:=\"\" none;=\"\" width:=\"\" 790px;=\"\" box-sizing:=\"\" border-box;=\"\" overflow:=\"\" hidden;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" font-family:=\"\" -apple-system,=\"\" &quot;pingfang=\"\" sc&quot;,=\"\" &quot;hiragino=\"\" sans=\"\" gb&quot;,=\"\" system-ui,=\"\" &quot;segoe=\"\" ui&quot;,=\"\" roboto,=\"\" &quot;helvetica=\"\" neue&quot;,=\"\" arial,=\"\" sans-serif;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" text-transform:=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h1margin: 0px;=\"\" padding:=\"\" border:=\"\" font-size:=\"\" 16px;=\"\" font-weight:=\"\" normal;=\"\" vertical-align:=\"\" baseline;=\"\" outline:=\"\" none;=\"\" line-height:=\"\" 32px;=\"\" color:=\"\" rgb(20,=\"\" 25,=\"\" 30);\"=\"\"><spanmargin: 0px;=\"\" padding:=\"\" border:=\"\" font-size:=\"\" 20px;=\"\" font-weight:=\"\" 600;=\"\" vertical-align:=\"\" top;=\"\" outline:=\"\" none;\"=\"\"><divpost-content\"><img src=\"https://auto-chess-box-1251720259.cos.ap-chengdu.myqcloud.com/file/1558806455489.jpg\" style=\"max-width:100%;\"><br></divpost-content\"></spanmargin:></h1margin:></divmargin:></p><h2>正文</h2><p><divmargin: 0px=\"\" 6px;=\"\" padding:=\"\" 56px;=\"\" border:=\"\" 0px;=\"\" font-size:=\"\" 16px;=\"\" font-weight:=\"\" 400;=\"\" vertical-align:=\"\" baseline;=\"\" outline:=\"\" none;=\"\" width:=\"\" 790px;=\"\" box-sizing:=\"\" border-box;=\"\" overflow:=\"\" hidden;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0);=\"\" font-family:=\"\" -apple-system,=\"\" &quot;pingfang=\"\" sc&quot;,=\"\" &quot;hiragino=\"\" sans=\"\" gb&quot;,=\"\" system-ui,=\"\" &quot;segoe=\"\" ui&quot;,=\"\" roboto,=\"\" &quot;helvetica=\"\" neue&quot;,=\"\" arial,=\"\" sans-serif;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" text-transform:=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><h1margin: 0px;=\"\" padding:=\"\" border:=\"\" font-size:=\"\" 16px;=\"\" font-weight:=\"\" normal;=\"\" vertical-align:=\"\" baseline;=\"\" outline:=\"\" none;=\"\" line-height:=\"\" 32px;=\"\" color:=\"\" rgb(20,=\"\" 25,=\"\" 30);\"=\"\"><spanmargin: 0px;=\"\" padding:=\"\" border:=\"\" font-size:=\"\" 20px;=\"\" font-weight:=\"\" 600;=\"\" vertical-align:=\"\" top;=\"\" outline:=\"\" none;\"=\"\"><divpost-content\"></divpost-content\"></spanmargin:></h1margin:></divmargin:></p><p>在开始讲运营策略之前先科普一下完全体兽人法的阵容：4兽人（剑圣、兽王、斧王、萨尔）3法师（光法、冰女、电棍）恶魔（影魔）船长（船长作为人类和战士牌，能够和斧王剑圣组成3战士BUFF，并且可以作为一个前排人类沉默对手控制牌，本身还有一手控制，是最契合兽人法的紫卡），以上就是9人口完全体兽人法的阵容，然后如果有机会上10人口，根据对手和自己搜牌的情况补一张针对性的橙卡。来两张完全体兽人法供大家参考：</p><p><img src=\"https://auto-chess-box-1251720259.cos.ap-chengdu.myqcloud.com/file/1558806924066.PNG\" style=\"max-width:100%;\"><br></p><p>下面先解答一些常见的兽人法问题：</p><p></p><p><b>什么情况下判断可以选择兽人法？</b></p><p>答：15回合前有2-3张2星兽人牌（斧王、剑圣、兽王），并且影魔+电棍的数量大于等于2，光法、冰女和萨尔随缘。</p><p></p><p><b>玩兽人法前期什么情况下打连胜？什么情况下吃连败？</b></p><p>答：前期拿到兽人牌，并且都是一星的时候，其他打工牌质量也不高的时候，选择不跳人口吃连败存钱。反之，前期拿到兽人牌，有1-2张2星兽人，并且其他打工牌有1-2张能凑出羁绊的2星牌时（双亡灵、双元素、3猎人这种），选择升人口打连胜。</p><p></p><p><b>兽人法前期怎么过渡？</b></p><p>答：因为有兽人加成，一星斧王剑圣兽王也是不错的前排，2星的兽人在前期是非常优质的前排，所以过渡时我们以选择有输出的后排为主，第一档当然是影魔和电棍，但是这两张牌比较佛系，所以在没有影魔和电棍时，因为兽人里本身有兽王这个猎人的存在，并且1-3费的猎人牌可供选择的比较多，所以第二档我们考虑拿猎人牌凑3猎人过渡。另外第三档的话由于兽人本身有斧王和剑圣这两战士，也可以考虑拿战士牌出3战士BUFF，如果恰好拿的是小小的话，就可以考虑再拿水人凑双元素了。灵活运营双元素双亡灵和蓝胖的食人魔这种BUFF羁绊，能在前期不顺吃连败时节约很多血。</p><p></p><p><b>一局游戏法师比较多的时候我该如何抉择？</b></p><p>答：如果我们在一局游戏时开局打算走兽人法，并且拿到了不错的牌，到了十几波发现场上走法师的对手比较多，这时候因为我们拿了兽人，自带一猎人和2战士，并且在上一条里提到了可以考虑拿战士和猎人牌过渡。这时候我们可以考虑转型战兽猎或者娜迦战，这两套牌都可以在8人口出双娜迦BUFF来对抗法师，不用头铁继续去跟他们抢法师牌，这样中期可能会导致大家一起死的局面。</p><p></p><p>以上就是兽人法运营中比较常见的一些问题，如果大家还有问题可以在评论或者私信我跟我交流。下面进入正题：</p><p>1-10回合：</p><p>这里拿牌过渡分三档：</p><p>T1：兽人牌、影魔、电魂、灵魂守卫（这个来的比较早的情况下真的良心推荐，前期拿一个2星塞点装备，能保证前期稳定吃到连胜，即便输也不会掉多少血）</p><p>T2：猎人（小黑、风行、老虎、火枪）小小（小小排在T2是因为第一可以和剑圣、斧王出3战士羁绊，第二可以和电棍或者水人组成双元素，这个羁绊在前期无论玩家对战还是打野都非常有用。</p><p>T3：其他1-3费战士、地精牌。</p><p>这里尽量保证能吃到连胜/连败的额外经济，让血量掉的有价值。实在吃不到的情况下选择卖一些边缘牌来吃到利息。除了以上T1档里牌是必拿的以外（灵魂守卫除外），其他牌都可以考虑卖掉优先利息。冰女在前面尽量不要合到两星，通常存一个就够了。10回合20块钱是最低底线，最好存到30块左右。</p><p>小技巧：到了第七八波时如果场上没有2星牌，这时候有元素即便卡经济也提前存一个备用，第十波没有2星的情况下就只能靠双元素打野了。</p><p>11-15回合：</p><p>到了这个阶段拿牌就不要考虑过渡的问题了，不卡经济的情况下可以合一些1-2费牌打工，除了体系牌来了必拿以外，其他一切以经济为重。目标是15回合保底6人口50块，无论连胜/连败/一胜一负的情况下最少都要达到这个目标，否则后面的运营就会比较困难。</p><p>小技巧：15波的狼是尽量保证要打过的，这波狼的装备非常关键。同1-10波，如果11-15波场面上的怪比较弱，提前备好双元素，然后在打狼时如果都是一星牌，可以花10-20块提前启动搜牌/跳人口来保证打过这波野。</p><p>16-20回合：</p><p>这个阶段是整个运营最关键的阶段之一。前面提到最少15回合保底要有6人口50块，到了这个阶段我们要根据血量来判断继续贪经济还是提前启动了。</p><p>①前面吃连败血量在40-50之间了，这个血量已经不足以支持我们再卖4波到8人口启动了，所以考虑在16波再卖一波，17波拉7搜牌，这里搜牌的目标是：斧王兽王剑圣2星，影魔/电棍至少一个2星，冰女最少要有一个，光法萨尔随缘，如果没光法的话先拿一个火女/帕克凑三法BUFF。搜到以上目标里的牌可以保证在这个阶段锁血或者少掉血了，再考虑升8找光法和萨尔。</p><p>②16波时血量在65以上，这里就不用急着搜牌，即便听4-5对也考虑先卖血拉人口速8，我们可以考虑卖到40左右，对血量把握比较差的朋友可以吃1-2波考虑卖到50左右直接拉8搜牌。搜牌目标同上：：斧王兽王剑圣2星，影魔/电棍至少一个2星，冰女最好2星，至少有一张，光法没有拿别的法师牌替代（2星火女大于宙斯大于其他法师），萨尔最少有一个凑4兽人羁绊。</p><p>21-25回合：</p><p>这个阶段是整个运营最关键的第二个阶段，这里来到21回合一定要拉8人口，然后最少给自己留30块左右搜牌，前面的运营才算成功。这个阶段根据搜牌顺不顺决定是否跳9人口。</p><p>①搜牌比较顺：冰女斧王兽王剑圣，影魔和电魂最少有一个2星或者2个都2星，有至少一个萨尔，有光法/宙斯。搜到这个战力时在21-25回合基本能锁血，而且看上面情况我们对1-3费牌已经基本没需求了，只需求紫卡/橙卡，这时考虑拉9提升紫卡/橙卡概率。</p><p>②搜牌比较崩：冰女斧王兽王剑圣都是比较容易2星的，但是影魔电魂迟迟不2，或者萨尔光法搜不到。这时候在搜牌过程中就要留一些牌来代替了。影魔没2的情况下搜到飞机/宙斯可以代替，光法搜不到拿其他法师牌代替（2星火女大于宙斯大于其他法师）。如果没萨尔的情况下上兽王剑圣，斧王可以先不上，有第二个2星兽王/剑圣考虑上双兽王/剑圣，或者有船长上船长。这个阶段如果搜牌不太顺的话就只能考虑苟排名8人口打到底了。</p><p>26-30回合：</p><p>通常情况下打到这个阶段的成型兽人法前四基本已经稳了，21-25回合的两种情况，搜牌比较崩的情况下到这个阶段基本就只能尽可能提升战力苟排名了。所以这里我们讨论搜牌比较顺的情况下如何继续运营。</p><p>搜牌比较顺的情况下前面拿50块利息跳人口，到了26回合直接上9，然后开始找2星光法、船长、萨尔这三张核心紫卡。其他牌的话不卡格子的情况下兽人牌都值得追3，卡格子的情况下优先放弃斧王，剑圣和兽王在这套牌里都是最值得追3的。这里不推荐影魔和电魂追3，因为这两张牌都3块钱，如果追3的话后面放4-5张卡十几块经济不划算。船长如果迟迟不2并且搜到宙斯/飞机/潮汐的话，可以考虑先上橙卡船长放在后面，对手里有比较顺的精灵的话有炸弹优先考虑炸弹，有比较顺的战士/骑士有谜团优先考虑谜团。否则优先考虑飞机/潮汐，第二优先级是宙斯。</p><p>31-35回合：</p><p>通常游戏到了这个阶段的兽人法已经可以展望吃鸡了，这个阶段只需要考虑两点：</p><p>①通常情况下我是不建议跳10人口的，存钱在9人口搜兽人牌的质量追3，如果在搜牌过程中恰好存到了一对橙卡（飞机=潮汐＞宙斯，决赛对手是精灵飞机=炸弹=潮汐，决赛对手是战士/骑士，谜团＞飞机=潮汐），并且兽人牌暂时没听3星，这时考虑存钱跳10人口找橙卡2星，因为这版本有赖子牌精灵球的存在，所以2星橙卡相对比较容易合成。</p><p>②9人口时没有2星橙卡听牌，但是其他牌暂时没有听牌，搜牌没有提升时。这时考虑存钱升10人口找橙卡来提升战力。橙卡优先级依旧参考上一条括号里的内容。</p><p>36-40回合：</p><p>现版本的节奏，成型的兽人法很少有打到这个阶段的，通常到了这个阶段最后也只剩你兽人法和对手solo了，这时候参考上面31-35回合的运营策略，阵容有提升没橙卡听牌优先搜牌，阵容没提升时无论有没有橙卡听牌考虑升10人口找针对对手的橙卡来提升战力。</p><p>最后给大家模拟几张兽人法对战版本热门卡组时的站位图：</p><p>站位通常自己判断战力，己方控制（萨尔、船长、潮汐）能抢到先手的情况下顶对手脸，这样融化起来更快，但是要注意对手人类/敌法的位置，把己方控制避开对手人类和敌法。己方控制抢不到先说的情况下站对角线。还剩多家的情况下优先考虑对自己威胁最大的对手的站位，通常对兽人法威胁最大的是法师同行，或者比较胡的带双娜迦的阵容（战兽猎/娜迦战）。</p><p><img src=\"https://auto-chess-box-1251720259.cos.ap-chengdu.myqcloud.com/file/1558806968938.PNG\" style=\"max-width:100%;\"><br></p><p style=\"text-align: center;\">常规兽人法站位</p><p><br></p><p><img src=\"https://auto-chess-box-1251720259.cos.ap-chengdu.myqcloud.com/file/1558807026264.PNG\" style=\"max-width:100%;\"><br></p><h4 style=\"text-align: center;\">对战刺客/精灵时兽人法站位</h4><p><br></p><p><img src=\"https://auto-chess-box-1251720259.cos.ap-chengdu.myqcloud.com/file/1558807049264.PNG\" style=\"max-width:100%;\"><br></p><h4 style=\"text-align: center;\">法师内战时兽人法站位</h4><p><br></p><p>以上三张分别是常规站位，对战精灵/刺客时站位以及法师内战时站位。萨尔和船长的位置可以根据对方人类和控制的位置微调，尽量拿船长去对位对方的控制。己方萨尔在没有回蓝装的情况下可以摆到吸收伤害最高的位置，有4兽人加成的存在，没有回蓝装的2星萨尔去吸收伤害抢先手是完全没有问题的。<br></p><p><br></p>");
        messageRequestVO.setMediaType(0);
        messageRequestVO.setPic("https://auto-chess-box-1251720259.cos.ap-chengdu.myqcloud.com/file/1558806442584.jpg");
        messageRequestVO.setVideoLinkUrl("");
        messageRequestVO.setPriority(new Double(19.00));
        messageRequestVO.setIsRecommend(false);

        this.messageService.updateMessage(messageRequestVO);
        System.out.println("test end...");
    }

    @Test
    public void getHotMessageList(){

        List<MessageVO> messageVOList = this.messageService.getHotMessageList(3l);
        messageVOList.stream().map(e ->{
            System.out.println(e.getMessageId()+ "  "+ e.getCategoryId()+ "  "+e.getCreateTime()+"  "+
            e.getPriority()+"  "+ e.getTitle());
            e.getExcellentComments().stream().map(ee ->{
                System.out.println("     "+ee.getMessageCommentId()+"  "+ ee.getUserId()+ "  "+ ee.getCommentContent()+ "  "+
                ee.getNickname()+ "  "+ee.getUserHeadPortrait());
                return null;
            }).collect(Collectors.toList());
            System.out.println("*****************************************************");
            return null;
        }).collect(Collectors.toList());
    }


}

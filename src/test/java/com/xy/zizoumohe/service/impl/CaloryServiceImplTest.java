package com.xy.zizoumohe.service.impl;

import com.xy.zizoumohe.service.CaloryService;
import com.xy.zizoumohe.vo.CaloryTradeVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CaloryServiceImplTest {

    @Autowired
    private CaloryService caloryService;

    @Test
    public void getCaloryNum(){
        Long caloryNum = this.caloryService.getCaloryNum(10L);
        System.out.println(caloryNum);
    }

    @Test
    public void getCaloryTrade()throws Exception{
        Long userId = 7122L;
        Date myDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-08-15");
        List<CaloryTradeVO> caloryTradeVOList = this.caloryService.getCaloryTrade(userId, myDate);
        System.out.println(caloryTradeVOList.size());
    }

}

package com.xy.zizoumohe.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.xy.zizoumohe.service.TokenService;
import com.xy.zizoumohe.service.UserService;
import com.xy.zizoumohe.vo.UserBaseStatisticVO;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Test
    public void getUserBaseStatistic(){
        UserBaseStatisticVO userBaseStatisticVO = this.userService.getUserBaseStatistic(null,1l);
        System.out.println(userBaseStatisticVO.getUserId()+"  " + userBaseStatisticVO.getToSupports() + "  "+
                userBaseStatisticVO.getFans()+"  "+userBaseStatisticVO.getAttentions()+ "  "+ userBaseStatisticVO.getAttentionState());

        System.out.println("---------------------------------------------------------");
        UserBaseStatisticVO userBaseStatisticVO2 = this.userService.getUserBaseStatistic(13399l,1l);
        System.out.println(userBaseStatisticVO2.getUserId()+"  " + userBaseStatisticVO2.getToSupports() + "  "+
                userBaseStatisticVO2.getFans()+"  "+userBaseStatisticVO2.getAttentions()+ "  "+ userBaseStatisticVO2.getAttentionState());


    }

    @Autowired
    private TokenService tokenService;
    @Test
    public void token() throws InterruptedException {
        for (int i=0;i<100000;i++) {
           String startToken =  userService.refreshToken(1L);
            String myToken = tokenService.selectByUserId(1L).getToken();
/*            System.out.println(startToken);
            System.out.println(myToken);
            System.out.println("||||||||||||||||||||||||||||||||||||||");
            Thread.sleep(1000);*/
            if (!startToken.equals(myToken)) {
                System.out.println("过期");
            }
        }
    }

    @Test
    public  void a() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String> map = new HashMap<>();
        String token =tokenService.selectByUserId(1L).getToken();
        for (int i=0;i<1000;i++) {

            HttpPost httpPost = new HttpPost("http://localhost:8080/api/user/refreshToken?userId=1");
            httpPost.setHeader("SmallTownToken", token);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpResponse response = httpClient.execute(httpPost);
            if (response != null) {
                HttpEntity resEntity = response.getEntity();
                if (resEntity != null) {
                    String str = EntityUtils.toString(resEntity, "utf-8");
                    map=mapper.readValue(str, new TypeReference<HashMap>(){});
                    token = map.get("result");
                    System.out.println(token);
                    if (token.equals("null")) {
                        System.err.println(i+"-----------------");
                    }
                }
            }
            Thread.sleep(10000);
        }
    }


}

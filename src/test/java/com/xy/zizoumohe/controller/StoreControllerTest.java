package com.xy.zizoumohe.controller;

import com.xy.zizoumohe.utils.JsonResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zachary zhu
 * @date 2019/5/23  11:08
 * @corporation UESTC
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StoreControllerTest {

    @Autowired
    private StoreController storeController;

    /*
     *     public JsonResponse createStore(@RequestParam("currentLoginUserId") Long currentLoginUserId,
     *                              @RequestParam("storeCategoryId") Long storeCategoryId,
     *                              @RequestParam("entityId") Long entityId) ;
     */
    @Test
    public void createStore() {
        Long currentLoginUserId = new Long(1);
        Long storeCategoryId = new Long(1);
        Long entityId = new Long(3);
        JsonResponse response =this.storeController.createStore(currentLoginUserId,storeCategoryId,entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test end...");
    }

    @Test
    public void createStore2() {
        Long currentLoginUserId = new Long(1088);
        Long storeCategoryId = new Long(1);
        Long entityId = new Long(3);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }

    @Test
    public void createStore3() {
        Long currentLoginUserId = new Long(1);
        Long storeCategoryId = new Long(11);
        Long entityId = new Long(3);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }

    @Test
    public void createStore4() {
        Long currentLoginUserId = new Long(1);
        Long storeCategoryId = new Long(1);
        Long entityId = new Long(32);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }

    @Test
    public void createStore5() {
        Long currentLoginUserId = new Long(1);
        Long storeCategoryId = new Long(2);
        Long entityId = new Long(32);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }

    @Test
    public void createStore6() {
        Long currentLoginUserId = new Long(2);
        Long storeCategoryId = new Long(1);
        Long entityId = new Long(2);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }

    @Test
    public void createStore7() {
        Long currentLoginUserId = new Long(3);
        Long storeCategoryId = new Long(1);
        Long entityId = new Long(3);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }

    @Test
    public void createStore8() {
        Long currentLoginUserId = new Long(3);
        Long storeCategoryId = new Long(1);
        Long entityId = new Long(1);
        JsonResponse response = this.storeController.createStore(currentLoginUserId, storeCategoryId, entityId);
        System.out.println(response.getCode());
        System.out.println(response.getMessage());
        System.out.println("test1 end...");
    }











}

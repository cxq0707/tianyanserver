package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryMapperTest {

    @Autowired
    private CategoryMapper categoryMapper;

    @Test
    public void selectByPrimaryKey(){
        Category category = this.categoryMapper.selectByPrimaryKey(1L);
        System.out.println(category.getName());
    }



}

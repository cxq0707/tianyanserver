package com.xy.zizoumohe.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LuckyTicketMapperTest {

    @Autowired
    LuckyTicketMapper luckyTicketMapper;

    @Test
    public void updateNumByUserId(){
        Long userId = 10L;
        int flag = this.luckyTicketMapper.updateNumByUserId(userId, -5);
        System.out.println(flag);
    }

}

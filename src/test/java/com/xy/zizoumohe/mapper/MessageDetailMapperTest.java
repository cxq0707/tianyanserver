package com.xy.zizoumohe.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageDetailMapperTest {

    @Autowired
    private MessageDetailMapper messageDetailMapper;

    @Test
    public void selectVideoLinkUrlByPrimaryKey(){
        String videoLinkUrl = this.messageDetailMapper.selectVideoLinkUrlByPrimaryKey(new Long(37));
        System.out.println(videoLinkUrl);
        System.out.println("test end");
    }

    @Test
    public void selectVideoLinkUrlByMessageId(){

        String videoLinkUrl = this.messageDetailMapper.selectVideoLinkUrlByMessageId(new Long(38));
        System.out.println(videoLinkUrl);
        System.out.println("test end");


    }


}

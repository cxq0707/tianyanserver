package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Attention;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AttentionMapperTest {

    @Autowired
    private AttentionMapper attentionMapper;

    @Test
    public void selectByUserId(){
        List<Attention> attentionList= this.attentionMapper.selectByUserId(10L);
        attentionList.stream().map(e ->{
            System.out.println(e.getId() + "  " +e.getUserId() +"  " + e.getToUserId() + "  " + e.getAttentionTime().toString());
            return null;
        }).collect(Collectors.toList());
    }

    @Test
    public void selectByToUserId(){
        List<Attention> attentionList = this.attentionMapper.selectByToUserId(13l);
        attentionList.stream().map(e ->{
            System.out.println(e.getId() + "  " +e.getUserId() +"  " + e.getToUserId() + "  " + e.getAttentionTime().toString());
            return null;
        }).collect(Collectors.toList());
    }


}

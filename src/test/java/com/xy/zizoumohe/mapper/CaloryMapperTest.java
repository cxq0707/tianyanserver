package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Calory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CaloryMapperTest {
    @Autowired
    private CaloryMapper caloryMapper;

    @Test
    public void selectByUserId(){
        Calory calory = this.caloryMapper.selectByUserId(10L);
        if (calory != null){
            System.out.println(calory.getId());
        }
    }

    @Test
    public void updateByUserId(){
        Calory calory = new Calory();
        calory.setUserId(10L);
        calory.setCaloryNum(90L);
        this.caloryMapper.updateByUserId(calory);
        System.out.println("test end...");
    }

    @Test
    public void decreaseByUserId(){
        Long userId = 7122L;
        this.caloryMapper.decreaseByUserId(userId, 1);
        System.out.println("test end ...");
    }

    @Test
    public void increaseByUserId(){
        Long userId = 7122L;
        this.caloryMapper.increaseByUserId(userId, 10);
        System.out.println("test end ...");

    }

}

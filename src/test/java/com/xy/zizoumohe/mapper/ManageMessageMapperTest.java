package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.ManageMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManageMessageMapperTest {

    @Autowired
    private ManageMessageMapper manageMessageMapper;

    @Test
    public void selectByToUserId(){
        List<ManageMessage> manageMessageList = this.manageMessageMapper.selectByToUserId(186l);
        manageMessageList.stream().map(e ->{
            System.out.println(e.getId()+"  "+e.getUserId()+ "  "+e.getToUserId()+"  "+
            e.getEntityId()+"  "+ e.getActionId()+"  "+e.getManageMessageCategoryId()+"  "+ e.getActionType());
            return null;
        }).collect(Collectors.toList());

    }


}

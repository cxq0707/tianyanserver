package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Autowired
    UserMapper userMapper;

    @Test
    public void selectByPrimaryKey(){
        Long userId = 10L ;
        User user = this.userMapper.selectByPrimaryKey(userId);
        System.out.println(user.getHasReceivePacket());
    }

    @Test
    public void updateByPrimaryKeySelective(){
        Long userId = 30L;
        User user = this.userMapper.selectByPrimaryKey(userId);
        if (user != null){
            user.setHasReceivePacket(true);
            this.userMapper.updateByPrimaryKeySelective(user);
        }
        System.out.println("test end ...");
    }

}

package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Message;
import com.xy.zizoumohe.model.MessageModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageMapperTest {

    @Autowired
    private MessageMapper messageMapper;

    @Test
    public void selectByCategoryIdAndBoardType(){
        Long categoryId = 3L; // 3表示资讯
        Integer boardType = 0; // 0表示综合
        List<MessageModel> messageList = this.messageMapper.selectByCategoryIdAndBoardType(categoryId, boardType);
        System.out.println("end...");
    }

    @Test
    public void selectByIsRecommendAndBoardType(){
        Integer boardType = 0;
        List<MessageModel> messageModelList = this.messageMapper.selectByIsRecommendAndBoardType(0);
        System.out.println("end...");
    }

    @Test
    public void selectByIsRecommendAndApproveStateAndBoardType(){
        Integer approveState = 1;
        Integer boardType = 2;
//        List<MessageModel> messageModelList = this.messageMapper.selectByIsRecommendAndApproveStateAndBoardType(approveState,boardType);
        System.out.println("end ...");
    }

    @Test
    public void selectByRecommendAndCategoryId(){
        Long categoryId = 4L;
        List<Message> messageList = this.messageMapper.selectByRecommendAndCategoryId(true, null);
        System.out.println(messageList.size());
    }

    @Test
    public void selectByHotAndCategoryId(){
        Long catgoryId = 4L;
        List<Message> messageList = this.messageMapper.selectByHotAndCategoryId(true, catgoryId);
        System.out.println(messageList.size());
    }


}

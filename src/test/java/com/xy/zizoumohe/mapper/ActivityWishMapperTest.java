package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.ActivityWish;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActivityWishMapperTest {

    @Autowired
    private ActivityWishMapper activityWishMapper;

    @Test
    public void selectByOpenTimeLess(){
        List<ActivityWish> activityWishList = this.activityWishMapper.selectByOpenTimeLess(new Date());
        System.out.println("test end ...");
    }

    @Test
    public void selectByEndTimeLess(){
        List<ActivityWish> activityWishList = this.activityWishMapper.selectByEndTimeLess(new Date());
        System.out.println("test end ...");
    }

    @Test
    public void selectByTimeBetween() throws Exception{
        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-08-14 19:51:50");
        List<ActivityWish> activityWishList = this.activityWishMapper.selectByTimeBetween(date);
        System.out.println(activityWishList.size());
    }


}

package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.Sign;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SignMapperTest {

    @Autowired
    private SignMapper signMapper;

    @Test
    public void selectByUserIdAndCreateTime(){
        String signDateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        Sign sign= this.signMapper.selectByUserIdAndCreateTime(10L,signDateString);
        System.out.println(sign.getId());

    }







}

package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.MessageComment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MessageCommentMapperTest {

    @Autowired
    private MessageCommentMapper messageCommentMapper;

    @Test
    public void selectByIsExcellentAndMessageId(){
        List<MessageComment> messageCommentList= this.messageCommentMapper.selectByIsExcellentAndMessageId(17l);
        messageCommentList.stream().map(e ->{
            System.out.println(e.getId() + "  " + e.getMessageId() + "  " + e.getUserId() + "  " + e.getToMessageCommentId() + "  " + e.getCommentContent());
            return null;
        }).collect(Collectors.toList());
    }

    @Test
    public void selectByUserId(){

        List<MessageComment> messageCommentList= this.messageCommentMapper.selectByUserId(212l);
        messageCommentList.stream().map(e ->{
            System.out.println(e.getId() + "  "+ e.getCommentContent()+ "  "+ e.getUserId());
            return null;
        }).collect(Collectors.toList());
    }



}

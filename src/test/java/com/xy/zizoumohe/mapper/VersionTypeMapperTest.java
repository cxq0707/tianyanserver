package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.VersionType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VersionTypeMapperTest {

    @Autowired
    private VersionTypeMapper versionTypeMapper;

    @Test
    public void selectByAndroidVersionId(){
        Long androidVersionId = 1L;
        List<VersionType> versionTypeList = this.versionTypeMapper.selectByAndroidVersionId(androidVersionId);
        System.out.println("test end ...");

    }

}

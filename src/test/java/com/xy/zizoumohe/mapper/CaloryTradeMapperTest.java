package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.CaloryTrade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CaloryTradeMapperTest {

    @Autowired
    private CaloryTradeMapper caloryTradeMapper;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Test
    public void selectByAccountIdAndIsIncomeAndDuringDate() throws Exception{
        Long currentLoginUserId = 19771L;
        Boolean isIncome = true;
        Date startTime = this.simpleDateFormat.parse("2018-01-01 10:10:10");
        Date endTime = this.simpleDateFormat.parse("2019-07-03 10:54:00");
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndIsIncomeAndDuringDate(currentLoginUserId,isIncome,startTime,endTime);

        int totalEarning = 0;
        for (CaloryTrade caloryTrade:caloryTradeList){
            totalEarning = totalEarning + caloryTrade.getTradeNum();
        }
        System.out.println(totalEarning);
        System.out.println("end ...");
    }

    @Test
    public void selectByAccountIdAndTradeTypeAndEntityId(){

        Long accountId = 1662L;
        int tradeType = 16;
        Long entityId = 20L;
        List<CaloryTrade> caloryTradeList = this.caloryTradeMapper.selectByAccountIdAndTradeTypeAndEntityId(accountId,tradeType, entityId);
        System.out.println("end ...");

    }

}

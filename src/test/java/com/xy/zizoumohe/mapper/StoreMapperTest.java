package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.model.StoreModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StoreMapperTest {

    @Autowired
    private StoreMapper storeMapper;

    @Test
    public void selectByStoreCategoryIdAndTypeAndUserId(){

        List<StoreModel> storeModelList = this.storeMapper.selectByStoreCategoryIdAndTypeAndUserId(1L,1,18426L);
        storeModelList.stream().map(e->{
            System.out.println(e.getId() + "  " + e.getEntityId() + "  " + e.getStoreCategoryId() + "  " + e.getUserId() + e.getType());
            return null;
        }).collect(Collectors.toList());


    }


}

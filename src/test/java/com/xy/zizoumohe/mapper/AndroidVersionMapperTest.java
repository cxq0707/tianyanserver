package com.xy.zizoumohe.mapper;

import com.xy.zizoumohe.entity.AndroidVersion;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AndroidVersionMapperTest {
    @Autowired
    private AndroidVersionMapper androidVersionMapper;

    @Test
    public void selectByLatestVersion(){

        AndroidVersion androidVersion = this.androidVersionMapper.selectByLatestVersion();
        System.out.println("test end ...");

    }

}
